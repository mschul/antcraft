# AntCraft #

This is an Anthill Simulator with influences from Dungeon Keeper and Settlers. For lack of time and designers this project is currently frozen however.

### How do I get set up? ###

The easiest way would be to just download a prebuilt version (currently Windows only).

Or you can build the dependencies, which are

* Ogre 1.9 (and everything Ogre needs)
* CEGUI
* and the BOOST lib packaged with this repo, since Ogre seems to be quite picky about the exact version.

If i pick up this project anytime soon, i will create a cmake file for it and try a linux build.
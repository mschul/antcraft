#version 150

uniform sampler2D RT1;
uniform sampler2D RT2;
in vec2 oUv0;
out vec4 fragColour;

void main()
{
    vec3 greyscale = vec3(dot(texture(RT1, oUv0).rgb, vec3(0.3, 0.59, 0.11)));
	fragColour = vec4(greyscale, 1.0);
}

#include "AnPCH.h"
#include "AnAOE.h"
using namespace Ants;


AOE::AOE(ObjectID source, int sourceClientID,
	float attackPoints, const Ogre::Vector2& pos, unsigned int radius)
{
	mSourceID = source;
	mSourceClientID = sourceClientID;

	mAttackPoints = attackPoints;
	mSourcePosition = pos;
	Tile::hexCircle(pos, radius, mPositions);
}
AOE::AOE(ObjectID source, int sourceClientID,
	float attackPoints, const Ogre::Vector2& pos,
	const std::vector<Ogre::Vector2>& positions)
{
	mSourceID = source;
	mSourceClientID = sourceClientID;

	mAttackPoints = attackPoints;
	mSourcePosition = pos;
	mPositions.assign(positions.begin(), positions.end());
}
AOE::~AOE()
{
}

float AOE::getAttackPoints() const
{
	return mAttackPoints;
}
int AOE::getSourceClientID() const
{
	return mSourceClientID;
}
ObjectID AOE::getSourceID() const
{
	return mSourceID;
}
Ogre::Vector2 AOE::getSourcePosition() const
{
	return mSourcePosition;
}
std::vector<Ogre::Vector2>& AOE::getPositions()
{
	return mPositions;
}
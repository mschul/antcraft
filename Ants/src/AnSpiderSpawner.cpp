#include "AnPCH.h"
#include "AnSpiderSpawner.h"
#include "AnEntity.h"
using namespace Ants;

SpiderSpawner::SpiderSpawner (int id)
	: MobSpawner(id)
{
	mNextSpawnDelta = 150;
	mInitialSpawnDelta = 150;
}
SpiderSpawner::~SpiderSpawner ()
{
}
		
Room::RoomType SpiderSpawner::getType() const
{
	return Room::SPIDER_SPAWNER;
}

void SpiderSpawner::build(const std::vector<Ogre::Vector2>& positions, 
	const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
	std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	mPivot.clear();
	mPivot.push_back(pivot);
	tiles.push_back(std::make_pair(pivot, Tile::msTiles[Tile::NEST]));
}
Entity* SpiderSpawner::spawn(ActionReactor* reactor)
{
	return reactor->spawn(*mPivot.begin(), Entity::ET_SPIDER);
}
#include "AnPCH.h"
#include "AnEarthworm.h"
using namespace Ants;

Ogre::String Earthworm::msStandardMesh = "";
Ogre::String Earthworm::msMaterialName = "";


Earthworm::Earthworm (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition,
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
		: Mob(id, clientID, position, tilePosition, visibilityRadius,
		worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mActiveTrait = 0;

	mRanged = false;
	mAOE = false;
	mAOEDamage = 0;
	mRangedDamage = 0;
	mMeleeDamage = 10;
	mArmor = 0;
}
Earthworm::Earthworm (const EntityDesc& desc)
	: Mob(desc)
{
}
Earthworm::~Earthworm ()
{
}
		
Entity::EntityType Earthworm::getType() const
{
	return Entity::ET_EARTHWORM;
}

void Earthworm::selectBehaviour (float dt)
{
    int newTrait = 0;
    if (!mEnemies.empty ())
    {
        newTrait = 1; // warrrior
    }

    if (newTrait != mActiveTrait)
    {
        mCurrentState = FIND_JOB;
        mActiveTrait = newTrait;
#ifdef _DEBUG
        Framework::getSingleton ().mLog->logMessage (
            "earthworm " + Ogre::StringConverter::toString (getID ())
            + " switched to mode " + Ogre::StringConverter::toString (mActiveTrait)
            );
#endif
    }
}


Ogre::String Earthworm::getMaterialName() const
{
	return Earthworm::msMaterialName;
}
Ogre::String Earthworm::getStandardMesh() const
{
	return Earthworm::msStandardMesh;
}
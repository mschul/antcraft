#include "AnPCH.h"
#include "AnWorkerTrait.h"
#include "AnAnt.h"

#include "AnFetchJob.h"
#include "AnExitRoom.h"
#include "AnThroneChamber.h"
#include "AnBroodChamber.h"
#include "AnRoomCreateJob.h"
#include "AnRoomCreateJobEx.h"
using namespace Ants;

template class WorkerTrait<Ant>;

template<class T>
WorkerTrait<T>::WorkerTrait()
{
}

template<class T>
void WorkerTrait<T>::idleWalking(float dt, T* character, ActionReactor* reactor)
{
	if(!character->mPath.empty())
	{
		character->mChanged = true;
		bool success = character->walkWaypoint(dt);
		if (!success)
		{
			character->clearPath();
		}
	}
	else
	{
		if(character->mWorldQuery->isBlocking(character->getTilePosition()))
			return;

		if(character->mStateUpdateDelta < 0)
		{
			character->mStateUpdateDelta = ((float)(rand() % 1000) / 1000 + 1);
			character->mCurrentState = Character::IDLE_STANDING;
		}
		else if(character->getType() != Entity::ET_SOLDIER)
		{
			ThroneChamber* tc = character->mRoomQuery->getThroneChamber();
			switch (tc->getWorkerBehaviour())
			{
			case ThroneChamber::PROTECT:
				character->restrictedPath(Tile::msAnthillTiles);
				break;
			case ThroneChamber::EXPAND:
				character->randomPath();
				break;
			case ThroneChamber::DISCOVER:
				if (rand() % 2)
				{
					Job* job = 0;
					Ogre::Vector2 tilePosition = character->mTilePosition;
					Ogre::Vector2 target;
					std::vector<Ogre::Vector2> path;
					character->mWorldQuery->findNextZeroTile(tilePosition, target);
					character->mWorldQuery->findShortestPath(target, tilePosition, path);
					character->savePath(path);
				}
				else // every fourth 
				{
					Ogre::Vector2 p = tc->getPivot(character->mTilePosition);
					character->approach(p + Tile::getNeighbours(p)[0]);
				}
				break;
			}
		}
		else
		{
			character->randomPath();
		}
	}
	character->mStateUpdateDelta -= dt;
}
template<class T>
void WorkerTrait<T>::idleStanding(float dt, T* character, ActionReactor* reactor)
{
	if(character->mStateUpdateDelta < 0)
	{
		character->mCurrentState = Character::FIND_JOB;
	}
	character->mStateUpdateDelta -= dt;
}
template<class T>
void WorkerTrait<T>::findJob(float dt, T* character, ActionReactor* reactor)
{
	bool success = false;
	int jobIndex = character->mActiveJobIndex;
	jobIndex = (jobIndex + 1) % character->mJobs.size();
	character->mActiveJobIndex = jobIndex;

	if (character->mHomeRoom && !character->mFreelancing)
	{
		success = findJobWorker(dt, character, reactor, jobIndex);
		character->mCurrentState = Character::GET_JOBINFORMATION;
	}
	else
	{
		success = findJobFreelancer(dt, character, reactor, jobIndex);
		if (!success)
		{
			character->prepareForNewJob(jobIndex);
			// no job found, so idle some time away
			character->mStateUpdateDelta = 4 + ((float)(rand() % 1000) / 1000);
			character->mCurrentState = Character::IDLE_WALKING;
			character->mFreelancing = character->mHomeRoom == 0;
		}
		else
		{
			character->mCurrentState = Character::GET_JOBINFORMATION;
		}
	}

}
template<class T>
void WorkerTrait<T>::getJobInfo(float dt, T* character, ActionReactor* reactor)
{
	bool success = false;
	int jobIndex = character->mActiveJobIndex;

	if(character->mSubJobs[jobIndex].empty())
	{
		if (character->mHomeRoom && !character->mFreelancing)
		{
			success = getJobInfoWorker(dt, character, reactor, jobIndex);
			if (!success)
			{
				character->mFreelancing = true;
				character->mCurrentState = Character::FIND_JOB;
			}
		}
		else
		{
			success = getJobInfoFreelancer(dt, character, reactor, jobIndex);
			if (!success)
			{
				character->mFreelancing = character->mHomeRoom == 0;
				character->mStateUpdateDelta = 4 + ((float)(rand() % 1000) / 1000);
				character->mCurrentState = Character::IDLE_WALKING;
			}
		}
	}
	else
	{
		// we already know enough about this job
		success = true;
	}

	if (success)
	{
		character->mCurrentState = Character::FIND_PATH;
	}
	else
	{
		character->prepareForNewJob(jobIndex);
	}
}
template<class T>
void WorkerTrait<T>::findPath(float dt, T* character, ActionReactor* reactor)
{
	bool success = false;
	int jobIndex = character->mActiveJobIndex;
	if (character->mSubJobs[jobIndex].empty())
	{
		character->mCurrentState = Character::FIND_JOB;
		return;
	}
	
	if(character->mWorldQuery->isBlocking(character->getTilePosition()))
		return;

	if (character->mHomeRoom && !character->mFreelancing)
	{
		success = findPathWorker(dt, character, reactor, jobIndex);
		if (!success)
		{
			character->mFreelancing = true;
			character->mCurrentState = Character::FIND_JOB;
		}
	}
	else
	{
		success = findPathFreelancer(dt, character, reactor, jobIndex);
		if (!success)
		{
			character->mFreelancing = character->mHomeRoom == 0;
			character->mStateUpdateDelta = 4 + ((float)(rand() % 1000) / 1000);
			character->mCurrentState = Character::IDLE_WALKING;
		}
	}

	if (success)
	{
		character->mCurrentState = Character::REACH_JOB;
	}
	else
	{
		character->prepareForNewJob(jobIndex);
	}
}
template<class T>
void WorkerTrait<T>::reachJob(float dt, T* character, ActionReactor* reactor)
{
	int jobIndex = character->mActiveJobIndex;
	if (character->mSubJobs[jobIndex].empty())
	{
		character->mCurrentState = Character::FIND_JOB;
		return;
	}

	if(!character->mPath.empty())
	{
		character->mChanged = true;
		bool success = character->walkWaypoint(dt);
		if(!success)
		{
			character->clearPath();
			character->mCurrentState = Character::FIND_PATH;
		}
		return;
	}
	else
	{
		character->mCurrentState = Character::WORK_JOB;
	}
}
template<class T>
void WorkerTrait<T>::workJob(float dt, T* character, ActionReactor* reactor)
{
	int jobIndex = character->mActiveJobIndex;
	if (character->mSubJobs[jobIndex].empty())
	{
		character->mCurrentState = Character::FIND_JOB;
		return;
	}

	if (character->mHomeRoom && !character->mFreelancing)
	{
		workJobWorker(dt, character, reactor, jobIndex);
	}
	else
	{
		workJobFreelancer(dt, character, reactor, jobIndex);
	}
}

template<class T>
bool WorkerTrait<T>::findJobWorker(float dt, T* character, ActionReactor* reactor, int jobIndex)
{
	// workers already have a job (as workers in a room)
	character->mJobFill = 1;
	return true;
}
template<class T>
bool WorkerTrait<T>::findJobFreelancer(float dt, T* character, ActionReactor* reactor, int jobIndex)
{
	bool success = false;;
	// freelancer
	if (character->mJobQuery->getJob(character->mJobs[jobIndex]))
	{
		// there is a job active so get some info about it
		success = true;
	}
	else
	{
		// fetch a new job
		Job* job = 0;
		Ogre::Vector2 tilePosition = character->mTilePosition;
		character->mJobQuery->findNextJob(tilePosition, job, character->mP1Jobs, 60);
		if (character->mP2Jobs && !job)
			character->mJobQuery->findNextJob(tilePosition, job, character->mP2Jobs, 30);
		if (character->mP3Jobs && !job)
			character->mJobQuery->findNextJob(tilePosition, job, character->mP3Jobs, 60);
		
		if (job)
		{
			success = true;
			// set job
			character->mJobs[jobIndex] = job->getID();
			character->mJobFill++;
		}
	}
	return success;
}
template<class T>
bool WorkerTrait<T>::getJobInfoWorker(float dt, T* character, ActionReactor* reactor, int jobIndex)
{
	bool success = false;
	// try to find a job
	std::vector<Room::WorkplaceSubjobs> jobs;
	character->mHomeRoom->getJob(jobs);
	if (jobs.size() > 0)
	{
		success = true;

		for (Room::WorkplaceSubjobs job : jobs)
			character->mSubJobs[jobIndex].push(job);
	}
	return success;
}
template<class T>
bool WorkerTrait<T>::getJobInfoFreelancer(float dt, T* character, ActionReactor* reactor, int jobIndex)
{
	bool success = false;
	Job* job = character->mJobQuery->getJob(character->mJobs[jobIndex]);
	if (job && job->isAvailable())
	{
		std::vector<Job::SubJobs> jobs;
		job->getSubJobs(jobs);
		if (jobs.size() > 0)
		{
			success = true;

			character->mJobQuery->assignJob(character, character->mJobs[jobIndex]);
			for (Job::SubJobs job : jobs)
				character->mSubJobs[jobIndex].push(job);
		}
	}
	else
	{
		character->prepareForNewJob(jobIndex);
	}
	return success;
}
template<class T>
bool WorkerTrait<T>::findPathWorker(float dt, T* character, ActionReactor* reactor, int jobIndex)
{
	bool success = false;
	if (!character->mSubJobs[jobIndex].empty())
	{
		switch ((Room::WorkplaceSubjobs)character->mSubJobs[jobIndex].front())
		{
		case Room::FETCH_RESOURCE:
		{
			std::vector<Room::RoomType> allowedRooms;
			allowedRooms.push_back(Room::COPAL_SOURCE);
			allowedRooms.push_back(Room::FLORAL_SOURCE);
			allowedRooms.push_back(Room::FAUNAL_SOURCE);
			allowedRooms.push_back(Room::FUNGI_CHAMBER);
			character->mRoomQuery->findReachableRoom(character->mTilePosition,
				character->mTargetRooms[jobIndex], allowedRooms);

			if (character->mTargetRooms[jobIndex] && character->mAssignedSlots[jobIndex] == 0)
			{
				if (!character->mTargetRooms[jobIndex]->isEmptyIncReservations())
				{
					character->mTargetRooms[jobIndex]->reserveFetch(character,
						character->mAssignedSlots[jobIndex], character->mTilePosition);
				}

				if (character->mAssignedSlots[jobIndex])
				{
					character->mTargetRooms[jobIndex]->bookGuest(character);
					character->mFetching = true;

					std::vector<Ogre::Vector2> path;
					Room::slotItem si = *character->mAssignedSlots[jobIndex]->items.begin();
					assert(si.reservedFor == character);
					assert(si.ent);
					Entity* fetchItem = si.ent;
					character->mWorldQuery->findShortestPath(
						fetchItem->getTilePosition(), character->mTilePosition, path);
					character->savePath(path);
					success = true;
				}
			}
		}
			break;
		case Room::FETCH_FLORAL_FOOD:
		{
			if (character->mHomeRoom->getType() == Room::STORAGE_CHAMBER)
			{
				character->mRoomQuery->findReachableRoom(character->mTilePosition,
					character->mTargetRooms[jobIndex], Room::FLORAL_SOURCE);
			}
			else
			{
				character->mTargetRooms[jobIndex] = character->mRoomQuery->findStorageWithResource(
					Entity::ET_FLORAL_FOOD, character->mTilePosition);
				if (!character->mTargetRooms[jobIndex])
				{
					character->mRoomQuery->findReachableRoom(character->mTilePosition,
						character->mTargetRooms[jobIndex], Room::FLORAL_SOURCE);
				}
			}

			if (character->mTargetRooms[jobIndex] && character->mAssignedSlots[jobIndex] == 0)
			{
				if (!character->mTargetRooms[jobIndex]->isEmptyIncReservations())
				{
					character->mTargetRooms[jobIndex]->reserveFetch(character,
						character->mAssignedSlots[jobIndex],
						Entity::ET_FLORAL_FOOD, character->mTilePosition);
				}

				if (character->mAssignedSlots[jobIndex])
				{
					character->mTargetRooms[jobIndex]->bookGuest(character);
					character->mFetching = true;

					std::vector<Ogre::Vector2> path;
					Room::slotItem si = *character->mAssignedSlots[jobIndex]->items.begin();
					assert(si.reservedFor == character);
					assert(si.ent);
					Entity* fetchItem = si.ent;
					character->mWorldQuery->findShortestPath(
						fetchItem->getTilePosition(), character->mTilePosition, path);
					character->savePath(path);
					success = true;
				}
			}

		}
			break;
		case Room::FETCH_FAUNAL_FOOD:
		{
			if (character->mHomeRoom->getType() == Room::STORAGE_CHAMBER)
			{
				std::vector<Room::RoomType> allowedRooms;
				allowedRooms.push_back(Room::FAUNAL_SOURCE);
				allowedRooms.push_back(Room::FUNGI_CHAMBER);
				character->mRoomQuery->findReachableRoom(character->mTilePosition,
					character->mTargetRooms[jobIndex], allowedRooms);
			}
			else
			{
				character->mTargetRooms[jobIndex] = character->mRoomQuery->findStorageWithResource(
					Entity::ET_FAUNAL_FOOD, character->mTilePosition);
				if (!character->mTargetRooms[jobIndex])
				{
					std::vector<Room::RoomType> allowedRooms;
					allowedRooms.push_back(Room::FAUNAL_SOURCE);
					allowedRooms.push_back(Room::FUNGI_CHAMBER);
					character->mRoomQuery->findReachableRoom(character->mTilePosition,
						character->mTargetRooms[jobIndex], allowedRooms);
				}
			}

			if (character->mTargetRooms[jobIndex] && character->mAssignedSlots[jobIndex] == 0)
			{
				if (!character->mTargetRooms[jobIndex]->isEmptyIncReservations())
				{
					character->mTargetRooms[jobIndex]->reserveFetch(character,
						character->mAssignedSlots[jobIndex],
						Entity::ET_FAUNAL_FOOD, character->mTilePosition);
				}

				if (character->mAssignedSlots[jobIndex])
				{
					character->mTargetRooms[jobIndex]->bookGuest(character);
					character->mFetching = true;

					std::vector<Ogre::Vector2> path;
					Room::slotItem si = *character->mAssignedSlots[jobIndex]->items.begin();
					assert(si.reservedFor == character);
					assert(si.ent);
					Entity* fetchItem = si.ent;
					character->mWorldQuery->findShortestPath(
						fetchItem->getTilePosition(), character->mTilePosition, path);
					character->savePath(path);
					success = true;
				}
			}
		}
			break;
		case Room::FETCH_COPAL:
		{
			if (character->mHomeRoom->getType() == Room::STORAGE_CHAMBER)
			{
				character->mRoomQuery->findReachableRoom(character->mTilePosition,
					character->mTargetRooms[jobIndex], Room::COPAL_SOURCE);
			}
			else
			{
				character->mTargetRooms[jobIndex] = character->mRoomQuery->findStorageWithResource(
					Entity::ET_COPAL, character->mTilePosition);
				if (!character->mTargetRooms[jobIndex])
				{
					character->mRoomQuery->findReachableRoom(character->mTilePosition,
						character->mTargetRooms[jobIndex], Room::COPAL_SOURCE);
				}
			}

			if (character->mTargetRooms[jobIndex] && character->mAssignedSlots[jobIndex] == 0)
			{
				if (character->mAssignedSlots[jobIndex] == 0 &&
					!character->mTargetRooms[jobIndex]->isEmptyIncReservations())
				{
					character->mTargetRooms[jobIndex]->reserveFetch(character,
						character->mAssignedSlots[jobIndex],
						Entity::ET_COPAL, character->mTilePosition);
				}

				if (character->mAssignedSlots[jobIndex])
				{
					character->mTargetRooms[jobIndex]->bookGuest(character);
					character->mFetching = true;

					std::vector<Ogre::Vector2> path;
					Room::slotItem si = *character->mAssignedSlots[jobIndex]->items.begin();
					assert(si.reservedFor == character);
					assert(si.ent);
					Entity* fetchItem = si.ent;
					character->mWorldQuery->findShortestPath(
						fetchItem->getTilePosition(), character->mTilePosition, path);
					character->savePath(path);
					success = true;
				}
			}
		}
			break;
		case Room::FETCH_EGGS:
		{
			// first find the throne room
			character->mTargetRooms[jobIndex] = character->mRoomQuery->getThroneChamber();
			if (character->mTargetRooms[jobIndex])
			{
				if (character->mAssignedSlots[jobIndex] == 0 &&
					!character->mTargetRooms[jobIndex]->isEmptyIncReservations())
				{
					character->mTargetRooms[jobIndex]->bookGuest(character);
					character->mTargetRooms[jobIndex]->reserveFetch(character, 
						character->mAssignedSlots[jobIndex], character->mTilePosition);
					character->mFetching = true;
				}

				if (character->mAssignedSlots[jobIndex])
				{
					std::vector<Ogre::Vector2> path;
					character->mWorldQuery->findShortestPath(character->mAssignedSlots[jobIndex]->position, character->mTilePosition, path);
					character->savePath(path);
					success = true;
				}
				/*
				else
				{
					// reminds that loose egg should be found
					character->mTargetRooms[jobIndex] = 0;

					// query for any eggs in proximity of this ant
					Ogre::Vector2 eggPosition;
					Entity* egg;
					character->mEntityQuery->findNextEntity(character->mTilePosition,
						eggPosition, egg, (int)(1 << Entity::ET_EGG), -1, 20);

					if (egg)
					{
						std::vector<Ogre::Vector2> path;
						character->mWorldQuery->findShortestPath(eggPosition, character->mTilePosition, path);
						character->savePath(path);
						success = true;
					}
				}
				*/
			}
		}
			break;
		case Room::PLACE:
		{
			if (character->mHomeRoom->freeSlots() > 0)
			{
				character->mHomeRoom->reservePlace(character, 
					character->mAssignedSlots[jobIndex], character->mTilePosition);
				character->mFetching = false;
			}

			if (character->mAssignedSlots[jobIndex])
			{
				std::vector<Ogre::Vector2> path;
				character->mWorldQuery->findShortestPath(character->mAssignedSlots[character->mActiveJobIndex]->position, character->mTilePosition, path);
				character->savePath(path);
				success = true;
			}
		}
            break;
        case Room::CLEAN_ROOM:
		case Room::FEED:
		{
			std::vector<Ogre::Vector2> path;
			Ogre::Vector2 p = character->mHomeRoom->getPivot(character->mTilePosition);
			character->mWorldQuery->findShortestPath(p, character->mTilePosition, path);
			character->savePath(path);
			character->mFetching = false;
			success = true;
		}
			break;
		}
	}
	return success;
}
template<class T>
bool WorkerTrait<T>::findPathFreelancer(float dt, T* character, ActionReactor* reactor, int jobIndex)
{
	bool success = false;

	Job* j = character->mJobQuery->getJob(character->mJobs[jobIndex]);
	if (j)
	{
		std::vector<Ogre::Vector2> path;
		switch ((Job::SubJobs)character->mSubJobs[jobIndex].front())
		{
			case Job::WALK_TO_TARGET:
			{
				switch (j->getType())
				{
					case Job::FETCH_JOB:
					{
						FetchJob* fj = (FetchJob*)j;
						character->mTargetRooms[jobIndex] = fj->getTargetRoom();
						break;
					}
					case Job::ROOM_CREATE_JOB:
					{
						RoomCreateJob* fj = (RoomCreateJob*)j;
						character->mTargetRooms[jobIndex] = fj->getTargetRoom();
						break;
					}
					case Job::ROOM_CREATE_JOB_EX:
					{
						RoomCreateJobEx* fj = (RoomCreateJobEx*)j;
						character->mTargetRooms[jobIndex] = fj->getTargetRoom();
						break;
					}
				}
				Ogre::Vector2 p = character->mTargetRooms[jobIndex]->getPivot(character->mTilePosition);
				character->mWorldQuery->findShortestPath(p, character->mTilePosition, path);
				break;
			}
			case Job::PLACE:
			{	
				character->mTargetRooms[jobIndex]->bookGuest(character);
				character->mTargetRooms[jobIndex]->reservePlace(character,
					character->mAssignedSlots[jobIndex], character->mTilePosition);
				character->mFetching = false;
				character->mWorldQuery->findShortestPath(
					character->mAssignedSlots[jobIndex]->position,
					character->mTilePosition, path);
				break;
			}
			case Job::FETCH:
			{
				Entity* item = 0;
				switch (j->getType())
				{
					case Job::FETCH_JOB:
					{
						FetchJob* fj = (FetchJob*)j;
						item = fj->getFetchEntity();
						break;
					}
					case Job::ROOM_CREATE_JOB:
					{
						RoomCreateJob* fj = (RoomCreateJob*)j;
						item = fj->getFetchEntity();
						break;
					}
					case Job::ROOM_CREATE_JOB_EX:
					{
						RoomCreateJobEx* fj = (RoomCreateJobEx*)j;
						item = fj->getFetchEntity();
						break;
					}
				}

				if (item)
				{
					character->mWorldQuery->findShortestPath(
						item->getTilePosition(),
						character->mTilePosition, path);
				}
				break;
			}
			case Job::UPDATE:
			{
				character->mWorldQuery->findShortestPath(
					j->getPosition(),
					character->mTilePosition, path);
				break;
			}
			case Job::CHECK:
			{
				character->mWorldQuery->generateRandomWalkOnJobs(
					character->mTilePosition,
					20, path, character->mPheromoneMap, character->mJobQuery);
				break;
			}
		}
		if (!path.empty())
		{
			path.pop_back();
			// why is ant sometimes already at target position?
			if (!path.empty())
			{
				character->savePath(path);
			}
			success = true;
		}
	}
	else
	{
		character->prepareForNewJob(jobIndex);
		character->mCurrentState = Character::FIND_JOB;
		success = false;
	}
	return success;
}
template<class T>
bool WorkerTrait<T>::workJobWorker(float dt, T* character, ActionReactor* reactor, int jobIndex)
{
	Room::WorkplaceSubjobs job = (Room::WorkplaceSubjobs)character->mSubJobs[jobIndex].front();
	switch (job)
	{
	case Room::FETCH_RESOURCE:
	case Room::FETCH_FAUNAL_FOOD:
	case Room::FETCH_FLORAL_FOOD:
	case Room::FETCH_COPAL:
		if (character->mTargetRooms[jobIndex])
		{
			Entity* item = character->mTargetRooms[jobIndex]->fetch(character, character->mAssignedSlots[jobIndex]);
			character->pick(item, jobIndex);

			character->mTargetRooms[jobIndex]->unbookGuest(character);
			character->mAssignedSlots[jobIndex] = 0;

			// faunal resource rooms are destroyed (they don't grow back)
			if (character->mTargetRooms[jobIndex]->getType() == Room::FAUNAL_SOURCE)
				reactor->removeResource(character->mTargetRooms[jobIndex]);

		}
		break;
	case Room::FETCH_EGGS:
	{
		if (character->mTargetRooms[jobIndex])
		{
			Entity* item = character->mTargetRooms[jobIndex]->fetch(character, character->mAssignedSlots[jobIndex]);
			character->pick(item, jobIndex);

			character->mTargetRooms[jobIndex]->unbookGuest(character);
			character->mAssignedSlots[jobIndex] = 0;
		}
		else
		{
			// a bit messy here: empty except last one, which will be 
			// popped at the end of the funciton
			while (character->mSubJobs[jobIndex].size() > 1) character->mSubJobs[jobIndex].pop();
		}
		/*
		else
		{
			Ogre::Vector2 eggPosition;
			Entity* egg;
			character->mEntityQuery->findNextEntity(character->mTilePosition,
				eggPosition, egg, (int)(1 << Entity::ET_EGG), -1, 20);
			if (egg)
			{
				character->pick(egg, jobIndex);
			}
			else
			{
				// a bit messy here: empty except last one, which will be 
				// popped at the end of the funciton
				while (character->mSubJobs[jobIndex].size() > 1) character->mSubJobs[jobIndex].pop();
			}
		}
		*/
	}
		break;
	case Room::PLACE:
	{
		if (!character->mInventory.empty())
		{
			character->mHomeRoom->place(character, character->mInventory[jobIndex], character->mAssignedSlots[jobIndex]);
#ifdef _DEBUG
			Framework::getSingleton().mLog->logMessage("<from worker " + 
				Ogre::StringConverter::toString(character->getID()) + ", room::place>");
#endif
			character->mAssignedSlots[jobIndex] = 0;
			character->drop(character->mInventory[jobIndex], jobIndex);
		}
	}
		break;
	case Room::FEED:
	{
		switch (character->mHomeRoom->getType())
		{
		case Room::BROOD_CHAMBER:
		{
			BroodChamber* r = (BroodChamber*)character->mHomeRoom;
			r->feed(character->mInventory[jobIndex]);
			character->drop(character->mInventory[jobIndex], jobIndex);
		}
			break;
        }
    }
        break;
    case Room::CLEAN_ROOM:
    {
        character->mHomeRoom->clean (character->mInventory[jobIndex]);
        character->drop (character->mInventory[jobIndex], jobIndex);
    }
		break;
	}
	character->mSubJobs[jobIndex].pop();
	character->mCurrentState = Character::FIND_JOB;
	return true;
}
template<class T>
bool WorkerTrait<T>::workJobFreelancer(float dt, T* character, ActionReactor* reactor, int jobIndex)
{
	Job::SubJobs job = (Job::SubJobs)character->mSubJobs[jobIndex].front();
	Job* j = character->mJobQuery->getJob(character->mJobs[jobIndex]);
	bool success = false;
	if (j)
	{
		switch (job)
		{
		case Job::WALK_TO_TARGET:
			character->mSubJobs[jobIndex].pop();
			success = character->mSubJobs[jobIndex].empty();
			character->mCurrentState = Character::FIND_JOB;
			break;
		case Job::PLACE:
			if (character->mTargetRooms[jobIndex])
			{
				character->mTargetRooms[jobIndex]->place(character, character->mInventory[jobIndex],
					character->mAssignedSlots[jobIndex]);
#ifdef _DEBUG
				Framework::getSingleton().mLog->logMessage("<from freelancer " +
					Ogre::StringConverter::toString(character->getID()) + ", job::place>");
#endif
				character->drop(character->mInventory[jobIndex], jobIndex);

				character->mTargetRooms[jobIndex]->unbookGuest(character);
				character->mAssignedSlots[jobIndex] = 0;
				character->mTargetRooms[jobIndex] = 0;
				character->mSubJobs[jobIndex].pop();
				character->mCurrentState = Character::FIND_JOB;
			}
			success = character->mSubJobs[jobIndex].empty();
			break;
		case Job::FETCH:
		{
			Entity* item = 0;
			switch (j->getType())
			{
			case Job::FETCH_JOB:
			{
				FetchJob* fj = (FetchJob*)j;
				item = fj->getFetchEntity();
				break;
			}
			case Job::ROOM_CREATE_JOB:
			{
				RoomCreateJob* fj = (RoomCreateJob*)j;
				item = fj->getFetchEntity();
				break;
			}
			case Job::ROOM_CREATE_JOB_EX:
			{
				RoomCreateJobEx* fj = (RoomCreateJobEx*)j;
				item = fj->getFetchEntity();
				break;
			}
			}
			character->pick(item, jobIndex);
			character->mSubJobs[jobIndex].pop();
			character->mCurrentState = Character::FIND_JOB;
			reactor->jobMilestoneReached(j);
			success = character->mSubJobs[jobIndex].empty();
		}
			break;
		case Job::UPDATE:
			if (!j || j->update(dt, reactor))
			{
				//done
				character->mSubJobs[jobIndex].pop();
				character->mCurrentState = Character::FIND_JOB;
			}
			success = character->mSubJobs[jobIndex].empty();
			break;
		case Job::CHECK:
			character->mSubJobs[jobIndex].pop();
			character->mFreelancing = character->mHomeRoom == 0;
			if (j)
			{
				character->mJobQuery->unAssignJob(character, j->getID());
				character->prepareForNewJob(jobIndex);
			}
			break;
		}

		if (success)
		{
			j->finish(reactor);

			// if there are some dependencies remaining, drop them before assigning a new job
			character->prepareForNewJob(jobIndex);

			character->mFreelancing = character->mHomeRoom == 0;
		}

		if (character->mSubJobs[jobIndex].empty())
		{
			character->mCurrentState = Character::FIND_JOB;
		}
	}
	else
	{
		character->prepareForNewJob(jobIndex);
		character->mCurrentState = Character::FIND_JOB;
	}
	return true;
}


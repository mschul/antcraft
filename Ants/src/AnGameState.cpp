 #include "AnPCH.h"
#include "AnGameState.h"
#include "AnPlayerClient.h"
#include "AnEntity.h"
#include "AnAnt.h"
#include "AnTile.h"
#include "AnBroodChamber.h"
#include "AnThroneChamber.h"
#include "AnStorageChamber.h"
#include "AnStatistics.h"
using namespace Ants;

GameSettings GameState::Settings;
bool GameState::GameStatisticsSynched = true;
Statistics GameState::LastGameStatistics;

GameState::GameState()
{
    mMoveSpeed			= 300;
    mRotateSpeed		= 0.3f;

    mLMouseDown       = false;
    mRMouseDown       = false;
    mQuit             = false;
	mDragCam	 	  = false;
	mCtrlDown		  = false;
	mShiftDown		  = false;
	mTranslateVector = Ogre::Vector3::ZERO;
	mZoomSpeed		  = 0.0;
	mZoomFriction	  = 600;

	mCurrentNodeName = "";

	mLastMousePos = Ogre::Vector2(300,300);
	mLastPickedPos = Ogre::Vector2(300,300);
	mCameraTargetPos = Ogre::Vector3(0, 0, 0);
	mCurrentUserTool = NONE;
	mNextGuiUpdate = 0.5;

	mNextMouseHoldAction = 0.0;
	mMouseHoldActionActive = false;
	
	mNotifications.resize(10, 0);

	mCurrentRoomID = -1;
}

void GameState::enter()
{
    Framework::getSingletonPtr()->mLog->logMessage("Entering GameState...");

    mSceneMgr = Framework::getSingletonPtr()->mRoot->createSceneManager(Ogre::ST_GENERIC, "GameSceneMgr");
    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

	mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_TEXTURE_ADDITIVE_INTEGRATED);
	mSceneMgr->setShadowTextureConfig(0, 2048, 2048, Ogre::PF_FLOAT32_R);
	mSceneMgr->setShadowTextureSelfShadow( true );
	mSceneMgr->setShadowCasterRenderBackFaces( true );
	Framework::getSingleton().mViewport->setVisibilityMask(VISIBILE_FLAG);

	//LiSPSMShadowCameraSetup *shadowCameraSetup = new LiSPSMShadowCameraSetup();
	Ogre::FocusedShadowCameraSetup* shadowCameraSetup = new Ogre::FocusedShadowCameraSetup();
	//PlaneOptimalShadowCameraSetup *shadowCameraSetup = new PlaneOptimalShadowCameraSetup();
	
    mRSQ = mSceneMgr->createRayQuery(Ogre::Ray());

	mSceneMgr->setShadowCameraSetup(Ogre::ShadowCameraSetupPtr(shadowCameraSetup));

    mSceneMgr->addRenderQueueListener(Framework::getSingletonPtr()->mOverlaySystem);
	
	mCameraNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("CameraNode1");
	

	Ogre::Real width = Ogre::Real(Framework::getSingletonPtr()->mViewport->getActualWidth());
	Ogre::Real height = Ogre::Real(Framework::getSingletonPtr()->mViewport->getActualHeight());

    mCamera = mSceneMgr->createCamera("GameCamera");
    mCamera->setNearClipDistance(5);
	mCamera->setAspectRatio(width / height);
    Framework::getSingletonPtr()->mViewport->setCamera(mCamera);
	mCameraNode->attachObject(mCamera);
	
	mServer = new0 Server();
	mServer->init(150, 150, Settings.getFOW(), Settings.getRevealMap(), mSceneMgr);
	

	mPlayer = new0 PlayerClient();
	mPlayer->login(mServer);
	mPlayer->init(150, 150);
	mPlayer->setFOW(Settings.getFOW());
	if (Settings.getUpgrades())
	{
		mPlayer->setUpgradeLevel((int)Entity::U_COUNT);
	}

	if (Settings.getMobAmount() > 0)
	{
		mMobs = new0 MobClient();
		mMobs->setMobAmount(Settings.getMobAmount());
		mMobs->login(mServer);
		mMobs->init(150, 150);
	}

	mPowerWheelFader = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeIn");

	createScene();
	buildGUI();
}
bool GameState::pause()
{
	mRoomButtons.clear();
    Framework::getSingletonPtr()->mLog->logMessage("Pausing GameState...");
	
    return true;
}
void GameState::resume()
{
    Framework::getSingletonPtr()->mLog->logMessage("Resuming GameState...");

	buildGUI();

	// do a full gui update (my hawklike eyes can see this one frame the update is late otherwise)
	mNextGuiUpdate = 0.5f;
	updateStatsPanel();
	updateInfoLabel();

    Framework::getSingletonPtr()->mViewport->setCamera(mCamera);
    mQuit = false;
}
void GameState::exit()
{
    Framework::getSingletonPtr()->mLog->logMessage("Leaving GameState...");
	
	Statistics* stats = mPlayer->getStatistics();
	Framework::getSingleton().mAllTimeStats->add(*stats);


	LastGameStatistics.set(*stats);

	GameStatisticsSynched = false;

	mRoomButtons.clear();

	mPlayer->logoff();
	delete0(mPlayer);
	mPlayer = 0;

	if (mMobs)
	{
		mMobs->logoff();
		delete0(mMobs);
		mMobs = 0;
	}

	delete0(mServer);
	mServer = 0;
	
    if(mSceneMgr)
	{
		mSceneMgr->destroyCamera(mCamera);
		mSceneMgr->destroyQuery(mRSQ);
        Framework::getSingletonPtr()->mRoot->destroySceneManager(mSceneMgr);
	}
	mSceneMgr = 0;
}
void GameState::createScene()
{
	int width = Framework::getSingletonPtr()->mViewport->getActualWidth();
	int height = Framework::getSingletonPtr()->mViewport->getActualHeight();

	Ogre::Light* l = mSceneMgr->createLight("Light1");
	l->setPosition(20,75,75);
	l->setDiffuseColour(Ogre::ColourValue(0.6f, 0.6f, 0.6f));

	mServer->createWorld(Settings.getRockAmount(), Settings.getWaterAmount(),
		Settings.getCaveAmount(), Settings.getGrassAmount());
	mPlayer->initScene(mSceneMgr, mCamera, width, height);
	std::vector<Ogre::Vector2> locations;
	mPlayer->settle(locations);

	/*
	Ogre::Rectangle2D *mMiniScreen = new Ogre::Rectangle2D(true);
	mMiniScreen->setCorners(0.48f, -0.74f, 0.98f, -0.98f);
	mMiniScreen->setBoundingBox(Ogre::AxisAlignedBox(-100000.0f * Ogre::Vector3::UNIT_SCALE,
		100000.0f * Ogre::Vector3::UNIT_SCALE));
	Ogre::SceneNode* miniScreenNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("MiniScreenNode");
	miniScreenNode->attachObject(mMiniScreen);
	mMiniScreen->setMaterial("MiniMapMaterial");
	*/

	Ogre::Vector2 settlePoint = locations.front();
	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(Ogre::Vector2(settlePoint.x, settlePoint.y), hex);
	
	mHomePosition.x = hex.x * 2;
	mHomePosition.y = hex.y * 2;
	mHomePosition.z = 0;
	mCameraTargetPos = mHomePosition + Ogre::Vector3(0, 10, 40);
	mCameraNode->setPosition(mCameraTargetPos);
	mCameraNode->lookAt(mHomePosition, Ogre::Node::TS_WORLD);

	if (mMobs)
	{
		locations.clear();
		mMobs->settle(locations);
	}
}

bool GameState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	std::vector<bool> tookUpgrades;
	mPlayer->getUpgrades(tookUpgrades);
	OIS::Keyboard* k = Framework::getSingletonPtr()->mKeyboard;
	if (k->isKeyDown(OIS::KC_F1))
	{
		//mWireframeCheckbox->toggle();
	}
	if (k->isKeyDown(OIS::KC_ESCAPE))
	{
		pushAppState(findByName("PauseState"));
		return true;
	}
	if (k->isKeyDown(OIS::KC_O))
	{
		//mWireframeCheckbox->isVisible() ? mWireframeCheckbox->hide() : mWireframeCheckbox->show();
	}


	if (k->isKeyDown(OIS::KC_D))
	{
		mCurrentUserTool = mCurrentUserTool == DIG ? NONE : DIG;
	}
	if (k->isKeyDown(OIS::KC_F))
	{
		mCurrentUserTool = mCurrentUserTool == DEFEND ? NONE : DEFEND;
	}
	if (k->isKeyDown(OIS::KC_G))
	{
		mCurrentUserTool = mCurrentUserTool == HOLD ? NONE : HOLD;
		mCurrentUserTool = HOLD;
	}
	if (k->isKeyDown(OIS::KC_W))
	{
		mCurrentUserTool = mCurrentUserTool == ASSIGN_STORAGE_CHAMBER ? NONE : ASSIGN_STORAGE_CHAMBER;
	}
	if (k->isKeyDown(OIS::KC_H))
	{
		mPlayer->toggleHealthBarsVisible();
	}
	if (k->isKeyDown(OIS::KC_E))
	{
		mCurrentUserTool = mCurrentUserTool == ASSIGN_BROOD_CHAMBER ? NONE : ASSIGN_BROOD_CHAMBER;
	}
	if (k->isKeyDown(OIS::KC_R))
	{
		mCurrentUserTool = mCurrentUserTool == ASSIGN_FUNGI_CHAMBER ? NONE : ASSIGN_FUNGI_CHAMBER;
	}

	Framework::getSingletonPtr()->keyPressed(keyEventRef);
	return true;
}
bool GameState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	Framework::getSingletonPtr()->keyReleased(keyEventRef);
	return true;
}

bool GameState::mouseMoved(const OIS::MouseEvent &evt)
{
	Framework::getSingletonPtr()->mouseMoved(evt);
	mLastMousePos = Ogre::Vector2(real(evt.state.X.abs), real(evt.state.Y.abs));

	if (mLMouseDown && mRMouseDown)
	{
		onLRDrag(evt);
	}
	else if (mLMouseDown)
	{
		onLeftDrag(evt);
	}
	else if (mRMouseDown)
	{
		onRightDrag(evt);
	}

	else if (mCurrentUserTool != NONE)
	{
		Ogre::Vector2 position;
		if (pickTile(evt, position))
		{
			if (mCurrentUserTool == DIG)
				mPlayer->hover(position, PlayerClient::DIG);
			else
				mPlayer->hover(position, PlayerClient::ROOMS);
		}
		else
		{
			mPlayer->hideHover();
		}
	}
	else
	{
		mPlayer->hideHover();
	}

	mZoomSpeed -= evt.state.Z.rel*0.5f;
	return true;
}
bool GameState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if (id == OIS::MB_Left) mLMouseDown = true;
	else if (id == OIS::MB_Right) mRMouseDown = true;

	bool handled = Framework::getSingletonPtr()->mousePressed(evt, id);
	if (mLMouseDown && mRMouseDown)
	{
		onLRPressed(evt, handled);
	}
	else if (id == OIS::MB_Left)
	{
		onLeftPressed(evt, handled);
	}
	else if (id == OIS::MB_Right)
	{
		onRightPressed(evt, handled);
	}
	return true;
}
bool GameState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	if (id == OIS::MB_Left) mLMouseDown = false;
	else if (id == OIS::MB_Right) mRMouseDown = false;

	bool handled = Framework::getSingletonPtr()->mouseReleased(evt, id);

	if (mLMouseDown && mRMouseDown)
	{
		onLRReleased(evt, handled);
	}
	else if (id == OIS::MB_Left)
	{
		onLeftReleased(evt, handled);
	}
	else if (id == OIS::MB_Right)
	{
		onRightReleased(evt, handled);
		if (mLMouseDown)
		{
			mPlayer->clearPreselection();
		}
	}
	return true;
}

void GameState::onLeftPressed(const OIS::MouseEvent &evt, bool handled)
{
	if (!handled)
	{
		Ogre::Vector2 position;
		Entity* ent = 0;
		if (pickTile(evt, position))
		{
			mLastMouseDown = position;

			std::vector<Ogre::Vector2> positions;
			positions.push_back(position);
			switch (mCurrentUserTool)
			{
			case NONE:
			{
				Room* room = mPlayer->getRoomAt(position);
				if (room)
				{
					showRoom(room->getID());
				}
				else
				{

					Room* room = mPlayer->getResourceAt(position);
					if (room)
					{
						showRoom(room->getID());
					}
					else
					{
						Room* room = mPlayer->getExitAt(position);
						if (room)
						{
							showRoom(room->getID());
						}
					}
				}
				break;
			}
			case DIG:
			{
				if (mCtrlDown)
					mPlayer->preUnSelect(position);
				else
					mPlayer->preSelect(position);
			}
				break;
			case ASSIGN_ROOM:
				if (mCtrlDown)
				{
					if (mCtrlDown)
						mPlayer->preUnAssign(position);
				}
				break;
			case ASSIGN_BROOD_CHAMBER:
			case ASSIGN_STORAGE_CHAMBER:
			case ASSIGN_FUNGI_CHAMBER:
			{
				if (mCtrlDown)
					mPlayer->preUnAssign(position);
				else
					mPlayer->preAssign(position);
			}
				break;
			case HOLD:
			case DEFEND:
				mMouseHoldActionActive = true;
				break;
			}
		}
	}
}
void GameState::onLeftDrag(const OIS::MouseEvent &evt)
{
	Ogre::Vector2 position;
	if (pickTile(evt, position))
	{
		switch (mCurrentUserTool)
		{
		case DIG:
			if (mCtrlDown)
			{
				if (!mShiftDown) mPlayer->preUnSelectLine(mLastMouseDown, position);
				else mPlayer->preUnSelectCircle(mLastMouseDown, position);
			}
			else
			{
				if (!mShiftDown) mPlayer->preSelectLine(mLastMouseDown, position);
				else mPlayer->preSelectCircle(mLastMouseDown, position);
			}
			break;
		case ASSIGN_ROOM:
			if (mCtrlDown)
			{
				if (!mShiftDown) mPlayer->preUnAssignLine(mLastMouseDown, position);
				else mPlayer->preUnAssignCircle(mLastMouseDown, position);
			}
			break;
		case ASSIGN_BROOD_CHAMBER:
		case ASSIGN_STORAGE_CHAMBER:
		case ASSIGN_FUNGI_CHAMBER:
			if (mCtrlDown)
			{
				if (!mShiftDown) mPlayer->preUnAssignLine(mLastMouseDown, position);
				else mPlayer->preUnAssignCircle(mLastMouseDown, position);
			}
			else
			{
				if (!mShiftDown) mPlayer->preAssignLine(mLastMouseDown, position);
				else mPlayer->preAssignCircle(mLastMouseDown, position);
			}
			break;
		}
	}
}
void GameState::onLeftReleased(const OIS::MouseEvent &evt, bool handled)
{
	mMouseHoldActionActive = false;
	mDragCam = false;

	if (!handled)
	{
		switch (mCurrentUserTool)
		{
		case DIG:
			mPlayer->select();
			break;
		case ASSIGN_ROOM:
			if (mCtrlDown)
			{
				mPlayer->assign(Room::NONE);
			}
			break;
		case ASSIGN_BROOD_CHAMBER:
			mPlayer->assign(mCtrlDown ? Room::NONE : Room::BROOD_CHAMBER);
			break;
		case ASSIGN_STORAGE_CHAMBER:
			mPlayer->assign(mCtrlDown ? Room::NONE : Room::STORAGE_CHAMBER);
			break;
		case ASSIGN_FUNGI_CHAMBER:
			mPlayer->assign(mCtrlDown ? Room::NONE : Room::FUNGI_CHAMBER);
			break;
		}
	}
	else
	{
		mPlayer->clearPreassignment();
		mPlayer->clearPreselection();
	}
}

void GameState::onRightPressed(const OIS::MouseEvent &evt, bool handled)
{
	if (!handled)
	{
		mCurrentUserTool = NONE;

		CEGUI::UVector2 popup_pos;

		popup_pos.d_x = CEGUI::UDim((float)evt.state.X.abs /
			Framework::getSingleton().mViewport->getActualWidth(), 0);
		popup_pos.d_y = CEGUI::UDim((float)evt.state.Y.abs /
			Framework::getSingleton().mViewport->getActualHeight(), 0);

		popup_pos.d_x -= 0.3f*mPowerWheel->getSize().d_width;
		popup_pos.d_y -= 0.6f*mPowerWheel->getSize().d_height;

		// set position of menu so it's around where the mouse was clicked.
		mPowerWheel->setPosition(popup_pos);

		// show the menu.
		if (!mPowerWheelFader->isRunning())
		{
			mPowerWheel->show();
			mPowerWheelFader->setTargetWindow(mPowerWheel);
			mPowerWheelFader->start();
		}

		std::vector<bool> tookUpgrades;
		mPlayer->getUpgrades(tookUpgrades);

		if (tookUpgrades[Entity::U_FUNGI])
		{
			mPowerWheel->getChild("FungiTool")->show();
		}
		else
		{
			mPowerWheel->getChild("FungiTool")->hide();
		}
	}
}
void GameState::onRightDrag(const OIS::MouseEvent &evt)
{
}
void GameState::onRightReleased(const OIS::MouseEvent &evt, bool)
{
	mPowerWheel->hide();
	mDragCam = false;
}

void GameState::onLRPressed(const OIS::MouseEvent &evt, bool handled)
{
	if (!handled)
	{
		if (mCurrentPage == mFloralPage
			|| mCurrentPage == mCopalPage
			|| mCurrentPage == mExitPage)
			onHideInfoButtonClicked(CEGUI::EventArgs());

		mDragCam = true;
		mMouseHoldActionActive = false;
		mPlayer->clearPreassignment();
		mPlayer->clearPreselection();
		mPowerWheel->hide();
	}
}
void GameState::onLRDrag(const OIS::MouseEvent &evt)
{
	// assert(mDragCam);
	Ogre::Real x = real(evt.state.X.rel);
	Ogre::Real y = real(evt.state.Y.rel);
	mTranslateVector = Ogre::Vector3(-x, y, 0);
	mZoomSpeed = 0;
}
void GameState::onLRReleased(const OIS::MouseEvent &evt, bool)
{
	mDragCam = false;
	mMouseHoldActionActive = false;
}


void GameState::buildGUI()
{
	CEGUI::Window* wnd = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("MainGame.layout");
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(wnd);

	//embed minimap in CEGUI
	// make the texture
	Ogre::TexturePtr tex = Ogre::TextureManager::getSingleton().getByName("MiniMapWorldTexture");

	if (Framework::getSingleton().mRenderer->isTextureDefined("MinimapWorldCEGUITexture"))
		Framework::getSingleton().mRenderer->destroyTexture("MinimapWorldCEGUITexture");
	CEGUI::Texture &guiTex = Framework::getSingleton().mRenderer->createTexture("MinimapWorldCEGUITexture", tex);

	// put the texture in an image
	const CEGUI::Rectf rect(CEGUI::Vector2f(0.0f, 0.0f), guiTex.getOriginalDataSize());
	CEGUI::BasicImage* image;
	if (!CEGUI::ImageManager::getSingleton().isImageTypeAvailable("RTTImageset"))
	{
		CEGUI::ImageManager::getSingleton().addImageType<CEGUI::BasicImage>("RTTImageset");
		image = (CEGUI::BasicImage*)(&CEGUI::ImageManager::getSingleton().create("RTTImageset", "MiniMapImage"));
	}
	else
	{
		image = (CEGUI::BasicImage*)(&CEGUI::ImageManager::getSingleton().get("MiniMapImage"));
	}

	image->setTexture(&guiTex);
	image->setArea(rect);
	image->setAutoScaled(CEGUI::ASM_Both);

	// make a static window
	CEGUI::Window *miniMap = wnd->getChild("MiniMapScreen/MiniMap");
	miniMap->setProperty("Image", "MiniMapImage");
	miniMap->setUpdateMode(CEGUI::WindowUpdateMode::WUM_ALWAYS);
	

	mMiniMap = wnd->getChild("MiniMapScreen");
	mMiniMap->setUpdateMode(CEGUI::WindowUpdateMode::WUM_ALWAYS);

	mPowerWheel = wnd->getChild("PowerWheel");

	wnd->getChild("RoomListBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onRoomButtonClicked, this));
	wnd->getChild("UpgradesBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeShowButtonClicked, this));
	wnd->getChild("StatsBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onStatsButtonClicked, this));

	((CEGUI::PushButton*)mPowerWheel->getChild("SelectTool"))->
		subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
		CEGUI::Event::Subscriber(&GameState::onPowerWheelEnter, this));

	((CEGUI::PushButton*)mPowerWheel->getChild("DigTool"))->
		subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
		CEGUI::Event::Subscriber(&GameState::onPowerWheelEnter, this));
	((CEGUI::PushButton*)mPowerWheel->getChild("DefendTool"))->
		subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
		CEGUI::Event::Subscriber(&GameState::onPowerWheelEnter, this));
	((CEGUI::PushButton*)mPowerWheel->getChild("AttackTool"))->
		subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
		CEGUI::Event::Subscriber(&GameState::onPowerWheelEnter, this));

	((CEGUI::PushButton*)mPowerWheel->getChild("StorageTool"))->
		subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
		CEGUI::Event::Subscriber(&GameState::onPowerWheelEnter, this));
	((CEGUI::PushButton*)mPowerWheel->getChild("BroodTool"))->
		subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
		CEGUI::Event::Subscriber(&GameState::onPowerWheelEnter, this));
	((CEGUI::PushButton*)mPowerWheel->getChild("FungiTool"))->
		subscribeEvent(CEGUI::PushButton::EventMouseEntersSurface,
		CEGUI::Event::Subscriber(&GameState::onPowerWheelEnter, this));

	mInfoTab = static_cast<CEGUI::FrameWindow*>(wnd->getChild("InfoTab"));

	mThronePage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("ThronePage.layout");
	((CEGUI::PushButton*)mThronePage->getChild("ExpandBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onWorkerBehaviourClicked, this));
	((CEGUI::PushButton*)mThronePage->getChild("DiscoverBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onWorkerBehaviourClicked, this));
	((CEGUI::PushButton*)mThronePage->getChild("ProtectBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onWorkerBehaviourClicked, this));

	mBroodPage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("BroodPage.layout");
	((CEGUI::PushButton*)mBroodPage->getChild("SmallWorkerBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUnitProductionClicked, this));
	((CEGUI::PushButton*)mBroodPage->getChild("WorkerBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUnitProductionClicked, this));
	((CEGUI::PushButton*)mBroodPage->getChild("CarrierBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUnitProductionClicked, this));
	((CEGUI::PushButton*)mBroodPage->getChild("SoldierBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUnitProductionClicked, this));
	((CEGUI::PushButton*)mBroodPage->getChild("KamikazeBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUnitProductionClicked, this));

	mStoragePage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("StoragePage.layout");
	mFungiPage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("FungiPage.layout");
	mFloralPage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("FloralPage.layout");
	mCopalPage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("CopalPage.layout");
	mExitPage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("ExitPage.layout");

	mUpgradesPage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("UpgradesPage.layout");
	mStatsPage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("StatsPage.layout");
	mRoomsPage = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("RoomListPage.layout");

	mCurrentPage = 0;
	mInfoTab->setVisible(false);

	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeHorns"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));
	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeMovement"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));
	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeHarvest"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));

	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeAcid"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));
	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeCarrier"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));
	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeAntibiotics"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));

	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeHoneydew"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));
	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeKamikaze"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));

	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeBulldog"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));
	((CEGUI::PushButton*)mUpgradesPage->getChild("UpgradeFungi"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onUpgradeButtonClicked, this));

	((CEGUI::PushButton*)mThronePage->getChild("BackBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onBackToRoomListButtonClicked, this));
	((CEGUI::PushButton*)mBroodPage->getChild("BackBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onBackToRoomListButtonClicked, this));
	((CEGUI::PushButton*)mStoragePage->getChild("BackBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onBackToRoomListButtonClicked, this));
	((CEGUI::PushButton*)mFungiPage->getChild("BackBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onBackToRoomListButtonClicked, this));

	((CEGUI::PushButton*)mFloralPage->getChild("OkBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onHideInfoButtonClicked, this));
	((CEGUI::PushButton*)mCopalPage->getChild("OkBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onHideInfoButtonClicked, this));
	((CEGUI::PushButton*)mExitPage->getChild("OkBtn"))->
		subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&GameState::onHideInfoButtonClicked, this));
}

void GameState::switchInfoPage(CEGUI::Window* page)
{
	if (mCurrentPage)
		mInfoTab->removeChild(mCurrentPage);
	mCurrentPage = page;
	if (page)
	{
		mInfoTab->addChild(mCurrentPage);
		mInfoTab->setVisible(true);
	}
	else
		mInfoTab->setVisible(false);
}

void GameState::updateInfoLabel()
{
	CEGUI::Window* wnd = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow()->getChild("InfoGroup");

	int upgrades = mPlayer->getAvailableUpgradePoint();
	int pointsLeft = mPlayer->getUnitsNeededForUpgrade();
	Statistics* stat = mPlayer->getStatistics();

	Ogre::String upgradeInfo;
	if (pointsLeft >= 0)
		upgradeInfo = " (+" + Ogre::StringConverter::toString(pointsLeft) + ")";
	else
		upgradeInfo = " (maxed)";

	wnd->getChild("Units")->setText(Ogre::StringConverter::toString(stat->getUnitCount()));
	wnd->getChild("Floral")->setText(Ogre::StringConverter::toString(stat->FloralStored) + "g");
	wnd->getChild("Faunal")->setText(Ogre::StringConverter::toString(stat->FaunalStored) + "p");
	wnd->getChild("Copal")->setText(Ogre::StringConverter::toString(stat->CopalStored) + "c");
	wnd->getChild("Score")->setText(Ogre::StringConverter::toString(stat->getScore()));
	wnd->getChild("Upgrades")->setText(Ogre::StringConverter::toString(upgrades) + upgradeInfo);
}
void GameState::updateStatsPanel()
{

	if (mCurrentPage)
	{
		if (mCurrentPage == mStatsPage)
		{
			Statistics* stats = mPlayer->getStatistics();

			mCurrentPage->getChild("PlayTime")->setText(stats->getTimePassed());

			mCurrentPage->getChild("Rooms")->setText(Ogre::StringConverter::toString(stats->getRoomCount()));
			mCurrentPage->getChild("Area")->setText(Ogre::StringConverter::toString(stats->AnthillArea));
			mCurrentPage->getChild("RoomsArea")->setText(Ogre::StringConverter::toString(stats->getRoomArea()));

			mCurrentPage->getChild("Produced")->setText(Ogre::StringConverter::toString(stats->getUnitsProduced()));
			mCurrentPage->getChild("Alive")->setText(Ogre::StringConverter::toString(stats->getUnitCount()));
			mCurrentPage->getChild("Killed")->setText(Ogre::StringConverter::toString(stats->getUnitsKilled()));

			mCurrentPage->getChild("SmallWorker")->setText(Ogre::StringConverter::toString(stats->SmallWorker.Alive));
			mCurrentPage->getChild("Worker")->setText(Ogre::StringConverter::toString(stats->Worker.Alive));
			mCurrentPage->getChild("Carrier")->setText(Ogre::StringConverter::toString(stats->SmallWorker.Alive));
			mCurrentPage->getChild("Soldier")->setText(Ogre::StringConverter::toString(stats->Soldier.Alive));
			mCurrentPage->getChild("Kamikaze")->setText(Ogre::StringConverter::toString(stats->Kamikaze.Alive));

		}
		else if (mCurrentPage == mUpgradesPage)
		{
			std::vector<bool> available;
			mPlayer->getAvailableUpgrade(available);

			int points = mPlayer->getAvailableUpgradePoint();
			int unitsLeft = mPlayer->getUnitsNeededForUpgrade();
			if (unitsLeft >= 0)
			{
				mCurrentPage->setText("Upgrades (" + Ogre::StringConverter::toString(points) + ")");
				mCurrentPage->getChild("PointsLbl")->setText(Ogre::StringConverter::toString(points) + " upgrades available");
				mCurrentPage->getChild("UnitsLeftLbl")->setText(Ogre::StringConverter::toString(unitsLeft) + " points to next upgrade");

			}
			else
			{
				mCurrentPage->setText("Upgrades (MAX)");
				mCurrentPage->getChild("PointsLbl")->setText(Ogre::StringConverter::toString(points) + " upgrades available");
				mCurrentPage->getChild("UnitsLeftLbl")->setText("Upgrades already maxed");
			}

			CEGUI::PushButton* upgrade;
			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeHorns");
			available[Entity::U_HORNS] ? upgrade->enable() : upgrade->disable();
			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeMovement");
			available[Entity::U_MOVEMENT_SPEED] ? upgrade->enable() : upgrade->disable();
			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeHarvest");
			available[Entity::U_HARVEST_SPEED] ? upgrade->enable() : upgrade->disable();

			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeAcid");
			available[Entity::U_ACID] ? upgrade->enable() : upgrade->disable();
			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeCarrier");
			available[Entity::U_CARRIER] ? upgrade->enable() : upgrade->disable();
			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeAntibiotics");
			available[Entity::U_ANTIBIOTICS] ? upgrade->enable() : upgrade->disable();

			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeHoneydew");
			available[Entity::U_HONEYDEW] ? upgrade->enable() : upgrade->disable();
			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeKamikaze");
			available[Entity::U_KAMIKAZE] ? upgrade->enable() : upgrade->disable();

			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeBulldog");
			available[Entity::U_BULLDOGS] ? upgrade->enable() : upgrade->disable();
			upgrade = (CEGUI::PushButton*)mCurrentPage->getChild("UpgradeFungi");
			available[Entity::U_FUNGI] ? upgrade->enable() : upgrade->disable();
		}
		else if (mCurrentPage == mRoomsPage)
		{
			std::vector<Room*> rooms;
			mPlayer->getRoomList(rooms);

			int i = 0;
			std::set<int> openRoomIds;
			for (std::pair<int, CEGUI::PushButton*> p : mRoomButtons)
			{
				openRoomIds.insert(p.first);
			}

			for (Room* room : rooms)
			{
				CEGUI::PushButton* btn;
				Ogre::String caption;
				switch (room->getType())
				{
				case Room::THRONE_CHAMBER: caption = "Throne Chamber"; break;
				case Room::BROOD_CHAMBER: caption = "Brood Chamber"; break;
				case Room::STORAGE_CHAMBER: caption = "Storage Chamber"; break;
				case Room::FUNGI_CHAMBER:  caption = "Fungi Chamber"; break;
				default:  caption = "Unkown Room"; break;
				}
				caption += " (" + Ogre::StringConverter::toString(room->slotCount() - room->freeSlots())
					+ " / " + Ogre::StringConverter::toString(room->slotCount()) + " slots)";

				int id = room->getID();
				std::set<int>::const_iterator iter = openRoomIds.find(id);
				if (iter == openRoomIds.end())
				{
					btn = (CEGUI::PushButton*)mCurrentPage->createChild("OgreTray/Button",
						"room" + Ogre::StringConverter::toString(room->getID()));
					btn->subscribeEvent(CEGUI::PushButton::EventClicked,
						CEGUI::Event::Subscriber(&GameState::onRoomListClicked, this));
					btn->setUserString("id", Ogre::StringConverter::toString(room->getID()));
					mRoomButtons.insert(std::make_pair(room->getID(), btn));
				}
				else
				{
					btn = mRoomButtons[id];
				}
				openRoomIds.erase(id);

				btn->setText(caption);
				btn->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f),
					CEGUI::UDim(((float)i) / 20, 0.0f)));
				btn->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f),
					CEGUI::UDim(0.05f, 0.0f)));

				i++;
			}

			for (int id : openRoomIds)
			{
				mCurrentPage->destroyChild(mRoomButtons[id]);
				mRoomButtons.erase(id);
			}
		}
		else if (mCurrentRoomID >= 0)
		{
			Room* room = mPlayer->getRoom(mCurrentRoomID);
			if (room)
			{
				int freeSlots = room->freeSlots();
				int usedSlots = room->slotCount() - freeSlots;

				mCurrentPage->getChild("FreeSlots")->setText(Ogre::StringConverter::toString(freeSlots));
				mCurrentPage->getChild("UsedSlots")->setText(Ogre::StringConverter::toString(usedSlots));

				if (room->getType() == Room::BROOD_CHAMBER)
				{
					std::vector<bool> tookUpgrades;
					mPlayer->getUpgrades(tookUpgrades);

					if (tookUpgrades[Entity::U_CARRIER])
					{
						mCurrentPage->getChild("CarrierBtn")->show();
					}
					else
					{
						mCurrentPage->getChild("CarrierBtn")->hide();
					}
					if (tookUpgrades[Entity::U_KAMIKAZE])
					{
						mCurrentPage->getChild("KamikazeBtn")->show();
					}
					else
					{
						mCurrentPage->getChild("KamikazeBtn")->hide();
					}

					BroodChamber* bc = static_cast<BroodChamber*>(room);

					Ogre::String productionInfo = "";
					switch (bc->getProduction())
					{
					case Entity::ET_SMALL_WORKER: productionInfo = "Small Worker";  break;
					case Entity::ET_WORKER: productionInfo = "Worker";  break;
					case Entity::ET_CARRIER: productionInfo = "Carrier";  break;
					case Entity::ET_KAMIKAZE: productionInfo = "Kamikaze";  break;
					case Entity::ET_SOLDIER: productionInfo = "Soldier";  break;
					}
					mCurrentPage->getChild("Selection")->setText(productionInfo);


					int floralD, floralR, faunalD, faunalR, copalR, copalD;
					bc->getReserve(floralR, faunalR, copalR);
					bc->getDemand(floralD, faunalD, copalD);
					mCurrentPage->getChild("GCLbl")->setText(Ogre::StringConverter::toString(floralD));
					mCurrentPage->getChild("GPLbl")->setText(Ogre::StringConverter::toString(floralR));
					mCurrentPage->getChild("PCLbl")->setText(Ogre::StringConverter::toString(faunalD));
					mCurrentPage->getChild("PPLbl")->setText(Ogre::StringConverter::toString(faunalR));
					mCurrentPage->getChild("CCLbl")->setText(Ogre::StringConverter::toString(copalD));
					mCurrentPage->getChild("CPLbl")->setText(Ogre::StringConverter::toString(copalR));
				}
				else if (room->getType() == Room::THRONE_CHAMBER)
				{
					ThroneChamber* tc = static_cast<ThroneChamber*>(room);

					Ogre::String workerBehaviour = "Workers are ";
					switch (tc->getWorkerBehaviour())
					{
					case ThroneChamber::EXPAND: workerBehaviour += "expanding"; break;
					case ThroneChamber::DISCOVER: workerBehaviour += "discovering"; break;
					case ThroneChamber::PROTECT: workerBehaviour += "protecting"; break;
					}
					mCurrentPage->getChild("WorkerBehaviour")->setText(workerBehaviour);
				}
			}
			else
			{
				switchInfoPage(mStatsPage);
				mCurrentRoomID = -1;
			}
		}
	}
}

bool GameState::onUpgradeShowButtonClicked(const CEGUI::EventArgs &e)
{
	if (mUpgradesPage != mCurrentPage)
		switchInfoPage(mUpgradesPage);
	else
		switchInfoPage(0);
	mCurrentRoomID = -1;
	return true;
}
bool GameState::onRoomButtonClicked(const CEGUI::EventArgs &e)
{
	if (mRoomsPage != mCurrentPage)
		switchInfoPage(mRoomsPage);
	else
		switchInfoPage(0);
	mCurrentRoomID = -1;
	return true;
}
bool GameState::onStatsButtonClicked(const CEGUI::EventArgs &e)
{
	if (mStatsPage != mCurrentPage)
		switchInfoPage(mStatsPage);
	else
		switchInfoPage(0);
	mCurrentRoomID = -1;
	return true;
}
bool GameState::onBackToRoomListButtonClicked(const CEGUI::EventArgs &e)
{
	switchInfoPage(mRoomsPage);
	mCurrentRoomID = -1;
	return true;
}
bool GameState::onHideInfoButtonClicked(const CEGUI::EventArgs &e)
{
	switchInfoPage(0);
	mCurrentRoomID = -1;
	return true;
}

bool GameState::onUpgradeButtonClicked(const CEGUI::EventArgs &e)
{
	const CEGUI::MouseEventArgs& we = static_cast<const CEGUI::MouseEventArgs&>(e);
	CEGUI::String senderID = we.window->getName();
	if (senderID == "UpgradeHorns")
		takeUpgrade(Entity::U_HORNS);
	else if (senderID == "UpgradeMovement")
		takeUpgrade(Entity::U_MOVEMENT_SPEED);
	else if (senderID == "UpgradeHarvest")
		takeUpgrade(Entity::U_HARVEST_SPEED);
	else if (senderID == "UpgradeAcid")
		takeUpgrade(Entity::U_ACID);
	else if (senderID == "UpgradeCarrier")
		takeUpgrade(Entity::U_CARRIER);
	else if (senderID == "UpgradeAntibiotics")
		takeUpgrade(Entity::U_ANTIBIOTICS);
	else if (senderID == "UpgradeHoneydew")
		takeUpgrade(Entity::U_HONEYDEW);
	else if (senderID == "UpgradeKamikaze")
		takeUpgrade(Entity::U_KAMIKAZE);
	else if (senderID == "UpgradeBulldog")
		takeUpgrade(Entity::U_BULLDOGS);
	else if (senderID == "UpgradeFungi")
		takeUpgrade(Entity::U_FUNGI);
	return true;
}
bool GameState::onPowerWheelEnter(const CEGUI::EventArgs &e)
{
	const CEGUI::MouseEventArgs& we = static_cast<const CEGUI::MouseEventArgs&>(e);
	CEGUI::String senderID = we.window->getName();
	if (senderID == "SelectTool") mCurrentUserTool = NONE;
	else if (senderID == "DigTool") mCurrentUserTool = DIG;
	else if (senderID == "DefendTool") mCurrentUserTool = DEFEND;
	else if (senderID == "AttackTool") mCurrentUserTool = HOLD;
	else if (senderID == "StorageTool")mCurrentUserTool = ASSIGN_STORAGE_CHAMBER;
	else if (senderID == "BroodTool") mCurrentUserTool = ASSIGN_BROOD_CHAMBER;
	else if (senderID == "FungiTool") mCurrentUserTool = ASSIGN_FUNGI_CHAMBER;
	return true;
}
void GameState::showRoom(int roomID)
{
	Room* room = mPlayer->getRoom(roomID);

	if (room)
	{
		if (mRoomsPage != mCurrentPage)
			switchInfoPage(mRoomsPage);

		mCurrentRoomID = room->getID();
		mInfoTab->removeChild(mCurrentPage);
		switch (room->getType())
		{
		case Room::THRONE_CHAMBER:
			mCurrentPage = mThronePage;
			break;
		case Room::STORAGE_CHAMBER:
			mCurrentPage = mStoragePage;
			break;
		case Room::BROOD_CHAMBER:
			mCurrentPage = mBroodPage;
			break;
		case Room::FUNGI_CHAMBER:
			mCurrentPage = mFungiPage;
			break;
		case Room::FLORAL_SOURCE:
			mCurrentPage = mFloralPage;
			break;
		case Room::COPAL_SOURCE:
			mCurrentPage = mCopalPage;
			break;
		case Room::EXIT_ROOM:
			mCurrentPage = mExitPage;
			break;
		}
		mInfoTab->addChild(mCurrentPage);
	}
}
bool GameState::onRoomListClicked(const CEGUI::EventArgs &e)
{
	const CEGUI::MouseEventArgs& we = static_cast<const CEGUI::MouseEventArgs&>(e);
	int roomID = Ogre::StringConverter::parseInt(Ogre::String(we.window->getUserString("id").c_str()));

	showRoom(roomID);

	mNextGuiUpdate = -1.f;
	return true;
}
bool GameState::onUnitProductionClicked(const CEGUI::EventArgs &e)
{
	const CEGUI::MouseEventArgs& we = static_cast<const CEGUI::MouseEventArgs&>(e);
	CEGUI::String senderID = we.window->getName();

	if (mCurrentRoomID < 0)
		return true;
	Room* room = mPlayer->getRoom(mCurrentRoomID);
	if (!room)
		return true;

	BroodChamber* bc = static_cast<BroodChamber*>(room);

	if (senderID == "SmallWorkerBtn") bc->setProduction(Entity::ET_SMALL_WORKER);
	else if (senderID == "WorkerBtn") bc->setProduction(Entity::ET_WORKER);
	else if (senderID == "CarrierBtn") bc->setProduction(Entity::ET_CARRIER);
	else if (senderID == "SoldierBtn") bc->setProduction(Entity::ET_SOLDIER);
	else if (senderID == "KamikazeBtn") bc->setProduction(Entity::ET_KAMIKAZE);

	mNextGuiUpdate = -1.f;
	return true;
}
bool GameState::onWorkerBehaviourClicked(const CEGUI::EventArgs &e)
{
	const CEGUI::MouseEventArgs& we = static_cast<const CEGUI::MouseEventArgs&>(e);
	CEGUI::String senderID = we.window->getName();

	if (mCurrentRoomID < 0)
		return true;
	Room* room = mPlayer->getRoom(mCurrentRoomID);
	if (!room)
		return true;

	ThroneChamber* bc = static_cast<ThroneChamber*>(room);

	if (senderID == "ExpandBtn") bc->setWorkerBehaviour(ThroneChamber::EXPAND);
	else if (senderID == "DiscoverBtn") bc->setWorkerBehaviour(ThroneChamber::DISCOVER);
	else if (senderID == "ProtectBtn") bc->setWorkerBehaviour(ThroneChamber::PROTECT);

	mNextGuiUpdate = -1.f;
	return true;
}


void GameState::takeUpgrade(Entity::Upgrade u)
{
	std::vector<bool> tookUpgrades;
	mPlayer->getUpgrades(tookUpgrades);
	if (!tookUpgrades[u])
		mPlayer->addUpgrade(u);
}

bool GameState::pickTile(const OIS::MouseEvent &evt, Ogre::Vector2& position)
{
	Ogre::Real x = real(Framework::getSingletonPtr()->mMouse->getMouseState().X.abs);
	Ogre::Real y = real(Framework::getSingletonPtr()->mMouse->getMouseState().Y.abs);
	Ogre::Vector2 mousePos(x, y);
    Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(x / real(evt.state.width), 
		y / real(evt.state.height));

	Ogre::Plane xPlane(Ogre::Vector3::UNIT_Z, 1.1f);
	std::pair<bool, Ogre::Real> point = mouseRay.intersects(xPlane);
	if (point.first)
    {
        Ogre::Vector3 pos = mouseRay.getPoint(point.second);
		pos.x += 1;
		pos.y += 1;
		position = Ogre::Vector2(pos.x, pos.y);
		mLastPickedPos = position;
		return true;
    }
	return false;
}
bool GameState::pickEntity(const OIS::MouseEvent &evt, Entity*& ent)
{
	Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(
		Framework::getSingletonPtr()->mMouse->getMouseState().X.abs / float(evt.state.width),
        Framework::getSingletonPtr()->mMouse->getMouseState().Y.abs / float(evt.state.height));
    mRSQ->setRay(mouseRay);
    mRSQ->setSortByDistance(true);

    Ogre::RaySceneQueryResult &result = mRSQ->execute();

	Ogre::SceneNode* tmp = 0;
    for(Ogre::RaySceneQueryResultEntry entry : result)
    {
        if(entry.movable)
        {
			Ogre::Entity* e;
			try
			{
				e = mSceneMgr->getEntity(entry.movable->getName());
			}
			catch(Ogre::Exception)
			{
				continue;
			}

            tmp = e->getParentSceneNode();			
			Ogre::Any any = tmp->getUserObjectBindings().getUserAny();
			if(any.isEmpty())
				continue;

			tmp->showBoundingBox(true);
			
			if(mSceneMgr->hasSceneNode(mCurrentNodeName))
				mSceneMgr->getSceneNode(mCurrentNodeName)->showBoundingBox(false);
			mCurrentNodeName = tmp->getName();
			ent = Ogre::any_cast<Entity*>(any);
			if(ent)
			{
				return true;
			}
        }
    }
	return false;
}


void GameState::processInput()
{
	OIS::Keyboard* k = Framework::getSingletonPtr()->mKeyboard;

	if (k->isKeyDown(OIS::KC_LEFT)) mTranslateVector.x = -mMoveScale;
	else if (k->isKeyDown(OIS::KC_RIGHT)) mTranslateVector.x = mMoveScale;
	if (k->isKeyDown(OIS::KC_UP)) mTranslateVector.y = mMoveScale;
	else if (k->isKeyDown(OIS::KC_DOWN)) mTranslateVector.y = -mMoveScale;

	if (k->isKeyDown(OIS::KC_LCONTROL) || k->isKeyDown(OIS::KC_RCONTROL))
		mCtrlDown = true;
	else mCtrlDown = false;
	if (k->isKeyDown(OIS::KC_RSHIFT) || k->isKeyDown(OIS::KC_LSHIFT))
		mShiftDown = true;
	else mShiftDown = false;
}
void GameState::updateCamera(double dt)
{
	float MousePX = mLastMousePos.x / Framework::getSingletonPtr()->mViewport->getActualWidth();
	float MousePY = mLastMousePos.y / Framework::getSingletonPtr()->mViewport->getActualHeight();

	// edge scrolling
	if (!mDragCam)
	{
		if (MousePX < 0.01) mTranslateVector.x = -mMoveScale;
		else if (MousePX > 0.99) mTranslateVector.x = mMoveScale;
		if (MousePY < 0.01) mTranslateVector.y = mMoveScale;
		else if (MousePY > 0.99) mTranslateVector.y = -mMoveScale;
	}

	float zoomDelta = mZoomFriction*dt;
	if (fabsf(mZoomSpeed) < fabsf(zoomDelta))
		mZoomSpeed = 0.0f;
	else mZoomSpeed -= zoomDelta * (mZoomSpeed>0 ? 1 : -1);

	mTranslateVector.z += mZoomSpeed*dt;
	mCameraTargetPos += mTranslateVector / 5;
	mCameraTargetPos.z = std::min(200.f, std::max(10.f, mCameraTargetPos.z));

	Ogre::Vector3 cp = mCameraNode->getPosition();
	cp = 0.8*cp + 0.2*mCameraTargetPos;
	mCameraNode->setPosition(cp);
	
	mTranslateVector = Ogre::Vector3::ZERO;
}

void GameState::updateNotifications(float dT)
{
	CEGUI::Window* rootWindow = CEGUI::System::getSingleton().getDefaultGUIContext().getRootWindow();

	std::vector<DisplayMessage*> messages;
	mPlayer->getDisplayableMessages(messages);

	std::vector<DisplayMessage*>::iterator iter, iend;
	iter = messages.begin(); iend = messages.end();
	for (int i = 0; i < 10, iter != iend; ++i)
	{
		if (!mNotifications[i])
		{
			DisplayMessage* m = *iter;
			CEGUI::PushButton* b = (CEGUI::PushButton*)rootWindow->createChild("OgreTray/Button");
			b->setText(m->getTitle());
			b->setPosition(CEGUI::UVector2(CEGUI::UDim(0.005f, 0.0f),
				CEGUI::UDim(0.13 + ((float)i) / 20, 0.0f)));
			b->setSize(CEGUI::USize(CEGUI::UDim(0.18f, 0.0f),
				CEGUI::UDim(0.05f, 0.0f)));

			Notification* n = new Notification;
			n->b = b;
			n->m = m;
			mNotifications[i] = n;
			++iter;
		}
	}
	for (; iter != iend; ++iter)
		delete0(*iter);

	// display messages
	std::vector<int> holes;
	for (int i = 0; i < 10; ++i)
	{
		if (mNotifications[i])
		{
			Notification* n = mNotifications[i];
			n->m->update(dT);
			if (!n->m->isAlive())
			{
				rootWindow->destroyChild(n->b);
				delete0(n->m);
				delete0(n);
				mNotifications[i] = 0;
				holes.push_back(i);
			}
		}
		else
			holes.push_back(i);
	}
	for (int i = 0; i < 10; ++i)
	{
		if (holes.empty()) break;

		if (i > holes[0] && mNotifications[i])
		{
			// move notification to hole
			mNotifications[i]->b->setPosition(CEGUI::UVector2(CEGUI::UDim(0.005f, 0.0f),
				CEGUI::UDim(0.13 + ((float)holes[0]) / 20, 0.0f)));
			mNotifications[holes.front()] = mNotifications[i];
			mNotifications[i] = 0;
			holes.push_back(i);
			holes.erase(holes.begin());
			std::sort(holes.begin(), holes.end());
		}
	}

}
void GameState::update(float dT)
{
	// check if app is quiting
    if(mQuit == true)
    {
        popAppState();
        return;
    }

    
	// get view rect of world
	Ogre::Vector3 pointLT, pointRB;
	{
		Ogre::Plane x(Ogre::Vector3::UNIT_Z, 1.1f);
		std::pair<bool, Ogre::Real> point;
		Ogre::Ray rayLT = mCamera->getCameraToViewportRay(0, 0);
		Ogre::Ray rayRB = mCamera->getCameraToViewportRay(1, 1);

		point = rayLT.intersects(x);
		pointLT = rayLT.getPoint(point.second);

		point = rayRB.intersects(x);
		pointRB = rayRB.getPoint(point.second);
	}


	// gui updates
	mNextGuiUpdate -= dT;
	if(mNextGuiUpdate < 0)
	{
		mNextGuiUpdate = 0.5f;
		updateStatsPanel();
		updateInfoLabel();
	}
	
	// input handling
	if(mMouseHoldActionActive)
	{
		mNextMouseHoldAction -= dT;
		if(mNextMouseHoldAction < 0)
		{
			if(mCtrlDown)
			{
				switch(mCurrentUserTool)
				{
				case HOLD: mPlayer->shrink(Job::HOLD_JOB, mLastPickedPos); break;
				case DEFEND: mPlayer->shrink(Job::DEFEND_JOB, mLastPickedPos); break;
				}
			}
			else
			{
				switch(mCurrentUserTool)
				{
				case HOLD: mPlayer->grow(Job::HOLD_JOB, mLastPickedPos); break;
				case DEFEND: mPlayer->grow(Job::DEFEND_JOB, mLastPickedPos); break;
				}
			}
			mNextMouseHoldAction = 0.06f;
		}
	}

	processInput();


	// server updates
	mServer->update(dT);
	
	if (mMobs)
	{
		// non player updates
		mMobs->receiveUpdates();
		mMobs->processUpdates();
		mMobs->update(dT);
		mMobs->sendUpdates();

	}

	// player updates
	mPlayer->receiveUpdates();
	mPlayer->updateAchievements();
	updateNotifications(dT);
	mPlayer->processUpdates();

	mPlayer->setViewRect(Ogre::Rect(pointLT.x, pointLT.y, pointRB.x, pointRB.y));
	mPlayer->update(dT);
	mPlayer->sendUpdates();

	// update camera
	updateCamera(dT);
	mMoveScale = mMoveSpeed   * dT;
	mRotScale = mRotateSpeed * dT;

	// check if we lost
	Statistics* stats = mPlayer->getStatistics();
	if (stats->getUnitCount() == 0)
	{
		stats->GameWon = 0;
		stats->GameCancelled = 0;
		popAllAndPushAppState(findByName("MenuState"));
	}
}


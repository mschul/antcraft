#include "AnPCH.h"
#include "AnCopal.h"
using namespace Ants;

Ogre::String Copal::msStandardMesh = "";
Ogre::String Copal::msMaterialName = "";

Copal::Copal(int id, int clientID,
	const Ogre::Vector2& position,
	const Ogre::Vector2& tilePosition,
	int visibilityRadius)
	: Entity(id, clientID, position, tilePosition, visibilityRadius)
{
}
Copal::Copal(const EntityDesc& desc)
: Entity(desc)
{
}
Copal::~Copal()
{
}
Entity::EntityType Copal::getType() const
{
	return Entity::ET_COPAL;
}
bool Copal::isResource() const
{
	return true;
}

Ogre::String Copal::getMaterialName() const
{
	return Copal::msMaterialName;
}
Ogre::String Copal::getStandardMesh() const
{
	return Copal::msStandardMesh;
}

void Copal::update(float dt, ActionReactor* reactor)
{
}

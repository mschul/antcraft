#include "AnPCH.h"
#include "AnDefendJob.h"
using namespace Ants;

DefendJob::DefendJob (int id, const Ogre::Vector2& position, double duration)
	: Job(id, 1, position)
{
	mDuration = duration;
	mT = duration;
}
DefendJob::~DefendJob ()
{
}
		
bool DefendJob::update(double dt, ActionReactor* reactor)
{
	mT -= dt;
	if(mT <= 0)
	{
		mT = mDuration;
		return true;
	}
	return false;
}
Job::JobType DefendJob::getType() const
{
	return Job::DEFEND_JOB;
}
void DefendJob::getSubJobs(std::vector<Job::SubJobs>& jobs) const
{
	jobs.push_back(Job::UPDATE);
	jobs.push_back(Job::CHECK);
}
#include "AnPCH.h"
#include "AnFowMaskMap.h"
using namespace Ants;

FowMaskMap::FowMaskMap()
{
	mNodes = 0;
	mWidth = 0;
	mHeight = 0;
	mMap = 0;
}
FowMaskMap::~FowMaskMap()
{
	SafeDelete1(mNodes);
	SafeDelete1(mMap);
}

void FowMaskMap::init(unsigned int width, unsigned int height, Ogre::SceneNode* rootNode,
	Ogre::SceneManager* sceneMgr)
{
	mWidth = width;
	mHeight = height;
	mRootNode = rootNode;
	mSceneMgr = sceneMgr;

	mNodes = new1<Ogre::SceneNode*>(width*height);
	memset(&mNodes[0], 0, sizeof(Ogre::SceneNode*)*width*height);

	mMap = new0 VisibilityMap();
	mMap->init(width, height);
}
void FowMaskMap::init(const Ogre::Rect& area, Ogre::SceneNode* rootNode,
	Ogre::SceneManager* sceneMgr)
{
	init(area.width(), area.height(), rootNode, sceneMgr);
}
void FowMaskMap::update(Entity* ent)
{
	typedef std::map<Entity*, int>::iterator mapiter;
	mapiter iter = mEntityMap.find(ent);

	Ogre::Vector2 tilePos = ent->getTilePosition();
	const unsigned int tile = mWidth*tilePos.y + tilePos.x;
	const unsigned int radius = ent->getVisibilityRadius();


	if (iter == mEntityMap.end())
	{
		mEntityMap.insert(std::make_pair(ent, tile));
		mMap->increment(ent);
		std::vector<Ogre::Vector2> newCircle;
		{
			Ogre::Vector2 border = tilePos;
			border += Tile::getNeighbours(tilePos)[Tile::LEFT] * radius;
			Tile::hexCircle(tilePos, border, newCircle);
		}
		for (int i = 0; i < newCircle.size(); ++i)
		{
			Ogre::Vector2 p = newCircle[i];
			const unsigned int t = mWidth*p.y + p.x;
			if (!mNodes[t] && mMap->isVisible(p))
			{
				Ogre::String name = "VisibiltyMask_";
				name += Ogre::StringConverter::toString(t);
				Ogre::Vector2 hex;
				Tile::toHexagonalPosition(p, hex);

				Ogre::SceneNode* maskNode = mRootNode->createChildSceneNode();
				Ogre::Entity* vEntity = mSceneMgr->createEntity(name, "hexagonMesh");
				vEntity->setMaterialName("visibiltyMask");
				vEntity->setVisibilityFlags(FOWMASK_VISIBILITY_FLAG);
				maskNode->attachObject(vEntity);
				maskNode->setPosition(hex.x * 2, hex.y * 2, 1.0f);
				mNodes[t] = maskNode;
			}
		}
	}
	else
	{
		if (iter->second != tile)
		{
			Ogre::Vector2 oldPos(iter->second % mWidth, iter->second / mWidth);
			iter->second = tile;

			mMap->decrement(oldPos, radius);
			mMap->increment(tilePos, radius);


			std::vector<Ogre::Vector2> oldCircle;
			{
				Ogre::Vector2 border = oldPos;
				border += Tile::getNeighbours(oldPos)[Tile::LEFT] * radius;
				Tile::hexCircle(oldPos, border, oldCircle);
			}
			for (int i = 0; i < oldCircle.size(); ++i)
			{
				Ogre::Vector2 p = oldCircle[i];
				const unsigned int t = mWidth*p.y + p.x;

				if (mNodes[t] && !mMap->isVisible(p))
				{
					Ogre::SceneNode* n = mNodes[t];
					if (!n)
						continue;

					destroyAllAttachedMovableObjects(n);
					mNodes[t]->removeAndDestroyAllChildren();
					mSceneMgr->destroySceneNode(mNodes[t]);
					mNodes[t] = 0;
				}
			}
			std::vector<Ogre::Vector2> newCircle;
			{
				Ogre::Vector2 border = tilePos;
				border += Tile::getNeighbours(tilePos)[Tile::LEFT] * radius;
				Tile::hexCircle(tilePos, border, newCircle);
			}
			for (int i = 0; i < newCircle.size(); ++i)
			{
				Ogre::Vector2 p = newCircle[i];
				const unsigned int t = mWidth*p.y + p.x;
				if (!mNodes[t] && mMap->isVisible(p))
				{
					Ogre::String name = "VisibiltyMask_";
					name += Ogre::StringConverter::toString(t);
					Ogre::Vector2 hex;
					Tile::toHexagonalPosition(p, hex);

					Ogre::SceneNode* maskNode = mRootNode->createChildSceneNode();
					Ogre::Entity* vEntity = mSceneMgr->createEntity(name, "hexagonMesh");
					vEntity->setMaterialName("visibiltyMask");
					vEntity->setVisibilityFlags(FOWMASK_VISIBILITY_FLAG);
					maskNode->attachObject(vEntity);
					maskNode->setPosition(hex.x * 2, hex.y * 2, 1.0f);
					mNodes[t] = maskNode;
				}
			}
		}

	}
}
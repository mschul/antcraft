#include "AnPCH.h"
#include "AnStatistics.h"
#include "AnEntity.h"
using namespace Ants;

Statistics::Statistics()
: GameObject()
{
	Name = "NoName";

	// P, A, K, D
	SmallWorker = { 0, 0, 0 };
	Worker = { 0, 0, 0 };
	Carrier = { 0, 0, 0 };
	Soldier = { 0, 0, 0 };
	Kamikaze = { 0, 0, 0 };
	Ladybug = { 0, 0, 0 };
	Spider = { 0, 0, 0 };
	BombardierBeetle = { 0, 0, 0 };

	// C, A, S, SU
	Storage = { 0, 0, 0, 0 };
	Brood = { 0, 0, 0, 0 };
	Fungi = { 0, 0, 0, 0 };

	AnthillArea = 0;
	TimePassed = 0;

	GameWon = 0;
	GameCancelled = 0;

	FaunalStored = 0;
	FloralStored = 0;
	CopalStored = 0;
	AStorageFull = 0;
}
Statistics::Statistics(ObjectID id)
	: GameObject(id)
{
	Name = "NoName";

	// P, A, K, D
	SmallWorker = { 0, 0, 0 };
	Worker = { 0, 0, 0 };
	Carrier = { 0, 0, 0 };
	Soldier = { 0, 0, 0 };
	Kamikaze = { 0, 0, 0 };
	Ladybug = { 0, 0, 0 };
	Spider = { 0, 0, 0 };
	BombardierBeetle = { 0, 0, 0 };

	// C, A, S, SU
	Storage = { 0, 0, 0, 0 };
	Brood = { 0, 0, 0, 0 };
	Fungi = { 0, 0, 0, 0 };

	AnthillArea = 0;
	TimePassed = 0;

	GameWon = 0;
	GameCancelled = 0;

	FaunalStored = 0;
	FloralStored = 0;
	CopalStored = 0;
	AStorageFull = 0;
}
Statistics::~Statistics()
{

}

void Statistics::set(const Statistics& other)
{
	Name = other.Name;

	SmallWorker = other.SmallWorker;
	Worker = other.Worker;
	Carrier = other.Carrier;
	Soldier = other.Soldier;
	Kamikaze = other.Kamikaze;
	Ladybug = other.Ladybug;
	Spider = other.Spider;
	BombardierBeetle = other.BombardierBeetle;

	Storage = other.Storage;
	Brood = other.Brood;
	Fungi = other.Fungi;

	AnthillArea = other.AnthillArea;
	TimePassed = other.TimePassed;

	GameWon = other.GameWon;
	GameCancelled = other.GameCancelled;

	FaunalStored = 0;
	FloralStored = 0;
	CopalStored = 0;
	AStorageFull = 0;
}
void Statistics::add(const Statistics& other)
{
	SmallWorker.Alive += other.SmallWorker.Alive;
	SmallWorker.Killed += other.SmallWorker.Killed;
	SmallWorker.Produced += other.SmallWorker.Produced;
	Worker.Alive += other.Worker.Alive;
	Worker.Killed += other.Worker.Killed;
	Worker.Produced += other.Worker.Produced;
	Carrier.Alive += other.Carrier.Alive;
	Carrier.Killed += other.Carrier.Killed;
	Carrier.Produced += other.Carrier.Produced;
	Soldier.Alive += other.Soldier.Alive;
	Soldier.Killed += other.Soldier.Killed;
	Soldier.Produced += other.Soldier.Produced;
	Kamikaze.Alive += other.Kamikaze.Alive;
	Kamikaze.Killed += other.Kamikaze.Killed;
	Kamikaze.Produced += other.Kamikaze.Produced;
	Ladybug.Alive += other.Ladybug.Alive;
	Ladybug.Killed += other.Ladybug.Killed;
	Ladybug.Produced += other.Ladybug.Produced;
	Spider.Alive += other.Spider.Alive;
	Spider.Killed += other.Spider.Killed;
	Spider.Produced += other.Spider.Produced;
	BombardierBeetle.Alive += other.BombardierBeetle.Alive;
	BombardierBeetle.Killed += other.BombardierBeetle.Killed;
	BombardierBeetle.Produced += other.BombardierBeetle.Produced;

	Storage.Area += other.Storage.Area;
	Storage.Count += other.Storage.Count;
	Storage.Slots += other.Storage.Slots;
	Storage.SlotsFree += other.Storage.SlotsFree;
	Brood.Area += other.Brood.Area;
	Brood.Count += other.Brood.Count;
	Brood.Slots += other.Brood.Slots;
	Brood.SlotsFree += other.Brood.SlotsFree;
	Fungi.Area += other.Fungi.Area;
	Fungi.Count += other.Fungi.Count;
	Fungi.Slots += other.Fungi.Slots;
	Fungi.SlotsFree += other.Fungi.SlotsFree;

	AnthillArea += other.AnthillArea;
	TimePassed += other.TimePassed;

	GameWon += other.GameWon;
	GameCancelled += other.GameCancelled;

	FaunalStored += other.FaunalStored;
	FloralStored += other.FloralStored;
	CopalStored += other.CopalStored;
	AStorageFull += other.AStorageFull;
}

void Statistics::getTimePassed(int& hours, int& mins, int& seconds)
{
	double time = TimePassed;
	hours = (int)time / 3600;
	time -= hours * 3600;
	mins = (int)time / 60;
	time -= mins * 60;
	seconds = (int)time;
}
Ogre::String Statistics::getTimePassed()
{
	int hours, mins, seconds;
	getTimePassed(hours, mins, seconds);
	return Ogre::StringConverter::toString(hours) + ":"
		+ Ogre::StringConverter::toString(mins) + ":"
		+ Ogre::StringConverter::toString(seconds);
}

void Statistics::killed(int type)
{
	switch ((Entity::EntityType) type)
	{
	case Entity::ET_SMALL_WORKER:
		SmallWorker.Killed++;
		break;
	case Entity::ET_WORKER:
		Worker.Killed++;
		break;
	case Entity::ET_CARRIER:
		Carrier.Killed++;
		break;
	case Entity::ET_SOLDIER:
		Soldier.Killed++;
		break;
	case Entity::ET_KAMIKAZE:
		Kamikaze.Killed++;
		break;
	case Entity::ET_BOMBARDIER_BEETLE:
		BombardierBeetle.Killed++;
		break;
	case Entity::ET_LADYBUG:
		Ladybug.Killed++;
		break;
	case Entity::ET_SPIDER:
		Spider.Killed++;
		break;
	}
}

int Statistics::getRoomCount() const
{
	return Storage.Count + Brood.Count + Fungi.Count;
}
int Statistics::getRoomArea() const
{
	return Storage.Area + Brood.Area + Fungi.Area;
}
int Statistics::getUnitsKilled() const
{
	return SmallWorker.Killed + Worker.Killed + Carrier.Killed + Soldier.Killed + Kamikaze.Killed
		+ Ladybug.Killed + BombardierBeetle.Killed + Spider.Killed;
}
int Statistics::getUnitsProduced() const
{
	return SmallWorker.Produced + Worker.Produced + Carrier.Produced + Soldier.Produced + Kamikaze.Produced
		+ Ladybug.Produced + BombardierBeetle.Produced + Spider.Produced;
}
int Statistics::getUnitCount() const
{
	return SmallWorker.Alive + Worker.Alive + Carrier.Alive + Soldier.Alive + Kamikaze.Alive
		+ Ladybug.Alive + BombardierBeetle.Alive + Spider.Alive;
}

int Statistics::getScore() const
{
	int unitsProduced = getUnitsProduced();
	int roomArea = getRoomArea();
	int killed = getUnitsKilled();
	return 10 * unitsProduced + 10 * killed + 2 * roomArea + AnthillArea + integer(TimePassed * 0.1f)
		+ (GameWon * 1000) + (GameCancelled * -10);
}

//--------------------------------------------------------
const Ogre::String Statistics::TYPE("Statistics");
const Ogre::String& Statistics::getClassType() const
{
	return TYPE;
}

bool Statistics::msStreamRegistered = false;
bool Statistics::registerFactory()
{
	if (!msStreamRegistered)
	{
		InitTerm::addInitializer(Statistics::initializeFactory);
		msStreamRegistered = true;
	}
	return msStreamRegistered;
}

GameObject* Statistics::Factory(Deserializer& deserializer)
{
	Statistics* object = new0 Statistics();
	object->deserialize(deserializer);
	return object;
}
void Statistics::initializeFactory()
{
	if (!msFactories)
	{
		msFactories = new0 GameObject::FactoryMap();
	}
	msFactories->insert(std::make_pair(TYPE, Factory));
}

int Statistics::getStreamingSize() const
{
	int size = GameObject::getStreamingSize();

	size += stringSize(Name);
	size += sizeof(SmallWorker);
	size += sizeof(Worker);
	size += sizeof(Carrier);
	size += sizeof(Soldier);
	size += sizeof(Kamikaze);
	size += sizeof(Ladybug);
	size += sizeof(Spider);
	size += sizeof(BombardierBeetle);
	size += sizeof(Storage);
	size += sizeof(Brood);
	size += sizeof(Fungi);
	size += sizeof(AnthillArea);
	size += sizeof(TimePassed);
	size += sizeof(GameWon);
	size += sizeof(GameCancelled);

	return size;
}

void Statistics::serialize(Serializer& serializer) const
{
	GameObject::serialize(serializer);

	serializer.writeString(Name);
	serializer.write(SmallWorker);
	serializer.write(Worker);
	serializer.write(Carrier);
	serializer.write(Soldier);
	serializer.write(Kamikaze);
	serializer.write(Ladybug);
	serializer.write(Spider);
	serializer.write(BombardierBeetle);
	serializer.write(Storage);
	serializer.write(Brood);
	serializer.write(Fungi);
	serializer.write(AnthillArea);
	serializer.write(TimePassed);
	serializer.write(GameWon);
	serializer.write(GameCancelled);
}
void Statistics::deserialize(Deserializer& deserializer)
{
	GameObject::deserialize(deserializer);

	deserializer.readString(Name);
	deserializer.read(SmallWorker);
	deserializer.read(Worker);
	deserializer.read(Carrier);
	deserializer.read(Soldier);
	deserializer.read(Kamikaze);
	deserializer.read(Ladybug);
	deserializer.read(Spider);
	deserializer.read(BombardierBeetle);
	deserializer.read(Storage);
	deserializer.read(Brood);
	deserializer.read(Fungi);
	deserializer.read(AnthillArea);
	deserializer.read(TimePassed);
	deserializer.read(GameWon);
	deserializer.read(GameCancelled);
}
void Statistics::resolvePointers(Deserializer& deserializer)
{
	GameObject::resolvePointers(deserializer);
}
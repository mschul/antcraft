#include "AnPCH.h"
#include "AnIO.h"
using namespace Ants;

IO::IO()
{
	reset();
}
IO::~IO()
{
	reset();
}

void IO::open(int size, char* buffer)
{
	mBuffer = buffer;
	mTotalBytes = size;
	mProcessedBytes = 0;
}
void IO::close()
{
	reset();
}

bool IO::done() const
{
	return mProcessedBytes >= mTotalBytes;
}

bool IO::read(size_t size, void* datum)
{
	int nextBytes = mProcessedBytes + integer(size);
	if (nextBytes <= mTotalBytes)
	{
		char* src = mBuffer + mProcessedBytes;
		mProcessedBytes = nextBytes;
		memcpy(datum, src, size);
		return true;
	}
	mProcessedBytes = mTotalBytes;
	return false;
}
bool IO::read(size_t size, int items, void* datum)
{
	int totalSize = integer(size)*items;
	int nextBytes = mProcessedBytes + totalSize;
	if (nextBytes <= mTotalBytes)
	{
		char* src = mBuffer + mProcessedBytes;
		mProcessedBytes = nextBytes;
		memcpy(datum, src, totalSize);
		return true;
	}
	mProcessedBytes = mTotalBytes;
	return false;
}
bool IO::write(size_t size, const void* datum)
{
	int nextBytes = mProcessedBytes + integer(size);
	if (nextBytes <= mTotalBytes)
	{
		char* dst = mBuffer + mProcessedBytes;
		mProcessedBytes = nextBytes;
		memcpy(dst, datum, size);
		return true;
	}
	mProcessedBytes = mTotalBytes;
	return false;
}
bool IO::write(size_t size, int items, const void* datum)
{
	int totalSize = integer(size)*items;
	int nextBytes = mProcessedBytes + totalSize;
	if (nextBytes <= mTotalBytes)
	{
		char* dst = mBuffer + mProcessedBytes;
		mProcessedBytes = nextBytes;
		memcpy(dst, datum, totalSize);
		return true;
	}
	mProcessedBytes = mTotalBytes;
	return false;
}

const char* IO::current() const
{
	return mBuffer + mProcessedBytes;
}
bool IO::advance(int bytes)
{
	int nextBytes = mProcessedBytes + bytes;
	if (nextBytes <= mTotalBytes)
	{
		mProcessedBytes = nextBytes;
		return true;
	}
	mProcessedBytes = mTotalBytes;
	return false;
}

void IO::reset()
{
	mBuffer = 0;
	mTotalBytes = 0;
	mProcessedBytes = 0;
}

bool IO::load(const Ogre::String& filename, bool binary, int& size, char*& buffer)
{
	struct stat statistics;
	if (stat(filename.c_str(), &statistics) != 0)
	{
		buffer = 0;
		size = 0;
		return false;
	}

	FILE* file;
	fopen_s(&file, filename.c_str(), binary ? "rb" : "rt");

	if (!file)
	{
		buffer = 0;
		size = 0;
		return false;
	}

	size = statistics.st_size;
	buffer = new1<char>(size);
	int read = integer(fread_s(buffer, size*sizeof(char), sizeof(char), size, file));
	if (fclose(file) != 0 || read != size)
	{
		delete1(buffer);
		buffer = 0;
		size = 0;
		return false;
	}
	return true;
}
bool IO::save(const Ogre::String& filename, bool binary, int size, const char* buffer)
{
	if (!buffer || size <= 0)
		return false;

	FILE* file;
	fopen_s(&file, filename.c_str(), binary ? "wb" : "wt");

	if (!file)
		return false;

	int written = integer(fwrite(buffer, sizeof(char), size, file));
	if (fclose(file) != 0 || written != size)
		return false;
	return true;
}
bool IO::append(const Ogre::String& filename, bool binary, int size, const char* buffer)
{
	if (!buffer || size <= 0)
		return false;

	FILE* file;
	fopen_s(&file, filename.c_str(), binary ? "ab" : "at");

	if (!file)
		return false;

	int written = integer(fwrite(buffer, sizeof(char), size, file));
	if (fclose(file) != 0 || written != size)
		return false;
	return true;
}
#include "AnPCH.h"
#include "AnGameSettings.h"
using namespace Ants;

GameSettings::GameSettings()
{
	mWater = 2;
	mCaves = 2;
	mRock = 4;
	mMobs = 4;
	mGrass = 3;
	mFOW = false;
	mRevealMap = false;
	mUpgrades = false;
}

void GameSettings::setRockAmount(int rock)
{
	mRock = rock;
}
int GameSettings::getRockAmount() const
{
	return mRock;
}

void GameSettings::setWaterAmount(int water)
{
	mWater = water;
}
int GameSettings::getWaterAmount() const
{
	return mWater;
}

void GameSettings::setCaveAmount(int caves)
{
	mCaves = caves;
}
int GameSettings::getCaveAmount() const
{
	return mCaves;
}

void GameSettings::setMobAmount(int mobs)
{
	mMobs = mobs;
}
int GameSettings::getMobAmount() const
{
	return mMobs;
}

void GameSettings::setGrassAmount(int grass)
{
	mGrass = grass;
}
int GameSettings::getGrassAmount() const
{
	return mGrass;
}

void GameSettings::setFOW(bool fow)
{
	mFOW = fow;
}
bool GameSettings::getFOW() const
{
	return mFOW;
}

void GameSettings::setRevealMap(bool reveal)
{
	mRevealMap = reveal;
}
bool GameSettings::getRevealMap() const
{
	return mRevealMap;
}

void GameSettings::setUpgrades(bool upgrades)
{
	mUpgrades = upgrades;
}
bool GameSettings::getUpgrades() const
{
	return mUpgrades;
}
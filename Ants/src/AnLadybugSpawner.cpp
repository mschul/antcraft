#include "AnPCH.h"
#include "AnLadybugSpawner.h"
#include "AnEntity.h"
using namespace Ants;

LadybugSpawner::LadybugSpawner (int id)
	: MobSpawner(id)
{
	mNextSpawnDelta = 100;
	mInitialSpawnDelta = 100;
}
LadybugSpawner::~LadybugSpawner ()
{
}
		
Room::RoomType LadybugSpawner::getType() const
{
	return Room::LADYBUG_SPAWNER;
}

void LadybugSpawner::build(const std::vector<Ogre::Vector2>& positions, 
	const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
	std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	mPivot.clear();
	mPivot.push_back(pivot);
	tiles.push_back(std::make_pair(pivot, Tile::msTiles[Tile::NEST]));
}
Entity* LadybugSpawner::spawn(ActionReactor* reactor)
{
	return reactor->spawn(*mPivot.begin(), Entity::ET_LADYBUG);
}
#include "AnPCH.h"
#include "AnJobQuery.h"
using namespace Ants;

JobQuery::JobQuery (const WorldMap* world, JobMap* jobs)
{
	mWorld = world;
	mJobs = jobs;
}
JobQuery::~JobQuery ()
{
}
		
void JobQuery::findNextJob(const Ogre::Vector2& start, Job*& job,  int filter, int depth) const
{
	job = 0;
	if(!mJobs->jobsAvailable())
		return;

	
	int d = depth;
	if(depth < 0)
		d = std::numeric_limits<int>::max();

	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::set<int> marks;
	marks.insert(((int)start.y)*width + start.x);

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(start);

	while(!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		if(exploreTile(u, job, filter))
			break;

		Tile* current = mWorld->getTile(u);
		// don't explore neighbours of blocking tile (obviously)
		if(!current->isBlocking())
		{
			// explore neighbours
			for(Ogre::Vector2 d : Tile::getNeighbours(u))
			{
				Ogre::Vector2 pos = u + d;
				if(pos.x < 0 || pos.x >= width
				|| pos.y < 0 || pos.y >= height)
					continue;
			
				Tile* v = mWorld->getTile(pos);
				if(v == 0) continue;

				int p = ((int)pos.y)*width + pos.x;
				if(marks.find(p) == marks.end())
				{
					marks.insert(p);
					nextLevel.push_back(pos);
				}
			}
		}
		if(currentLevel.empty())
		{
			if(--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}
}

Job* JobQuery::getJob(int id) const
{
	return (id >= 0) ? mJobs->get(id) : 0;
}
bool JobQuery::hasJob(const Ogre::Vector2& u) const
{
	return !mJobs->get(u).empty();
}
bool JobQuery::hasJob(const Ogre::Vector2& u, Job::JobType type) const
{
	std::vector<Job*> jobs = mJobs->get(u);
	for (Job* j : jobs)
		if (j->getType() == type)
			return true;
	return false;
}
bool JobQuery::assignJob(Entity* ent, int id)
{
	return mJobs->assign(ent, id);
}
bool JobQuery::unAssignJob(Entity* ent, int id)
{
	return mJobs->unAssign(ent, id);
}

bool JobQuery::exploreTile(const Ogre::Vector2& u, Job*& job, int filter) const
{
	std::vector<Job*> jobs = mJobs->get(u);
	if(jobs.empty())
		return false;
	std::vector<Job*>::iterator iter, iend;
	iter = jobs.begin(); iend = jobs.end();
	for(; iter != iend; ++iter)
	{
		Job* j = *iter;
		if(j->isAvailable() && (j->getType() & filter))
		{
			job = j;
			break;
		}
	}
	return iter != iend;
}
#include "AnPCH.h"
#include "AnSerializer.h"
#include "AnGameObject.h"
#include "AnIO.h"
using namespace Ants;


Serializer::Serializer()
{

}
Serializer::~Serializer()
{

}

bool Serializer::insert(GameObject* obj)
{
	if (obj && !isTopLevel(obj))
	{
		mObjects.push_back(obj);
		return true;
	}
	return false;
}

bool Serializer::isTopLevel(const GameObject* object) const
{
	std::vector<GameObject*>::const_iterator iter = mObjects.begin();
	std::vector<GameObject*>::const_iterator end = mObjects.end();
	for (/**/; iter != end; ++iter)
	{
		if (object == *iter)
		{
			return true;
		}
	}
	return false;
}
void Serializer::toMemory(int& size, char*& buffer)
{
	const GameObject* object = 0;
	unsigned int uniqueID = 0;
	mRegistered.insert(std::make_pair(object, uniqueID));
	mOrdered.push_back(object);

	size = 0;
	
	Ogre::String topLevel = "ROOT";
	int numTopLevelBytes = stringSize(topLevel);
	size += numTopLevelBytes*(int)mObjects.size();

	std::vector<GameObject*>::iterator iterT = mObjects.begin();
	std::vector<GameObject*>::iterator endT = mObjects.end();
	for (/**/; iterT != endT; ++iterT)
	{
		object = *iterT;
		object->registerObjects(*this);
	}
	std::vector<const GameObject*>::iterator iterO = mOrdered.begin();
	std::vector<const GameObject*>::iterator endO = mOrdered.end();
	for (++iterO; iterO != endO; ++iterO)
	{
		object = *iterO;
		size += object->getStreamingSize();
	}

	buffer = new1<char>(size);
	mIO.open(size, buffer);

	iterO = mOrdered.begin();
	endO = mOrdered.end();
	for (++iterO, uniqueID = 1; iterO != endO; ++iterO, ++uniqueID)
	{
		object = *iterO;
		if (isTopLevel(object))
		{
			writeString(topLevel);
		}
		object->serialize(*this);
	}
}
bool Serializer::toFile(const Ogre::String& filename)
{
	int size;
	char* buffer;
	toMemory(size, buffer);

	if (!IO::save(filename, true, size, buffer))
	{
		delete1(buffer);
		return false;
	}
	delete1(buffer);
	return true;
}

bool Serializer::writeString(const Ogre::String& datum)
{
	int length = integer(datum.length());
	if (!mIO.write(sizeof(int), &length))
		return false;

	if (length > 0)
	{
		if (!mIO.write(sizeof(char), length, datum.c_str()))
			return false;

		char zero[4] = { 0, 0, 0, 0 };
		int padding = length % 4;
		if (padding > 0)
		{
			padding = 4 - padding;
			if (!mIO.write(sizeof(char), padding, zero))
				return false;
		}
	}
	return true;
}
bool Serializer::writeBool(const bool datum)
{
	unsigned int value = (datum ? 0xFFFFFFFFu : 0u);
	return mIO.write(sizeof(unsigned int), &value);
}

bool Serializer::registerRoot(const GameObject* obj)
{
	if (mRegistered.find(obj) == mRegistered.end())
	{
		unsigned int uniqueID = (unsigned int)mOrdered.size();
		mRegistered.insert(std::make_pair(obj, uniqueID));
		mOrdered.push_back(obj);
		return true;
	}
	return false;
}
void Serializer::writeUniqueID(const GameObject* obj)
{
	unsigned int uniqueID = mRegistered.find(obj)->second;
	mIO.write(sizeof(unsigned int), &uniqueID);
}
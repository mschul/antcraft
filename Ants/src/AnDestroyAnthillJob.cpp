#include "AnPCH.h"
#include "AnDestroyAnthillJob.h"
#include "AnClient.h"
using namespace Ants;

DestroyAnthillJob::DestroyAnthillJob (int id, const Ogre::Vector2& position, double duration)
: Job (id, 1, position)
{
    mT = duration;
}
DestroyAnthillJob::~DestroyAnthillJob ()
{
}

Job::JobType DestroyAnthillJob::getType () const
{
    return Job::DESTROY_ANTHILL_JOB;
}

bool DestroyAnthillJob::update (double dt, ActionReactor* reactor)
{
    mT -= dt;
    if (mT <= 0)
    {
        return true;
    }
    return false;
}
void DestroyAnthillJob::getSubJobs (std::vector<Job::SubJobs>& jobs) const
{
    jobs.push_back (Job::UPDATE);
}

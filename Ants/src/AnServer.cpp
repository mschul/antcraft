#include "AnPCH.h"
#include "AnServer.h"
#include "AnProceduralGeometry.h"
#include "AnPerlin.h"
#include "AnSmallWorker.h"
#include "AnWorker.h"
#include "AnSoldier.h"
#include "AnKamikaze.h"
#include "AnCarrier.h"
#include "AnEgg.h"
#include "AnGrub.h"
#include "AnFloralFood.h"
#include "AnExitRoom.h"
#include "AnDebris.h"
#include "AnSpider.h"
#include "AnFaunalFood.h"
#include "AnLadybug.h"
#include "AnEarthworm.h"
#include "AnBombardierBeetle.h"
#include "AnMobSpawner.h"
#include "AnCopal.h"
#include "AnProjectile.h"
using namespace Ants;

Server::Server ()
{
	mWorld = 0;
	mWater = 0;
	mRooms = 0;
	mNextClientID = 0;
	mLastEntityID = 0;
	mFirstTileID = -1;
}
Server::~Server ()
{
	SafeDelete0(mWorld);
	SafeDelete0(mRooms);
	SafeDelete0(mWater);
	for(int i = 0 ; i < (int)mClientInfos.size(); ++i)
	{
		ClientInfo* ci = mClientInfos[i];
		if(ci)
		{
			ci->ChangedTiles.clear();
			delete0(ci->World);
			delete0(ci->Entities);
			delete0(ci->Visibilities);
			delete0(ci);
			mClientInfos[i] = 0;
		}
	}
}

void Server::init(unsigned int width, unsigned int height, bool fogOfWar, bool revealMap,
				  Ogre::SceneManager* sceneMgr)
{	
	mFogOfWar = fogOfWar;
	mRevealMap = revealMap;
	mWorld = new0 WorldMap();
	mWorld->init(width, height);

	mWater = new0 WaterMap(mWorld);	
	mWaterUpdateDelta = 0.5;

	mRooms = new0 RoomMap();
	mRooms->init(width, height);

	initTiles(sceneMgr);
}
void Server::init(const Ogre::Rect& area, bool fogOfWar, bool revealMap,
				  Ogre::SceneManager* sceneMgr)
{
	mFogOfWar = fogOfWar;
	mRevealMap = revealMap;
	mWorld = new0 WorldMap();
	mWorld->init(area);

	mWater = new0 WaterMap(mWorld);	
	mWaterUpdateDelta = 0.5;

	mRooms = new0 RoomMap();
	mRooms->init(area);

	initTiles(sceneMgr);
}

void Server::update(float dt)
{
	if(mWater)
	{
		mWaterUpdateDelta -= dt;
		if(mWaterUpdateDelta < 0)
		{
			std::vector<PositionTilePair> tiles;
			mWater->update(tiles);
			mWaterUpdateDelta = 0.5;
			setTiles(-1, tiles);
		}
	}
	mRooms->update(dt, this);
}

int Server::loginClient(Client* client)
{
	ClientInfo* ci = new0 ClientInfo();
	ci->Entities = new0 EntityMap();
	ci->Entities->init(mWorld->getSize());
	ci->Visibilities = new0 VisibilityMap();
	ci->Visibilities->init(mWorld->getSize(), mRevealMap && !mFogOfWar);
	ci->World = new0 WorldMap();
	ci->World->init(mWorld->getSize());
	if(mRevealMap)
	{
		int h = mWorld->getSize().height();
		int w = mWorld->getSize().width();
		for(int y=0; y < h; ++y)
			for(int x=0; x < w; ++x)
				ci->ChangedTiles.push_back(Ogre::Vector2(x, y));
	}
	mClientInfos.push_back(ci);
	return mNextClientID++;
}
void Server::logoffClient(Client* client, int clientID)
{
	ClientInfo* ci = mClientInfos[clientID];
	if(ci)
	{
		ci->ChangedTiles.clear();
		delete0(ci->Entities);
		delete0(ci->Visibilities);
		delete0(ci);
		mClientInfos[clientID] = 0;
	}
}

void Server::createSpawners(int count, std::vector<SpawnerIdPositionPair>& spawners)
{
	MobSpawner::init();
	int c = MobSpawner::msSpawnerInfos.size();
	Ogre::Rect size = mWorld->getSize();
		
	int builtSpawners = 0;
	while(builtSpawners < count)
	{
		int spawnerId = rand() % c;

		int x = (rand()%(size.width()-1))+1;
		std::vector<int> ys;

		for(int y = 0; y < size.height(); ++y)
		{
			Tile* tile = mWorld->getTile(Ogre::Vector2(x,y));
			for(int t : MobSpawner::msSpawnerInfos[spawnerId]->allowedTiles)
			{
				if(t == tile->getType())
				{
					ys.push_back(y);
					break;
				}
			}
			if(!ys.empty() && MobSpawner::msSpawnerInfos[spawnerId]->randomPositionAllowed)
				break;
		}

		if(!ys.empty())
		{
			builtSpawners++;
			spawners.push_back(std::make_pair(spawnerId, Ogre::Vector2(x, ys[rand()%ys.size()])));
		}
	}
}
void Server::settleClient(int clientId, std::vector<PositionTilePair>& environment,
			Ogre::Vector2& thronePosition, std::vector<Ogre::Vector2>& throneCircle,
			Ogre::Vector2& exitPosition, std::vector<Ogre::Vector2>& exitTriangle)
{
	Ogre::Rect size = mWorld->getSize();

	Ogre::Vector2 pos(0, 0);
	for (;;)
	{
		pos.x = (rand() % (size.width() - 20)) + 10;
		Ogre::Vector2 delta = Tile::getNeighbours(pos)[Tile::TOP_RIGHT];
		for (;;) // ever
		{
			Tile* t = mWorld->getTile(pos);
			if (!t)
			{
				pos.y = 0;
				break;
			}
			if (t && t->isOverWorld())
				break;
			pos += delta;
		}
		if (pos.y > 30)
		{
			pos.y -= 15;
			break;
		}
	}

	thronePosition = pos;
	
	Ogre::Vector2 start = thronePosition;
	Ogre::Vector2 border = thronePosition + 3*Tile::getNeighbours(thronePosition)[Tile::LEFT];
	Tile::hexCircle(start, border, throneCircle);

	for(;;) // ever
	{
		Tile* t = mWorld->getTile(pos);
		if(t->isOverWorld())
			break;

		if (pos.x < 10)
		{
			pos += Tile::getNeighbours(pos)[Tile::TOP_RIGHT];
		}
		else if (pos.x > size.width() - 10)
		{

			pos += Tile::getNeighbours(pos)[Tile::TOP_LEFT];
		}
		else
		{

			pos += Tile::getNeighbours(pos)[Tile::TOP_LEFT + (rand() % 2)];
		}

	}

	exitPosition = pos;
	createExit(exitPosition, exitTriangle);

	// create environment
	std::vector<Ogre::Vector2> environmentPath;
	Tile::hexLineEnvironment(thronePosition, exitPosition, 8, environmentPath);
	for(Ogre::Vector2 p : environmentPath)
	{
		Tile* t = mWorld->getTile(p);
		if(t)
		{
			environment.push_back(std::make_pair(p, t));
		}
	}
}

void Server::createExit(const Ogre::Vector2& exitPosition, std::vector<Ogre::Vector2>& exitTriangle)
{
	// create exit triangle
	std::vector<Ogre::Vector2> triangle;
	ExitRoom::createTriangle(exitPosition, triangle);
	for(Ogre::Vector2 p : triangle)
	{
		Tile* t = mWorld->getTile(p);
		if(t && t->isOverWorld())
			exitTriangle.push_back(p);
	}
}

void Server::getUpdates(int clientID,
						std::vector<PositionTilePair>& tiles,
						std::vector<EntityDesc>& addedEnts,
						std::vector<EntityDesc>& changedEnts,
						std::vector<EntityDesc>& removedEnts,
						std::vector<Attack*>& attacks,
						std::vector<Message*>& messages)
{
	ClientInfo* ci = mClientInfos[clientID];
	if(ci)
	{
		messages.assign(ci->Messages.begin(), ci->Messages.end());
		ci->Messages.clear();
		attacks.assign(ci->Attacks.begin(), ci->Attacks.end());
		ci->Attacks.clear();
		for(int i = 0; i < (int)ci->ChangedTiles.size(); ++i)
		{
			Ogre::Vector2 p = ci->ChangedTiles[i];
			tiles.push_back(std::make_pair(p, mWorld->getTile(p)));
		}
		ci->ChangedTiles.clear();
		
		ci->Entities->getUpdates(addedEnts, changedEnts, removedEnts);

		ci->Entities->clearUpdates();
	}
}
void Server::setUpdates(int clientID,
			const std::vector<PositionTilePair>& tiles,
			const std::vector<EntityDesc>& addedEnts,
			const std::vector<EntityDesc>& changedEnts,
			const std::vector<EntityDesc>& removedEnts,
			const std::vector<Attack*>& attacks,
			const std::vector<Message*>& messages,
			const std::vector<AOE*>& aoes)
{
	setTiles(clientID, tiles);
	{
		std::vector<EntityDesc>::const_iterator iter, iend;
		iter = addedEnts.cbegin(); iend = addedEnts.cend();
		for(; iter != iend; ++iter)
			addEntity(*iter, clientID);
		iter = changedEnts.cbegin(); iend = changedEnts.cend();
		for(; iter != iend; ++iter)
			changeEntity(*iter, clientID);
		iter = removedEnts.cbegin(); iend = removedEnts.cend();
		for(; iter != iend; ++iter)
			removeEntity(*iter, clientID);
	}

	for(Attack* a : attacks)
	{
		int attackClientID = a->getTargetClientID();
		ClientInfo* ci = mClientInfos[attackClientID];
		ci->Attacks.push_back(a);
	}
	for (Message* m : messages)
	{
		int attackClientID = m->getReceiver();
		ClientInfo* ci = mClientInfos[attackClientID];
		ci->Messages.push_back(m);
	}
	for (AOE* a : aoes)
	{
		std::vector<Ogre::Vector2>::iterator iter, iend;
		int sourceClient = a->getSourceClientID();
		ObjectID sourceID = a->getSourceID();
		for (int i = 0; i < mNextClientID; ++i)
		{
			ClientInfo* ci = mClientInfos[i];
			if (ci && i != sourceClient)
			{
				iter = a->getPositions().begin();
				iend = a->getPositions().end();
				for (; iter != iend; ++iter)
				{
					std::vector<Entity*> ents = ci->Entities->get(*iter);
					for (Entity* e : ents)
					{
						if (e->getClientID() == i && e->isCharacter())
						{
							float damage = a->getAttackPoints();
							damage -= Tile::dist(a->getSourcePosition(), e->getTilePosition());
							damage -= e->getArmor();
							if (damage > 0)
								ci->Attacks.push_back(new0 Attack(damage, sourceID, e->getID(), sourceClient, i));
						}
					}
				}
			}
		}
	}
}

void Server::setTile(int clientID, const Ogre::Vector2& position, Tile* tile)
{
	if (clientID >= 0)
	{
		ClientInfo* ci = mClientInfos[clientID];
		ci->World->setTile(position, tile);
		Tile* oldTile = mWorld->getTile(position);
		if (oldTile && tile->getType() != Tile::WATER &&
			oldTile->getType() == Tile::WATER)
		{
			mWater->remove(position);
		}

	}


	mWorld->setTile(position, tile);
	mRooms->reportTile(position, tile);

	if (mFogOfWar && clientID >= 0)
	{
		ClientInfo* ci = mClientInfos[clientID];
		if (tile->isAnthill())
			ci->Visibilities->increment(position, 0);
	}

	for (int i = 0; i < mNextClientID; ++i)
	{
		ClientInfo* ci = mClientInfos[i];
		if (ci && i != clientID)
		{
			if (tile != ci->World->getTile(position)
				&& (ci->Visibilities->isVisible(position)))
			{
				ci->World->setTile(position, tile);
				ci->ChangedTiles.push_back(position);
			}
		}
	}

}
void Server::setTiles(int clientID, const std::vector<Ogre::Vector2>& positions, const std::vector<Tile*>& tiles)
{
	if (clientID >= 0)
	{
		ClientInfo* ci = mClientInfos[clientID];
		ci->World->setTiles(positions, tiles);
		for (int i = 0; i < (int)positions.size(); ++i)
		{
			Tile* tile = tiles[i];
			if (tile->getType() != Tile::WATER)
			{
				Ogre::Vector2 position = positions[i];
				Tile* oldTile = mWorld->getTile(position);
				if (oldTile && oldTile->getType() == Tile::WATER)
					mWater->remove(position);
			}
		}
	}

	mWorld->setTiles(positions, tiles);
	for(int i = 0; i < (int)positions.size(); ++i)
	{
		Tile* tile = tiles[i];
		Ogre::Vector2 position = positions[i];
		if (mFogOfWar && clientID >= 0)
		{
			ClientInfo* ci = mClientInfos[clientID];
			if (tile->isAnthill())
				ci->Visibilities->increment(position, 0);
		}
		mRooms->reportTile(position, tile);
	}

	for(int i = 0; i < mNextClientID; ++i)
	{
		ClientInfo* ci = mClientInfos[i];
		if(ci && i != clientID)
		{
			for(int j = 0; j < (int)positions.size(); ++j)
			{
				Ogre::Vector2 position = positions[j];
				Tile* tile = tiles[j];
				if (tile != ci->World->getTile(position)
					&& (ci->Visibilities->isVisible(position)))
				{
					ci->World->setTile(position, tile);
					ci->ChangedTiles.push_back(position);
				}
			}
		}
	}
}
void Server::setTiles(int clientID, const std::vector<PositionTilePair>& tiles)
{
	if (clientID >= 0)
	{
		ClientInfo* ci = mClientInfos[clientID];
		ci->World->setTiles(tiles);
		for (PositionTilePair pair : tiles)
		{
			Tile* tile = pair.second;
			if (tile->getType() != Tile::WATER)
			{
				Ogre::Vector2 position = pair.first;
				Tile* oldTile = mWorld->getTile(position);
				if (oldTile && oldTile->getType() == Tile::WATER)
					mWater->remove(position);
			}
		}
	}

	mWorld->setTiles(tiles);

	for(PositionTilePair pair : tiles)
	{
		Tile* tile = pair.second;
		Ogre::Vector2 position = pair.first;
		if (mFogOfWar && clientID >= 0)
		{
			ClientInfo* ci = mClientInfos[clientID];
			if (tile->isAnthill())
				ci->Visibilities->increment(position, 0);
		}
		mRooms->reportTile(position, tile);
	}

	for(int i = 0; i < mNextClientID; ++i)
	{
		ClientInfo* ci = mClientInfos[i];
		if(ci && i != clientID)
		{
			for(int j = 0; j < (int)tiles.size(); ++j)
			{
				Ogre::Vector2 position = tiles[j].first;
				Tile* tile = tiles[j].second;
				if (tile != ci->World->getTile(position)
					&& (ci->Visibilities->isVisible(position)))
				{
					ci->World->setTile(position, tile);
					ci->ChangedTiles.push_back(position);
				}
			}
		}
	}
}

int Server::createEntityID()
{
	return mLastEntityID++;
}
void Server::createEntityIDs(int count, std::vector<int>& ids)
{
	for(int i = 0; i < count; ++i)
	{
		ids.push_back(createEntityID());
	}
}
		
void Server::initTiles(Ogre::SceneManager* sceneMgr)
{
	//TODO find a proper place for this line
	SmallWorker::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Worker::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Carrier::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Soldier::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Kamikaze::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Egg::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Grub::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	FloralFood::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	FaunalFood::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Debris::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Projectile::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Spider::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Ladybug::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Copal::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	Earthworm::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");
	BombardierBeetle::msStandardMesh = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");

	SmallWorker::msMaterialName = "smallWorker";
	Worker::msMaterialName = "worker";
	Carrier::msMaterialName = "carrier";
	Soldier::msMaterialName = "soldier";
	Kamikaze::msMaterialName = "kamikaze";
	Egg::msMaterialName = "egg";
	Grub::msMaterialName = "grub";
	FloralFood::msMaterialName = "floralFood";
	FaunalFood::msMaterialName = "faunalFood";
	Copal::msMaterialName = "copal";
	Debris::msMaterialName = "debris";
	Projectile::msMaterialName = "projectile";
	Spider::msMaterialName = "spider";
	Ladybug::msMaterialName = "ladybug";
	Earthworm::msMaterialName = "earthworm";
	BombardierBeetle::msMaterialName = "bombardierbeetle";

	Tile::init(mLastEntityID);
	mFirstTileID = mLastEntityID;
}
void Server::createWorld(int rock, int water, int cave, int grass)
{
	Ant::msMovementPerlin = new0 Perlin(4, 5, 0.7f, rand()%100);

	WorldMap::msUndergroundPerlin = new0 Perlin(6, 12, 0.8f, rand() % 100);
	WorldMap::msDirtPerlin = new0 Perlin(6, 6, 1, rand() % 100);
	WorldMap::msRockPerlin = new0 Perlin(12, 12, 1, rand() % 100);
	WorldMap::msCavePerlin = new0 Perlin(12, 7, 1, rand() % 100);
	WorldMap::msWaterPerlin = new0 Perlin(12, 12, 1, rand()%100);

	WorldMap::msSurfacePerlin = new0 Perlin(12, 3, 1, rand()%100);
	WorldMap::msSurfaceEntityPerlin = new0 Perlin(5, 8, 1, rand()%100);

	std::vector<PositionTilePair> pairs;
	mWorld->createEmptyWorld(pairs, rock, water, cave);
	setTiles(-1, pairs);

	pairs.clear();
	mWorld->createTrees(pairs, 20, 0.04f);
	setTiles(-1, pairs);
	
	if (grass > 0)
	{
		pairs.clear();
		std::vector<std::vector<Ogre::Vector2>> leafs;
		std::vector<std::vector<Ogre::Vector2>> copal;
		mWorld->createResources(leafs, copal, 5, 10, (float)grass / 5);
		for(std::vector<Ogre::Vector2> leaf : leafs)
		{
			int roomID = mRooms->assignRoom(Room::FLORAL_SOURCE, leaf);
			std::vector<PositionTilePair> tiles;
			mRooms->buildRoom(roomID, leaf[0], tiles);
			pairs.insert(pairs.end(), tiles.begin(), tiles.end());
		}
		for (std::vector<Ogre::Vector2> c : copal)
		{
			int roomID = mRooms->assignRoom(Room::COPAL_SOURCE, c);
			std::vector<PositionTilePair> tiles;
			mRooms->buildRoom(roomID, c[0], tiles);
			pairs.insert(pairs.end(), tiles.begin(), tiles.end());
		}
		
		setTiles(-1, pairs);
	}

	mWater->build();

}

void Server::addEntity(const EntityDesc& desc, int clientID)
{
	ClientInfo* ci = mClientInfos[clientID];
	if(ci)
	{
		ci->Entities->add(desc);
		Entity* e = ci->Entities->get(desc.getEntityID());

		// visibility updates
		if (mFogOfWar)
			ci->Visibilities->increment(e);
		else
			ci->Visibilities->set(e);

		// update world information based on new visibilities
		const Ogre::Vector2 pos = e->getTilePosition();
		const unsigned int radius = e->getVisibilityRadius();

		std::vector<Ogre::Vector2> circle;
		Ogre::Vector2 border = pos;
		border += Tile::getNeighbours(pos)[Tile::LEFT] * radius;
		Tile::hexCircle(pos, border, circle);

		for (Ogre::Vector2 p : circle)
		{
			Tile* t0 = mWorld->getTile(p);
			Tile* t1 = ci->World->getTile(p);
			if (t0 != t1)
			{
				ci->World->setTile(p, t0);
				ci->ChangedTiles.push_back(p);
			}

			// get entities from all clients
			for (int i = 0; i < mNextClientID; ++i)
			{
				ClientInfo* ciother = mClientInfos[i];
				if (ciother && i != clientID)
				{
					std::vector<Entity*> ents = ciother->Entities->get(p);
					for (Entity* otherEnt : ents)
					{
						if (!ci->Entities->get(otherEnt->getID()))
						{
							ci->Entities->add(EntityDesc(otherEnt), true);
#ifdef _DEBUG
							Framework::getSingleton().mLog->logMessage(
								"add entity " + Ogre::StringConverter::toString(otherEnt->getID())
								+ " in client " + Ogre::StringConverter::toString(i)
								);
#endif
						}	
					}
				}
			}
		}


		// report to all clients
		for(int i = 0; i < mNextClientID; ++i)
		{
			ClientInfo* ciother = mClientInfos[i];
			if (ciother && i != clientID)
			{
				if (ciother->Visibilities->isVisible(e) || i == desc.getClientID())
				{
					ciother->Entities->add(desc, true);
#ifdef _DEBUG
					Framework::getSingleton().mLog->logMessage(
						"add entity " + Ogre::StringConverter::toString(e->getID())
						+ " in client " + Ogre::StringConverter::toString(i)
						);
#endif
				}
			}
		}
	}
}
void Server::changeEntity(const EntityDesc& desc, int clientID)
{
	ClientInfo* ci = mClientInfos[clientID];
	if(ci)
	{
		Entity* e = ci->Entities->get(desc.getEntityID());
		bool tpChanged = e->getTilePosition() != desc.getTilePosition();

		// PRE CHANGE SEMANTICS
		// if fog of war is on and tile position changed
		if (mFogOfWar && tpChanged)
		{
			// visibility values should be decreased
			ci->Visibilities->decrement(e);
		}

		// if foreign client sighted this ent before, but cannot see it now, remove it.
		for (int i = 0; i < mNextClientID; ++i)
		{
			ClientInfo* ciother = mClientInfos[i];
			if (ciother && i != clientID)
			{
				bool prev = ciother->Visibilities->isVisible(e);
				bool postv = ciother->Visibilities->isVisible(desc.getTilePosition());
				if (prev && !postv)
				{
					ciother->Entities->remove(desc, true);
#ifdef _DEBUG
					Framework::getSingleton().mLog->logMessage(
						"remove entity " + Ogre::StringConverter::toString(e->getID())
						+ " in client " + Ogre::StringConverter::toString(i)
						);
#endif
				}
				else if(!prev && postv)
				{
					ciother->Entities->add(desc, true);
#ifdef _DEBUG
					Framework::getSingleton().mLog->logMessage(
						"add entity " + Ogre::StringConverter::toString(e->getID())
						+ " in client " + Ogre::StringConverter::toString(i)
						);
#endif
				}
			}
		}

		// IN CHANGE SEMANTICS
		// now apply update
		e->set(desc);
		if(desc.getParent() >= 0)
		{
			if(e->getParent() != 0)
			{
				e->setParent(ci->Entities->get(desc.getParent()));
			}
		}
		else
		{
			e->setParent(0);
		}
		ci->Entities->change(e);

		// update visibility values
		if (tpChanged)
		{
			if (mFogOfWar)
				ci->Visibilities->increment(e);
			else
				ci->Visibilities->set(e);
		}
		
		// POST CHANGE SEMANTICS
		// update world information based on new visibilities
		const Ogre::Vector2 pos = e->getTilePosition();
		const unsigned int radius = e->getVisibilityRadius();

		std::vector<Ogre::Vector2> circle;
		Ogre::Vector2 border = pos;
		border += Tile::getNeighbours(pos)[Tile::LEFT] * radius;
		Tile::hexCircle(pos, border, circle);

		for(Ogre::Vector2 p : circle)
		{
			Tile* t0 = mWorld->getTile(p);
			Tile* t1 = ci->World->getTile(p);
			if(t0 != t1)
			{
				ci->World->setTile(p, t0);
				ci->ChangedTiles.push_back(p);
			}
		}

		if (clientID != desc.getClientID())
		{
			ClientInfo* ciother = mClientInfos[0];
		}

		// report to all clients
		for (int i = 0; i < mNextClientID; ++i)
		{
			ClientInfo* ciother = mClientInfos[i];
			if (ciother && i != clientID)
			{
				if (ciother->Visibilities->isVisible(e) || i == desc.getClientID())
				{
					ciother->Entities->change(desc, true);
				}
			}
		}
	}

}
void Server::removeEntity(const EntityDesc& desc, int clientID)
{
	ClientInfo* ci = mClientInfos[clientID];
	if(ci)
	{
		Entity* e = ci->Entities->get(desc.getEntityID());
		ci->Entities->remove(e);

		// visibility updates
		if(mFogOfWar)
			ci->Visibilities->decrement(e);

		// report to all clients
		for(int i = 0; i < mNextClientID; ++i)
		{
			ClientInfo* ciother = mClientInfos[i];
			if (ciother && i != clientID)
			{
				ciother->Entities->remove(desc, true);
			}
		}
	}
}

void Server::jobMilestoneReached(Job* job)
{
}
void Server::jobFinished(Job* job)
{
}
void Server::setExitTiles(const std::vector<std::pair<Ogre::Vector2, Tile*>>& tiles)
{
}
void Server::setTile(const Ogre::Vector2& position, int type)
{
	setTile(-1, position, Tile::msTiles[type]);
}
void Server::createResource(const Ogre::Vector2& position, int roomType,
	Character* resource)
{
}
void Server::removeResource(Room* r)
{
}
Egg* Server::spawnEgg(const Ogre::Vector2& position)
{
	return 0;
}
Grub* Server::hatchEgg(Egg* egg)
{
	return 0;
}
void Server::unitDied(Character* unit, Attack* a)
{

}
Ant* Server::spawnAnt(Grub* grub, int type)
{
	return 0;
}
Entity* Server::spawn(const Ogre::Vector2& position, int type)
{
	return 0;
}
void Server::removeEntities(const std::vector<Entity*>& ents)
{
}
void Server::aoeAttack(int sourceID, float attackPoints, const Ogre::Vector2& position, float radius)
{
}
void Server::meleeAttack(float attackPoints, Character* source, Entity* target)
{
}
void Server::rangedAttack(float attackPoints, Character* source, Entity* target)
{
}


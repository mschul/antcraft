#include "AnPCH.h"
#include "AnWorker.h"
using namespace Ants;

Ogre::String Worker::msStandardMesh = "";
Ogre::String Worker::msMaterialName = "";

Worker::Worker (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
	: Ant(id, clientID, position, tilePosition, visibilityRadius,
	worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mInventory.resize(1);
	mSubJobs.resize(1);
	mTargetRooms.resize(1);
	mAssignedSlots.resize(1);
	mJobs.resize(1);
	mJobs.assign(1, -1);

	mP1Jobs = Job::FETCH_JOB | Job::ROOM_CREATE_JOB | Job::ROOM_CREATE_JOB_EX;
	mP2Jobs = Job::DIG_JOB | Job::DIG_RESOURCE_JOB;
	mP3Jobs = Job::DEFEND_JOB;

	mAOE = false;
	mAOEDamage = 0;
	mMeleeDamage = 12;
	mRangedDamage = 14;
	mActiveTrait = 0;
	mArmor = 3;
	mRanged = false;
}
Worker::Worker (const EntityDesc& desc)
	: Ant(desc)
{
}
Worker::~Worker ()
{
}

void Worker::applyUpgrade(Entity::Upgrade u)
{
	if (u == Entity::U_ACID)
		mRanged = true;
	else
	{
		Ant::applyUpgrade(u);
	}
}

void Worker::selectBehaviour(float dt)
{
	int newTrait = 0;
	if(!mEnemies.empty())
	{
		if(mHealth > 10 && mEnemies.size() < 5)
		{
			newTrait = 1; // warrrior
		}
		else
		{
			newTrait = 2; // escape
		}
	}
	else
	{
		newTrait = 0;
	}

	if(newTrait != mActiveTrait)
	{
		mCurrentState = FIND_JOB;
		mActiveTrait = newTrait;
#ifdef _DEBUG
		Framework::getSingleton().mLog->logMessage(
			"worker " + Ogre::StringConverter::toString(getID())
			+ " switched to mode " + Ogre::StringConverter::toString(mActiveTrait)
			);
#endif
	}
}

Entity::EntityType Worker::getType() const
{
	return Entity::ET_WORKER;
}

Ogre::String Worker::getMaterialName() const
{
	return Worker::msMaterialName;
}
Ogre::String Worker::getStandardMesh() const
{
	return Worker::msStandardMesh;
}
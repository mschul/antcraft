#include "AnPCH.h"
#include "AnGameObject.h"
using namespace Ants;

GameObject::FactoryMap* GameObject::msFactories = 0;
bool GameObject::msStreamRegistered = false;

GameObject::GameObject()
{
	mID = -1;
}
GameObject::GameObject (int id)
{
	mID = id;
}
GameObject::~GameObject ()
{
}

const Ogre::String GameObject::TYPE("GameObject");
const Ogre::String& GameObject::getClassType() const
{
	return TYPE;
}

void GameObject::setID(ObjectID id)
{
	mID = id;
}
int GameObject::getID() const
{
	return mID;
}

GameObject* GameObject::Factory(Deserializer&)
{
	return 0;
}
bool GameObject::registerFactory()
{
	if (!msStreamRegistered)
	{
		InitTerm::addInitializer(GameObject::initializeFactory);
		InitTerm::addTerminator(GameObject::terminateFactory);
		msStreamRegistered = true;
	}
	return msStreamRegistered;
}
void GameObject::initializeFactory()
{
	if (!msFactories)
	{
		msFactories = new0 GameObject::FactoryMap();
	}
	msFactories->insert(std::make_pair(TYPE, Factory));
}
void GameObject::terminateFactory()
{
	delete0(msFactories);
	msFactories = 0;
}
GameObject::FactoryFunction GameObject::getFactory(const Ogre::String& name)
{
	FactoryMap::iterator iter = msFactories->find(name);
	if (iter != msFactories->end())
	{
		return iter->second;
	}
	return 0;
}
int GameObject::getStreamingSize() const
{
	int size = stringSize(getClassType());
	// unique id
	size += sizeof(unsigned int);
	// id
	size += sizeof(ObjectID);
	return size;
}
bool GameObject::registerObjects(Serializer& serializer) const
{
	return serializer.registerRoot(this);
}
void GameObject::resolvePointers(Deserializer&)
{
}
void GameObject::serialize(Serializer& serializer) const
{
	serializer.writeString(getClassType());
	serializer.writeUniqueID(this);
	serializer.write(mID);
}
void GameObject::deserialize(Deserializer& deserializer)
{
	deserializer.readUniqueID(this);
	deserializer.read(mID);
}
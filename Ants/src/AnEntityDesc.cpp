#include "AnPCH.h"
#include "AnEntityDesc.h"
using namespace Ants;

EntityDesc::EntityDesc (Entity* ent)
{
	mID = ent->getID();
	mClientID = ent->getClientID();
	mEntityType = ent->getType();
	mOrientation = ent->getOrientation();
	mPosition = ent->getPosition();
	mTilePosition = ent->getTilePosition();
	mParent = ent->getParent() == 0 ? -1 : ent->getParent()->getID();
	mHealth = ent->getHealth();
	mVisibilityRadius = ent->getVisibilityRadius();
	mArmor = ent->getArmor();
}
EntityDesc::~EntityDesc ()
{
}

Entity::EntityType EntityDesc::getEntityType() const
{
	return mEntityType;
}
ObjectID EntityDesc::getEntityID() const
{
	return mID;
}
int EntityDesc::getClientID() const
{
	return mClientID;
}
		
int EntityDesc::getVisibilityRadius() const
{
	return mVisibilityRadius;
}

Ogre::Radian EntityDesc::getOrientation() const
{
	return mOrientation;
}
Ogre::Vector2 EntityDesc::getPosition() const
{
	return mPosition;
}
Ogre::Vector2 EntityDesc::getTilePosition() const
{
	return mTilePosition;
}
		
ObjectID EntityDesc::getParent() const
{
	return mParent;
}
		
float EntityDesc::getHealth() const
{
	return mHealth;
}
int EntityDesc::getArmor() const
{
	return mArmor;
}
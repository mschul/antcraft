#include "AnPCH.h"
#include "AnWorldPage.h"
using namespace Ants;

WorldPage::WorldPage (int x0, int y0, int width, Ogre::SceneManager* sceneManager, bool instanced)
{
	mCreated = false;
	mLoaded = false;
	mBuilt = false;
	mInstanced = instanced;

	mX0 = x0;
	mY0 = y0;
	mWidth = width;

	mSceneManager = sceneManager;

	if(mInstanced)
	{
		mRoot = mSceneManager->getRootSceneNode();
	}
	else
	{
		mStaticGeometry = mSceneManager->createStaticGeometry("worldPage" +
			Ogre::StringConverter::toString(x0) + "," +
			Ogre::StringConverter::toString(y0));
	}
}
WorldPage::~WorldPage ()
{
}

void WorldPage::build(const std::vector<Tile*>& map, int width, int height, const std::vector<Ogre::InstanceManager*>& managers)
{
	if(!mLoaded)
		load(map, width, height, managers);

	if(!mInstanced && !mBuilt)
		mStaticGeometry->build();

	mBuilt = true;
}
void WorldPage::load(const std::vector<Tile*>& map, int width, int height, const std::vector<Ogre::InstanceManager*>& managers)
{
	if(mLoaded)
		return;
	
	if(mInstanced)
	{
		for(int y = mY0; y < mY0 + mWidth && y < height; ++y)
		{
			for(int x = mX0; x < mX0 + mWidth && x < width; ++x)
			{
				int index = y*width + x;
				Tile* tile = map[index];
				if(!tile)
					continue;
			
				Ogre::InstancedEntity* ent = 0;
				Ogre::String material = tile->getMaterialName();
				if(material != "")
				{
					ent = managers[tile->getMeshID()]->createInstancedEntity(material + "_instanced");
					Ogre::Vector2 hp;
					Tile::toHexagonalPosition(Ogre::Vector2(real(x), real(y)), hp);
					ent->setPosition(Ogre::Vector3(2.0f*hp.x, 2.0f*hp.y, 0) + 2*tile->getOffset());
				}
				mEntities.push_back(ent);
			}
		}
	}
	else
	{
		for(int y = mY0; y < mY0 + mWidth && y < height; ++y)
		{
			for(int x = mX0; x < mX0 + mWidth && x < width; ++x)
			{
				int index = y*width + x;
				Tile* tile = map[index];
				if(!tile)
					continue;
				
				Ogre::String material = tile->getMaterialName();
				if(material != "")
				{
					Ogre::String meshName = Tile::msMeshNames[tile->getMeshID()];
					Ogre::Entity* ent = mSceneManager->createEntity(meshName);
					ent->setMaterialName(material);
					Ogre::Vector2 hp;
					Tile::toHexagonalPosition(Ogre::Vector2(real(x), real(y)), hp);
					mStaticGeometry->addEntity(ent, Ogre::Vector3(2.0f*hp.x, 2.0f*hp.y, 0) + 2*tile->getOffset());
				}
			}
		}
		mStaticGeometry->setVisibilityFlags(WORLD_VISIBILITY_FLAG);
	}
	mLoaded = true;
}
void WorldPage::drop(const std::vector<Tile*>& map, int width, const std::vector<Ogre::InstanceManager*>& managers)
{
	if(!mLoaded)
		return;

	if(mInstanced)
	{
		std::vector<Ogre::InstancedEntity*>::const_iterator iter, iend;
		iter = mEntities.begin(); iend = mEntities.end();
		for(; iter != iend; ++iter)
		{
			if(*iter) mSceneManager->destroyInstancedEntity(*iter);
		}
		mEntities.clear();

		for(Ogre::InstanceManager* mgr : managers)
		{
			if(mgr) mgr->cleanupEmptyBatches();
		}
	}
	else
	{
		mStaticGeometry->reset();
	}

	mLoaded = false;
	mBuilt = false;
}
void WorldPage::change(const Ogre::Vector2& position, Tile* tile,
					const std::vector<Ogre::InstanceManager*>& managers)
{
	if(!mLoaded)
		return;

	if(mInstanced)
	{
		Ogre::Vector2 p(position.x - mX0, position.y - mY0);
		int i = integer(p.y * mWidth + p.x);

		if(mEntities[i]) mSceneManager->destroyInstancedEntity(mEntities[i]);

		Ogre::InstancedEntity* ent = 0;
		Ogre::String material = tile->getMaterialName();
		if(material != "")
		{
			ent = managers[tile->getMeshID()]->createInstancedEntity(material);
			Ogre::Vector2 hp;
			Tile::toHexagonalPosition(position, hp);
			ent->setPosition(Ogre::Vector3(2.0f*hp.x, 2.0f*hp.y, 0) + 2*tile->getOffset());
			mRoot->attachObject(ent);
		}
		mEntities[i] = ent;
	}
	else
	{
		// implicit drop
		mStaticGeometry->reset();
		mLoaded = false;
		mBuilt = false;
		return;
	}
}

void WorldPage::setVisible(bool visibility)
{
	if (!mLoaded)
		return;

	if (mInstanced)
	{
		mRoot->setVisible(visibility);
	}
	else
	{
		// implicit drop
		mStaticGeometry->setVisible(visibility);
	}
}
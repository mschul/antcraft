#include "AnPCH.h"
#include "AnBroodChamber.h"
#include "AnEgg.h"
#include "AnGrub.h"
#include "AnAnt.h"
using namespace Ants;

BroodChamber::BroodChamber (int id)
	: Room(id, 1)
{
	mProduction = Entity::ET_SMALL_WORKER;
	mGrubCount = 0;
	mFloralReserve = 0;
	mFaunalReserve = 0;
    mCopalReserve = 0;
    mHygieneDec = 0.1f;
}
BroodChamber::~BroodChamber ()
{
}
		
void BroodChamber::checkValid(const Ogre::String& caption)
{
	std::set<int> ents;
	bool dump = false;
	for (slot* s : mSlots)
	{
		for (int i = 0; i < mSlotCapacity; ++i)
		{
			if (s->items[i].ent)
			{
				// correctness
				if (s->items[i].ent->getID() < 0)
					dump = true;

				// uniquness
				if (ents.find(s->items[i].ent->getID()) != ents.end())
					dump = true;
				ents.insert(s->items[i].ent->getID());

				if (s->items[i].ent->getType() == Entity::ET_DEBRIS)
					dump = true;
			}
		}
	}

	if (dump)
	{
		Framework::getSingleton().mLog->logMessage("invalid at " + caption + " dumping");
		for (slot* sl : mSlots)
			for (int i = 0; i < mSlotCapacity; ++i)
				Framework::getSingleton().mLog->logMessage(
				Ogre::StringConverter::toString(sl->items[i].ent->getID()) + " " +
				Ogre::StringConverter::toString(sl->items[i].ent->getType()) + " "
				);
	}
}

void BroodChamber::setProduction(Entity::EntityType type)
{
	mProduction = type;
}
Entity::EntityType BroodChamber::getProduction() const
{
	return mProduction;
}

Room::RoomType BroodChamber::getType() const
{
	return Room::BROOD_CHAMBER;
}
void BroodChamber::build(const std::vector<Ogre::Vector2>& positions,
	const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
	std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
#ifdef _DEBUG
	checkValid("build BEGIN");
#endif

	mPivot.clear();
	mPivot.push_back(pivot);

	float minX, maxX, minY, maxY;
	calculateEnclosingRectangle(positions, minX, maxX, minY, maxY);

	// setup assignment map
	std::vector<int> assignedTile;
	std::vector<int> allTileIndices;
	std::vector<int> tileIndices;
	std::vector<int> borderIndices;
	int width = integer(maxX - minX + 1);
	int height = integer(maxY - minY + 1);
	std::vector<Ogre::Vector2> oldSlots;
	std::map<int, slot*> positionedSlots;
	for (slot* s : mSlots)
	{
		Ogre::Vector2 p = s->position;
		const int i = (p.y - minY)*width + (p.x - minX);
		positionedSlots.insert(std::make_pair(i, s));
		oldSlots.push_back(p);
	}
	mSlots.clear();

	assignedTile.resize(width*height, -1);
	for (Ogre::Vector2 position : positions)
	{
		int index = integer((int)(position.y - minY)*width + position.x - minX);
		allTileIndices.push_back(index);
		assignedTile[index] = 0;
	}

	std::map<int, Entity*> positionedEnts;
	for (Entity* e : se)
	{
		Ogre::Vector2 p = e->getTilePosition();
		const int i = integer((int)(p.y - minY)*width + (p.x - minX));
		positionedEnts.insert(std::make_pair(i, e));
	}


	// find borders
	findBorder(width, height, integer(minX), integer(minY),
		allTileIndices, assignedTile, borderIndices, tileIndices);


	// check if old slot setup is still applicable to this room
	bool oldSlotsStillValid = true;
	std::vector<int> oldSlotTiles;
	std::vector<int> oldSlotTilesIndices;
	oldSlotTiles.resize(width*height, -1);
	for (Ogre::Vector2 s : oldSlots)
	{
		Ogre::Vector2 localP = s;
		localP.x -= minX;
		localP.y -= minY;

		// this slot is out of bounds
		if (localP.x < 0 || localP.x >= width
			|| localP.y < 0 || localP.y >= height)
		{
			oldSlotsStillValid = false;
			break;
		}

		// this slot is in a border now
		int index = integer(localP.y * width + localP.x);
		if (assignedTile[index] < 0)
		{
			oldSlotsStillValid = false;
			break;
		}

		oldSlotTilesIndices.push_back(index);
		oldSlotTiles[index] = 0;
	}

	// sort indices and create room tiles according to these rules
	//  ?  ^  ?
	// 2 | x |  ? -> 3
	//  ?  V  ?
	//
	//  ?  ^  ?
	// ? | x |  ? -> 2
	//  ?  V  1
	//
	//  ?  ^  ?
	// ? | x |  ? -> 4
	//  ?  V  3
	//
	//  ?  ^  ?
	// ? | x |  ? -> 1
	//  -1 V  -1
	//
	//  ?  ^  ?
	// ? | x |  ? -> 3
	//  1  V  ?
	// and create slot if x > 0
	// this is a very simple greedy bin packing with some simple constraints
	// so the solution might not be optimal!
	// this will hold the indices of the slots
	std::vector<int> slotTilesIndices;
	std::vector<int> slotTiles;
	slotTiles.resize(width*height, -1);
	std::sort(tileIndices.begin(), tileIndices.end());
	for (int i : tileIndices)
	{
		int currentValue = -1;
		Ogre::Vector2 localP(real(i % width), real(i / width));
		Ogre::Vector2 globalP = localP;
		globalP.x += minX;
		globalP.y += minY;
		{
			Ogre::Vector2 left = localP + Tile::getNeighbours(globalP)[Tile::LEFT];
			int l = assignedTile[integer((int)left.y * width + left.x)];
			if (l == 2)
			{
				currentValue = 3;
			}
			else if (l == -1)
			{
				Ogre::Vector2 rightBottom = localP +
					Tile::getNeighbours(globalP)[Tile::BOTTOM_RIGHT];
				Ogre::Vector2 leftBottom = localP +
					Tile::getNeighbours(globalP)[Tile::BOTTOM_LEFT];
				int lb = assignedTile[integer((int)leftBottom.y * width + leftBottom.x)];
				int rb = assignedTile[integer((int)rightBottom.y * width + rightBottom.x)];
				if (rb == 1)
				{
					currentValue = 2;
				}
				else if (rb == 3)
				{
					currentValue = 4;
				}
				else if (lb == -1)
				{
					if (rb == -1) currentValue = 1;
				}
				else if (lb == 1)
				{
					currentValue = 3;
				}
			}
		}

		Ogre::Vector2 position(localP.x + minX, localP.y + minY);
		int index = integer((int)localP.y * width + localP.x);
		assignedTile[index] = currentValue;


		if (currentValue > 0)
		{
			slotTilesIndices.push_back(index);
			slotTiles[index] = 0;
		}
	}

	// if the solution is not as optimal as the last one (from a smaller room setup)
	// just create the slots from the old setup
	if (oldSlotsStillValid && oldSlotTilesIndices.size() >= slotTilesIndices.size())
	{
		slotTilesIndices.clear();
		slotTilesIndices.assign(oldSlotTilesIndices.begin(), oldSlotTilesIndices.end());
		slotTiles.clear();
		slotTiles.assign(oldSlotTiles.begin(), oldSlotTiles.end());
	}


	// create slots and capture slots that are empty
	std::vector<slot*> newSlots;
	for (int tile : allTileIndices)
	{
		Ogre::Vector2 localP(real(tile % width), real(tile / width));
		Ogre::Vector2 position(localP.x + minX, localP.y + minY);
		if (slotTiles[tile] == 0)
		{
			if (mSlots.size() % 5 == 0)
			{
				workplace* w = new0 workplace;
				w->worker = 0;
				w->position = position;
				mWorkPlaces.push_back(w);
				mFreeWorkplaces++;
			}

			std::map<int, slot*>::iterator slotIter = positionedSlots.find(tile);
			slot* s;
			if (slotIter == positionedSlots.end())
			{
				s = new0 slot;
				s->reserved = 0;
				s->capacity = mSlotCapacity;
				s->position = position;

				s->items.resize(mSlotCapacity);

				for (int i = 0; i < mSlotCapacity; ++i)
				{
					s->items[i].ent = 0;
					s->items[i].reservedFor = 0;
				}
				newSlots.push_back(s);
			}
			else
			{
				s = slotIter->second;

				Entity* e = s->items[0].ent;
				positionedSlots.erase(slotIter);
			}
			mSlots.push_back(s);
			tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::NEST]));
		}
		else
			tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::ROOM_FLOOR]));
	}




	while (!workers.empty() && mFreeWorkplaces > 0)
	{
		assignToWorkplace(workers.back());
		workers.pop_back();
	}
	workers.clear();

	std::map<int, slot*>::iterator iter, iend;
	iter = positionedSlots.begin(); iend = positionedSlots.end();
	for (; iter != iend; ++iter)
	{
		slot* s = iter->second;
		Entity* reserved = s->items[0].reservedFor;
		if (reserved)
		{
			((Ant*)reserved)->prepareForNewJob(s);
		}
		Entity* ent = s->items[0].ent;
		if (ent)
		{
			se.push_back(ent);
		}
		SafeDelete0(s);
	}

	// try to put loose entities into empty slots
	while (!se.empty() && !newSlots.empty())
	{
		slot* s = newSlots.back();
		Entity* e = se.back();

		// beam eggs only
		if (e->getType() == Entity::ET_EGG)
		{
			Ogre::Vector2 hp;
			Tile::toHexagonalPosition(s->position, hp);
			e->setPosition(hp);
			e->setTilePosition(s->position);
		}
		s->items[mSlotCapacity - s->capacity].ent = e;
		s->capacity--;
		newSlots.pop_back();
		se.pop_back();
	}
	se.clear();


	mWorkerRefillRequested = true;

#ifdef _DEBUG
	checkValid("build END");
#endif

}
void BroodChamber::update(float dt, ActionReactor* reactor)
{
    Room::update (dt, reactor);

#ifdef _DEBUG
	checkValid("update BEGIN");
#endif


	for(slot* s : mSlots)
	{
		if(!s->items.empty())
		{
			Entity* ent = s->items[0].ent;
			if (ent)
			{

				switch (ent->getType())
				{
				case Entity::ET_EGG:
				{
					Egg* e = (Egg*)ent;
					if (e->hatch(dt))
					{
						Grub* g = reactor->hatchEgg(e);
						++mGrubCount;
						//g->assignSlot(s);
						s->items[0].ent = g;
					}
				}
					break;
				case Entity::ET_GRUB:
				{
					Grub* g = (Grub*)ent;

					if (g->spawn(dt))
					{
						int faunaD, floraD, copalD;
						getDemand(floraD, faunaD, copalD);

						if (mFaunalReserve >= faunaD
							&& mFloralReserve >= floraD
							&& mCopalReserve >= copalD)
						{
							Ant* a = reactor->spawnAnt(g, mProduction);

							mFaunalReserve -= faunaD;
							mFloralReserve -= floraD;
							mCopalReserve -= copalD;

							//a->assignSlot(s);
							s->items[0].ent = a;
						}
					}
				}
					break;
				}
			}
		}
	}
#ifdef _DEBUG
	checkValid("update END");
#endif
}
void BroodChamber::getDemand(int& floral, int& faunal, int& copal)
{
	switch(mProduction)
	{
	case Entity::ET_SMALL_WORKER:
		floral = 0; faunal = 0; copal = 0;
		break;
	case Entity::ET_WORKER:
		floral = 1; faunal = 0; copal = 0;
		break;
	case Entity::ET_CARRIER:
		floral = 3; faunal = 0; copal = 0;
		break;
	case Entity::ET_SOLDIER:
		floral = 2; faunal = 2; copal = 0;
		break;
	case Entity::ET_KAMIKAZE:
		floral = 2; faunal = 0; copal = 1;
		break;
	}
}
void BroodChamber::getReserve(int& floral, int& faunal, int& copal)
{
	floral = mFloralReserve;
	faunal = mFaunalReserve;
	copal = mCopalReserve;
}
void BroodChamber::getJob(std::vector<WorkplaceSubjobs>& jobs)
{
    float r = rand () % 100; r /= 10;
    bool hygieneNeeded = (r*r) > mHygiene;
    if (hygieneNeeded)
    {
        jobs.push_back (Room::FETCH_COPAL);
        jobs.push_back (Room::CLEAN_ROOM);
    }
    else
    {
        int floral, faunal, copal;
        getDemand (floral, faunal, copal);
        if (mGrubCount > 0 && (floral > mFloralReserve || faunal > mFaunalReserve || copal > mCopalReserve))
        {
            if (floral > mFloralReserve)
            {
                jobs.push_back (Room::FETCH_FLORAL_FOOD);
                jobs.push_back (Room::FEED);
            }
            else if (faunal > mFaunalReserve)
            {
                jobs.push_back (Room::FETCH_FAUNAL_FOOD);
                jobs.push_back (Room::FEED);
            }
            else if (copal > mCopalReserve)
            {
                jobs.push_back (Room::FETCH_COPAL);
                jobs.push_back (Room::FEED);
            }
        }
        else if (freeSlots () > 0)
        {
            jobs.push_back (Room::FETCH_EGGS);
            jobs.push_back (Room::PLACE);
        }
    }
}
void BroodChamber::feed(Entity* ent)
{
	mDeleteCache.push_back(ent);
	switch(ent->getType())
	{
	case Entity::ET_FLORAL_FOOD:
		mFloralReserve++;
		break;
	case Entity::ET_FAUNAL_FOOD:
		mFaunalReserve++;
		break;
	case Entity::ET_COPAL:
		mCopalReserve++;
		break;
	}
}
#include "AnPCH.h"
#include "AnLadybug.h"
using namespace Ants;

Ogre::String Ladybug::msStandardMesh = "";
Ogre::String Ladybug::msMaterialName = "";


Ladybug::Ladybug (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition,
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
		: Mob(id, clientID, position, tilePosition, visibilityRadius,
		worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mActiveTrait = 0;

	mRanged = false;
	mAOE = false;
	mAOEDamage = 0;
	mRangedDamage = 0;
	mMeleeDamage = 10;
    mArmor = 4;

    mSubJobs.resize (1);
    mJobs.resize (1);
    mJobs.assign (1, -1);

    mP1Jobs = Job::DESTROY_ANTHILL_JOB;
    mP2Jobs = 0;
    mP3Jobs = 0;
}
Ladybug::Ladybug (const EntityDesc& desc)
	: Mob(desc)
{
}
Ladybug::~Ladybug ()
{
}
		
Entity::EntityType Ladybug::getType() const
{
	return Entity::ET_LADYBUG;
}

void Ladybug::selectBehaviour (float dt)
{
    int newTrait = 3;
    if (!mEnemies.empty ())
    {
        newTrait = 1; // warrrior
    }

    if (newTrait != mActiveTrait)
    {
        mCurrentState = FIND_JOB;
        mActiveTrait = newTrait;
#ifdef _DEBUG
        Framework::getSingleton ().mLog->logMessage (
            "ladybug " + Ogre::StringConverter::toString (getID ())
            + " switched to mode " + Ogre::StringConverter::toString (mActiveTrait)
            );
#endif
    }
}

Ogre::String Ladybug::getMaterialName() const
{
	return Ladybug::msMaterialName;
}
Ogre::String Ladybug::getStandardMesh() const
{
	return Ladybug::msStandardMesh;
}
#include "AnPCH.h"
#include "AnMobSpawner.h"
#include "AnEntity.h"
using namespace Ants;

std::vector<MobSpawner::SpawnerInfo*> MobSpawner::msSpawnerInfos;
int MobSpawner::msSpawnerCount = 0;


MobSpawner::MobSpawner (int id)
	: Room(id, 1)
{
	msSpawnerCount++;
	mSpawnCount = 0;
}
MobSpawner::~MobSpawner ()
{
	if(msSpawnerCount == 0)
	{
		delete0(msSpawnerInfos[0]);
		delete0(msSpawnerInfos[1]);
		delete0(msSpawnerInfos[2]);
		//delete0(msSpawnerInfos[3]);
	}
}

void MobSpawner::init()
{
	if(msSpawnerInfos.empty())
	{
		{
			std::vector<Tile::TileType> t; t.push_back(Tile::GROUND);
			msSpawnerInfos.push_back(new0 SpawnerInfo(Room::SPIDER_SPAWNER, true, t));
		}
		{
			std::vector<Tile::TileType> t; t.push_back(Tile::TREE);
			msSpawnerInfos.push_back(new0 SpawnerInfo(Room::BOMBARDIER_BEETLE_SPAWNER, true, t));
		}
		{
			std::vector<Tile::TileType> t; t.push_back(Tile::GRASS);
			msSpawnerInfos.push_back(new0 SpawnerInfo(Room::LADYBUG_SPAWNER, false, t));
		}
		//{
		//	std::vector<Tile::TileType> t; t.push_back(Tile::DIRT); t.push_back(Tile::ROUGH);
		//	msSpawnerInfos.push_back(new0 SpawnerInfo(Room::EARTHWORM_SPAWNER, true, t));
		//}
	}
}
bool MobSpawner::isDistructible() const
{
	return false;
}

void MobSpawner::update(float dt, ActionReactor* reactor)
{
    Room::update (dt, reactor);
	mNextSpawnDelta -= dt;
	
	if(mNextSpawnDelta < 0)
	{
		mSpawnCount++;
		mNextSpawnDelta = std::max(0.1*mInitialSpawnDelta,
			std::pow(0.75, mSpawnCount) * mInitialSpawnDelta);
		spawn(reactor);
	}
}
void MobSpawner::getJob(std::vector<WorkplaceSubjobs>& jobs)
{
}
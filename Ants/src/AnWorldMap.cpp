#include "AnPCH.h"
#include "AnWorldMap.h"
using namespace Ants;

Perlin* WorldMap::msSurfacePerlin = 0;
Perlin* WorldMap::msUndergroundPerlin = 0;
Perlin* WorldMap::msSurfaceEntityPerlin = 0;
Perlin* WorldMap::msDirtPerlin = 0;
Perlin* WorldMap::msWaterPerlin = 0;
Perlin* WorldMap::msRockPerlin = 0;
Perlin* WorldMap::msCavePerlin = 0;

WorldMap::WorldMap()
{
	mWidth = 0;
	mHeight = 0;
}
WorldMap::~WorldMap ()
{
}

void WorldMap::init(unsigned int width, unsigned int height)
{
	mWidth = width;
	mHeight = height;
	mAnthillTiles = 0;
	mMap.resize(width*height, 0);
}
void WorldMap::init(const Ogre::Rect& area)
{
	init(area.width(), area.height());
}

bool WorldMap::tileExists(const Ogre::Vector2& position) const
{
	return position.x >= 0 && position.x < mWidth
		&& position.y >= 0 && position.y < mHeight
		&& mMap[mWidth*((int)position.y) + (int)position.x];
}

void WorldMap::createEmptyWorld(std::vector<PositionTilePair>& pairs, int rock, int water, int cave)
{
	typedef std::vector<Tile::TileType> Palette;
	typedef std::pair<Palette, Perlin*> PaletteNoisePair;
	std::vector<PaletteNoisePair> metaPalette;

	// dirt palette
	Palette dp;
	dp.push_back(Tile::DIRT);

	// cave palette
	Palette cp;
	cp.push_back(Tile::ROUGH);

	// rock palette
	Palette rp;
	rp.push_back(Tile::ROCK);

	// water palette
	Palette wp;
	wp.push_back(Tile::DIRT);
	wp.push_back(Tile::WATER);
	wp.push_back(Tile::WATER);
	wp.push_back(Tile::ROCK);
	wp.push_back(Tile::DIRT);
	
	int dirt = 7 - cave;

	for (int i = 0; i < rock / 2; ++i)
		metaPalette.push_back(std::make_pair(rp, msRockPerlin));
	for (int i = 0; i < dirt; ++i)
		metaPalette.push_back(std::make_pair(dp, msDirtPerlin));
	for (int i = 0; i < rock / 3; ++i)
		metaPalette.push_back(std::make_pair(rp, msRockPerlin));
	for (int i = 0; i < cave; ++i)
		metaPalette.push_back(std::make_pair(cp, msCavePerlin));
	for (int i = 0; i < water; ++i)
		metaPalette.push_back(std::make_pair(wp, msWaterPerlin));

	float h = 0.4f*mHeight;
	float lastH = 0.4f*mHeight;
	for(unsigned int x = 0; x < (unsigned int) mWidth; ++x)
	{
		float xN = ((float)x)/mWidth;
		h += msSurfacePerlin->get(xN);
		h = std::max(h, 20.f);

		unsigned int maxH = std::max((unsigned int)h, (unsigned int)lastH);
		unsigned int minH = std::min((unsigned int)h, (unsigned int)lastH);
		int modCheck = h < lastH;
		bool groundBlocked = false;

		for(unsigned int y = 0; y < (unsigned int) mHeight; ++y)
		{
			bool border = y == 0 || y == mHeight-1 || x == 0 || x == mWidth-1;
			bool ground = y >= minH && !groundBlocked;
			bool overworldEdge = y % 2 == modCheck && maxH != minH && y == maxH;
			bool overworld = y > maxH;

			Ogre::Vector2 pos = Ogre::Vector2(real(x), real(y));
			
			Ogre::Vector2 posN = Ogre::Vector2((float)x/mWidth, (float)y/mHeight);
			PaletteNoisePair palette;
			{
				float noise = std::max(0.0f, std::min(1.0f,
					msUndergroundPerlin->get(posN) + 0.5f));
				int index = integer((metaPalette.size()-1) * noise);
				palette = metaPalette[index];
			}
			Tile::TileType currentTile;
			{
				float noise = std::max(0.0f, std::min(1.0f, 
					palette.second->get(posN) + 0.5f));
				int index = integer((palette.first.size() - 1) * noise);
				currentTile = palette.first[index];

				if(ground && currentTile == Tile::WATER)
				{
					groundBlocked = true;
					ground = false;
				}
			}

			if(overworld || overworldEdge)
			{
				currentTile = Tile::AIR;
			}
			else if(ground)
			{
				if(border)
				{
					currentTile = Tile::AIR;
				}
				else
				{
					currentTile = Tile::GROUND;
				}
			}
			else if(border)
			{
				currentTile = Tile::ROCK;
			}

			pairs.push_back(std::make_pair(pos, Tile::msTiles[currentTile]));
		}

		lastH = h;
	}
}
void WorldMap::createTrees(std::vector<PositionTilePair>& pairs, int treeWidth, float density)
{
#define NOISE_RESOULTION 10000
	Ogre::Vector2 minP(real(rand() % NOISE_RESOULTION), real(rand() % NOISE_RESOULTION));
	Ogre::Vector2 maxP(real(rand() % (NOISE_RESOULTION - (int)minP.x) + minP.x),
		real(rand() % (NOISE_RESOULTION - (int)minP.y) + minP.y));
	minP /= NOISE_RESOULTION;
	maxP /= NOISE_RESOULTION;
#undef NOISE_RESOULTION
	Ogre::Vector2 step = (maxP - minP) / real((mWidth - 2));

	float accu = 1.0f / density;
	int currentTree = 0;
	for(unsigned int x = 1; x < (unsigned int) mWidth-1; ++x)
	{
		if(currentTree)
		{
			currentTree--;
			for(unsigned int y = mHeight-1; y > 0; --y)
			{
				Ogre::Vector2 pos = Ogre::Vector2(real(x), real(y));
				const unsigned int i = uinteger(mWidth*((int)pos.y) + pos.x);
				Tile* oldTile = mMap[i];
				
				if(oldTile && !oldTile->isOverWorld())
					break;

				pairs.push_back(std::make_pair(pos, Tile::msTiles[Tile::TREE]));
			}
		}
		else
		{
			accu -= fabsf(msSurfaceEntityPerlin->get(minP.x, minP.y));
			minP += step;
			if(accu < 0)
			{
				accu = 1.0f / density;
				currentTree = treeWidth;
			}
		}
	}

}
void WorldMap::createResources(std::vector<std::vector<Ogre::Vector2>>& grassPairs, 
	std::vector<std::vector<Ogre::Vector2>>& copalPairs,
	int grassMinHeight, int grassMaxHeight, float density)
{
#define NOISE_RESOULTION 10000
	Ogre::Vector2 minP(real(rand() % NOISE_RESOULTION), real(rand() % NOISE_RESOULTION));
	Ogre::Vector2 maxP(real(rand() % (NOISE_RESOULTION - (int)minP.x) + minP.x),
		real(rand() % (NOISE_RESOULTION - (int)minP.y) + minP.y));
	minP /= NOISE_RESOULTION;
	maxP /= NOISE_RESOULTION;
#undef NOISE_RESOULTION
	Ogre::Vector2 step = (maxP - minP) / real((mWidth - 2));
	
	float accu = 1.0f / density;
	int currentLeaf = 0;
	for(unsigned int x = 1; x < (unsigned int) mWidth-1; ++x)
	{
		if(currentLeaf)
		{
			bool copal = false;
			unsigned int y = mHeight-1;
			for(; y > 0; --y)
			{
				Ogre::Vector2 pos = Ogre::Vector2(real(x), real(y));
				const unsigned int i = uinteger(mWidth*((int)pos.y) + pos.x);
				Tile* oldTile = mMap[i];
				
				if(oldTile)
				{
					if(!oldTile->isOverWorld())
					{
						y++;
						break;
					}
					if(oldTile->getType() == Tile::TREE)
						copal = true;
				}
			}

			if (!copal)
			{
				std::vector<Ogre::Vector2> path;
				Tile::createUpPath(Ogre::Vector2(real(x), real(y)), currentLeaf, path);
				while (!path.empty() && path.back().y >= mHeight - 1) path.pop_back();

				if (!path.empty())
					grassPairs.push_back(path);
			}
			else
			{
				std::vector<Ogre::Vector2> path;
				std::vector<Ogre::Vector2> filteredPath;

				Tile::createUpPath(Ogre::Vector2(real(x), real(y)), currentLeaf, path);
				Ogre::Vector2 center = path.back();
				path.clear();

				Tile::hexCircle(center, 1, path);
				for (Ogre::Vector2 p : path)
				{
					if (p.y < mHeight - 1)
					{
						const unsigned int i = uinteger(mWidth*((int)p.y) + p.x);
						Tile* oldTile = mMap[i];
						if (oldTile->getType() == Tile::TREE)
							filteredPath.push_back(p);
					}
				}
				if (!filteredPath.empty())
					copalPairs.push_back(filteredPath);
			}

			currentLeaf = 0;
		}
		else
		{
			accu -= fabsf(msSurfaceEntityPerlin->get(minP.x, minP.y));
			minP += step;
			if(accu < 0)
			{
				accu = 1.0f / density;
				currentLeaf = (rand() % (grassMaxHeight - grassMinHeight)) + grassMinHeight;
			}
		}
	}
}

void WorldMap::getUpdates(std::vector<PositionTilePair>& tiles)
{
	tiles.assign(mTileUpdates.begin(), mTileUpdates.end());
}
void WorldMap::clearUpdates()
{
	mTileUpdates.clear();
}

Ogre::Rect WorldMap::getSize() const
{
	return Ogre::Rect(0, 0, mWidth, mHeight);
}

bool WorldMap::setTile(const Ogre::Vector2& position, Tile* tile, bool tracking)
{
	const unsigned int i = uinteger(mWidth*((int)position.y) + position.x);
	Tile* oldTile = mMap[i];
	if(oldTile == tile)
		return false;

	if (tile->isAnthill() && oldTile && !oldTile->isAnthill())
		mAnthillTiles++;
	if (!tile->isAnthill() && oldTile && oldTile->isAnthill())
		mAnthillTiles--;

	mMap[i] = tile;


	if(tracking)
		mTileUpdates.push_back(std::make_pair(position, tile));
	return true;
}
bool WorldMap::setTiles(const std::vector<Ogre::Vector2>& positions,
						const std::vector<Tile*>& tiles, bool tracking)
{
	bool changed = false;
	for (unsigned int i = 0; i < positions.size(); ++i)
	{
		Tile* tile = tiles[i];
		Ogre::Vector2 position = positions[i];

		const unsigned int index = uinteger(mWidth*((int)position.y) + position.x);
		Tile* oldTile = mMap[index];
		if (oldTile == tile)
			continue;

		if (tile->isAnthill() && oldTile && !oldTile->isAnthill())
			mAnthillTiles++;
		if (!tile->isAnthill() && oldTile && oldTile->isAnthill())
			mAnthillTiles--;

		mMap[index] = tile;
		if (tracking)
			mTileUpdates.push_back(std::make_pair(position, tile));

		changed = true;
	}
	return changed;
}
bool WorldMap::setTiles(const std::vector<PositionTilePair>& tiles, bool tracking)
{
	bool changed = false;
	for (unsigned int i = 0; i < tiles.size(); ++i)
	{
		Tile* tile = tiles[i].second;
		Ogre::Vector2 position = tiles[i].first;

		const unsigned int index = uinteger(mWidth*((int)position.y) + position.x);
		Tile* oldTile = mMap[index];
		if (oldTile == tile)
			continue;

		if (tile->isAnthill() && oldTile && !oldTile->isAnthill())
			mAnthillTiles++;
		if (!tile->isAnthill() && oldTile && oldTile->isAnthill())
			mAnthillTiles--;

		mMap[index] = tile;
		if (tracking)
			mTileUpdates.push_back(std::make_pair(position, tile));
		changed = true;
	}
	return changed;
}
Tile* WorldMap::getTile(const Ogre::Vector2& position) const
{
	if(position.y < 0 || position.x < 0
	|| position.x >= mWidth || position.y >= mHeight)
		return 0;
	const unsigned int tile = uinteger(mWidth*((int)position.y) + position.x);
	return mMap[tile];
}

unsigned WorldMap::getAnthillTileCount() const
{
	return mAnthillTiles;
}

bool WorldMap::isDiggable(int tile) const
{
	Tile* t = mMap[tile];
	if(t)
		return t->isDiggable();
	return false;
}
bool WorldMap::isDiggable(const Ogre::Vector2& position) const
{
	const unsigned int tile = uinteger(mWidth*((int)position.y) + position.x);
	return isDiggable(tile);
}
bool WorldMap::isResource(int tile) const
{
	Tile* t = mMap[tile];
	if(t)
		return t->isResource();
	return false;
}
bool WorldMap::isResource(const Ogre::Vector2& position) const
{
	const unsigned int tile = uinteger(mWidth*((int)position.y) + position.x);
	return isResource(tile);
}
bool WorldMap::isBuildable(int tile) const
{
	Tile* t = mMap[tile];
	if(t)
		return t->isAnthill()
		|| t->getType() == Tile::ROUGH || t->getType() == Tile::MUD
		|| t->getType() == Tile::DIRT; // buildable through roomcreateex
	return false;
}
bool WorldMap::isBuildable(const Ogre::Vector2& position) const
{
	const unsigned int tile = uinteger(mWidth*((int)position.y) + position.x);
	return isBuildable(tile);
}
bool WorldMap::isBlocking(int tile) const
{
	Tile* t = mMap[tile];
	if(t)
		return t->isBlocking();
	return false;
}
bool WorldMap::isBlocking(const Ogre::Vector2& position) const
{
	const unsigned int tile = uinteger(mWidth*((int)position.y) + position.x);
	return isBlocking(tile);
}

#include "AnPCH.h"
#include "AnEgg.h"
using namespace Ants;

Ogre::String Egg::msStandardMesh = "";
Ogre::String Egg::msMaterialName = "";

Egg::Egg (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius)
	: Entity(id, clientID, position, tilePosition, visibilityRadius)
{
	mHatchDelta = 30;
}
Egg::Egg (const EntityDesc& desc)
	: Entity(desc)
{
}
Egg::~Egg ()
{
}
Entity::EntityType Egg::getType() const
{
	return Entity::ET_EGG;
}

Ogre::String Egg::getMaterialName() const
{
	return Egg::msMaterialName;
}
Ogre::String Egg::getStandardMesh() const
{
	return Egg::msStandardMesh;
}
		
void Egg::update(float dt, ActionReactor* reactor)
{
}
bool Egg::hatch(float dt)
{
	mHatchDelta -= dt;
	if(mHatchDelta < 0)
		return true;
	return false;
}
		
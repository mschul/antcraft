#include "AnPCH.h"
#include "AnRansackTrait.h"
#include "AnMob.h"
#include "AnAnt.h"
using namespace Ants;

template class RansackTrait<Mob>;
template class RansackTrait<Ant>;

template<class T>
RansackTrait<T>::RansackTrait ()
{
}

template<class T>
void RansackTrait<T>::idleWalking (float dt, T* character, ActionReactor* reactor)
{
    if (!character->mPath.empty ())
    {
        character->mChanged = true;
        bool success = character->walkWaypoint (dt);
        if (!success)
        {
            character->clearPath ();
        }
    }
    else
    {
        if (character->mWorldQuery->isBlocking (character->getTilePosition ()))
            return;

        if (character->mStateUpdateDelta < 0)
        {
            character->mStateUpdateDelta = ((float)(rand () % 1000) / 1000 + 1);
            character->mCurrentState = Character::IDLE_STANDING;
        }
        else
        {
            character->randomPath ();
        }
    }
    character->mStateUpdateDelta -= dt;
}
template<class T>
void RansackTrait<T>::idleStanding (float dt, T* character, ActionReactor* reactor)
{
    if (character->mStateUpdateDelta < 0)
    {
        character->mCurrentState = Character::FIND_JOB;
    }
    character->mStateUpdateDelta -= dt;
}
template<class T>
void RansackTrait<T>::findJob (float dt, T* character, ActionReactor* reactor)
{
    bool success = false;
    int jobIndex = character->mActiveJobIndex;
    jobIndex = (jobIndex + 1) % character->mJobs.size ();
    character->mActiveJobIndex = jobIndex;

    {
        bool success = false;;
        // freelancer
        if (character->mJobQuery->getJob (character->mJobs[jobIndex]))
        {
            // there is a job active so get some info about it
            success = true;
        }
        else
        {
            // fetch a new job
            Job* job = 0;
            Ogre::Vector2 tilePosition = character->mTilePosition;
            int r = character->getVisibilityRadius ();
            character->mJobQuery->findNextJob (tilePosition, job, character->mP1Jobs, r);
            if (character->mP2Jobs && !job)
                character->mJobQuery->findNextJob (tilePosition, job, character->mP2Jobs, r);
            if (character->mP3Jobs && !job)
                character->mJobQuery->findNextJob (tilePosition, job, character->mP3Jobs, r);

            if (job)
            {
                success = true;
                // set job
                character->mJobs[jobIndex] = job->getID ();
                character->mJobFill++;
            }
        }
        if (!success)
        {
            character->prepareForNewJob (jobIndex);
            // no job found, so idle some time away
            character->mStateUpdateDelta = 4 + ((float)(rand () % 1000) / 1000);
            character->mCurrentState = Character::IDLE_WALKING;
        }
        else
        {
            character->mCurrentState = Character::GET_JOBINFORMATION;
        }
    }
}
template<class T>
void RansackTrait<T>::getJobInfo (float dt, T* character, ActionReactor* reactor)
{
    bool success = false;
    int jobIndex = character->mActiveJobIndex;

    if (character->mSubJobs[jobIndex].empty ())
    {
        {
            Job* job = character->mJobQuery->getJob (character->mJobs[jobIndex]);
            if (job && job->isAvailable ())
            {
                std::vector<Job::SubJobs> jobs;
                job->getSubJobs (jobs);
                if (jobs.size () > 0)
                {
                    success = true;

                    character->mJobQuery->assignJob (character, character->mJobs[jobIndex]);
                    for (Job::SubJobs job : jobs)
                        character->mSubJobs[jobIndex].push (job);
                }
            }
            else
            {
                character->prepareForNewJob (jobIndex);
            }
            if (!success)
            {
                character->mStateUpdateDelta = 4 + ((float)(rand () % 1000) / 1000);
                character->mCurrentState = Character::IDLE_WALKING;
            }
        }
    }
    else
    {
        // we already know enough about this job
        success = true;
    }

    if (success)
    {
        character->mCurrentState = Character::FIND_PATH;
    }
    else
    {
        character->prepareForNewJob (jobIndex);
    }
}
template<class T>
void RansackTrait<T>::findPath (float dt, T* character, ActionReactor* reactor)
{
    bool success = false;
    int jobIndex = character->mActiveJobIndex;
    if (character->mSubJobs[jobIndex].empty ())
    {
        character->mCurrentState = Character::FIND_JOB;
        return;
    }

    if (character->mWorldQuery->isBlocking (character->getTilePosition ()))
        return;

    {

        Job* j = character->mJobQuery->getJob (character->mJobs[jobIndex]);
        if (j)
        {
            std::vector<Ogre::Vector2> path;
            switch ((Job::SubJobs)character->mSubJobs[jobIndex].front ())
            {
                case Job::UPDATE:
                {
                    character->mWorldQuery->findShortestPath (
                        j->getPosition (),
                        character->mTilePosition, path);
                    break;
                }
            }
            if (!path.empty ())
            {
                path.pop_back ();
                // why is ant sometimes already at target position?
                if (!path.empty ())
                {
                    character->savePath (path);
                }
                success = true;
            }
        }
        else
        {
            character->prepareForNewJob (jobIndex);
            character->mCurrentState = Character::FIND_JOB;
            success = false;
        }

        if (!success)
        {
            character->mStateUpdateDelta = 4 + ((float)(rand () % 1000) / 1000);
            character->mCurrentState = Character::IDLE_WALKING;
        }
    }

    if (success)
    {
        character->mCurrentState = Character::REACH_JOB;
    }
    else
    {
        character->prepareForNewJob (jobIndex);
    }
}
template<class T>
void RansackTrait<T>::reachJob (float dt, T* character, ActionReactor* reactor)
{
    int jobIndex = character->mActiveJobIndex;
    if (character->mSubJobs[jobIndex].empty ())
    {
        character->mCurrentState = Character::FIND_JOB;
        return;
    }

    if (!character->mPath.empty ())
    {
        character->mChanged = true;
        bool success = character->walkWaypoint (dt);
        if (!success)
        {
            character->clearPath ();
            character->mCurrentState = Character::FIND_PATH;
        }
        return;
    }
    else
    {
        character->mCurrentState = Character::WORK_JOB;
    }
}
template<class T>
void RansackTrait<T>::workJob (float dt, T* character, ActionReactor* reactor)
{
    int jobIndex = character->mActiveJobIndex;
    if (character->mSubJobs[jobIndex].empty ())
    {
        character->mCurrentState = Character::FIND_JOB;
        return;
    }

    {
        Job::SubJobs job = (Job::SubJobs)character->mSubJobs[jobIndex].front ();
        Job* j = character->mJobQuery->getJob (character->mJobs[jobIndex]);
        if (j)
        {
            bool success = false;
            switch (job)
            {
                case Job::UPDATE:
                    if (!j || j->update (dt, reactor))
                    {
                        //done
                        character->mSubJobs[jobIndex].pop ();
                        character->mCurrentState = Character::FIND_JOB;
                    }
                    success = character->mSubJobs[jobIndex].empty ();
                    break;
            }

            if (success)
            {
                j->finish (reactor);

                // if there are some dependencies remaining, drop them before assigning a new job
                character->prepareForNewJob (jobIndex);
            }

            if (character->mSubJobs[jobIndex].empty ())
            {
                character->mCurrentState = Character::FIND_JOB;
            }
        }
        else
        {
            character->prepareForNewJob (jobIndex);
            character->mCurrentState = Character::FIND_JOB;
        }
    }
}
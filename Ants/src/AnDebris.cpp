#include "AnPCH.h"
#include "AnDebris.h"
using namespace Ants;

Ogre::String Debris::msStandardMesh = "";
Ogre::String Debris::msMaterialName = "";

Debris::Debris (int id, int clientID,
					  const Ogre::Vector2& position,
					  const Ogre::Vector2& tilePosition, 
					  int visibilityRadius)
	: Entity(id, clientID, position, tilePosition, visibilityRadius)
{
}
Debris::Debris (const EntityDesc& desc)
	: Entity(desc)
{
}
Debris::~Debris ()
{
}

Entity::EntityType Debris::getType() const
{
	return Entity::ET_DEBRIS;
}

Ogre::String Debris::getMaterialName() const
{
	return Debris::msMaterialName;
}
Ogre::String Debris::getStandardMesh() const
{
	return Debris::msStandardMesh;
}
		
void Debris::update(float dt, ActionReactor* reactor)
{
}
		
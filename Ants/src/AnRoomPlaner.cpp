#include "AnPCH.h"
#include "AnRoomPlaner.h"
using namespace Ants;

RoomPlaner::RoomPlaner(RoomQuery* rooms, WorldQuery* world, JobQuery* jobs)
{
	mRooms = rooms;
	mWorld = world;
	mJobs = jobs;
}

bool RoomPlaner::findBestSite(Ogre::Vector2& bestSite, RoomGeom geom,
	RoomSize size, Room::RoomType type, int distFromTC)
{
	ThroneChamber* tc = mRooms->getThroneChamber();
	std::vector<Ogre::Vector2> filteredPositions;

	int offset = 0;
	switch (geom)
	{
	case RG_ROUND:
	{
		switch (size)
		{
		case RS_SMALL:	offset = 3; break;
		case RS_MEDIUM: offset = 4; break;
		case RS_LARGE:	offset = 5; break;
		}
		break;
	}
	case RG_RECT:
	{
		switch (size)
		{
		case RS_SMALL:	offset = 3; break;
		case RS_MEDIUM: offset = 3; break;
		case RS_LARGE:	offset = 5; break;
		}
		break;
	}
	}

	std::vector<Ogre::Vector2> path;
	Tile::hexRing(tc->getFirstPivot(), 3 + distFromTC + offset, path);
	for (int i = 0; i < path.size(); ++i)
	{
		Ogre::Vector2 p = path[i];
		std::vector<Ogre::Vector2> mask;
		createMask(p, geom, size, mask);
		if (isValidSite(mask))
		{
			filteredPositions.push_back(p);
		}
	}

	if (filteredPositions.empty())
		return false;

	int score = -std::numeric_limits<int>::max();
	for (int i = 0; i < filteredPositions.size(); ++i)
	{
		Ogre::Vector2 p = filteredPositions[i];
		int s = evalSiteScore(p, geom, size, type);
		if (s >= score)
		{
			score = s;
			bestSite = p;
		}
	}
	return true;
}

void RoomPlaner::createMask(const Ogre::Vector2& origin, RoomGeom geom, RoomSize size,
	std::vector<Ogre::Vector2>& mask)
{
	switch (geom)
	{
	case RG_ROUND:
	{
		switch (size)
		{
		case RS_SMALL:	Tile::hexCircle(origin, 2, mask); break;
		case RS_MEDIUM: Tile::hexCircle(origin, 3, mask); break;
		case RS_LARGE:	Tile::hexCircle(origin, 4, mask); break;
		}
		break;
	}
	case RG_RECT:
	{
		switch (size)
		{
		case RS_SMALL:	Tile::hexRect(origin, 3, 2, mask); break;
		case RS_MEDIUM: Tile::hexRect(origin, 3, 3, mask); break;
		case RS_LARGE:	Tile::hexRect(origin, 4, 5, mask); break;
		}
		break;
	}
	}
}
float RoomPlaner::evalSiteScore(const Ogre::Vector2& origin, RoomGeom geom,
	RoomSize size, Room::RoomType type)
{
	float score;

	switch (type)
	{
	case Room::BROOD_CHAMBER:
		score = -mRooms->distToNearestRoom(Room::THRONE_CHAMBER, origin) + origin.y;
		break;
	case Room::STORAGE_CHAMBER:
		score = -std::min(
			mRooms->distToNearestRoom(Room::BROOD_CHAMBER, origin),
			mRooms->distToNearestRoom(Room::STORAGE_CHAMBER, origin));
		break;
	case Room::FUNGI_CHAMBER:
		score = -mRooms->distToNearestRoom(Room::STORAGE_CHAMBER, origin) + origin.y;
		break;
	}

	return score;
}
bool RoomPlaner::isValidSite(const std::vector<Ogre::Vector2>& mask)
{
	for (int i = 0; i < mask.size(); ++i)
	{
		Ogre::Vector2 u = mask[i];
		if (!mWorld->isRoomPlacable(u)
		|| mJobs->hasJob(u, Job::ROOM_CREATE_JOB)
		|| mJobs->hasJob(u, Job::ROOM_CREATE_JOB_EX))
			return false;
	}
	return true;
}

#include "AnPCH.h"

#include "AnGameApplication.h"

#include "AnMenuState.h"
#include "AnEditorState.h"
#include "AnGameState.h"
#include "AnPauseState.h"

using namespace Ants;

GameApplication::GameApplication()
{
	mAppStateManager = 0;

	new0 Framework();
}
GameApplication::~GameApplication()
{
	SafeDelete0(mAppStateManager);
	Framework* frmwrk = Framework::getSingletonPtr();
	SafeDelete0(frmwrk);
}

void GameApplication::start()
{
	if(!Framework::getSingletonPtr()->init("Antcraft 0.2.1 alpha", 0, 0))
		return;

	Framework::getSingletonPtr()->mLog->logMessage("Game initialized!");

	mAppStateManager = new0 AppStateManager();

	MenuState::create(mAppStateManager, "MenuState");
	EditorState::create(mAppStateManager, "EditorState");
    PauseState::create(mAppStateManager, "PauseState");
	GameState::create(mAppStateManager, "GameState");

	mAppStateManager->changeAppState(mAppStateManager->findByName("MenuState"));
	
	Framework::getSingletonPtr()->mRoot->addFrameListener(mAppStateManager);

    if (Framework::getSingletonPtr()->mRoot->getRenderSystem() != NULL)
    {
		Framework::getSingletonPtr()->mRoot->startRendering();    // start the render loop
    }

}
Ogre::String GameApplication::getWritableFolder() const
{
	return Framework::getSingletonPtr()->WritableFolder;
}

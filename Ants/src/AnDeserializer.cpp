#include "AnPCH.h"
#include "AnDeserializer.h"
#include "AnGameObject.h"
#include "AnIO.h"
using namespace Ants;

Deserializer::Deserializer()
{

}
Deserializer::~Deserializer()
{

}

bool Deserializer::empty() const
{
	return mObjects.empty();
}
GameObject* Deserializer::operator[] (const int index)
{
	return mObjects[index];
}

void Deserializer::fromMemory(int& size, char*& buffer)
{
	mIO.open(size, buffer);

	Ogre::String root = "ROOT";
	GameObject* obj;
	while (!mIO.done())
	{
		Ogre::String name;
		readString(name);
		bool isRoot = (name == root);
		if (isRoot) readString(name);

		obj = (*GameObject::getFactory(name))(*this);
		if (isRoot) mObjects.push_back(obj);
	}

	std::vector<GameObject*>::iterator iter = mOrdered.begin();
	std::vector<GameObject*>::iterator end = mOrdered.end();
	for (/**/; iter != end; ++iter)
	{
		(*iter)->resolvePointers(*this);
	}

	mLinked.clear();
	mOrdered.clear();
	mIO.close();
}
bool Deserializer::fromFile(const Ogre::String& filename)
{
	int size = 0;
	char* buffer = 0;
	if (!IO::load(filename, true, size, buffer))
		return false;

	fromMemory(size, buffer);

	delete1(buffer);
	return true;
}

bool Deserializer::readString(Ogre::String& datum)
{
	int length;
	mIO.read(sizeof(int), &length);
	if (length <= 0)
	{
		datum.clear();
		return false;
	}

	int padding = length % 4;
	if (padding > 0) padding = 4 - padding;

	const char* text = mIO.current();
	datum.assign(text, length);
	mIO.advance(length + padding);
	return true;
}
bool Deserializer::readBool(bool& datum)
{
	unsigned int value;
	if (!mIO.read(sizeof(unsigned int), &value))
		return false;
	datum = (value != 0);
	return true;
}

void Deserializer::readUniqueID(GameObject* obj)
{
	unsigned int uniqueID;
	if (mIO.read(sizeof(unsigned int), &uniqueID))
	{
		mLinked.insert(std::make_pair((uint64_t)uniqueID, obj));
		mOrdered.push_back(obj);
	}
}
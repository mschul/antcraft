#include "AnPCH.h"
#include "AnBombardierBeetle.h"
using namespace Ants;

Ogre::String BombardierBeetle::msStandardMesh = "";
Ogre::String BombardierBeetle::msMaterialName = "";


BombardierBeetle::BombardierBeetle (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition,
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
		: Mob(id, clientID, position, tilePosition, visibilityRadius,
		worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mActiveTrait = 0;

	mAOE = true;
	mRangedDamage = 15;
	mMeleeDamage = 2;
	mAOEDamage = 100;
	mAttackTime = 1.0f;
	mArmor = 0;
}
BombardierBeetle::BombardierBeetle (const EntityDesc& desc)
	: Mob(desc)
{
}
BombardierBeetle::~BombardierBeetle ()
{
}

void BombardierBeetle::selectBehaviour (float dt)
{
    int newTrait = 0;
    if (!mEnemies.empty ())
    {
        newTrait = 1; // warrrior
    }

    if (newTrait != mActiveTrait)
    {
        mCurrentState = FIND_JOB;
        mActiveTrait = newTrait;
#ifdef _DEBUG
        Framework::getSingleton ().mLog->logMessage (
            "bombbeetle " + Ogre::StringConverter::toString (getID ())
            + " switched to mode " + Ogre::StringConverter::toString (mActiveTrait)
            );
#endif
    }
}

Entity::EntityType BombardierBeetle::getType() const
{
	return Entity::ET_BOMBARDIER_BEETLE;
}

Ogre::String BombardierBeetle::getMaterialName() const
{
	return BombardierBeetle::msMaterialName;
}
Ogre::String BombardierBeetle::getStandardMesh() const
{
	return BombardierBeetle::msStandardMesh;
}
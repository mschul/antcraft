#include "AnPCH.h"
#include "AnAIClient.h"
#include "AnProjectile.h"
#include "AnDebris.h"
#include "AnAOE.h"
#include "AnFlattenJob.h"
#include "AnRoomCreateJob.h"
#include "AnRoomCreateJobEx.h"
#include "AnSmallWorker.h"
#include "AnWorker.h"
#include "AnCarrier.h"
#include "AnSoldier.h"
#include "AnKamikaze.h"
#include "AnCopal.h"
#include "AnFaunalFood.h"
#include "AnFloralFood.h"
#include "AnDigResourceJob.h"
#include "AnFetchJob.h"
#include "AnFaunalSource.h"
using namespace Ants;

AIClient::AIClient()
{
	mStatistics = 0;

	mWorld = 0;
	mWorldQuery = 0;

	mEntities = 0;
	mEntityQuery = 0;

	mJobs = 0;
	mJobQuery = 0;

	mRooms = 0;
	mRoomQuery = 0;

	mPheromoneMap = 0;
}
AIClient::~AIClient()
{
	SafeDelete0(mStatistics);
	SafeDelete0(mWorld);
	SafeDelete0(mWorldQuery);
	SafeDelete0(mEntities);
	SafeDelete0(mEntityQuery);
	SafeDelete0(mJobs);
	SafeDelete0(mJobQuery);
	SafeDelete0(mRooms);
	SafeDelete0(mRoomQuery);
	SafeDelete0(mPheromoneMap);
}

void AIClient::init(unsigned int width, unsigned int height)
{
	mStatistics = new0 Statistics(0);

	mWorld = new0 WorldMap();
	mWorld->init(width, height);
	mWorldQuery = new0 WorldQuery(mWorld);

	mEntities = new0 EntityMap();
	mEntities->init(width, height);
	mEntityQuery = new0 EntityQuery(mWorld, mEntities);

	mJobs = new0 JobMap();
	mJobs->init(width, height);
	mJobQuery = new0 JobQuery(mWorld, mJobs);

	mRooms = new0 RoomMap();
	mRooms->init(width, height);
	mRoomQuery = new0 RoomQuery(mRooms, mWorld);

	mPheromoneMap = new0 PheromoneMap();
	mPheromoneMap->init(width, height);
}
void AIClient::init(const Ogre::Rect& area)
{
	mStatistics = new0 Statistics(0);

	mWorld = new0 WorldMap();
	mWorld->init(area);
	mWorldQuery = new0 WorldQuery(mWorld);

	mEntities = new0 EntityMap();
	mEntities->init(area);
	mEntityQuery = new0 EntityQuery(mWorld, mEntities);

	mJobs = new0 JobMap();
	mJobs->init(area);
	mJobQuery = new0 JobQuery(mWorld, mJobs);

	mRooms = new0 RoomMap();
	mRooms->init(area);
	mRoomQuery = new0 RoomQuery(mRooms, mWorld);

	mPheromoneMap = new0 PheromoneMap();
	mPheromoneMap->init(area);
}

void AIClient::createThroneChamber(const Ogre::Vector2& position,
	const std::vector<Ogre::Vector2>& throneCircle)
{
	int roomID = mRooms->assignRoom(Room::THRONE_CHAMBER, throneCircle);

	std::vector<PositionTilePair> positionedTiles;
	mRooms->buildRoom(roomID, position, positionedTiles);

	mWorld->setTiles(positionedTiles, true);

	Ogre::Rect size = mWorld->getSize();
	std::set<int> fixedTile;
	std::vector<PositionTilePair> fixes;
	for (PositionTilePair pair : positionedTiles)
	{
		Ogre::Vector2 p = pair.first;
		for (Ogre::Vector2 d : Tile::getNeighbours(p))
		{
			Ogre::Vector2 position = p + d;
			if (position.x < 0 || position.x >= size.width()
				|| position.y < 0 || position.y >= size.height())
				continue;

			int tile = integer(size.width()*((int)position.y) + position.x);
			Tile* t = mWorld->getTile(position);
			if (t)
			{
				Tile::TileType tp = t->getType();
				if ((tp == Tile::ROUGH || tp == Tile::MUD || tp == Tile::WATER)
					&& fixedTile.find(tile) == fixedTile.end())
				{
					fixedTile.insert(tile);
					fixes.push_back(std::make_pair(position, Tile::msTiles[Tile::DIRT]));
				}
			}
		}
	}
	mWorld->setTiles(fixes, true);
}
void AIClient::createExit(const Ogre::Vector2& exitPosition,
	const std::vector<Ogre::Vector2>& triangle)
{
	int roomID = mRooms->assignExitRoom(triangle);

	std::vector<PositionTilePair> positionedTiles;
	mRooms->buildRoom(roomID, exitPosition, positionedTiles);
	mWorld->setTiles(positionedTiles, true);
}
void AIClient::connect(const Ogre::Vector2& thronePosition,
	const Ogre::Vector2& exitPosition,
	std::vector<Ogre::Vector2>& path)
{
	Tile::hexLine(thronePosition, exitPosition, path);

	std::vector<PositionTilePair> positionedTiles;
	for (Ogre::Vector2 p : path)
		positionedTiles.push_back(std::make_pair(p, Tile::msTiles[Tile::FLOOR]));
	positionedTiles.pop_back();
	mWorld->setTiles(positionedTiles, true);

	Ogre::Rect size = mWorld->getSize();
	std::set<int> fixedTile;
	std::vector<PositionTilePair> fixes;
	for (Ogre::Vector2 p : path)
	{
		for (Ogre::Vector2 d : Tile::getNeighbours(p))
		{
			Ogre::Vector2 position = p + d;
			if (position.x < 0 || position.x >= size.width()
				|| position.y < 0 || position.y >= size.height())
				continue;

			int tile = integer(size.width()*((int)position.y) + position.x);
			Tile::TileType t = mWorld->getTile(position)->getType();
			if ((t == Tile::ROUGH || t == Tile::MUD || t == Tile::WATER)
				&& fixedTile.find(tile) == fixedTile.end())
			{
				fixedTile.insert(tile);
				fixes.push_back(std::make_pair(position, Tile::msTiles[Tile::DIRT]));
			}
		}
	}
	mWorld->setTiles(fixes, true);
}

int AIClient::assignRoom(Room::RoomType type, const Ogre::Vector2& position, Tile* t)
{
	int roomID = -1;
	if (t)
	{
		if (t->isAnthill())
		{
			bool clear = true;
			for (Entity* e : mEntities->get(position))
			{
				if (e->getType() == Entity::ET_DEBRIS && e->getParent() == 0)
				{
					clear = false;
					break;
				}
			}
			if (clear) roomID = mRooms->assignRoom(type, position);
		}
		else if ((t->getType() == Tile::ROUGH || t->getType() == Tile::MUD)
			&& !mJobQuery->hasJob(position, Job::ROOM_CREATE_JOB))
		{

			Room* r = mRoomQuery->findNearestRoom(Room::EXIT_ROOM, position);
			if (r)
			{
				int jobID = mJobs->add(Job::ROOM_CREATE_JOB, position);
				RoomCreateJob* j = (RoomCreateJob*)mJobs->get(jobID);
				j->setRoomType(type);
				j->setTargetRoom(r);
			}
		}
		else if (t->getType() == Tile::DIRT
			&& !mJobQuery->hasJob(position, Job::ROOM_CREATE_JOB_EX))
		{
			Room* r = mRoomQuery->findNearestRoom(Room::EXIT_ROOM, position);
			if (r)
			{
				int jobID = mJobs->add(Job::ROOM_CREATE_JOB_EX, position);
				RoomCreateJobEx* j = (RoomCreateJobEx*)mJobs->get(jobID);
				j->setRoomType(type);
				j->setTargetRoom(r);
			}
		}
	}
	return roomID;
}
void AIClient::assignRoom(Room::RoomType type, const Ogre::Vector2& position)
{
	std::vector<Ogre::Vector2> filteredPositions;

	Room* r = mRooms->getRoom(position);
	if (r && r->getType() == Room::THRONE_CHAMBER)
		return;

	Tile* t = mWorld->getTile(position);
	int roomID = -1;
	if (t)
	{

		int id = assignRoom(type, position, t);
		if (id >= 0) roomID = id;
		if (t->isAnthill())
			filteredPositions.push_back(position);
	}
	if (roomID >= 0)
	{
		std::vector<PositionTilePair> positionedTiles;
		mRooms->buildRoom(roomID, position, positionedTiles);
		mWorld->setTiles(positionedTiles, true);
	}
}

void AIClient::removeJob(Job* job)
{
	mJobs->remove(job->getID());
}
void AIClient::removeJobs(int tile, Job::JobType type)
{
	Ogre::Rect size = mWorld->getSize();
	Ogre::Vector2 position(real(tile % size.width()), real(tile / size.width()));
	Ogre::Vector2 targetPosition;

	mJobs->removeJobs(position, type);
}
void AIClient::createFlattenJob(const Ogre::Vector2& pos)
{
	Tile* t = mWorld->getTile(pos);

	if (t && (t->getType() == Tile::ROUGH || t->getType() == Tile::MUD))
	{
		if (mJobs->get(pos).empty())
		{
			mJobs->add(Job::FLATTEN_JOB, pos);
		}
	}
}
void AIClient::createFlattenJobEnvironment(const Ogre::Vector2& pos)
{
	for (Ogre::Vector2 d : Tile::getNeighbours(pos))
		createFlattenJob(pos + d);
}
void AIClient::removeStrategy(Job* j)
{
	while (j->getCapacity() > 0)
	{
		j->setCapacity(j->getCapacity() - 1);
	}
	removeJob(j);
}

void AIClient::assignWorkers(Room* room)
{
	room->setWorkersRefilled();
	while (room->freeWorkplaces() > 0)
	{
		Entity* ent;
		Ogre::Vector2 tp;
		if (room->getType() == Room::BROOD_CHAMBER && room->workerCount() == 0)
		{
			std::vector<Entity::EntityType> allowedTypes;
			allowedTypes.push_back(Entity::ET_SMALL_WORKER);
			allowedTypes.push_back(Entity::ET_WORKER);
			allowedTypes.push_back(Entity::ET_CARRIER);
			int flags = mEntityQuery->getEntityFlag(allowedTypes);
			mEntityQuery->findNextEntity(room->getFirstPivot(), tp, ent, flags, mID, 50);
		}
		else
		{
			std::vector<Entity::EntityType> allowedTypes;
			allowedTypes.push_back(Entity::ET_SMALL_WORKER);
			allowedTypes.push_back(Entity::ET_WORKER);
			allowedTypes.push_back(Entity::ET_CARRIER);
			int flags = mEntityQuery->getEntityFlag(allowedTypes);
			mEntityQuery->findNextFreelancer(room->getFirstPivot(), tp, ent, flags, mID, 50);
		}

		if (ent)
			room->assignToWorkplace((Ant*)ent);
		else
			break;
	}
}
void AIClient::updateWorkerAssignments(Room::RoomType type)
{
	roomIterator iter, iend;
	iter = mRooms->getRoomIter(type);
	iend = mRooms->getRoomIterEnd(type);
	for (; iter != iend; ++iter)
	{
		Room* room = mRooms->getRoom(*iter);
		if (room->workerRefillRequested())
			assignWorkers(room);
	}
}

void AIClient::update(float dt)
{
	mRooms->update(dt, this);
	updateWorkerAssignments(Room::BROOD_CHAMBER);
	updateWorkerAssignments(Room::FUNGI_CHAMBER);
	updateWorkerAssignments(Room::STORAGE_CHAMBER);

	mEntities->update(dt, mID, this);
}

void AIClient::settle(std::vector<Ogre::Vector2>& locations)
{
}

// -> ACTION REACTOR IMPL
void AIClient::jobMilestoneReached(Job* job)
{
	switch (job->getType())
	{
	case Job::ROOM_CREATE_JOB:
	{
		RoomCreateJob* j = (RoomCreateJob*)job;
		Ogre::Vector2 targetPosition = job->getPosition();

		Room::RoomType t = (Room::RoomType)j->getRoomType();
		assignRoom(t, targetPosition);
		break;
	}
	case Job::ROOM_CREATE_JOB_EX:
	{
		RoomCreateJobEx* j = (RoomCreateJobEx*)job;
		Ogre::Vector2 targetPosition = job->getPosition();

		Room::RoomType t = (Room::RoomType)j->getRoomType();
		assignRoom(t, targetPosition);
		break;
	}
	}
}
void AIClient::jobFinished(Job* job)
{
	switch (job->getType())
	{
	case Job::ROOM_CREATE_JOB:
	{
		RoomCreateJob* j = (RoomCreateJob*)job;
		Ogre::Vector2 targetPosition = j->getPosition();

		Ogre::Rect size = mWorld->getSize();
		int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
		removeJobs(tile, Job::ROOM_CREATE_JOB);
	}
		break;
	case Job::ROOM_CREATE_JOB_EX:
	{
		RoomCreateJobEx* j = (RoomCreateJobEx*)job;
		Ogre::Vector2 targetPosition = j->getPosition();

		Ogre::Rect size = mWorld->getSize();
		int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
		removeJobs(tile, Job::ROOM_CREATE_JOB_EX);
	}
		break;
	case Job::DIG_JOB:
	{
		DigJob* j = (DigJob*)job;
		Ogre::Vector2 targetPosition = j->getPosition();
		mWorld->setTile(targetPosition, Tile::msTiles[Tile::ROUGH], true);

		for (Ogre::Vector2 d : Tile::getNeighbours(targetPosition))
		{
			Ogre::Vector2 p = targetPosition + d;
			Tile* t = mWorld->getTile(p);
			if (t && t->isOverWorld())
			{
				std::vector<Ogre::Vector2> exitTriangle;
				mServer->createExit(p, exitTriangle);
				createExit(p, exitTriangle);
			}
		}

		Ogre::Rect size = mWorld->getSize();
		int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
		removeJobs(tile, Job::DIG_JOB);

		bool createFlatten = false;
		for (Ogre::Vector2 d : Tile::getNeighbours(targetPosition))
		{
			Ogre::Vector2 p = targetPosition + d;
			Tile* t = mWorld->getTile(p);
			if (t && t->isAnthill())
			{
				createFlatten = true;
				break;
			}
		}
		if (createFlatten) createFlattenJob(targetPosition);
	}
		break;
	case Job::FLATTEN_JOB:
	{
		FlattenJob* j = (FlattenJob*)job;
		Ogre::Vector2 targetPosition = j->getPosition();
		mWorld->setTile(targetPosition, Tile::msTiles[Tile::FLOOR], true);

		// this is expensive as fuck (no static geometry instead?)
		Ogre::Rect size = mWorld->getSize();
		int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
		removeJobs(tile, Job::FLATTEN_JOB);

		// create debris
		Ogre::Vector2 hex;
		Tile::toHexagonalPosition(targetPosition, hex);
		Entity* ent = new0 Debris(mServer->createEntityID(), mID, hex, targetPosition, 0);
		mEntities->add(ent, true);

		Room* r = mRoomQuery->findNearestRoom(Room::EXIT_ROOM, targetPosition);
		if (r)
		{
			int jobID = mJobs->add(Job::FETCH_JOB, targetPosition);
			FetchJob* job = (FetchJob*)mJobs->get(jobID);
			job->setFetchEntity(ent);
			job->setTargetRoom(r);
		}

		createFlattenJobEnvironment(targetPosition);
	}
		break;
	case Job::GOTO_JOB:
		removeJob(job);
		break;

	case Job::DIG_RESOURCE_JOB:
	{
		DigResourceJob* j = (DigResourceJob*)job;
		Ogre::Vector2 targetPosition = j->getPosition();

		std::vector<Entity*> ents;

		// this is expensive as fuck (no static geometry instead?)
		int roomID = j->getResourceID();
		Room* r = mRooms->getRoom(roomID);
		if (r)
		{
			if (r->getType() == Room::FLORAL_SOURCE)
			{
				// create food
				{
					Ogre::Vector2 hex;
					Tile::toHexagonalPosition(targetPosition, hex);
					Entity* ent = new0 FloralFood(mServer->createEntityID(),
						mID, hex, targetPosition, 0);
					mEntities->add(ent, true);
					ents.push_back(ent);
				}
				mWorld->setTile(targetPosition, Tile::msTiles[Tile::HARVESTED_GRASS], true);
			}
			else if (r->getType() == Room::COPAL_SOURCE)
			{
				// create food
				{
					Ogre::Vector2 hex;
					Tile::toHexagonalPosition(targetPosition, hex);
					Entity* ent = new0 Copal(mServer->createEntityID(),
						mID, hex, targetPosition, 0);
					mEntities->add(ent, true);
					ents.push_back(ent);
				}
				mWorld->setTile(targetPosition, Tile::msTiles[Tile::TREE], true);
			}
			else
			{
				// create food
				{
					Ogre::Vector2 hex;
					Tile::toHexagonalPosition(targetPosition, hex);
					Entity* ent = new0 FaunalFood(mServer->createEntityID(),
						mID, hex, targetPosition, 0);
					mEntities->add(ent, true);
					ents.push_back(ent);
				}

				FaunalSource* source = (FaunalSource*)r;

				Character* resource = source->getSource();
				mEntities->remove(resource);

				source->setSource(0);
			}
			r->placeManual(ents, targetPosition);
		}


		Ogre::Rect size = mWorld->getSize();
		int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
		removeJobs(tile, Job::DIG_RESOURCE_JOB);
	}
		break;
	}
}
void AIClient::setExitTiles(const std::vector<std::pair<Ogre::Vector2, Tile*>>& tiles)
{
	std::vector<PositionTilePair> border;
	for (PositionTilePair p : tiles)
	{
		if (p.second->isBlocking())
		{
			for (Ogre::Vector2 d : Tile::getNeighbours(p.first))
			{
				Ogre::Vector2 pos = p.first + d;
				Tile* neighbour = mWorld->getTile(pos);
				if (neighbour && neighbour->isOverWorld() && neighbour->isBlocking())
				{
					border.push_back(std::make_pair(pos, Tile::msTiles[Tile::GROUND]));
				}
			}

			// beam entities out of tight spot
			std::vector<Entity*> ents = mEntities->get(p.first);
			if (!ents.empty())
			{
				Ogre::Vector2 beamTo = p.first;
				for (Ogre::Vector2 delta : Tile::getNeighbours(p.first))
				{
					beamTo = delta + p.first;
					if (!mWorld->isBlocking(beamTo))
						break;
				}
				for (Entity* e : ents)
				{
					e->setTilePosition(beamTo);
					Ogre::Vector2 hex;
					Tile::toHexagonalPosition(beamTo, hex);
					e->setPosition(hex);
					mEntities->change(e, true);
				}
			}

			// kill all jobs residing there
			for (Job* j : mJobs->get(p.first))
			{
				if (j->getType() == Job::HOLD_JOB || j->getType() == Job::DEFEND_JOB)
					removeStrategy(j);
				else
					removeJob(j);
			}
		}
	}
	mWorld->setTiles(border, true);
	mWorld->setTiles(tiles, true);
}
void AIClient::setTile(const Ogre::Vector2& position, int type)
{
	mWorld->setTile(position, Tile::msTiles[type], true);
}
void AIClient::createResource(const Ogre::Vector2& position, int roomType,
	Character* resource)
{
	int id = mRooms->assignRoom((Room::RoomType)roomType, position);

	if (id >= 0)
	{
		std::vector<PositionTilePair> positionedTiles;
		mRooms->buildRoom(id, position, positionedTiles);
		mWorld->setTiles(positionedTiles, true);

		if (resource && roomType == Room::FAUNAL_SOURCE)
		{
			FaunalSource* source = (FaunalSource*)mRooms->getRoom(id);
			source->setSource(resource);
		}
	}
}
void AIClient::removeResource(Room* r)
{
	mRooms->remove(r->getID());
}
Egg* AIClient::spawnEgg(const Ogre::Vector2& position)
{
	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(position, hex);
	Egg* egg = new0 Egg(mServer->createEntityID(), mID, hex, position, 5);
	mEntities->add(egg, true);
	return egg;
}
Grub* AIClient::hatchEgg(Egg* egg)
{
	if (egg->getParent() != 0)
		((Ant*)egg->getParent())->drop(egg);
	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(egg->getTilePosition(), hex);
	Grub* grub = new0 Grub(mServer->createEntityID(), mID,
		hex, egg->getTilePosition(),
		5, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
	mEntities->add(grub, true);
	mEntities->remove(egg, true);
	return grub;
}
void AIClient::unitDied(Character* unit, Attack* a)
{
	switch (unit->getType())
	{
	case Entity::ET_SMALL_WORKER:
		mStatistics->SmallWorker.Alive--;
		break;
	case Entity::ET_WORKER:
		mStatistics->Worker.Alive--;
		break;
	case Entity::ET_CARRIER:
		mStatistics->Carrier.Alive--;
		break;
	case Entity::ET_SOLDIER:
		mStatistics->Soldier.Alive--;
		break;
	case Entity::ET_KAMIKAZE:
		mStatistics->Kamikaze.Alive--;
		break;
	}

	if (a)
	{

	}
}
Ant* AIClient::spawnAnt(Grub* grub, int type)
{
	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(grub->getTilePosition(), hex);

	Ant* ant = 0;
	switch ((Entity::EntityType)type)
	{
	case Entity::ET_SMALL_WORKER:
		ant = new0 SmallWorker(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->SmallWorker.Produced++;
		mStatistics->SmallWorker.Alive++;
		break;
	case Entity::ET_WORKER:
		ant = new0 Worker(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->Worker.Produced++;
		mStatistics->Worker.Alive++;
		break;
	case Entity::ET_CARRIER:
		ant = new0 Carrier(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->Carrier.Produced++;
		mStatistics->Carrier.Alive++;
		break;
	case Entity::ET_SOLDIER:
		ant = new0 Soldier(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->Soldier.Produced++;
		mStatistics->Soldier.Alive++;
		break;
	case Entity::ET_KAMIKAZE:
		ant = new0 Kamikaze(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->Kamikaze.Produced++;
		mStatistics->Kamikaze.Alive++;
		break;
	}
	mEntities->remove(grub, true);
	mEntities->add(ant, true);
	return ant;
}
Entity* AIClient::spawn(const Ogre::Vector2& position, int type)
{
	if (mWorld->isBlocking(position))
		return 0;

	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(position, hex);

	Entity* ent = 0;
	switch ((Entity::EntityType)type)
	{
	case Entity::ET_DEBRIS:
		ent = new0 Debris(mServer->createEntityID(), mID, hex, position, 0);
		break;
	case Entity::ET_FAUNAL_FOOD:
		ent = new0 FaunalFood(mServer->createEntityID(), mID, hex, position, 0);
		break;
	}
	mEntities->add(ent, true);
	return ent;
}
void AIClient::removeEntities(const std::vector<Entity*>& ents)
{
	mEntities->remove(ents, true);
}
void AIClient::aoeAttack(int sourceID, float attackPoints, const Ogre::Vector2& position, float radius)
{
	mAOEs.push_back(new0 AOE(sourceID, mID, attackPoints, position, radius));
}
void AIClient::meleeAttack(float attackPoints, Character* source, Entity* target)
{
	mAttacks.push_back(new0 Attack(attackPoints, source->getID(), target->getID(),
		source->getClientID(), target->getClientID()));
}
void AIClient::rangedAttack(float attackPoints, Character* source, Entity* target)
{
	Projectile* proj = new0 Projectile(mServer->createEntityID(), attackPoints,
		source, target->getPosition(), target->getTilePosition());
	mEntities->add(proj, true);
}
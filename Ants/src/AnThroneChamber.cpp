#include "AnPCH.h"
#include "AnThroneChamber.h"
#include "AnAnt.h"
using namespace Ants;

ThroneChamber::ThroneChamber (int id)
	: Room(id, 1)
{
	mWorkerBehaviour = PROTECT;
}
ThroneChamber::~ThroneChamber ()
{
}
		
Room::RoomType ThroneChamber::getType() const
{
	return Room::THRONE_CHAMBER;
}
bool ThroneChamber::isDistructible() const
{
	return false;
}

void ThroneChamber::build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	{
		reset(workers, se);
		mPivot.clear();
	}

	float minX = std::numeric_limits<float>::max();
	float maxX = std::numeric_limits<float>::min();
	float minY = std::numeric_limits<float>::max();
	float maxY = std::numeric_limits<float>::min();
	for(Ogre::Vector2 position : positions)
	{
		if(minX > position.x) minX = position.x;
		if(maxX < position.x) maxX = position.x;
		if(minY > position.y) minY = position.y;
		if(maxY < position.y) maxY = position.y;
	}

	mPivot.push_back(pivot);

	mSlots.resize(3);
	for(Ogre::Vector2 position : positions)
	{
		Ogre::Vector2 delta = position-pivot;

		int dx = fabsf(delta.x);
		int dy = fabsf(delta.y);

		Tile::TileType type = Tile::ROOM_FLOOR;
		if(dy == 0 && dx == 0) type = Tile::QUEEN;

		tiles.push_back(std::make_pair(position, Tile::msTiles[type]));

		if(position.y == minY+1 && fabsf(delta.x) < 2)
		{
			slot* s = new0 slot;
			s->items.resize(mSlotCapacity);
			for (int i = 0; i < mSlotCapacity; ++i)
			{
				s->items[i].ent = 0;
				s->items[i].reservedFor = 0;
			}
			s->reserved = 0;
			s->capacity = mSlotCapacity;
			s->position = position;
			mSlots[delta.x+1] = s;
		}
	}
	mNextSpawnDelta = 30;

	while(!workers.empty() && mFreeWorkplaces > 0)
	{
		assignToWorkplace(workers.back());
		workers.pop_back();
	}
}
void ThroneChamber::update(float dt, ActionReactor* reactor)
{
    Room::update (dt, reactor);
	mNextSpawnDelta -= dt;
	
	if(mNextSpawnDelta < 0)
	{
		mNextSpawnDelta = 30;
		for(slot* s : mSlots)
		{
			if(s->capacity > 0)
			{
				Entity* item = reactor->spawnEgg(s->position);
				s->items[0].ent = item;
				s->capacity--;
				break;
			}
		}
	}
}
void ThroneChamber::getJob(std::vector<WorkplaceSubjobs>& jobs)
{
	jobs.push_back(Room::FETCH_FLORAL_FOOD);
	jobs.push_back(Room::FEED);
}

void ThroneChamber::setWorkerBehaviour(WorkerBehaviour wb)
{
	mWorkerBehaviour = wb;
}
ThroneChamber::WorkerBehaviour ThroneChamber::getWorkerBehaviour() const
{
	return mWorkerBehaviour;
}
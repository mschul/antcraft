#include "AnPCH.h"
#include "AnWaterMap.h"
using namespace Ants;

WaterMap::WaterMap (WorldMap* world)
{
	mWorld = world;
	mBuilt = false;
	mWaterCount = 0;
}
WaterMap::~WaterMap ()
{
}

void WaterMap::build()
{
	mPressure.clear();
	Ogre::Rect size = mWorld->getSize();
	unsigned int width = size.width();
	unsigned int height = size.height();
	mPressure.resize(width*height);
	mCurrentInactive.resize(width);
	mNextInactive.resize(width);
	for(unsigned int x = 0; x < width; ++x)
	{
		mNextInactive[x] = false;
		mCurrentInactive[x] = false;
	}
	
	for(int y = height-1; y >= 0; --y)
	{
		for(unsigned int x = 0; x < width; ++x)
		{
			Ogre::Vector2 pos(x, y);
			Tile* t = mWorld->getTile(pos);
			int tileIndex = y*width + x;

			if(t && t->getType() == Tile::WATER)
			{
				mWaterCount++;
				// calculate pressure
				int pressure = 0;
				std::vector<Ogre::Vector2> deltas = Tile::getNeighbours(pos);
				Ogre::Vector2 n = pos + deltas[Tile::TOP_LEFT];
				if(n.x >= 0 && n.x < width && n.y >= 0 && n.y < height)
				{
					int neighbourIndex = n.y*width + n.x;
					pressure = mPressure[neighbourIndex] + 1;
				}

				n = pos + deltas[Tile::TOP_RIGHT];
				if(n.x >= 0 && n.x < width && n.y >= 0 && n.y < height)
				{
					int neighbourIndex = n.y*width + n.x;
					pressure = mPressure[neighbourIndex] + 1;
				}
				mPressure[tileIndex] = pressure;
			}
			else
			{
				mPressure[tileIndex] = -1;
			}
		}
	}

	mBuilt = true;
}
void WaterMap::remove(const std::vector<Ogre::Vector2>& tiles)
{
	Ogre::Rect size = mWorld->getSize();
	unsigned int width = size.width();
	for (Ogre::Vector2 p : tiles)
	{
		int tileIndex = p.y*width + p.x;
		mPressure[tileIndex] = -1;
	}
	mWaterCount -= tiles.size();
}
void WaterMap::remove(const Ogre::Vector2& tile)
{
	Ogre::Rect size = mWorld->getSize();
	unsigned int width = size.width();
	int tileIndex = tile.y*width + tile.x;
	mPressure[tileIndex] = -1;
	mWaterCount--;
}

Tile::Direction WaterMap::fallWater(int index, std::vector<PositionTilePair>& tiles,
						bool* boundaries, bool* water, int* pressures)
{
	Ogre::Rect size = mWorld->getSize();
	unsigned int width = size.width();
	unsigned int height = size.height();

	Ogre::Vector2 p(index % width, index / width);
	std::vector<Ogre::Vector2> deltas = Tile::getNeighbours(p);
		
	if(boundaries[Tile::BOTTOM_LEFT] || water[Tile::BOTTOM_LEFT])
		return Tile::BOTTOM_RIGHT;
	return Tile::BOTTOM_LEFT;
}
Tile::Direction WaterMap::freeFallWater(int index, std::vector<PositionTilePair>& tiles,
							 bool* boundaries, bool* water, int* pressures)
{
	Ogre::Rect size = mWorld->getSize();
	unsigned int width = size.width();
	unsigned int height = size.height();

	Ogre::Vector2 p(index % width, index / width);
	std::vector<Ogre::Vector2> deltas = Tile::getNeighbours(p);

	bool pressLeft = water[Tile::TOP_LEFT];
	bool pressRight = water[Tile::TOP_RIGHT];
		

	if(pressRight && pressLeft)
	{
		pressRight = pressures[Tile::TOP_RIGHT] > pressures[Tile::TOP_LEFT];
	}
	if(!pressRight && !pressLeft)
	{
		pressRight = ((int)p.y) & 1;
	}
	
	if(pressRight)
		return Tile::BOTTOM_RIGHT;
	return Tile::BOTTOM_LEFT;
}
void WaterMap::flowWater(int index, std::vector<PositionTilePair>& tiles)
{
	Ogre::Rect size = mWorld->getSize();
	unsigned int width = size.width();
	unsigned int height = size.height();

	Ogre::Vector2 p(index % width, index / width);
	std::vector<Ogre::Vector2> deltas = Tile::getNeighbours(p);
		
	int currentPressure = mPressure[index];

	bool boundaries[6];
	bool water[6];
	int pressures[6];
	for(int i = 0; i < 6; ++i)
	{
		boundaries[i] = true;
		water[i] = false;
		pressures[i] = -1;

		Ogre::Vector2 n = p + deltas[i];
		if(n.x >= 0 && n.x < width && n.y >= 0 && n.y < height)
		{
			int neighbourIndex = n.y*width + n.x;

			pressures[i] = mPressure[neighbourIndex];

			Tile* t = mWorld->getTile(n);
			if(mPressure[neighbourIndex] < 0 && t && !t->isBlocking())
				boundaries[i] = false;

			if(mPressure[neighbourIndex] >= 0)
				water[i] = true;
		}
	}
		
	Tile::Direction flow = Tile::NONE;

	if(!boundaries[Tile::BOTTOM_LEFT] && !boundaries[Tile::BOTTOM_RIGHT])
	{
		flow = freeFallWater(index, tiles, boundaries, water, pressures);
	}
	else if(!boundaries[Tile::BOTTOM_LEFT] || !boundaries[Tile::BOTTOM_RIGHT])
	{
		flow = fallWater(index, tiles, boundaries, water, pressures);
	}
	else
	{
		if(!boundaries[Tile::LEFT] && currentPressure > 0)
			flow = Tile::LEFT;
		else if(!boundaries[Tile::RIGHT] && currentPressure > 0)
			flow = Tile::RIGHT;
	}
	
	switch(flow)
	{
	case Tile::LEFT:
	case Tile::RIGHT:
		{
			Ogre::Vector2 n = p + deltas[flow];
			mCurrentInactive[n.x] = true;
		}
		break;
	case Tile::BOTTOM_RIGHT:
	case Tile::BOTTOM_LEFT:
		{
			Ogre::Vector2 n = p + deltas[flow];
			mNextInactive[n.x] = true;
		}
		break;
	}

	switch(flow)
	{
	case Tile::LEFT:
	case Tile::RIGHT:
	case Tile::TOP_RIGHT:
	case Tile::TOP_LEFT:
	case Tile::BOTTOM_RIGHT:
	case Tile::BOTTOM_LEFT:
		{
			Ogre::Vector2 n = p + deltas[flow];
			if(n.x >= 0 && n.x < width && n.y >= 0 && n.y < height)
			{
				int i = n.y * width + n.x;
				tiles.push_back(std::make_pair(n, Tile::msTiles[Tile::WATER]));
				tiles.push_back(std::make_pair(p, Tile::msTiles[Tile::MUD]));
			
				mPressure[index] = -1;
				
				assert(mPressure[i] < 0);
				std::vector<Ogre::Vector2> d = Tile::getNeighbours(n);
				Ogre::Vector2 n1 = n + d[Tile::TOP_RIGHT];
				int index1 = n1.y * width + n1.x;
				Ogre::Vector2 n2 = n + d[Tile::TOP_LEFT];
				int index2 = n2.y * width + n2.x;

				mPressure[i] = (mPressure[index1] > mPressure[index2]) ?
					mPressure[index1] + 1 : mPressure[index2] + 1;
				assert(mPressure[i] >= 0);
			}
		}
		break;
	case Tile::NONE:
		for(int i = 0; i < 6; ++i)
		{
			if(pressures[i] + deltas[i].y > currentPressure)
				currentPressure = pressures[i] + deltas[i].y;
		}
		assert(mPressure[index] >= 0);
		mPressure[index] = currentPressure;
		assert(mPressure[index] >= 0);
		break;
	}
}

void WaterMap::update(std::vector<PositionTilePair>& tiles)
{
	if(!mBuilt)
		return;
	
	Ogre::Rect size = mWorld->getSize();
	unsigned int width = size.width();
	unsigned int height = size.height();
	
	for(int y = height-1; y >= 0; --y)
	{
		for(unsigned int x = 0; x < width; ++x)
		{
			int tileIndex = y*width + x;
			if(mPressure[tileIndex] >= 0)
			{
				Ogre::Vector2 p(tileIndex % width, tileIndex / width);
				std::vector<Ogre::Vector2> deltas = Tile::getNeighbours(p);

				for(Ogre::Vector2 d : Tile::getNeighbours(p))
				{
					Ogre::Vector2 n = p + d;
					if(n.x >= 0 && n.x < width && n.y >= 0 && n.y < height)
					{
						Tile* t = mWorld->getTile(n);
						int neighbourIndex = n.y*width + n.x;

						// filter blocking non-water
						if(mPressure[neighbourIndex] < 0 && (!t || t->isBlocking()))
							continue;

						// react on pressure disturbance
						if(mPressure[tileIndex] != mPressure[neighbourIndex] + d.y)
						{
							if(!mCurrentInactive[x])
							{
								flowWater(tileIndex, tiles);
							}
							else
							{
								mCurrentInactive[x] = false;
							}
							break;
						}
					}
				}
			}

		}
		std::swap(mCurrentInactive, mNextInactive);
	}
}
#include "AnPCH.h"
#include "AnGrub.h"
using namespace Ants;

Ogre::String Grub::msStandardMesh = "";
Ogre::String Grub::msMaterialName = "";


Grub::Grub (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition,
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
		: Mob(id, clientID, position, tilePosition, visibilityRadius,
		worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mSpawnDelta = 30;

	mActiveTrait = 0;

	mRanged = false;
	mRangedDamage = 0;
	mMeleeDamage = 2;
	mArmor = 0;
}
Grub::Grub (const EntityDesc& desc)
	: Mob(desc)
{
}
Grub::~Grub ()
{
}
		
Entity::EntityType Grub::getType() const
{
	return Entity::ET_GRUB;
}

void Grub::selectBehaviour (float dt)
{
    if (0 != mActiveTrait)
    {
        mCurrentState = FIND_JOB;
        mActiveTrait = 0;
#ifdef _DEBUG
        Framework::getSingleton ().mLog->logMessage (
            "grub " + Ogre::StringConverter::toString (getID ())
            + " switched to mode " + Ogre::StringConverter::toString (mActiveTrait)
            );
#endif
    }
}


Ogre::String Grub::getMaterialName() const
{
	return Grub::msMaterialName;
}
Ogre::String Grub::getStandardMesh() const
{
	return Grub::msStandardMesh;
}

bool Grub::spawn(float dt)
{
	mSpawnDelta -= dt;
	if(mSpawnDelta < 0)
		return true;
	return false;
}
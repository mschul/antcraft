#include "AnPCH.h"
#include "AnDigJob.h"
#include "AnClient.h"
using namespace Ants;

DigJob::DigJob (int id, const Ogre::Vector2& position, double duration)
	: Job(id, 1, position)
{
	mT = duration;
}
DigJob::~DigJob ()
{
}

Job::JobType DigJob::getType() const
{
	return Job::DIG_JOB;
}

bool DigJob::update(double dt, ActionReactor* reactor)
{
	mT -= dt;
	if(mT <= 0)
	{
		return true;
	}
	return false;
}
void DigJob::getSubJobs(std::vector<Job::SubJobs>& jobs) const
{
	jobs.push_back(Job::UPDATE);
}

#include "AnPCH.h"
#include "AnFloralSource.h"
#include "AnAnt.h"
using namespace Ants;

FloralSource::FloralSource (int id)
	: Room(id, 1)
{
	mGrowthDelta = 90;
	mRegrowing = false;
}
FloralSource::~FloralSource ()
{
}
		
Room::RoomType FloralSource::getType() const
{
	return Room::FLORAL_SOURCE;
}
bool FloralSource::isResource() const
{
	return true;
}
bool FloralSource::isDistructible() const
{
	return false;
}

void FloralSource::build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	{
		reset(workers, se);
		mPivot.clear();
	}
	mGrowingSlots.clear();
	mPivot.push_back(pivot);

	float minX = std::numeric_limits<float>::max();
	float maxX = std::numeric_limits<float>::min();
	float minY = std::numeric_limits<float>::max();
	float maxY = std::numeric_limits<float>::min();
	for(Ogre::Vector2 position : positions)
	{
		if(minX > position.x) minX = position.x;
		if(maxX < position.x) maxX = position.x;
		if(minY > position.y) minY = position.y;
		if(maxY < position.y) maxY = position.y;
	}
	int width = (maxX-minX+1);
	std::map<int, Entity*> positionedEnts;
	for(Entity* e : se)
	{
		Ogre::Vector2 p = e->getTilePosition();
		const int i = (p.y - minY)*width + (p.x - minX);
		positionedEnts.insert(std::make_pair(i, e));
	}

	for(Ogre::Vector2 position : positions)
	{
		slot* s = new0 slot;
		s->reserved = 0;
		s->capacity = mSlotCapacity;
		s->position = position;

		s->items.resize(mSlotCapacity);
		for (int i = 0; i < mSlotCapacity; ++i)
		{
			s->items[i].ent = 0;
			s->items[i].reservedFor = 0;
		}

		const int i = (position.y - minY)*width + (position.x - minX);
		std::map<int, Entity*>::iterator iter = positionedEnts.find(i);
		if(iter != positionedEnts.end())
		{
			Entity* e = iter->second;
			s->items[mSlotCapacity - s->capacity].ent = e;
			s->capacity--;
			Ogre::Vector2 hp;
			Tile::toHexagonalPosition(position, hp);
			e->setPosition(hp);
			e->setTilePosition(position);
			se.pop_back();
		}
		mSlots.push_back(s);
		mGrowingSlots.push_back(false);
		tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::GRASS]));
	}

	for(Entity* e : se)
	{
		slot* s = new0 slot;
		s->reserved = 0;
		s->capacity = 0;
		s->items.resize(mSlotCapacity);
		for (int i = 0; i < mSlotCapacity; ++i)
		{
			s->items[i].ent = 0;
			s->items[i].reservedFor = 0;
		}
		s->position = e->getTilePosition();
		s->items[mSlotCapacity - s->capacity].ent = e;
		mSlots.push_back(s);
		mGrowingSlots.push_back(false);
	}
}
void FloralSource::update(float dt, ActionReactor* reactor)
{
	if(mRegrowing)
	{
		mGrowthDelta -= dt;
		if(mGrowthDelta <= 0)
		{
			mGrowthDelta = 90;

			int i = 0; 
			for(; i < (int)mSlots.size(); ++i) if(mGrowingSlots[i]) break;
			if(i < (int)mSlots.size())
			{
				slot* s = mSlots[i];
				reactor->setTile(s->position, Tile::GRASS);
				mGrowingSlots[i] = false;
			}
			else
			{
				mRegrowing = false;
			}
		}
	}
}
bool FloralSource::slotEmpty (const Ogre::Vector2& position) const
{
    for (int i = 0; i < (int)mSlots.size (); ++i)
    {
        slot* s = mSlots[i];
        if (s->position == position)
        {
            if (s->capacity != mSlotCapacity)
                return false;
            else return !mGrowingSlots[i];
        }
    }

    return false;
}
void FloralSource::getJob(std::vector<WorkplaceSubjobs>& jobs)
{
}
void FloralSource::grow(const Ogre::Vector2& position)
{
	for(int i = 0; i < (int)mSlots.size(); ++i)
	{
		slot* s = mSlots[i];
		if(s->position == position)
		{
			mRegrowing = true;
			mGrowingSlots[i] = true;
		}
	}
}
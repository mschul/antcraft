#include "AnPCH.h"
#include "AnRoomCreateJobEx.h"
#include "AnClient.h"
using namespace Ants;

RoomCreateJobEx::RoomCreateJobEx(int id, const Ogre::Vector2& position, double duration)
: Job(id, 1, position)
{
	mT = duration;
	mRoomType = Room::NONE;
	mFetchEnt = 0;
	mTargetRoom = 0;
}
RoomCreateJobEx::~RoomCreateJobEx()
{
}

Job::JobType RoomCreateJobEx::getType() const
{
	return Job::ROOM_CREATE_JOB_EX;
}

bool RoomCreateJobEx::update(double dt, ActionReactor* reactor)
{
	mT -= dt;
	if (mT <= 0)
	{
		if (!mFetchEnt)
		{
			reactor->setTile(mPosition, Tile::FLOOR);
			mFetchEnt = reactor->spawn(mPosition, Entity::ET_DEBRIS);
		}
		return true;
	}
	return false;
}
void RoomCreateJobEx::getSubJobs(std::vector<Job::SubJobs>& jobs) const
{
	if (!mFetchEnt)
		jobs.push_back(Job::UPDATE);
	jobs.push_back(Job::FETCH);
	jobs.push_back(Job::WALK_TO_TARGET);
	jobs.push_back(Job::PLACE);
}

Entity* RoomCreateJobEx::getFetchEntity() const
{
	return mFetchEnt;
}
void RoomCreateJobEx::setFetchEntity(Entity* fetchEnt)
{
	mFetchEnt = fetchEnt;
}

Room* RoomCreateJobEx::getTargetRoom() const
{
	return mTargetRoom;
}
void RoomCreateJobEx::setTargetRoom(Room* targetRoom)
{
	mTargetRoom = targetRoom;
}

Room::RoomType RoomCreateJobEx::getRoomType() const
{
	return mRoomType;
}
void RoomCreateJobEx::setRoomType(Room::RoomType roomType)
{
	mRoomType = roomType;
}

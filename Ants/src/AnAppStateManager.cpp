#include "AnPCH.h"
#include "AnAppStateManager.h"
using namespace Ants;

#include <OgreWindowEventUtilities.h>

AppStateManager::AppStateManager()
{
	mShutdown = false;
}
AppStateManager::~AppStateManager()
{
	state_info si;

    while(!mActiveStateStack.empty())
	{
		mActiveStateStack.back()->exit();
		mActiveStateStack.pop_back();
	}

	while(!mStates.empty())
	{
		si = mStates.back();
        si.state->destroy();
        mStates.pop_back();
	}
}

void AppStateManager::manageAppState(Ogre::String stateName, AppState* state)
{
	try
	{
		state_info new_state_info;
		new_state_info.name = stateName;
		new_state_info.state = state;
		mStates.push_back(new_state_info);
	}
	catch(std::exception& e)
	{
		delete state;
		throw Ogre::Exception(Ogre::Exception::ERR_INTERNAL_ERROR, "Error while trying to manage a new AppState\n" + Ogre::String(e.what()), "AppStateManager.cpp (39)");
	}
}

AppState* AppStateManager::findByName(Ogre::String stateName)
{
	for(state_info info : mStates)
	{
		if(info.name==stateName)
			return info.state;
	}

	return 0;
}

bool AppStateManager::frameStarted(const Ogre::FrameEvent& evt)
{
	return true;
}
bool AppStateManager::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
	if(Framework::getSingletonPtr()->mRenderWnd->isClosed() || mShutdown) 
		return false;

	CEGUI::System::getSingleton().injectTimePulse(evt.timeSinceLastFrame);
	
	Ogre::WindowEventUtilities::messagePump();
		
	Framework::getSingletonPtr()->mKeyboard->capture();
	if(!Framework::getSingletonPtr()->mMouseOff)
		Framework::getSingletonPtr()->mMouse->capture();

	mActiveStateStack.back()->update(evt.timeSinceLastFrame);
		
	return true;
}
bool AppStateManager::frameEnded(const Ogre::FrameEvent& evt)
{
	return true;
}

void AppStateManager::changeAppState(AppState* state)
{
	if(!mActiveStateStack.empty())
	{
		mActiveStateStack.back()->exit();
		mActiveStateStack.pop_back();
	}

	mActiveStateStack.push_back(state);
	init(state);
	mActiveStateStack.back()->enter();
}
bool AppStateManager::pushAppState(AppState* state)
{
	if(!mActiveStateStack.empty())
	{
		if(!mActiveStateStack.back()->pause())
			return false;
	}

	mActiveStateStack.push_back(state);
	init(state);
	mActiveStateStack.back()->enter();

	return true;
}
void AppStateManager::popAppState()
{
	if(!mActiveStateStack.empty())
	{
		mActiveStateStack.back()->exit();
		mActiveStateStack.pop_back();
	}

	if(!mActiveStateStack.empty())
	{
		init(mActiveStateStack.back());
		mActiveStateStack.back()->resume();
	}
    else
		shutdown();
}
void AppStateManager::popAllAndPushAppState(AppState* state)
{
    while(!mActiveStateStack.empty())
    {
        mActiveStateStack.back()->exit();
        mActiveStateStack.pop_back();
    }

    pushAppState(state);
}
void AppStateManager::pauseAppState()
{
	if(!mActiveStateStack.empty())
	{
		mActiveStateStack.back()->pause();
	}

	if(mActiveStateStack.size() > 2)
	{
		init(mActiveStateStack.at(mActiveStateStack.size() - 2));
		mActiveStateStack.at(mActiveStateStack.size() - 2)->resume();
	}
}
void AppStateManager::shutdown()
{
	mShutdown = true;
}
void AppStateManager::init(AppState* state)
{
	Framework::getSingletonPtr()->mKeyboard->setEventCallback(state);
	Framework::getSingletonPtr()->mMouse->setEventCallback(state);
	//Framework::getSingletonPtr()->mTrayMgr->setListener(state);

	Framework::getSingletonPtr()->mRenderWnd->resetStatistics();
}

#include "AnPCH.h"
#include "AnRoomQuery.h"
#include "AnStorageChamber.h"
using namespace Ants;

RoomQuery::RoomQuery (const RoomMap* rooms, const WorldMap* world)
{
	mRooms = rooms;
	mWorld = world;
}
RoomQuery::~RoomQuery ()
{
}
		
Room* RoomQuery::findRoom(Room::RoomType type)
{
	std::vector<Room*> rooms;
	mRooms->getRoom(type);
	return mRooms->getFirstRoom(type);
}
Room* RoomQuery::findNearestRoom(Room::RoomType type, const Ogre::Vector2& position)
{
	Room* result = 0;
	int dist = std::numeric_limits<int>::max();
	if(mRooms->hasRooms(type))
	{
		std::set<int>::const_iterator iter, iend;
		iter = mRooms->getRoomIter(type); iend = mRooms->getRoomIterEnd(type);
		for(; iter != iend; ++iter)
		{
			Room* r = mRooms->getRoom(*iter);
			int d = Tile::dist(position, r->getPivot(position));
			if(d < dist)
			{
				dist = d;
				result = r;
			}
		}
	}
	return result;
}
int RoomQuery::distToNearestRoom(Room::RoomType type, const Ogre::Vector2& position)
{
	Room* room = 0;
	if (type == Room::THRONE_CHAMBER)
		room = getThroneChamber();
	else 
		room = findNearestRoom(type, position);

	if (room)
		return Tile::dist(room->getFirstPivot(), position);
	return std::numeric_limits<int>::max();
}

Room* RoomQuery::findStorageWithResource(Entity::EntityType type,
	const Ogre::Vector2& position)
{
	Room* result = 0;
	int dist = std::numeric_limits<int>::max();
	if (mRooms->hasRooms(Room::STORAGE_CHAMBER))
	{
		std::set<int>::const_iterator iter, iend;
		iter = mRooms->getRoomIter(Room::STORAGE_CHAMBER);
		iend = mRooms->getRoomIterEnd(Room::STORAGE_CHAMBER);
		for (; iter != iend; ++iter)
		{
			StorageChamber* r = (StorageChamber*)mRooms->getRoom(*iter);
			int d = Tile::dist(position, r->getPivot(position));
			if (d < dist && r->has(type))
			{
				dist = d;
				result = r;
			}
		}
	}
	return result;
}

void RoomQuery::findReachableRoom(const Ogre::Vector2& position, Room*& room,
	Room::RoomType type, int depth)
{
	std::vector<Room::RoomType> allowedRooms;
	allowedRooms.push_back(type);
	findReachableRoom(position, room, allowedRooms, depth);
}
void RoomQuery::findReachableRoom(const Ogre::Vector2& position, Room*& room,
	const std::vector<Room::RoomType>& allowedTypes, int depth)
{
	int d = depth;
	if(depth < 0)
		d = std::numeric_limits<int>::max();
	
	room = 0;

	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::set<int> marks;
	marks.insert(((int)position.y)*width + position.x);

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(position);

	while(!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		if (exploreTile(u, allowedTypes, room))
			break;

		// explore neighbours
		for(Ogre::Vector2 d : Tile::getNeighbours(u))
		{
			Ogre::Vector2 pos = u + d;
			if(pos.x < 0 || pos.x >= width
			|| pos.y < 0 || pos.y >= height)
				continue;
			
			Tile* v = mWorld->getTile(pos);
			if(v == 0 || v->isBlocking()) continue;

			int p = ((int)pos.y)*width + pos.x;
			if(marks.find(p) == marks.end())
			{
				marks.insert(p);
				nextLevel.push_back(pos);
			}
		}
		if(currentLevel.empty())
		{
			if(--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}
}
bool RoomQuery::exploreTile(const Ogre::Vector2& u, const std::vector<Room::RoomType>& allowedTypes, Room*& room) const
{
	for (Room::RoomType type : allowedTypes)
	{
		Room* r = mRooms->getRoom(u, type);
		if (r && r->getType() == type)
		{
			if (!r->isEmpty())
			{
				room = r;
				break;
			}
		}
	}
	return room != 0;
}
ThroneChamber* RoomQuery::getThroneChamber()
{
	return mRooms->getThroneChamber();
}
Room* RoomQuery::get(int id)
{
	return mRooms->getRoom(id);
}
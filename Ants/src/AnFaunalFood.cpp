#include "AnPCH.h"
#include "AnFaunalFood.h"
using namespace Ants;

Ogre::String FaunalFood::msStandardMesh = "";
Ogre::String FaunalFood::msMaterialName = "";

FaunalFood::FaunalFood(int id, int clientID,
	const Ogre::Vector2& position,
	const Ogre::Vector2& tilePosition,
	int visibilityRadius)
	: Entity(id, clientID, position, tilePosition, visibilityRadius)
{
}
FaunalFood::FaunalFood(const EntityDesc& desc)
: Entity(desc)
{
}
FaunalFood::~FaunalFood()
{
}
Entity::EntityType FaunalFood::getType() const
{
	return Entity::ET_FAUNAL_FOOD;
}
bool FaunalFood::isResource() const
{
	return true;
}

Ogre::String FaunalFood::getMaterialName() const
{
	return FaunalFood::msMaterialName;
}
Ogre::String FaunalFood::getStandardMesh() const
{
	return FaunalFood::msStandardMesh;
}

void FaunalFood::update(float dt, ActionReactor* reactor)
{
}

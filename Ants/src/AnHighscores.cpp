#include "AnPCH.h"
#include "AnHighscores.h"
using namespace Ants;

bool Compare(Statistics* i, Statistics* j)
{
	return (i->getScore() > j->getScore());
}

Highscores::Highscores()
{

}
Highscores::Highscores(ObjectID id) 
	: GameObject(id)
{

}
Highscores::~Highscores()
{
	clear();
}

void Highscores::clear()
{
	for (Statistics* entry : mEntries)
	{
		delete0(entry);
	}
	mEntries.clear();
}
void Highscores::addEntry(Statistics* entry)
{
	mEntries.push_back(entry);
	sort();
}
bool Highscores::isInTopTen(Statistics* entry) const
{
	return mEntries.size() < 10 || mEntries[mEntries.size() - 1]->getScore() < entry->getScore();
}
void Highscores::sort()
{
	std::sort(mEntries.begin(), mEntries.end(), Compare);
	while (mEntries.size() > 10)
		mEntries.pop_back();
}
int Highscores::size() const
{
	return mEntries.size();
}
Statistics* Highscores::operator[] (const int index)
{
	return mEntries[index];
}

//--------------------------------------------------------
const Ogre::String Highscores::TYPE("Highscores");
const Ogre::String& Highscores::getClassType() const
{
	return TYPE;
}

bool Highscores::msStreamRegistered = false;
bool Highscores::registerFactory()
{
	if (!msStreamRegistered)
	{
		InitTerm::addInitializer(Highscores::initializeFactory);
		msStreamRegistered = true;
	}
	return msStreamRegistered;
}

GameObject* Highscores::Factory(Deserializer& deserializer)
{
	Highscores* object = new0 Highscores();
	object->deserialize(deserializer);
	return object;
}
void Highscores::initializeFactory()
{
	if (!msFactories)
	{
		msFactories = new0 GameObject::FactoryMap();
	}
	msFactories->insert(std::make_pair(TYPE, Factory));
}

bool Highscores::registerObjects(Serializer& serializer) const
{
	if (GameObject::registerObjects(serializer))
	{
		const int count = integer(mEntries.size());
		for (int i = 0; i < count; ++i)
		{
			serializer.registerObject(mEntries[i]);
		}
		return true;
	}
	return false;
}

int Highscores::getStreamingSize() const
{
	int size = GameObject::getStreamingSize();
	const int count = integer(mEntries.size());
	size += sizeof(count);
	size += count*POINTER_SIZE(mEntries[0]);
	return size;
}

void Highscores::serialize(Serializer& serializer) const
{
	GameObject::serialize(serializer);

	const int count = integer(mEntries.size());
	serializer.write(count);
	for (int i = 0; i < count; ++i)
	{
		serializer.writePointer(mEntries[i]);
	}
}
void Highscores::deserialize(Deserializer& deserializer)
{
	GameObject::deserialize(deserializer);

	int count;
	deserializer.read(count);
	if (count > 0)
	{
		mEntries.resize(count);
		deserializer.readPointerVV(count, &mEntries[0]);
	}
}
void Highscores::resolvePointers(Deserializer& deserializer)
{
	GameObject::resolvePointers(deserializer);

	const int count = integer(mEntries.size());
	for (int i = 0; i < count; ++i)
	{
		deserializer.resolvePointer(mEntries[i]);
	}
	sort();
}
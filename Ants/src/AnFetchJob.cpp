#include "AnPCH.h"
#include "AnFetchJob.h"
#include "AnClient.h"
using namespace Ants;

FetchJob::FetchJob (int id, const Ogre::Vector2& position)
	: Job(id, 1, position)
{
}
FetchJob::~FetchJob ()
{
}

Entity* FetchJob::getFetchEntity() const
{
	return mFetchEnt;
}
void FetchJob::setFetchEntity(Entity* fetchEnt)
{
	mFetchEnt = fetchEnt;
}

Room* FetchJob::getTargetRoom() const
{
	return mTargetRoom;
}
void FetchJob::setTargetRoom(Room* targetRoom)
{
	mTargetRoom = targetRoom;
}

Job::JobType FetchJob::getType() const
{
	return Job::FETCH_JOB;
}

bool FetchJob::update(double dt, ActionReactor* reactor)
{
	return true;
}
void FetchJob::getSubJobs(std::vector<Job::SubJobs>& jobs) const
{
	jobs.push_back(Job::FETCH);
	jobs.push_back(Job::WALK_TO_TARGET);
	jobs.push_back(Job::PLACE);
}

#include "AnPCH.h"
#include "AnDigResourceJob.h"
#include "AnClient.h"
using namespace Ants;

DigResourceJob::DigResourceJob (int id, const Ogre::Vector2& position, double duration)
	: Job(id, 1, position)
{
	mResourceID = -1;
	mT = duration;
}
DigResourceJob::~DigResourceJob ()
{
}

Job::JobType DigResourceJob::getType() const
{
	return Job::DIG_RESOURCE_JOB;
}

bool DigResourceJob::update(double dt, ActionReactor* reactor)
{
	mT -= dt;
	if(mT <= 0)
	{
		return true;
	}
	return false;
}
void DigResourceJob::getSubJobs(std::vector<Job::SubJobs>& jobs) const
{
	jobs.push_back(Job::UPDATE);
}

void DigResourceJob::setResourceID(int id)
{
	mResourceID = id;
}
int DigResourceJob::getResourceID() const
{
	return mResourceID;
}

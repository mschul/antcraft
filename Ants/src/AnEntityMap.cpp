#include "AnPCH.h"
#include "AnEntityMap.h"
#include "AnEntityDesc.h"
#include "AnPuppetFactory.h"
#include "AnAnt.h"
using namespace Ants;

EntityMap::EntityMap ()
{
	mMap = 0;
	mWidth = 0;
	mHeight = 0;
	mBusy = false;
}
EntityMap::~EntityMap ()
{
	SafeDelete1(mMap);
}

void EntityMap::init(unsigned int width, unsigned int height)
{
	mWidth = width;
	mHeight = height;
	mMap = new1<std::vector<Entity*>>(width*height);
}
void EntityMap::init(const Ogre::Rect& area)
{
	init(area.width(), area.height());
}

void EntityMap::getUpdates(std::vector<EntityDesc>& addedEnts,
				std::vector<EntityDesc>& changedEnts,
				std::vector<EntityDesc>& removedEnts)
{
	addedEnts.assign(mAddedEnts.begin(), mAddedEnts.end());
	changedEnts.assign(mChangedEnts.begin(), mChangedEnts.end());
	removedEnts.assign(mRemovedEnts.begin(), mRemovedEnts.end());
}
void EntityMap::clearUpdates()
{
	mAddedEnts.clear();
	mChangedEnts.clear();
	mRemovedEnts.clear();
}

void EntityMap::add(Entity* ent, bool tracking)
{
	if(mBusy)
	{
		mAddCache.push_back(std::make_pair(ent, tracking));
		return;
	}
	assert(!get(ent->getID()));

	// add into current position, add to position lookup, add to entiy list
	const Ogre::Vector2 position = ent->getTilePosition();
	const unsigned int i = integer(mWidth*(int)position.y + position.x);
	int id = ent->getID();
	mMap[i].push_back(ent);

	if ((int)mInverseMap.size() <= id)
		mInverseMap.resize(id + 1);
	mInverseMap[ent->getID()] = ent->getTilePosition();


	mEntities.insert(std::make_pair(ent->getID(), ent));
	if(tracking)
		mAddedEnts.push_back(EntityDesc(ent));

}
void EntityMap::add(const std::vector<Entity*>& entities, bool tracking)
{
	for(Entity* ent : entities)
		add(ent, tracking);
}
void EntityMap::add(EntityDesc desc, bool tracking)
{
	Entity* e = PuppetFactory::getSingletonPtr()->create(desc);
	add(e, tracking);
}
void EntityMap::change(Entity* ent, bool tracking)
{
	ent->resetChangedFlag();
	if(mBusy)
	{
		mChangeCache.push_back(std::make_pair(ent, tracking));
		return;
	}

	assert(get(ent->getID()));
	//remove from old position
	int id = ent->getID();

	const Ogre::Vector2 position = mInverseMap[id];
	const Ogre::Vector2 newPosition = ent->getTilePosition();
	if (position != newPosition)
	{
		const unsigned int i = integer(mWidth*(int)position.y + position.x);

		int index = 0;
		for (; index < (int)mMap[i].size(); ++index)
			if (mMap[i][index]->getID() == id)
				break;
		if (index < (int)mMap[i].size())
		{
			mMap[i][index] = mMap[i].back();
			mMap[i].pop_back();
		}
		else
		{
			assert(false);
		}

		// add in new position
		const unsigned int j = integer(mWidth*((int)newPosition.y) + newPosition.x);
		mMap[j].push_back(ent);
		mInverseMap[id] = newPosition;
	}

	if(tracking)
		mChangedEnts.push_back(EntityDesc(ent));
}
void EntityMap::change(const std::vector<Entity*>& entities, bool tracking)
{
	for(Entity* ent : entities)
		change(ent, tracking);
}
void EntityMap::change(EntityDesc desc, bool tracking)
{
	Entity* ent = get(desc.getEntityID());
	if (!ent)
	{
		add(desc, tracking);
		ent = get(desc.getEntityID());
	}
	ent->set(desc);
	change(ent, tracking);
}
void EntityMap::remove(Entity* ent, bool tracking)
{
	if(mBusy)
	{
		mRemoveCache.push_back(std::make_pair(ent, tracking));
		return;
	}

	assert(get(ent->getID()));
	// remove from position (position lookup needs no change)
	std::vector<Entity*>::iterator iter, iend;
	int id = ent->getID();
	// use inverse map as item might be out of synch (for example projectile, as it is self destructive)
	const Ogre::Vector2 position = mInverseMap[id];

	const unsigned int i = integer(mWidth*((int)position.y) + position.x);

	int index = 0;
	for(; index < (int)mMap[i].size(); ++index)
		if(mMap[i][index]->getID() == id)
			break;
	assert(index < (int)mMap[i].size());
	{
		mMap[i][index] = mMap[i].back();
		mMap[i].pop_back();
	}

	// remove from entity list
	mEntities.erase(ent->getID());
	if(tracking)
		mRemovedEnts.push_back(EntityDesc(ent));

	SafeDelete0(ent);

}
void EntityMap::remove(const std::vector<Entity*>& entities, bool tracking)
{
	for(Entity* ent : entities)
		remove(ent, tracking);
}
void EntityMap::remove(EntityDesc desc, bool tracking)
{
	Entity* ent = get(desc.getEntityID());
	// we can be a bit more liberal here!
	if (ent)
		remove(ent, tracking);
}

void EntityMap::applyUpgrade(int clientID, Entity::Upgrade u)
{
	for (std::pair<int, Entity*> ent : mEntities)
	if (ent.second->getClientID() == clientID)
	{
		ent.second->applyUpgrade(u);
	}
}

void EntityMap::onEntityChange(Entity* ent)
{
}
void EntityMap::update(float dt, int clientID, ActionReactor* reactor)
{
	mBusy = true;

	for(std::pair<int, Entity*> pair : mEntities)
	{
		Entity* ent = pair.second;
		if(ent->getClientID() == clientID)
		{
			ent->update(dt, reactor);
		}
	}

	for(std::pair<int, Entity*> ent : mEntities)
	{
		if (ent.second->isChanged())
		{
			change(ent.second, true);
			onEntityChange(ent.second);
		}
	}
	mBusy = false;

	for(std::pair<Entity*, bool> p : mAddCache)
		add(p.first, p.second);
	for(std::pair<Entity*, bool> p : mChangeCache)
		change(p.first, p.second);
	for(std::pair<Entity*, bool> p : mRemoveCache)
		remove(p.first, p.second);

	mAddCache.clear();
	mChangeCache.clear();
	mRemoveCache.clear();
}

std::vector<Entity*>& EntityMap::get(const Ogre::Vector2& position) const
{
	const unsigned int i = integer(mWidth*((int)position.y) + position.x);
	return mMap[i];
}
void EntityMap::get(const std::vector<Ogre::Vector2>& positions, std::vector<Entity*>& ents) const
{
	for (Ogre::Vector2 position : positions)
	{
		const unsigned int i = integer(mWidth*((int)position.y) + position.x);
		if(i >= 0 && i < mWidth*mHeight)
		{
			if(!mMap[i].empty())
			{
				ents.insert(ents.end(), mMap[i].begin(), mMap[i].end());
			}
		}
	}
}
Entity* EntityMap::get(int id) const
{
	std::map<int, Entity*>::const_iterator iter = mEntities.find(id);
	return iter != mEntities.end() ? iter->second : 0;
}
Entity* EntityMap::findNext(int id) const
{
	std::map<int, Entity*>::const_iterator iter = mEntities.find(id);
	if(iter == mEntities.end())
		return mEntities.begin()->second;
	++iter;
	return iter != mEntities.end() ? iter->second : mEntities.begin()->second;
}

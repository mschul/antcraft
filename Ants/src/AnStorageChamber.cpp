#include "AnPCH.h"
#include "AnStorageChamber.h"
#include "AnAnt.h"
using namespace Ants;

StorageChamber::StorageChamber (int id)
	: Room(id, 1)
{
	mFloral = 0;
	mFaunal = 0;
    mCopal = 0;
    mHygieneDec = 0.01f;
}
StorageChamber::~StorageChamber ()
{
}
		
Room::RoomType StorageChamber::getType() const
{
	return Room::STORAGE_CHAMBER;
}
void StorageChamber::build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	mFloral = 0;
	mFaunal = 0;
	mCopal = 0;
	mPivot.clear();
	mPivot.push_back(pivot);

	float minX, maxX, minY, maxY;
	calculateEnclosingRectangle(positions, minX, maxX, minY, maxY);
	int width = (maxX - minX + 1);

	std::map<int, slot*> positionedSlots;
	for (slot* s : mSlots)
	{
		Ogre::Vector2 p = s->position;
		const int i = (p.y - minY)*width + (p.x - minX);
		positionedSlots.insert(std::make_pair(i, s));
	}

	//reset(workers, se);

	{ // slot and workplace reset/transfer
		mSlots.clear();
		for (workplace* w : mWorkPlaces)
		{
			if (w->worker)
				workers.push_back((Ant*)w->worker);
			SafeDelete0(w);
		}
		mWorkPlaces.clear();
		mFreeWorkplaces = 0;
	}

	std::vector<slot*> newSlots;
	for (Ogre::Vector2 position : positions)
	{
		const int i = (position.y - minY)*width + (position.x - minX);

		if (mSlots.size() % 4 == 0)
		{
			workplace* w = new0 workplace;
			w->worker = 0;
			w->position = position;
			mWorkPlaces.push_back(w);
			mFreeWorkplaces++;
		}

		std::map<int, slot*>::iterator slotIter = positionedSlots.find(i);
		slot* s;
		if (slotIter == positionedSlots.end())
		{
			s = new0 slot;
			s->reserved = 0;
			s->capacity = mSlotCapacity;
			s->position = position;

			s->items.resize(mSlotCapacity);

			for (int i = 0; i < mSlotCapacity; ++i)
			{
				s->items[i].ent = 0;
				s->items[i].reservedFor = 0;
			}
			newSlots.push_back(s);
		}
		else
		{
			s = slotIter->second;

			Entity* e = s->items[0].ent;

			if (e)
			{
				switch (e->getType())
				{
				case Entity::ET_FLORAL_FOOD:
					mFloral++;
					break;
				case Entity::ET_FAUNAL_FOOD:
					mFaunal++;
					break;
				case Entity::ET_COPAL:
					mCopal++;
					break;
				}
			}

			if (s->items[0].reservedFor)
			{
				Framework::getSingleton().mLog->logMessage("reserved");
			}

			positionedSlots.erase(slotIter);
		}
		tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::ROOM_FLOOR]));
		mSlots.push_back(s);
	}


	while (!workers.empty() && mFreeWorkplaces > 0)
	{
		assignToWorkplace(workers.back());
		workers.pop_back();
	}
	workers.clear();

	mWorkerRefillRequested = true;

	std::map<int, slot*>::iterator iter, iend;
	iter = positionedSlots.begin(); iend = positionedSlots.end();
	for (; iter != iend; ++iter)
	{
		slot* s = iter->second;
		Entity* reserved = s->items[0].reservedFor;
		if (reserved)
		{
			((Ant*)reserved)->prepareForNewJob(s);
		}
		Entity* ent = s->items[0].ent;
		if (ent)
		{
			se.push_back(ent);
		}
		SafeDelete0(s);
	}

	// try to put loose entities into empty slots
	// try to put loose entities into empty slots
	while (!se.empty() && !newSlots.empty())
	{
		slot* s = newSlots.back();
		Entity* e = se.back();
		if (e->isResource())
		{
			Ogre::Vector2 hp;
			Tile::toHexagonalPosition(s->position, hp);
			e->setPosition(hp);
			e->setTilePosition(s->position);
		}
		s->items[mSlotCapacity - s->capacity].ent = e;
		s->capacity--;
		newSlots.pop_back();
		se.pop_back();
	}
	se.clear();
}
void StorageChamber::update(float dt, ActionReactor* reactor)
{
    Room::update (dt, reactor);
}
void StorageChamber::getJob(std::vector<WorkplaceSubjobs>& jobs)
{
    float r = rand () % 100; r /= 10;
    bool hygieneNeeded = (r*r) > mHygiene;
    if (hygieneNeeded)
    {
        jobs.push_back (Room::FETCH_COPAL);
        jobs.push_back (Room::CLEAN_ROOM);
    }
    else if(freeSlots() > 0)
	{
		jobs.push_back(Room::FETCH_RESOURCE);
		jobs.push_back(Room::PLACE);
	}
}
bool StorageChamber::has(Entity::EntityType type) const
{
	switch (type)
	{
	case Entity::ET_FLORAL_FOOD:
		return mFloral > 0;
	case Entity::ET_FAUNAL_FOOD:
		return mFaunal > 0;
	case Entity::ET_COPAL:
		return mCopal > 0;
	default: return false;
	}
}
Entity* StorageChamber::fetch(Entity* caller, slot*& reservedSlot)
{
	Entity* item = Room::fetch(caller, reservedSlot);
	switch (item->getType())
	{
	case Entity::ET_FLORAL_FOOD:
		mFloral--;
		break;
	case Entity::ET_FAUNAL_FOOD:
		mFaunal--;
		break;
	case Entity::ET_COPAL:
		mCopal--;
		break;
	}
	return item;
}
void StorageChamber::place(Entity* caller, Entity* item, slot*& reservedSlot)
{
	Room::place(caller, item, reservedSlot);
	switch (item->getType())
	{
	case Entity::ET_FLORAL_FOOD:
		mFloral++;
		break;
	case Entity::ET_FAUNAL_FOOD:
		mFaunal++;
		break;
	case Entity::ET_COPAL:
		mCopal++;
		break;
	}
}

int StorageChamber::getFloralCount() const
{
	return mFloral;
}
int StorageChamber::getFaunalCount() const
{
	return mFaunal;
}
int StorageChamber::getCopalCount() const
{
	return mCopal;
}
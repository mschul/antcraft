#include "AnPCH.h"
#include "AnJobMap.h"
#include "AnDigJob.h"
#include "AnDigResourceJob.h"
#include "AnFetchJob.h"
#include "AnGotoJob.h"
#include "AnFlattenJob.h"
#include "AnRoomCreateJob.h"
#include "AnRoomCreateJobEx.h"
#include "AnHoldJob.h"
#include "AnDefendJob.h"
#include "AnDestroyAnthillJob.h"
using namespace Ants;

JobMap::JobMap ()
{
	mWidth = 0;
	mHeight = 0;
	mLastJobID = 0;
	mAvailableJobs = 0;
	mHarvestSpeed = 5;
}
JobMap::~JobMap ()
{
}
		
void JobMap::init(unsigned int width, unsigned int height)
{
	mWidth = width;
	mHeight = height;
	mMap.resize(width*height);
}
void JobMap::init(const Ogre::Rect& area)
{
	init(area.width(), area.height());
}

int JobMap::add(Job::JobType type, Ogre::Vector2 position)
{
	Job* job = 0;
	int id = mLastJobID++;
	switch(type)
	{
	case Job::DIG_JOB:
		job = new0 DigJob(id, position, mHarvestSpeed / 5);
		break;
	case Job::GOTO_JOB:
		job = new0 GotoJob(id, position);
		break;
	case Job::DIG_RESOURCE_JOB:
		job = new0 DigResourceJob(id, position, mHarvestSpeed);
		break;
	case Job::FETCH_JOB:
		job = new0 FetchJob(id, position);
		break;
	case Job::FLATTEN_JOB:
		job = new0 FlattenJob(id, position, mHarvestSpeed / 2);
		break;
	case Job::ROOM_CREATE_JOB:
		job = new0 RoomCreateJob(id, position, mHarvestSpeed / 2);
		break;
	case Job::ROOM_CREATE_JOB_EX:
		job = new0 RoomCreateJobEx(id, position, mHarvestSpeed / 2);
		break;
	case Job::HOLD_JOB:
		job = new0 HoldJob(id, position, mHarvestSpeed);
		break;
	case Job::DEFEND_JOB:
		job = new0 DefendJob(id, position, mHarvestSpeed);
		break;
    case Job::DESTROY_ANTHILL_JOB:
        job = new0 DestroyAnthillJob (id, position, mHarvestSpeed * 2);
        break;
	}
	mJobs.insert(std::make_pair(id, job));

	const unsigned int i = mWidth*position.y + position.x;
	mMap[i].push_back(job);
	
	if(job->isAvailable())
		mAvailableJobs++;

	return id;
}
void JobMap::remove(int id)
{
	std::vector<Job*>::iterator iter, iend;
	Job* job = mJobs[id];

	if(job->isAvailable())
		mAvailableJobs--;

	const Ogre::Vector2 position = job->getPosition();
	const unsigned int i = mWidth*((int)position.y) + position.x;
	iter = mMap[i].begin(); iend = mMap[i].end();
	for(; iter != iend; ++iter)
		if((*iter)->getID() == id)
			break;
	if(iter != iend)
		mMap[i].erase(iter);

	SafeDelete0(mJobs[id]);
	mJobs.erase(id);
}
void JobMap::removeJobs(const Ogre::Vector2& position, Job::JobType type)
{
	const unsigned int i = mWidth*((int)position.y) + position.x;
	if(i >= 0 && i < mWidth*mHeight)
	{
		std::vector<Job*> jobs;
		for(Job* j : mMap[i])
		{
			if(type == j->getType())
			{
				if(j->isAvailable())
					mAvailableJobs--;
				int id = j->getID();
				SafeDelete0(mJobs[id]);
				mJobs[id] = 0;
			}
			else
			{
				jobs.push_back(j);
			}
		}
		std::swap(mMap[i], jobs);
	}
}
		
std::vector<Job*> JobMap::get(const Ogre::Vector2& position) const
{
	const unsigned int i = mWidth*((int)position.y) + position.x;
	if(i >= 0 && i < mWidth*mHeight)
		return mMap[i];
	return std::vector<Job*>();
}
Job* JobMap::get(int id) const
{
	std::map<int, Job*>::const_iterator iter = mJobs.find(id);
	return iter == mJobs.end() ? 0 : iter->second;
}
bool JobMap::assign(Entity* ent, int id)
{
	Job* j = mJobs[id];
	if(j && j->isAvailable())
	{
		j->assign(ent);

		if(!j->isAvailable())
			mAvailableJobs--;

		return true;
	}
	else
	{
		return false;
	}
}
bool JobMap::unAssign(Entity* ent, int id)
{
	Job* j = mJobs[id];

	if(j)
	{
		j->unAssign(ent);

		if(j->isAvailable())
			mAvailableJobs++;
		return true;
	}
	else
	{
		return false;
	}
}
bool JobMap::jobsAvailable() const
{
	return mAvailableJobs > 0;
}

void JobMap::applyUpgrade(Entity::Upgrade u)
{
	switch (u)
	{
	case Entity::U_HARVEST_SPEED:
		mHarvestSpeed /= 2;
		break;
	}
}
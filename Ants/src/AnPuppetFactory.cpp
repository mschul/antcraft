#include "AnPCH.h"
#include "AnPuppetFactory.h"

#include "AnSmallWorker.h"
#include "AnWorker.h"
#include "AnCarrier.h"
#include "AnSoldier.h"

#include "AnEgg.h"
#include "AnGrub.h"
#include "AnFloralFood.h"
#include "AnFaunalFood.h"
#include "AnCopal.h"
#include "AnDebris.h"

#include "AnLadybug.h"
#include "AnEarthworm.h"
#include "AnSpider.h"
#include "AnBombardierBeetle.h"

#include "AnProjectile.h"
using namespace Ants;

template<> PuppetFactory* Ogre::Singleton<PuppetFactory>::msSingleton = 0;

PuppetFactory::PuppetFactory()
{
}
PuppetFactory::~PuppetFactory()
{
}
		
Entity* PuppetFactory::create(const EntityDesc& desc)
{
	Entity* ent = 0;
	switch(desc.getEntityType())
	{
	case Entity::ET_SMALL_WORKER:
		ent = new0 SmallWorker(desc);
		break;
	case Entity::ET_WORKER:
		ent = new0 Worker(desc);
		break;
	case Entity::ET_CARRIER:
		ent = new0 Carrier(desc);
		break;
	case Entity::ET_SOLDIER:
		ent = new0 Soldier(desc);
		break;

	case Entity::ET_EGG:
		ent = new0 Egg(desc);
		break;
	case Entity::ET_GRUB:
		ent = new0 Grub(desc);
		break;
	case Entity::ET_FLORAL_FOOD:
		ent = new0 FloralFood(desc);
		break;
	case Entity::ET_FAUNAL_FOOD:
		ent = new0 FaunalFood(desc);
		break;
	case Entity::ET_COPAL:
		ent = new0 Copal(desc);
		break;
	case Entity::ET_DEBRIS:
		ent = new0 Debris(desc);
		break;

	case Entity::ET_LADYBUG:
		ent = new0 Ladybug(desc);
		break;
	case Entity::ET_EARTHWORM:
		ent = new0 Earthworm(desc);
		break;
	case Entity::ET_SPIDER:
		ent = new0 Spider(desc);
		break;
	case Entity::ET_BOMBARDIER_BEETLE:
		ent = new0 BombardierBeetle(desc);
		break;

	case Entity::ET_PROJECTILE:
		ent = new0 Projectile(desc);
		break;
	}
	return ent;
}
#include "AnPCH.h"
#include "AnBombardierBeetleSpawner.h"
#include "AnEntity.h"
using namespace Ants;

BombardierBeetleSpawner::BombardierBeetleSpawner (int id)
	: MobSpawner(id)
{
	mNextSpawnDelta = 300;
	mInitialSpawnDelta = 300;
}
BombardierBeetleSpawner::~BombardierBeetleSpawner ()
{
}
		
Room::RoomType BombardierBeetleSpawner::getType() const
{
	return Room::BOMBARDIER_BEETLE_SPAWNER;
}

void BombardierBeetleSpawner::build(const std::vector<Ogre::Vector2>& positions, 
	const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
	std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	mPivot.clear();
	mPivot.push_back(pivot);
	tiles.push_back(std::make_pair(pivot, Tile::msTiles[Tile::NEST]));
}
Entity* BombardierBeetleSpawner::spawn(ActionReactor* reactor)
{
	return reactor->spawn(*mPivot.begin(), Entity::ET_BOMBARDIER_BEETLE);
}
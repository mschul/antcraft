#include "AnPCH.h"
#include "AnProjectile.h"
using namespace Ants;

Ogre::String Projectile::msStandardMesh = "";
Ogre::String Projectile::msMaterialName = "";

Projectile::Projectile(int id, float attackPoints, Character* source,
	const Ogre::Vector2& targetPosition, const Ogre::Vector2& targeTiletPosition)
	: Entity(id, source->getClientID(), source->getPosition(), source->getTilePosition(), 1)
{
	mAttackPoints = attackPoints;
	mTargetTilePosition = targeTiletPosition;
	mTargetPosition = targetPosition;
	mSourceID = source->getID();
}
Projectile::Projectile (const EntityDesc& desc)
	: Entity(desc)
{
}
Projectile::~Projectile ()
{
}
		
float Projectile::getAttackPoints() const
{
	return mAttackPoints;
}

Entity::EntityType Projectile::getType() const
{
	return Entity::ET_PROJECTILE;
}

Ogre::String Projectile::getMaterialName() const
{
	return msMaterialName;
}
Ogre::String Projectile::getStandardMesh() const
{
	return msStandardMesh;
}

ObjectID Projectile::getSource() const
{
	return mSourceID;
}

void Projectile::update(float dt, ActionReactor* reactor)
{
    mChanged = true;

	float speed = 10;
	float delta = speed * dt;
	Ogre::Vector2 ds = mTargetPosition - mPosition;

    bool hit = false;

	if (ds.squaredLength() < 0.05)
    {
        mPosition = mTargetPosition;
        mTilePosition = mTargetTilePosition;
        hit = true;
	}
	else
	{
		float dist = ds.length();
		if(delta > dist)
		{
			mPosition = mTargetPosition;
            mTilePosition = mTargetTilePosition;
            hit = true;
		}
		else
		{
			ds /= dist;
			mPosition += delta*ds;
			Tile::toTilePosition(mPosition, mTilePosition);
		}
	}

    if (hit)
    {
        std::vector<Entity*> ents;
        ents.push_back (this);
        reactor->removeEntities (ents);
        reactor->aoeAttack (mSourceID, mAttackPoints, mTargetTilePosition, 1);
    }
}
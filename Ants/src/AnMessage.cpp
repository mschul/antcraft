#include "AnPCH.h"
#include "AnMessage.h"
using namespace Ants;

DisplayMessage::DisplayMessage(const Ogre::String& title)
{
	mDisplayTime = 10.0f;
	mTitle = title;
}
DisplayMessage::~DisplayMessage()
{
}
void DisplayMessage::update(float dt)
{
	mDisplayTime -= dt;
}
bool DisplayMessage::isAlive() const
{
	return mDisplayTime >= 0;
}
Ogre::String DisplayMessage::getTitle() const
{
	return mTitle;
}

Message::Message(MessageType type, int receiver)
{
	mType = type;
	mReceiverID = receiver;
}
Message::~Message()
{

}
void Message::addDatum(const Ogre::String& name, const Ogre::String& value)
{
	mData.insert(std::make_pair(name, value));
}
Ogre::String Message::getDatum(const Ogre::String& name) const
{
	std::map<Ogre::String, Ogre::String>::const_iterator iter = mData.find(name);
	return iter != mData.end() ? iter->second : "";
}
Message::MessageType Message::getType() const
{
	return mType;
}
int Message::getReceiver() const
{
	return mReceiverID;
}
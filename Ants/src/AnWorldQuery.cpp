﻿#include "AnPCH.h"
#include "AnWorldQuery.h"
using namespace Ants;

#define SQRT_2 1.41421356237309504880f

WorldQuery::WorldQuery (const WorldMap* world)
{
	mWorld = world;
}
WorldQuery::~WorldQuery ()
{
}

bool WorldQuery::isBlocking(const Ogre::Vector2& pos) const
{
	Tile* t = mWorld->getTile(pos);
	return !t || t->isBlocking();
}
bool WorldQuery::isBlocking(const Ogre::Vector2& pos, int radius) const
{
	std::vector<Ogre::Vector2> path;
	Tile::hexCircle(pos, radius, path);
	for(Ogre::Vector2 p : path)
	{
		Tile* t = mWorld->getTile(p);
		if(!t || t->isBlocking())
			return true;
	}
	return false;
}
bool WorldQuery::isRoomPlacable(const Ogre::Vector2& pos) const
{
	Tile* t = mWorld->getTile(pos);
	return t && (t->isDiggable() || t->getType() == Tile::FLOOR || t->getType() == Tile::ROUGH);
}

float WorldQuery::damagePerSecond(const Ogre::Vector2& position) const
{
	Tile* t = mWorld->getTile(position);
	return t->getDamagePerSecond();
}

void WorldQuery::generateRandomWalk(const Ogre::Vector2& start,
								   int length, std::vector<Ogre::Vector2>& path) const
{
	Ogre::Vector2 last = start;
	Ogre::Vector2 current = start;
	while(--length > 0)
	{
		std::vector<Ogre::Vector2> neighbours;
		exploreNeighbours(neighbours, current, last);
		
		last = current;
		current = neighbours[rand() % neighbours.size()];
		path.push_back(current);
	}
}
void WorldQuery::generateRandomWalk(const Ogre::Vector2& start,
								   int length, std::vector<Ogre::Vector2>& path, Perlin* noise) const
{
#define NOISE_RESOULTION 10000
	Ogre::Vector2 minP(real(rand() % NOISE_RESOULTION), real(rand() % NOISE_RESOULTION));
	Ogre::Vector2 maxP(real(rand() % (NOISE_RESOULTION - (int)minP.x) + minP.x),
		real(rand() % (NOISE_RESOULTION - (int)minP.y) + minP.y));
	minP /= NOISE_RESOULTION;
	maxP /= NOISE_RESOULTION;
#undef NOISE_RESOULTION
	Ogre::Vector2 step = (maxP - minP) / real(length);
	
	Ogre::Vector2 last = start;
	Ogre::Vector2 current = start;
	while(--length > 0)
	{
		std::vector<Ogre::Vector2> neighbours;
		exploreNeighbours(neighbours, current, last);
		
		if(neighbours.size() == 0)
			return;

		float n = noise->get(minP.x, minP.y) + noise->getAmplitude();
		int index = (int)((n * neighbours.size()) / (2*noise->getAmplitude()));
		last = current;
		current = neighbours[index];
		path.push_back(current);
	}
}
void WorldQuery::generateRandomWalk(const Ogre::Vector2& start,
	int length, std::vector<Ogre::Vector2>& path, PheromoneMap* pheromones) const
{
	Ogre::Vector2 current = start;
	Tile::Direction dir = (Tile::Direction)(rand()%6);
	bool first = true;
	while(--length > 0)
	{
		unsigned char distro[6];
		unsigned int sum;
		pheromones->getDistribution(current, dir, distro, sum, first ? 2 : 1);
		
		std::vector<Ogre::Vector2> neighbours = Tile::getNeighbours(current);
		for(int i = 0; i < 6; ++i)
		{
			Ogre::Vector2 pos = current + neighbours[i];

			Tile* v = mWorld->getTile(pos);
			if(v == 0 || v->isBlocking())
			{
				sum -= distro[i];
				distro[i] = 0;
			}
		}

		if(sum == 0)
		{
			dir = (Tile::Direction)(((int)dir+3)%6);
			current += neighbours[dir];
		}
		else
		{
			int border = rand()%sum;
			int i = 0;
			for(; i < 6; ++i)
			{
				border -= distro[i];
				if(border < 0)
					break;
			}
			assert(distro[i] > 0);
			current += neighbours[i];
			dir = (Tile::Direction)i;
		}
		for (int i = 0; i < 6; ++i)
		{
			Ogre::Vector2 pos = current + neighbours[i];
			if (pos != current)
				pheromones->decrease(pos);
		}
		path.push_back(current);
		first = false;
	}
}
void WorldQuery::findNextTile(const Ogre::Vector2& start, 
		const std::vector<Tile::TileType>& allowedTiles,
		Ogre::Vector2& targetPosition, int depth) const
{
	int d = depth;
	if(depth < 0)
		d = std::numeric_limits<int>::max();

	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::set<int> marks;
	marks.insert(integer(((int)start.y)*width + start.x));

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(start);

	while(!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		Tile* currentTile = mWorld->getTile(u);

		for(Tile::TileType t : allowedTiles)
		{
			if(currentTile->getType() == t)
			{
				targetPosition = u;
				return;
			}
		}

		// explore neighbours
		for(Ogre::Vector2 d : Tile::getNeighbours(u))
		{
			Ogre::Vector2 pos = u + d;
			if(pos.x < 0 || pos.x >= width
			|| pos.y < 0 || pos.y >= height)
				continue;

			Tile* v = mWorld->getTile(pos);
			if(v == 0 || v->isBlocking())
				continue;

			int p = integer(((int)pos.y)*width + pos.x);
			if(marks.find(p) == marks.end())
			{
				marks.insert(p);
				nextLevel.push_back(pos);
			}
		}

		if(currentLevel.empty())
		{
			if(--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}
}
bool WorldQuery::findNextZeroTile(const Ogre::Vector2& start,
	Ogre::Vector2& targetPosition, int depth) const
{
	int d = depth;
	if (depth < 0)
		d = std::numeric_limits<int>::max();

	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::set<int> marks;
	marks.insert(integer(((int)start.y)*width + start.x));

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(start);

	while (!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		Tile* currentTile = mWorld->getTile(u);
		if (!currentTile)
		{
			targetPosition = u;
			return true;
		}

		// explore neighbours
		for (Ogre::Vector2 d : Tile::getNeighbours(u))
		{
			Ogre::Vector2 pos = u + d;
			if (pos.x < 0 || pos.x >= width
				|| pos.y < 0 || pos.y >= height)
				continue;

			Tile* v = mWorld->getTile(pos);
			if (v != 0 && v->isBlocking())
				continue;

			int p = integer(((int)pos.y)*width + pos.x);
			if (marks.find(p) == marks.end())
			{
				marks.insert(p);
				nextLevel.push_back(pos);
			}
		}

		if (currentLevel.empty())
		{
			if (--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}
	return false;
}
void WorldQuery::findNextJob(const Ogre::Vector2& start,
	JobQuery* jobQuery,
	Ogre::Vector2& targetPosition, int depth) const
{
	int d = depth;
	if (depth < 0)
		d = std::numeric_limits<int>::max();

	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::set<int> marks;
	marks.insert(integer(((int)start.y)*width + start.x));

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(start);

	while (!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		Tile* currentTile = mWorld->getTile(u);
		if (!currentTile->isBlocking() && jobQuery->hasJob(u))
		{
				targetPosition = u;
				return;
		}

		// explore neighbours
		for (Ogre::Vector2 d : Tile::getNeighbours(u))
		{
			Ogre::Vector2 pos = u + d;
			if (pos.x < 0 || pos.x >= width
				|| pos.y < 0 || pos.y >= height)
				continue;

			Tile* v = mWorld->getTile(pos);
			if (v == 0 || v->isBlocking())
				continue;

			int p = integer(((int)pos.y)*width + pos.x);
			if (marks.find(p) == marks.end())
			{
				marks.insert(p);
				nextLevel.push_back(pos);
			}
		}

		if (currentLevel.empty())
		{
			if (--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}
}
void WorldQuery::generateRestrictedPath(const Ogre::Vector2& start,
	int length, std::vector<Ogre::Vector2>& path, PheromoneMap* pheromones,
	const std::vector<Tile::TileType>& allowedTiles) const
{
	Ogre::Vector2 current = start;
	Tile* currentTile = mWorld->getTile(current);
	bool currentTileAllowed = false;
	for(Tile::TileType t : allowedTiles)
	{
		if(currentTile->getType() == t)
		{
			currentTileAllowed = true;
			break;
		}
	}
	
	if(!currentTileAllowed)
	{
		Ogre::Vector2 target = start;
		findNextTile(start, allowedTiles, target);
		if(target != start)
		{
			findShortestPath(target, start, path);
		}
		else
		{
			generateRandomWalk(start, length, path, pheromones);
		}
	}
	else
	{
		Tile::Direction dir = (Tile::Direction)(rand()%6);
		bool first = true;
		while(--length > 0)
		{
			unsigned char distro[6];
			unsigned int sum;
			pheromones->getDistribution(current, dir, distro, sum, first ? 2 : 1);
		
			std::vector<Ogre::Vector2> neighbours = Tile::getNeighbours(current);
			for(int i = 0; i < 6; ++i)
			{
				Ogre::Vector2 pos = current + neighbours[i];

				Tile* v = mWorld->getTile(pos);
				if(v == 0 || v->isBlocking())
				{
					sum -= distro[i];
					distro[i] = 0;
				}
				else
				{
					bool ta = false;
					for(Tile::TileType t : allowedTiles)
					{
						if(v->getType() == t)
						{
							ta = true;
							break;
						}
					}
					if(!ta)
					{
						sum -= distro[i];
						distro[i] = 0;
					}
				}
			}

			if(sum == 0)
			{
				dir = (Tile::Direction)(((int)dir+3)%6);
				current += neighbours[dir];
			}
			else
			{
				int border = rand()%sum;
				int i = 0;
				for(; i < 6; ++i)
				{
					border -= distro[i];
					if(border < 0)
						break;
				}
				assert(distro[i] > 0);
				current += neighbours[i];
				dir = (Tile::Direction)i;
			}
			path.push_back(current);
			for (int i = 0; i < 6; ++i)
			{
				Ogre::Vector2 pos = current + neighbours[i];
				if (pos != current)
					pheromones->decrease(pos);
			}
			first = false;
		}
	}
}
void WorldQuery::generateRandomWalkOnJobs(const Ogre::Vector2& start,
	int length, std::vector<Ogre::Vector2>& path,
	PheromoneMap* pheromones, JobQuery* jobQuery) const
{
	Ogre::Vector2 current = start;
	Tile* currentTile = mWorld->getTile(current);

	if (!jobQuery->hasJob(current))
	{
		Ogre::Vector2 target = start;
		findNextJob(start, jobQuery, target);
		if (target != start)
		{
			findShortestPath(target, start, path);
		}
		else
		{
			generateRandomWalk(start, length, path, pheromones);
		}
	}
	else
	{
		Tile::Direction dir = (Tile::Direction)(rand() % 6);
		bool first = true;
		while (--length > 0)
		{
			unsigned char distro[6];
			unsigned int sum;
			pheromones->getDistribution(current, dir, distro, sum, first ? 2 : 1);

			std::vector<Ogre::Vector2> neighbours = Tile::getNeighbours(current);
			for (int i = 0; i < 6; ++i)
			{
				Ogre::Vector2 pos = current + neighbours[i];

				Tile* v = mWorld->getTile(pos);
				if (v == 0 || v->isBlocking())
				{
					sum -= distro[i];
					distro[i] = 0;
				}
				else
				{
					if (!jobQuery->hasJob(pos))
					{
						sum -= distro[i];
						distro[i] = 0;
					}
				}
			}

			if (sum == 0)
			{
				dir = (Tile::Direction)(((int)dir + 3) % 6);
				current += neighbours[dir];
			}
			else
			{
				int border = rand() % sum;
				int i = 0;
				for (; i < 6; ++i)
				{
					border -= distro[i];
					if (border < 0)
						break;
				}
				assert(distro[i] > 0);
				current += neighbours[i];
				dir = (Tile::Direction)i;
			}
			path.push_back(current);
			first = false;
		}
	}
}

void WorldQuery::findShortestPath(const Ogre::Vector2& start,
	const Ogre::Vector2& target, std::vector<Ogre::Vector2>& path, float maxLength) const
{
	Ogre::Rect size = mWorld->getSize();
	int width = size.width();

	std::map<int, score*> scoreMap;
	std::vector<score*> allocatedScores;
	
	std::set<int> openSet;
	std::set<int> closedSet;

	int startIndex = integer(((int)start.y)*width+start.x);
	int targetIndex = integer(((int)target.y)*width + target.x);
	score* currentScore = new0 score();
	allocatedScores.push_back(currentScore);

	currentScore->gscore = 0;
	currentScore->randomizer = 0;
	currentScore->fscore = 0;
	currentScore->position = start;
	currentScore->cameFrom = -1;
	currentScore->length = 0;
	scoreMap.insert(std::make_pair(startIndex, currentScore));
	openSet.insert(startIndex);
	
	score::priorityqueue pq;
	currentScore->handle = pq.push(currentScore);

	while(!pq.empty())
	{
		score* u = pq.top();
		pq.pop();
		Ogre::Vector2 position = u->position;
		int current = integer(position.y*width + position.x);
		currentScore = scoreMap[current];

		if(current == targetIndex)
		{
			backtrack(target, start, scoreMap, path);
			break;
		}
		
		openSet.erase(current);
		closedSet.insert(current);

		// explore neighbours
		std::vector<Ogre::Vector2> neighbours;
		exploreNeighbours(neighbours, position);

		for(Ogre::Vector2 pos : neighbours)
		{
			// if neighbour in closed set, do nothing
			int neighbour = integer(((int)pos.y)*width + pos.x);
			if(closedSet.find(neighbour) != closedSet.end())
					continue;

			// get neighbour score
			score* neighbourScore;
			std::map<int, score*>::iterator iScore = scoreMap.find(neighbour);
			if(iScore == scoreMap.end())
			{
				neighbourScore = new0 score();
				allocatedScores.push_back(neighbourScore);

				neighbourScore->randomizer = (float)(rand()%1000)/10000;
				neighbourScore->fscore = std::numeric_limits<float>::max();
				neighbourScore->gscore = std::numeric_limits<float>::max();
				neighbourScore->position = pos;
				neighbourScore->cameFrom = -1;
				scoreMap.insert(std::make_pair(neighbour, neighbourScore));
			}
			else
			{
				neighbourScore = iScore->second;
			}
			
			// calculate tentative g score
			//int dist = L1(position, pos);
			float tentativeGScore = currentScore->gscore + 1; //((dist == 1) ? 1 : 1.5);

			// if already in open and not a better path found, do nothing
			bool inOpen = openSet.find(neighbour) != openSet.end();
			if(inOpen && tentativeGScore >= neighbourScore->gscore)
				continue;
			
			// set back edge and g score
			neighbourScore->cameFrom = current;
			neighbourScore->gscore = tentativeGScore;
			neighbourScore->length = currentScore->length + 1;

			// set f score
			neighbourScore->fscore =  tentativeGScore + Tile::dist(pos, target);

			if(inOpen)
			{
				pq.decrease(neighbourScore->handle);
			}
			else if(neighbourScore->length < maxLength)
			{
				openSet.insert(neighbour);
				neighbourScore->handle = pq.push(neighbourScore);
			}
		}
	}

	std::vector<score*>::iterator iter, iend;
	iter = allocatedScores.begin(); iend = allocatedScores.end();
	for(; iter != iend; ++iter)
		SafeDelete0(*iter);
}
void WorldQuery::findEscapePath(const Ogre::Vector2& start,
	const Ogre::Vector2& enemy, std::vector<Ogre::Vector2>& path, int length) const
{
	Ogre::Rect size = mWorld->getSize();
	int width = size.width();

	std::map<int, score*> scoreMap;
	std::vector<score*> allocatedScores;
	
	std::set<int> openSet;
	std::set<int> closedSet;

	int startIndex = integer(((int)start.y)*width + start.x);
	int enemyIndex = integer(((int)enemy.y)*width + enemy.x);
	if(enemyIndex == startIndex) enemyIndex = -1;

	score* currentScore = new0 score();
	score* bestScore;
	allocatedScores.push_back(currentScore);

	currentScore->gscore = 0;
	currentScore->randomizer = 0;
	currentScore->fscore = 0;
	currentScore->position = start;
	currentScore->length = 0;
	currentScore->cameFrom = -1;
	bestScore = currentScore;
	scoreMap.insert(std::make_pair(startIndex, currentScore));
	openSet.insert(startIndex);
	
	score::priorityqueue pq;
	currentScore->handle = pq.push(currentScore);
	
	Ogre::Vector2 position;
	while(!pq.empty())
	{
		score* u = pq.top();
		pq.pop();
		position = u->position;
		int current = integer(position.y*width + position.x);

		// get currrent score and neighbour score
		currentScore = scoreMap[current];
		if(current == enemyIndex)
			continue;

		if(currentScore->length == length)
			break;
		
		openSet.erase(current);
		closedSet.insert(current);

		// explore neighbours
		std::vector<Ogre::Vector2> neighbours;
		exploreNeighbours(neighbours, position);

		for(Ogre::Vector2 pos : neighbours)
		{
			// if neighbour in closed set, do nothing
			int neighbour = integer(((int)pos.y)*width + pos.x);
			if(closedSet.find(neighbour) != closedSet.end())
					continue;
			
			// get neighbour score
			score* neighbourScore;
			std::map<int, score*>::iterator iScore = scoreMap.find(neighbour);
			if(iScore == scoreMap.end())
			{
				neighbourScore = new0 score();
				allocatedScores.push_back(neighbourScore);

				neighbourScore->randomizer = (float)(rand()%1000)/10000;
				neighbourScore->fscore = std::numeric_limits<float>::max();
				neighbourScore->gscore = std::numeric_limits<float>::max();
				neighbourScore->position = pos;
				neighbourScore->cameFrom = -1;
				scoreMap.insert(std::make_pair(neighbour, neighbourScore));
			}
			else
			{
				neighbourScore = iScore->second;
			}
			
			// calculate tentative g score
			//int dist = L1(position, pos);
			float tentativeGScore = currentScore->gscore + 1; //((dist == 1) ? 1 : 1.5);

			// if already in open and not a better path found, do nothing
			bool inOpen = openSet.find(neighbour) != openSet.end();
			if(inOpen && tentativeGScore >= neighbourScore->gscore)
				continue;
			
			// set back edge and g score
			neighbourScore->cameFrom = current;
			neighbourScore->gscore = tentativeGScore;
			neighbourScore->length = currentScore->length + 1;

			// set f score
			neighbourScore->fscore = tentativeGScore - Tile::dist(pos, enemy);

			if(neighbourScore->fscore - neighbourScore->length < bestScore->fscore - bestScore->length)
				bestScore = neighbourScore;

			if(inOpen)
			{
				pq.decrease(neighbourScore->handle);
			}
			else
			{
				openSet.insert(neighbour);
				neighbourScore->handle = pq.push(neighbourScore);
			}
		}
	}

	backtrack(bestScore->position, scoreMap, path);

	std::vector<score*>::iterator iter, iend;
	iter = allocatedScores.begin(); iend = allocatedScores.end();
	for(; iter != iend; ++iter)
		SafeDelete0(*iter);
}
void WorldQuery::findReachPath(const Ogre::Vector2& start,
	const Ogre::Vector2& targetPosition, std::vector<Ogre::Vector2>& path, int depth) const
{
	int d = depth;
	if(depth < 0)
		d = std::numeric_limits<int>::max();

	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::map<int, int> cameFromMarks;
	int startIndex = integer(((int)start.y)*width + start.x);
	cameFromMarks.insert(std::make_pair(startIndex, startIndex));

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(start);

	bool reached = false;
	while(!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		if(u == targetPosition)
		{
			reached = true;
			break;
		}

		int t = integer(((int)u.y)*width + u.x);

		// explore neighbours
		for(Ogre::Vector2 d : Tile::getNeighbours(u))
		{
			Ogre::Vector2 pos = u + d;
			if(pos.x < 0 || pos.x >= width
			|| pos.y < 0 || pos.y >= height)
				continue;

			Tile* v = mWorld->getTile(pos);
			if(v == 0 || v->isBlocking())
				continue;

			int s = integer(((int)pos.y)*width + pos.x);
			if(cameFromMarks.find(s) == cameFromMarks.end())
			{
				cameFromMarks.insert(std::make_pair(s, t));
				nextLevel.push_back(pos);
			}
		}

		if(currentLevel.empty())
		{
			if(--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}

	if(reached)
	{
		backtrack(targetPosition, start, cameFromMarks, path);
	}
}
bool WorldQuery::isShootable(const Ogre::Vector2& start, const Ogre::Vector2& targetPosition) const
{
	std::vector<Ogre::Vector2> path;
	Tile::hexLine(start, targetPosition, path);
	for(Ogre::Vector2 p : path)
		if(mWorld->isBlocking(p))
			return false;
	return true;
}

void WorldQuery::backtrack(const Ogre::Vector2& u, const Ogre::Vector2& v, 
	const std::map<int, score*>& scoreMap, std::vector<Ogre::Vector2>& path) const
{
	Ogre::Vector2 pos = u;
	int width = mWorld->getSize().width();
	int target = integer(((int)v.y)*width + v.x);
	while(true)
	{
		path.push_back(Ogre::Vector2(pos));
		int p = integer(((int)pos.y)*width + pos.x);
		if(p == target)
			return;
		
		score* n = scoreMap.at(p);
		int i = n->cameFrom;
		pos.x = real(i % width);
		pos.y = real(i / width);
	}
}
void WorldQuery::backtrack(const Ogre::Vector2& u, 
	const std::map<int, score*>& scoreMap, std::vector<Ogre::Vector2>& path) const
{
	Ogre::Vector2 pos = u;
	int width = mWorld->getSize().width();
	while(true)
	{
		path.push_back(Ogre::Vector2(pos));
		int p = integer(((int)pos.y)*width + pos.x);

		score* n = scoreMap.at(p);
		if(n->cameFrom < 0)
			return;

		int i = n->cameFrom;
		pos.x = real(i % width);
		pos.y = real(i / width);
	}
}
void WorldQuery::backtrack(const Ogre::Vector2& u, const Ogre::Vector2& v, 
	const std::map<int, int>& cameFromMarks, std::vector<Ogre::Vector2>& path) const
{
	Ogre::Vector2 pos = u;
	int width = mWorld->getSize().width();
	int target = integer(((int)v.y)*width + v.x);
	while(true)
	{
		path.push_back(Ogre::Vector2(pos));
		int p = integer(((int)pos.y)*width + pos.x);
		if(p == target)
			return;
		
		const int i = cameFromMarks.at(p);
		pos.x = real(i % width);
		pos.y = real(i / width);
	}
}
void WorldQuery::exploreNeighbours(std::vector<Ogre::Vector2>& neighbours,
								  const Ogre::Vector2& position) const
{
	for(Ogre::Vector2 d : Tile::getNeighbours(position))
	{
		Ogre::Vector2 pos = position + d;
		Tile* v = mWorld->getTile(pos);
		if(v == 0 || v->isBlocking())
			continue;
		neighbours.push_back(pos);
	}
}
void WorldQuery::exploreNeighbours(std::vector<Ogre::Vector2>& neighbours,
								  const Ogre::Vector2& position, const Ogre::Vector2& exclude) const
{
	for(Ogre::Vector2 d : Tile::getNeighbours(position))
	{
		Ogre::Vector2 pos = position + d;
		if(pos == exclude)
			continue;

		Tile* v = mWorld->getTile(pos);
		if(v == 0 || v->isBlocking())
			continue;
		neighbours.push_back(pos);
	}
}
void WorldQuery::exploreNeighboursGrid(std::vector<Ogre::Vector2>& neighbours,
								  const Ogre::Vector2& position) const
{
	int n = 0;
	for(int i = 0; i < 4; ++i)
	{
		int a = i%2;
		int b = i/2;
		int dx =  b-a;
		int dy =  1-(a+b);
		Ogre::Vector2 pos(position.x  + dx, position.y + dy);

		Tile* v = mWorld->getTile(pos);
		if(v == 0 || v->isBlocking())
			continue;
		n |= (1 << i);
		neighbours.push_back(pos);
	}
	if(n & 1 && n & 2)
	{
		Ogre::Vector2 pos(position.x  - 1, position.y + 1);
		Tile* v = mWorld->getTile(pos);
		if(v != 0 && !v->isBlocking())
			neighbours.push_back(pos);
	}
	if(n & 1 && n & 4)
	{
		Ogre::Vector2 pos(position.x  + 1, position.y + 1);
		Tile* v = mWorld->getTile(pos);
		if(v != 0 && !v->isBlocking())
			neighbours.push_back(pos);
	}
	if(n & 2 && n & 4)
	{
		Ogre::Vector2 pos(position.x  + 1, position.y - 1);
		Tile* v = mWorld->getTile(pos);
		if(v != 0 && !v->isBlocking())
			neighbours.push_back(pos);
	}
	if(n & 2 && n & 8)
	{
		Ogre::Vector2 pos(position.x  - 1, position.y - 1);
		Tile* v = mWorld->getTile(pos);
		if(v != 0 && !v->isBlocking())
			neighbours.push_back(pos);
	}
}
void WorldQuery::exploreNeighboursGrid(std::vector<Ogre::Vector2>& neighbours,
								  const Ogre::Vector2& position, const Ogre::Vector2& exclude) const
{
	int n = 0;
	for(int i = 0; i < 4; ++i)
	{
		int a = i%2;
		int b = i/2;
		int dx =  b-a;
		int dy =  1-(a+b);
		Ogre::Vector2 pos(position.x  + dx, position.y + dy);
		if(pos == exclude)
			continue;

		Tile* v = mWorld->getTile(pos);
		if(v == 0 || v->isBlocking())
			continue;
		n |= (1 << i);
		neighbours.push_back(pos);
	}
	if(n & 1 && n & 2)
	{
		Ogre::Vector2 pos(position.x  - 1, position.y + 1);
		if(pos != exclude)
		{
			Tile* v = mWorld->getTile(pos);
			if(v != 0 && !v->isBlocking())
				neighbours.push_back(pos);
		}
	}
	if(n & 1 && n & 4)
	{
		Ogre::Vector2 pos(position.x  + 1, position.y + 1);
		if(pos != exclude)
		{
			Tile* v = mWorld->getTile(pos);
			if(v != 0 && !v->isBlocking())
				neighbours.push_back(pos);
		}
	}
	if(n & 2 && n & 4)
	{
		Ogre::Vector2 pos(position.x  + 1, position.y - 1);
		if(pos != exclude)
		{
			Tile* v = mWorld->getTile(pos);
			if(v != 0 && !v->isBlocking())
				neighbours.push_back(pos);
		}
	}
	if(n & 2 && n & 8)
	{
		Ogre::Vector2 pos(position.x  - 1, position.y - 1);
		if(pos != exclude)
		{
			Tile* v = mWorld->getTile(pos);
			if(v != 0 && !v->isBlocking())
				neighbours.push_back(pos);
		}
	}
}
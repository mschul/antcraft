#include "AnPCH.h"
#include "AnAssert.h"

#ifdef AN_USE_ASSERT

#ifdef AN_USE_ASSERT_WRITE_TO_OUTPUT_WINDOW
#include <windows.h>
#endif

#ifdef AN_USE_ASSERT_WRITE_TO_MESSAGE_BOX
#include <intrin.h>
#endif

using namespace Ants;

const char* Assert::msDebugPrompt = "Do you want to debug?";
const size_t Assert::msDebugPromptLength = strlen(Assert::msDebugPrompt);
const char* Assert::msMessagePrefix = "\nAssert failed at %s(%d):\n";

#ifdef AN_USE_ASSERT_WRITE_TO_MESSAGE_BOX
const char* Assert::msMessageBoxTitle = "Assert Failed";
#endif

//----------------------------------------------------------------------------
Assert::Assert (bool condition, const char* file, int line,
    const char* format, ...)
{
    if (condition)
    {
        // The assertion is satisfied.
        return;
    }

    // The message prefix.
    char message[MAX_MESSAGE_BYTES];
	wchar_t wtext[MAX_MESSAGE_BYTES];

    const size_t maxPrefixBytes = MAX_MESSAGE_BYTES - msDebugPromptLength - 1;
    _snprintf_s(message, maxPrefixBytes, msMessagePrefix, file, line);

    // Append user-specified arguments.
    va_list arguments;
    va_start(arguments, format);
    size_t length = strlen(message);
    _vsnprintf_s(message + length, MAX_MESSAGE_BYTES, maxPrefixBytes - length, format, arguments);
    va_end(arguments);

#ifdef AN_USE_ASSERT_LOG_TO_FILE
    // Write the message to the log file.
    FILE* logFile = fopen(msLogFileName, "at");
    if (logFile)
    {
        fprintf(logFile, message);
    }
    fclose(logFile);
#endif

#ifdef AN_USE_ASSERT_WRITE_TO_OUTPUT_WINDOW
    // Write the message to the output debug window.
    OutputDebugString(wtext);
#endif

#ifdef AN_USE_ASSERT_WRITE_TO_MESSAGE_BOX
    // Give the user a chance to break-and-debug, to continue, or to
    // terminate execution.
	wchar_t wtitle[20];
	size_t rvalue;
	mbstowcs_s(&rvalue, wtitle, msMessageBoxTitle, strlen(msMessageBoxTitle)+1);//Plus null
    strcat_s(message, msDebugPrompt);
    int selection = ::MessageBox(0, wtext, wtitle,
        MB_ICONERROR | MB_YESNOCANCEL | MB_APPLMODAL | MB_TOPMOST);

    switch (selection)
    {
    case IDYES:
        // Break and debug.
        __debugbreak();
        break;
    case IDNO:
        // Continue execution.
        break;
    case IDCANCEL:
    default:
        // Terminate execution.
        exit(0);
        break;
    }
#endif
}
//----------------------------------------------------------------------------
Assert::~Assert ()
{
}
//----------------------------------------------------------------------------

#endif
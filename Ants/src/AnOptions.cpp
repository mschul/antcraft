#include "AnPCH.h"
#include "AnOptions.h"
#include "AnIO.h"
using namespace Ants;

template<> Options* Ogre::Singleton<Options>::msSingleton = 0;

Options::Options()
{
}

void Options::save(const Ogre::String& filename)
{
	std::ofstream out(filename);
	out << "[Graphics]" << std::endl;
	out << "Renderer=" << Renderer() << std::endl;
	out << "FSAA=" << FSAA() << std::endl;
	out << "Full Screen=" << FullScreen() << std::endl;
	out << "Video Mode=" << VideoMode() << std::endl;
	out << "VSync=" << VSync() << std::endl;
	out << "Colour Depth=" << ColourDepth() << std::endl;
}
void Options::load(const Ogre::String& filename)
{
	try
	{
		mConfig.load(filename, "=", true);
	
		Ogre::ConfigFile::SectionIterator seci = mConfig.getSectionIterator();
		Ogre::String sectionName;
		Ogre::String keyName;
		Ogre::String valueName;

		while (seci.hasMoreElements())
		{
			sectionName = seci.peekNextKey();
			Ogre::ConfigFile::SettingsMultiMap *settings = seci.getNext();
			Ogre::ConfigFile::SettingsMultiMap::iterator i;
			for (i = settings->begin(); i != settings->end(); ++i)
			{
				keyName = i->first;
				valueName = i->second;
				mOptions.insert(option(sectionName + "/" + keyName, valueName));
			}
		}
	}
	catch (Ogre::FileNotFoundException)
	{
	}

	if (mOptions.find("Graphics/Renderer") == mOptions.end())
		mOptions.insert(option("Graphics/Renderer", "OpenGL Rendering Subsystem"));
	if (mOptions.find("Graphics/FSAA") == mOptions.end())
		mOptions.insert(option("Graphics/FSAA", "0"));
	if (mOptions.find("Graphics/Full Screen") == mOptions.end())
		mOptions.insert(option("Graphics/Full Screen", "No"));
	if (mOptions.find("Graphics/Video Mode") == mOptions.end())
		mOptions.insert(option("Graphics/Video Mode", "1024 x 768"));
	if (mOptions.find("Graphics/Colour Depth") == mOptions.end())
		mOptions.insert(option("Graphics/Colour Depth", "32"));
	if (mOptions.find("Graphics/VSync") == mOptions.end())
		mOptions.insert(option("Graphics/VSync", "No"));
}

Ogre::String Options::Renderer() const
{
	return mOptions.find("Graphics/Renderer")->second;
}
Ogre::String Options::StandardRenderer() const
{
	return "OpenGL Rendering Subsystem";
}
Ogre::String Options::FSAA() const
{
	return mOptions.find("Graphics/FSAA")->second;
}
Ogre::String Options::FullScreen() const
{
	return mOptions.find("Graphics/Full Screen")->second;
}
Ogre::String Options::VideoMode() const
{
	return mOptions.find("Graphics/Video Mode")->second;
}
Ogre::String Options::VSync() const
{
	return mOptions.find("Graphics/VSync")->second;
}
Ogre::String Options::ColourDepth() const
{
	return mOptions.find("Graphics/Colour Depth")->second;
}
void Options::setRenderer(const Ogre::String& value)
{
	mOptions["Graphics/Renderer"] = value;
}
void Options::setFullScreen(const Ogre::String& value)
{
	mOptions["Graphics/Full Screen"] = value;
}
void Options::setVideoMode(const Ogre::String& value)
{
	mOptions["Graphics/Video Mode"] = value;
}
void Options::setColourDepth(const Ogre::String& value)
{
	mOptions["Graphics/Colour Depth"] = value;
}
void Options::setFSAA(const Ogre::String& value)
{
	mOptions["Graphics/FSAA"] = value;
}
void Options::setVSync(const Ogre::String& value)
{
	mOptions["Graphics/VSync"] = value;
}
#include "AnPCH.h"
#include "AnDrawableWorldMap.h"
using namespace Ants;

DrawableWorldMap::DrawableWorldMap()
{
	mPageSize = 6;
	mPages = 0;
	mUseInstancing = false;

	minX = -1;
	minY = -1;
	maxX = -1;
	maxY = -1;
}
DrawableWorldMap::~DrawableWorldMap()
{
	SafeDelete1(mPages);
	Ogre::TextureManager::getSingleton().remove(mMiniMapTexture->getHandle());
	Ogre::MaterialManager::getSingleton().remove(mMiniMapMaterial->getHandle());
}

void DrawableWorldMap::initScene(Ogre::SceneManager* sceneMgr)
{
	mSceneManager = sceneMgr;

	int pagesX = ceil((float)mWidth / mPageSize);
	int pagesY = ceil((float)mHeight / mPageSize);
	int pageCount = pagesX*pagesY;

	mPages = new1<WorldPage*>(pageCount);
	for (int index = 0, py = 0; py < pagesY; ++py)
	{
		int y = py * mPageSize;
		for (int px = 0; px < pagesX; ++px, ++index)
		{
			int x = px * mPageSize;
			mPages[index] = new0 WorldPage(x, y, mPageSize, mSceneManager, mUseInstancing);
		}
	}

	if (mUseInstancing)
	{
		for (int i = 0; i < (int)Tile::msMeshNames.size(); ++i)
		{
			Ogre::String name = Tile::msMeshNames[i];
			if (name != "")
			{
				Ogre::uint16 flags = Ogre::IM_USEALL;
				mInstanceManagers.push_back(mSceneManager->createInstanceManager(
					"InstanceMgr" + Ogre::StringConverter::toString(i), name,
					Ogre::ResourceGroupManager::AUTODETECT_RESOURCE_GROUP_NAME,
					Ogre::InstanceManager::HWInstancingBasic, 50 * 50, flags));
			}
			else
			{
				mInstanceManagers.push_back(0);
			}
		}
	}

	{
		// Create the texture
		mMiniMapTexture = Ogre::TextureManager::getSingleton().createManual(
			"MiniMapWorldTexture", Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
			Ogre::TEX_TYPE_2D, 512, 256, 0,
			Ogre::PF_BYTE_BGRA, Ogre::TU_DYNAMIC_WRITE_ONLY_DISCARDABLE);

		// Get the pixel buffer
		Ogre::HardwarePixelBufferSharedPtr pixelBuffer = mMiniMapTexture->getBuffer();

		// Lock the pixel buffer and get a pixel box
		pixelBuffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);
		const Ogre::PixelBox& pixelBox = pixelBuffer->getCurrentLock();

		Ogre::ARGB* pDest = static_cast<Ogre::ARGB*>(pixelBox.data);
		Ogre::ARGB black = Ogre::ColourValue::Black.getAsARGB();

		// Fill in some pixel data. This will give a semi-transparent blue,
		// but this is of course dependent on the chosen pixel format.
		for (size_t j = 0; j < 256; j++)
		{
			for (size_t i = 0; i < 512; i++)
				*pDest++ = black;

			pDest += pixelBox.getRowSkip() * Ogre::PixelUtil::getNumElemBytes(pixelBox.format);
		}

		// Unlock the pixel buffer
		pixelBuffer->unlock();

		// Create a material using the texture
		mMiniMapMaterial = Ogre::MaterialManager::getSingleton().create(
			"MiniMapMaterial", // name
			Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME);

		mMiniMapMaterial->getTechnique(0)->getPass(0)->createTextureUnitState("MiniMapWorldTexture");
		mMiniMapMaterial->getTechnique(0)->getPass(0)->setSceneBlending(Ogre::SBT_TRANSPARENT_ALPHA);
	}
}

void DrawableWorldMap::redrawMinimap()
{
	Ogre::HardwarePixelBufferSharedPtr pixelBuffer = mMiniMapTexture->getBuffer();
	pixelBuffer->lock(Ogre::HardwareBuffer::HBL_DISCARD);
	const Ogre::PixelBox& pixelBox = pixelBuffer->getCurrentLock();
	Ogre::ARGB* pDest = static_cast<Ogre::ARGB*>(pixelBox.data);
	Ogre::ARGB black = Ogre::ColourValue::Black.getAsARGB();

	unsigned int width = maxX - minX;
	unsigned int height = maxY - minY;
	unsigned int dx = width > height ? 0 : (height - width) / 2;
	unsigned int dy = width > height ? (width - height) / 2 : 0;
	unsigned int edge = std::max(width, height);
	for (size_t y = 0; y < 256; y++)
	{
		float relY = 1.0f - (float)y / 256;
		for (size_t x = 0; x < 512; x++)
		{
			float relX = (float)x / 512;
			Ogre::ARGB colour = black;
			int px = relX*edge;
			int py = relY*edge;

			if (py > dy && py < edge - dy
				&& px > dx && px < edge - dx)
			{
				Tile* tile = getTile(Ogre::Vector2(minX + px - dx, minY + py - dy));
				if (tile)
					colour = tile->getColour();
			}
			*pDest++ = colour;
		}

		pDest += pixelBox.getRowSkip() * Ogre::PixelUtil::getNumElemBytes(pixelBox.format);
	}
	pixelBuffer->unlock();
}
bool DrawableWorldMap::setTile(const Ogre::Vector2& position, Tile* tile, bool tracking)
{
	bool changed = WorldMap::setTile(position, tile, tracking);

	if (changed)
	{
		minX = (minX < 0 || minX > position.x) ? position.x : minX;
		maxX = (maxX < 0 || maxX < position.x) ? position.x : maxX;
		minY = (minY < 0 || minY > position.y) ? position.y : minY;
		maxY = (maxY < 0 || maxY < position.y) ? position.y : maxY;

		int px = position.x / mPageSize;
		int py = position.y / mPageSize;
		int pagesX = ceil((float)mWidth / mPageSize);
		int pageIndex = py * pagesX + px;
		mPages[pageIndex]->change(position, tile, mInstanceManagers);

		redrawMinimap();
	}
	return changed;
}
bool DrawableWorldMap::setTiles(const std::vector<Ogre::Vector2>& positions,
	const std::vector<Tile*>& tiles, bool tracking)
{
	bool changed = WorldMap::setTiles(positions, tiles, tracking);

	if (changed)
	{
		for (unsigned int i = 0; i < positions.size(); ++i)
		{
			Tile* tile = tiles[i];
			Ogre::Vector2 position = positions[i];
			minX = (minX < 0 || minX > position.x) ? position.x : minX;
			maxX = (maxX < 0 || maxX < position.x) ? position.x : maxX;
			minY = (minY < 0 || minY > position.y) ? position.y : minY;
			maxY = (maxY < 0 || maxY < position.y) ? position.y : maxY;

			{
				int px = position.x / mPageSize;
				int py = position.y / mPageSize;
				int pagesX = ceil((float)mWidth / mPageSize);
				int pageIndex = py * pagesX + px;
				mPages[pageIndex]->change(position, tile, mInstanceManagers);
			}
		}

		redrawMinimap();
	}
	return changed;
}
bool DrawableWorldMap::setTiles(const std::vector<PositionTilePair>& tiles, bool tracking)
{
	bool changed = WorldMap::setTiles(tiles, tracking);

	if (changed)
	{
		for (unsigned int i = 0; i < tiles.size(); ++i)
		{
			Tile* tile = tiles[i].second;
			Ogre::Vector2 position = tiles[i].first;
			minX = (minX < 0 || minX > position.x) ? position.x : minX;
			maxX = (maxX < 0 || maxX < position.x) ? position.x : maxX;
			minY = (minY < 0 || minY > position.y) ? position.y : minY;
			maxY = (maxY < 0 || maxY < position.y) ? position.y : maxY;

			{
				int px = position.x / mPageSize;
				int py = position.y / mPageSize;
				int pagesX = ceil((float)mWidth / mPageSize);
				int pageIndex = py * pagesX + px;
				mPages[pageIndex]->change(position, tile, mInstanceManagers);
			}
		}
		redrawMinimap();
	}
	return changed;
}

void DrawableWorldMap::updatePages()
{
	Ogre::Rect buildRect(mViewRect);
	buildRect.bottom = floor((float)buildRect.bottom / mPageSize) - 1;
	buildRect.left = floor((float)buildRect.left / mPageSize) - 2;
	buildRect.top = ceil((float)buildRect.top / mPageSize) + 1;
	buildRect.right = ceil((float)buildRect.right / mPageSize) + 1;

	Ogre::Rect loadRect(buildRect);
	loadRect.bottom -= 1;
	loadRect.left -= 1;
	loadRect.top += 1;
	loadRect.right += 1;

	int pagesX = ceil((float)mWidth / mPageSize);
	int pagesY = ceil((float)mHeight / mPageSize);

	for (int py = loadRect.bottom; py < loadRect.top; ++py)
	{
		if (py < 0 || py >= pagesY)
			continue;

		for (int px = loadRect.left; px < loadRect.right; ++px)
		{
			if (px < 0 || px >= pagesX)
				continue;

			int index = py*pagesX + px;

			if (buildRect.bottom <= py && buildRect.top > py
				&& buildRect.left <= px && buildRect.right > px)
			{
				mPages[index]->build(mMap, mWidth, mHeight, mInstanceManagers);
			}
			else if (loadRect.bottom <= py && loadRect.top > py
				&& loadRect.left <= px && loadRect.right > px)
			{
				mPages[index]->load(mMap, mWidth, mHeight, mInstanceManagers);
			}
			else
			{
				mPages[index]->drop(mMap, mWidth, mInstanceManagers);
			}
		}
	}
}

void DrawableWorldMap::setVisible(bool visibility)
{
	Ogre::Rect buildRect(mViewRect);
	buildRect.bottom = floor((float)buildRect.bottom / mPageSize) - 1;
	buildRect.left = floor((float)buildRect.left / mPageSize) - 1;
	buildRect.top = ceil((float)buildRect.top / mPageSize) + 1;
	buildRect.right = ceil((float)buildRect.right / mPageSize) + 1;

	Ogre::Rect loadRect(buildRect);
	loadRect.bottom -= 1;
	loadRect.left -= 1;
	loadRect.top += 1;
	loadRect.right += 1;

	int pagesX = ceil((float)mWidth / mPageSize);
	int pagesY = ceil((float)mHeight / mPageSize);

	for (int py = loadRect.bottom; py < loadRect.top; ++py)
	{
		if (py < 0 || py >= pagesY)
			continue;

		for (int px = loadRect.left; px < loadRect.right; ++px)
		{
			if (px < 0 || px >= pagesX)
				continue;

			int index = py*pagesX + px;

			if (buildRect.bottom <= py && buildRect.top > py
				&& buildRect.left <= px && buildRect.right > px)
			{
				mPages[index]->setVisible(visibility);
			}
		}
	}
}

Ogre::Rect DrawableWorldMap::getViewRect() const
{
	return mViewRect;
}
void DrawableWorldMap::setViewRect(const Ogre::Rect& rect)
{
	mViewRect = rect;
}
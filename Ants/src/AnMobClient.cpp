#include "AnPCH.h"
#include "AnMobClient.h"
#include "AnSpider.h"
#include "AnLadybug.h"
#include "AnEarthworm.h"
#include "AnBombardierBeetle.h"
#include "AnMobSpawner.h"
#include "AnProjectile.h"
#include "AnAOE.h"
using namespace Ants;

MobClient::MobClient ()
{
	mMobAmount = 0;
}
MobClient::~MobClient ()
{
}

void MobClient::update(float dt)
{
	mRooms->update(dt, this);
	mEntities->update(dt, mID, this);
}

void MobClient::setMobAmount(int mobs)
{
	mMobAmount = mobs;
}

void MobClient::onUpdatesReceived (std::vector<PositionTilePair> tiles,
                        std::vector<EntityDesc> addedEnts,
                        std::vector<EntityDesc> changedEnts,
                        std::vector<EntityDesc> removedEnts)
{

    for (PositionTilePair& pair : tiles)
    {
        if (pair.second->isAnthill () && !mJobQuery->hasJob (pair.first))
        {
            mJobs->add (Job::DESTROY_ANTHILL_JOB, pair.first);
        }
    }
}

void MobClient::settle(std::vector<Ogre::Vector2>& locations)
{
	std::vector<SpawnerIdPositionPair> spawners;
	mServer->createSpawners(mMobAmount, spawners);
	std::vector<PositionTilePair> tiles;

	for(SpawnerIdPositionPair pair : spawners)
	{
		int id = mRooms->assignRoom(MobSpawner::msSpawnerInfos[pair.first]->type, pair.second);
		mRooms->buildRoom(id, pair.second, tiles);
	}

	mWorld->setTiles(tiles, true);
	for(PositionTilePair pair : tiles)
		locations.push_back(pair.first);


}

// -> ACTION REACTOR IMPL
void MobClient::jobMilestoneReached(Job* job)
{
}
void MobClient::jobFinished(Job* job)
{
    switch (job->getType ())
    {
        case Job::DESTROY_ANTHILL_JOB:
        {
            DigJob* j = (DigJob*)job;
            Ogre::Vector2 targetPosition = j->getPosition ();
            mWorld->setTile (targetPosition, Tile::msTiles[Tile::ROUGH], true);

            Ogre::Rect size = mWorld->getSize ();
            int tile = integer (size.width ()*((int)targetPosition.y) + targetPosition.x);
            removeJobs (tile, Job::DESTROY_ANTHILL_JOB);
        }
    }
}
void MobClient::setExitTiles(const std::vector<std::pair<Ogre::Vector2, Tile*>>& tiles)
{
}
void MobClient::setTile(const Ogre::Vector2& position, int type)
{
}
void MobClient::createResource(const Ogre::Vector2& position, int roomType,
	Character* resource)
{
}
void MobClient::removeResource(Room* r)
{
}
Egg* MobClient::spawnEgg(const Ogre::Vector2& position)
{
	return 0;
}
Grub* MobClient::hatchEgg(Egg* egg)
{
	return 0;
}
void MobClient::unitDied(Character* unit, Attack* a)
{
	if (a)
	{
		Message* m = new0 Message(Message::MT_KILL_NOTIFICATION, a->getSourceClientID());
		m->addDatum("killedType", Ogre::StringConverter::toString((int)unit->getType()));
		m->addDatum("killedID", Ogre::StringConverter::toString((int)unit->getID()));
		m->addDatum("killedClientID", Ogre::StringConverter::toString((int)mID));
		m->addDatum("killedPosition", Ogre::StringConverter::toString(unit->getPosition()));
		mMessages.push_back(m);
	}
}
Ant* MobClient::spawnAnt(Grub* grub, int type)
{
	return 0;
}
Entity* MobClient::spawn(const Ogre::Vector2& position, int type)
{
	if (mWorld->isBlocking(position))
		return 0;

	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(position, hex);

	Entity* ent = 0;
	switch((Entity::EntityType)type)
	{
	case Entity::ET_LADYBUG:
		ent = new0 Ladybug(mServer->createEntityID(), mID,
			hex, position, 5,
			mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		break;
	case Entity::ET_SPIDER:
		ent = new0 Spider(mServer->createEntityID(), mID,
			hex, position, 5,
			mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		break;
	case Entity::ET_EARTHWORM:
		ent = new0 Earthworm(mServer->createEntityID(), mID,
			hex, position, 5,
			mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		break;
	case Entity::ET_BOMBARDIER_BEETLE:
		ent = new0 BombardierBeetle(mServer->createEntityID(), mID,
			hex, position, 5,
			mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		break;
	}
	mEntities->add(ent, true);
	return ent;
}
void MobClient::removeEntities(const std::vector<Entity*>& ents)
{
	mEntities->remove(ents, true);
}
void MobClient::aoeAttack(int sourceID, float attackPoints, const Ogre::Vector2& position, float radius)
{
	mAOEs.push_back(new0 AOE(sourceID, mID, attackPoints, position, radius));
}
void MobClient::meleeAttack(float attackPoints, Character* source, Entity* target)
{
	mAttacks.push_back(new0 Attack(attackPoints, source->getID(), target->getID(),
		source->getClientID(), target->getClientID()));
}
void MobClient::rangedAttack(float attackPoints, Character* source, Entity* target)
{
	Projectile* proj = new0 Projectile(mServer->createEntityID(), attackPoints,
		source, target->getPosition(), target->getTilePosition());
	mEntities->add(proj, true);
}
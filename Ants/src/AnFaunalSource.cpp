#include "AnPCH.h"
#include "AnFaunalSource.h"
#include "AnAnt.h"
using namespace Ants;

FaunalSource::FaunalSource (int id)
	: Room(id, 1)
{
	mSource = 0;
}
FaunalSource::~FaunalSource ()
{
}
		
Room::RoomType FaunalSource::getType() const
{
	return Room::FAUNAL_SOURCE;
}
bool FaunalSource::isResource() const
{
	return true;
}
bool FaunalSource::isDistructible() const
{
	return false;
}

void FaunalSource::build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	{
		reset(workers, se);
		mPivot.clear();
	}
	mPivot.clear();

	float minY = std::numeric_limits<float>::max();
	for(Ogre::Vector2 position : positions)
	{
		if(minY > position.y) minY = position.y;
	}
	for(Ogre::Vector2 position : positions)
	{
		if(position.y == minY)
		{
			mPivot.push_back(position);
			break;
		}
	}

	for(Ogre::Vector2 pos : positions)
	{
		slot* s = new0 slot();
		s->items.resize(mSlotCapacity);
		for (int i = 0; i < mSlotCapacity; ++i)
		{
			s->items[i].ent = 0;
			s->items[i].reservedFor = 0;
		}
		s->position = pos;
		s->reserved = 0;
		s->capacity = mSlotCapacity;
		mSlots.push_back(s);
	}
}
void FaunalSource::update(float dt, ActionReactor* reactor)
{
    Room::update (dt, reactor);
}
void FaunalSource::getJob(std::vector<WorkplaceSubjobs>& jobs)
{
}

void FaunalSource::setSource(Character* source)
{
	mSource = source;
	mSlots[0]->items[0].ent = source;
}
Character* FaunalSource::getSource() const
{
	return mSource;
}
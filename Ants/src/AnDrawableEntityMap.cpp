#include "AnPCH.h"
#include "AnDrawableEntityMap.h"
#include "OgreBillboardSet.h"
#include "OgreBillboard.h"
using namespace Ants;

DrawableEntityMap::DrawableEntityMap()
{
	mSceneMgr = 0;
	mFowMaskMap = 0;
}
DrawableEntityMap::~DrawableEntityMap()
{
}

void DrawableEntityMap::initScene(Ogre::SceneManager* sceneMgr, bool fow)
{
	mSceneMgr = sceneMgr;
	mRootNode = sceneMgr->getRootSceneNode()->createChildSceneNode();
	mHealthBars = sceneMgr->createBillboardSet();
	mHealthBars->setMaterialName("healthBarsMat");
	mHealthBars->setDefaultDimensions(1, 0.1);
	mHealthBars->setVisibilityFlags(WORLD_VISIBILITY_FLAG);
	mRootNode->attachObject(mHealthBars);

	if (fow)
	{
		mFowMaskMap = new0 FowMaskMap();
		mFowMaskMap->init(mWidth, mHeight, mRootNode, sceneMgr);
	}
}

void DrawableEntityMap::add(Entity* ent, bool tracking)
{
	EntityMap::add(ent, tracking);

	if (!mBusy)
	{
		Ogre::String name = "Entity_";
		name += Ogre::StringConverter::toString(ent->getID());

		assert(!mSceneMgr->hasSceneNode(name));
		Ogre::SceneNode* node = mRootNode->createChildSceneNode(name);

		assert(!mSceneMgr->hasEntity(name));
		Ogre::Entity* entity = mSceneMgr->createEntity(name, ent->getStandardMesh());
		entity->setMaterialName(ent->getMaterialName());
		entity->setVisibilityFlags(WORLD_VISIBILITY_FLAG);
		node->attachObject(entity);
		node->setScale(0.3f, 0.5f, 0.3f);

		const Ogre::Vector2 p = ent->getPosition();
		node->setPosition(Ogre::Vector3(p.x * 2, p.y * 2, 0.3f));

		if (ent->isCharacter())
		{
			Ogre::Billboard* b = mHealthBars->createBillboard(Ogre::Vector3(p.x * 2, p.y * 2 + 0.5f, 0.7f));

			const float dy = 0.078125f;
			int h = ent->getHealth() / 10;
			b->setTexcoordRect(0.0f, dy*h, 1.0f, dy*(h + 1));
			
			Ogre::UserObjectBindings& bindings = node->getUserObjectBindings();
			bindings.setUserAny(Ogre::Any(b));
		}
	}
}
void DrawableEntityMap::add(const std::vector<Entity*>& entities, bool tracking)
{
	for (Entity* ent : entities)
		add(ent, tracking);
}
void DrawableEntityMap::change(Entity* ent, bool tracking)
{
	EntityMap::change(ent, tracking);

	if (!mBusy)
	{
		Ogre::String name = "Entity_";
		name += Ogre::StringConverter::toString(ent->getID());

		assert(mSceneMgr->hasSceneNode(name));
		Ogre::SceneNode* node = mSceneMgr->getSceneNode(name);
		const Ogre::Vector2 np = ent->getPosition();
		const Ogre::Quaternion nq = Ogre::Quaternion(ent->getOrientation(), Ogre::Vector3::UNIT_Z);
		node->setOrientation(nq);
		node->setPosition(Ogre::Vector3(np.x * 2, np.y * 2, 0.3f));

		if (ent->isCharacter())
		{
			Ogre::UserObjectBindings& bindings = node->getUserObjectBindings();

			Ogre::Billboard* b = bindings.getUserAny().get<Ogre::Billboard*>();

			const float dy = 0.078125f;
			int h = ent->getHealth() / 10;
			b->setTexcoordRect(0.0f, dy*h, 1.0f, dy*(h + 1));
			b->setPosition(Ogre::Vector3(np.x * 2, np.y * 2 + 0.5f, 0.7f));
		}
	}
}
void DrawableEntityMap::change(const std::vector<Entity*>& entities, bool tracking)
{
	for (Entity* ent : entities)
		change(ent, tracking);
}
void DrawableEntityMap::remove(Entity* ent, bool tracking)
{
	Ogre::String name;
	if (!mBusy)
	{
		name = "Entity_";
		name += Ogre::StringConverter::toString(ent->getID());

		assert(mSceneMgr->hasSceneNode(name));
		Ogre::SceneNode* node = mSceneMgr->getSceneNode(name);
		if (ent->isCharacter())
		{
			Ogre::UserObjectBindings& bindings = node->getUserObjectBindings();
			Ogre::Billboard* b = bindings.getUserAny().get<Ogre::Billboard*>();
			mHealthBars->removeBillboard(b);
		}
		destroyAllAttachedMovableObjects(node);
		mSceneMgr->destroySceneNode(node);
	}

	EntityMap::remove(ent, tracking);
}
void DrawableEntityMap::remove(const std::vector<Entity*>& entities, bool tracking)
{
	for (Entity* ent : entities)
		remove(ent, tracking);
}

void DrawableEntityMap::onEntityChange(Entity* ent)
{
	if (mFowMaskMap) mFowMaskMap->update(ent);
}

void DrawableEntityMap::setEntityVisible(bool visiblity)
{
	mRootNode->setVisible(visiblity);
}
void DrawableEntityMap::setHealthBarsVisible(bool visiblity)
{
	mHealthBars->setVisible(visiblity);
}
void DrawableEntityMap::toggleHealthBarsVisible()
{
	mHealthBars->setVisible(!mHealthBars->isVisible());
}
void DrawableEntityMap::setVisiblityMaskVisible(bool visiblity)
{
}
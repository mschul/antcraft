#include "AnPCH.h"
#include "AnAnt.h"
#include "AnWorkerTrait.h"
#include "AnWarriorTrait.h"
#include "AnEscapeTrait.h"
using namespace Ants;

std::vector<BehaviourTrait<Ant>*> Ant::msTraits;
int Ant::msAntCount = 0;

Ant::Ant (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
		: Character(id, clientID, position, tilePosition, visibilityRadius,
		worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mFreelancing = true;
	msAntCount++;
	if(msTraits.empty())
	{
		msTraits.push_back(new0 WorkerTrait<Ant>());
		msTraits.push_back(new0 WarriorTrait<Ant>());
		msTraits.push_back(new0 EscapeTrait<Ant>());
	}
	mHomeRoomType = Room::NONE;
	mJobFill = 0;
	mActiveJobIndex = 0;
	mHomeRoom = 0;
	mFetching = false;
	mAntibiotics = false;
	mInventoryFill = 0;
	mActiveTrait = -1;
}
Ant::Ant (const EntityDesc& desc)
	: Character(desc)
{
}
Ant::~Ant ()
{
	if(msAntCount == 0)
	{
		delete0(msTraits[0]);
		delete0(msTraits[1]);
		delete0(msTraits[2]);
		msTraits.clear();
	}
}

void Ant::applyUpgrade(Entity::Upgrade u)
{
	if (u == Entity::U_ANTIBIOTICS)
	{
		mAntibiotics = true;
	}
	else
	{
		Character::applyUpgrade(u);
	}
}
bool Ant::isAnt() const
{
	return true;
}
bool Ant::isFreelancer() const
{
	return mHomeRoom == 0;
}

void Ant::drop(Entity* item)
{
	int i = 0;
	for(; i < (int)mInventory.size(); ++i)
		if(item == mInventory[i])
			break;
	if(i < (int)mInventory.size())
	{
		drop(item, i);
	}
}
void Ant::drop(Entity* item, int index)
{
	assert(mInventory[index]);
	mInventory[index]->setParent(0);
	mInventory[index] = 0;
	mInventoryFill--;
}
void Ant::dropAll()
{
	for(int i = 0; i < (int)mInventory.size(); ++i)
	{
		Entity* item = mInventory[i];
		if(item)
		{
			item->setParent(0);
			mInventory[i] = 0;
		}
	}
	mInventoryFill = 0;
}
void Ant::pick(Entity* item, int index)
{
	assert(mInventoryFill < (int)mInventory.size());
	mInventory[index] = item;
	mInventoryFill++;
	item->setParent(this);
}

void Ant::move(float dt, ActionReactor* reactor)
{
	if(mInventoryFill > 0)
	{
		Ogre::Vector2 delta = rotateVector2(Ogre::Vector2::UNIT_Y, mOrientation);
		for(Entity* item : mInventory)
		{
			// TODO: add some (random?) disturbance
			if(item)
			{
				item->setPosition(mPosition + 0.3f*delta);
				item->setTilePosition(mTilePosition);
			}
		}
	}

	ThroneChamber* tc = mRoomQuery->getThroneChamber();

	if (getType() == Entity::ET_SMALL_WORKER)
	{
		if (tc->getWorkerBehaviour() == ThroneChamber::EXPAND)
		{
			mP3Jobs |= Job::FLATTEN_JOB;
		}
		else
		{
			mP3Jobs &= ~Job::FLATTEN_JOB;
		}
	}
	else if (getType() == Entity::ET_WORKER)
	{
		if (tc->getWorkerBehaviour() == ThroneChamber::EXPAND)
		{
			mP2Jobs |= Job::FLATTEN_JOB;
		}
		else
		{
			mP2Jobs &= ~Job::FLATTEN_JOB;
		}
	}
	else if (getType() == Entity::ET_CARRIER)
	{
		if (tc->getWorkerBehaviour() == ThroneChamber::EXPAND)
		{
			mP1Jobs |= Job::FLATTEN_JOB;
		}
		else
		{
			mP1Jobs &= ~Job::FLATTEN_JOB;
		}
	}

	msTraits[mActiveTrait]->update(dt, this, reactor);
}
void Ant::die(ActionReactor* reactor)
{
#ifdef _DEBUG
	Framework::getSingleton().mLog->logMessage(
		"ant " + Ogre::StringConverter::toString(getID())
		+ " died!!"
		);
#endif
	for (int i = 0; i < (int)mInventory.size(); ++i)
	{
		Room* r = mTargetRooms[i];
		Room::slot* s = mAssignedSlots[i];

		if (s)
		{
			s->reserved--;
			assert(s->reserved >= 0);
		}

		if (r)
		{
			std::vector<Entity*> guests = r->getGuests();
			for (Entity* e : guests)
			{
				if (e == this)
					r->unbookGuest(this);
			}
		}
		mTargetRooms[i] = 0;
	}

	msAntCount--;

	// is this ant currently trying to fetch or place something?
	for(int i = 0; i < (int)mJobs.size(); ++i)
	{
		while(!mSubJobs[i].empty())
			mSubJobs[i].pop();
		mAssignedSlots[i] = 0;
	}
	
	// just let go of item (jobs keep track of it)
	if(!mInventory.empty())
	{
		dropAll();
	}

	// clear path
	clearPath();

	// unassign from all previous jobs
	for(int j : mJobs)
	{
		if(j >= 0)
			mJobQuery->unAssignJob(this, j);
	}
	mJobs.assign(mJobs.size(), -1);
	mJobFill = 0;

	// unassign from homeroom
	if(mHomeRoom) mHomeRoom->unassignWorker(this);
	mHomeRoom = 0;

	// clear nest
	if(mSlot)
	{
		int i = 0;
		for(; i < (int)mSlot->items.size(); ++i) if(mSlot->items[i].ent == this) break;
		if(i < (int)mSlot->items.size())
		{
			mSlot->items[i].ent = 0;
			mSlot->capacity++;
		}
	}
	mSlot = 0;

	if (getType() == Entity::ET_SOLDIER)
		reactor->createResource(mTilePosition, Room::FAUNAL_SOURCE);
}

WorkerTrait<Ant>* Ant::getWorkerTrait()
{
	return (WorkerTrait<Ant>*)msTraits[0];
}
WarriorTrait<Ant>* Ant::getWarriorTrait()
{
	return (WarriorTrait<Ant>*)msTraits[1];
}
EscapeTrait<Ant>* Ant::getEscapeTrait()
{
	return (EscapeTrait<Ant>*)msTraits[2];
}


void Ant::prepareForNewJob(Room::slot* s)
{
	for (int i = 0; i < mAssignedSlots.size(); ++i)
		if (mAssignedSlots[i] == s)
			prepareForNewJob(i);
}
void Ant::prepareForNewJob(int jobIndex)
{
	while (!mSubJobs[jobIndex].empty()) mSubJobs[jobIndex].pop();
	if (mInventory[jobIndex])
		drop(mInventory[jobIndex]);
	if (mJobs[jobIndex] >= 0)
	{
		mJobQuery->unAssignJob(this, mJobs[jobIndex]);
		mJobs[jobIndex] = -1;
		mJobFill--;
	}

	if (mTargetRooms[jobIndex])
	{
		mTargetRooms[jobIndex]->unbookGuest(this);
		mTargetRooms[jobIndex] = 0;
	}
	if (mAssignedSlots[jobIndex])
	{
		mAssignedSlots[jobIndex]->reserved--;
		for (int i = 0; i < mAssignedSlots[jobIndex]->items.size(); ++i)
			if (mAssignedSlots[jobIndex]->items[i].reservedFor == this)
				mAssignedSlots[jobIndex]->items[i].reservedFor = 0;
		mAssignedSlots[jobIndex] = 0;
	}
}
void Ant::enqueueJob(Job* job)
{
	assert(mJobFill >= 0 && mJobFill < (int)mJobs.size());

	int i = 0;
	for(; i < (int)mJobs.size(); ++i)
		if(mJobs[i] < 0)
			break;

	if(i < (int)mJobs.size())
	{
		if(isIdle())
		{
			clearPath();
		}

		if(mActiveJobIndex < 0)
			mActiveJobIndex = 0;

		prepareForNewJob(i);
		mJobFill++;
		mJobs[i] = job->getID();
	}
}
void Ant::assignToWorkplace(Room* room)
{
	// this case is quite common if room is exanding.
	if (mHomeRoom == room)
	{
		return;
	}

	// find current state of ant
	if (!mHomeRoom)
	{
		// is this ant currently trying to fetch or place something?
		for(int i = 0; i < (int)mJobs.size(); ++i)
		{
			if(mAssignedSlots[i])
			{
				// it seems there is no need to tell either target or homeroom
				// but we should cancel the reservation
				mAssignedSlots[i]->reserved--;
			}

			prepareForNewJob(i);
		}

		// just let go of item (jobs keep track of it)
		if(!mInventory.empty())
		{
			dropAll();
		}

		// clear path
		clearPath();

		// unassign from all previous jobs
		for(int j : mJobs)
		{
			if(j >= 0)
				mJobQuery->unAssignJob(this, j);
		}
		mJobs.assign(mJobs.size(), -1);
		mJobFill = 0;
	}


	// unassign from old homeroom
	if (mHomeRoom) mHomeRoom->unassignWorker(this);

	mFreelancing = false;
	mHomeRoomType = room->getType();
	mHomeRoom = room;
	mCurrentState = FIND_JOB;
}
void Ant::unAssignFromWorkplace()
{
	clearPath();
	if(!mInventory.empty())
	{
		dropAll();
	}
	mHomeRoom = 0;
	mHomeRoomType = Room::NONE;
	mCurrentState = IDLE_WALKING;

	for(int i = 0; i < (int)mJobs.size(); ++i)
	{
		if(mAssignedSlots[i])
		{
			// it seems there is no need to tell either target or homeroom
			// but we should cancel the reservation
			mAssignedSlots[i]->reserved--;
		}
		while(!mSubJobs[i].empty())
			mSubJobs[i].pop();
		mAssignedSlots[i] = 0;
		mTargetRooms[i] = 0;
	}
	mFreelancing = true;
}
void Ant::targetRoomDestroyed(Room* room)
{
	for (int i = 0; i < mTargetRooms.size(); ++i)
	{
		if (mTargetRooms[i] == room)
		{
			prepareForNewJob(i);
		}
	}
}

bool Ant::isIdle() const
{
	return !mHomeRoom && (mCurrentState == IDLE_STANDING || mCurrentState == IDLE_WALKING)
		&& mJobFill < (int)mJobs.size();
}
bool Ant::canWork(Job::JobType type) const
{
	return ((mP1Jobs & type) | (mP2Jobs & type) | (mP3Jobs & type)) != 0;
}

bool Ant::findPathToWorkplace()
{
	std::vector<Ogre::Vector2> path;
	mWorldQuery->findShortestPath(mHomeRoom->getPivot(mTilePosition), mTilePosition, path);

	if(path.empty())
		return false;
	savePath(path);
	return true;
}
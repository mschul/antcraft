#include "AnPCH.h"
#include "AnGotoJob.h"
#include "AnClient.h"
using namespace Ants;

GotoJob::GotoJob (int id, const Ogre::Vector2& position, const Ogre::Vector2& targetPosition)
	: Job(id, 1, position)
{
	mTargetPosition = targetPosition;
}
GotoJob::GotoJob (int id, const Ogre::Vector2& position)
	: Job(id, 1, position)
{
	mTargetPosition = position;
}
GotoJob::~GotoJob ()
{
}

Job::JobType GotoJob::getType() const
{
	return Job::GOTO_JOB;
}

bool GotoJob::update(double dt, ActionReactor* reactor)
{
	return true;
}

Ogre::Vector2 GotoJob::getTargetPosition() const
{
	return mTargetPosition;
}
void GotoJob::getSubJobs(std::vector<Job::SubJobs>& jobs) const
{
	jobs.push_back(Job::UPDATE);
}


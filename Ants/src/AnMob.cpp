#include "AnPCH.h"
#include "AnMob.h"
#include "AnIdleTrait.h"
#include "AnWarriorTrait.h"
#include "AnEscapeTrait.h"
#include "AnRansackTrait.h"
using namespace Ants;

std::vector<BehaviourTrait<Mob>*> Mob::msTraits;
int Mob::msMobCount = 0;

Mob::Mob (int id, int clientID,
		const Ogre::Vector2& position,
		const Ogre::Vector2& tilePosition, 
		int visibilityRadius,
		WorldQuery* worldQuery, JobQuery* jobQuery,
		EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
	: Character(id, clientID, position, tilePosition, visibilityRadius,
	worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	msMobCount++;
	if(msTraits.empty())
	{
		msTraits.push_back(new0 IdleTrait<Mob>());
		msTraits.push_back(new0 WarriorTrait<Mob>());
        msTraits.push_back (new0 EscapeTrait<Mob> ());
        msTraits.push_back (new0 RansackTrait<Mob> ());
    }
    mJobFill = 0;
    mActiveJobIndex = 0;
}
Mob::Mob (const EntityDesc& desc)
	: Character(desc)
{
}
Mob::~Mob ()
{
	if (msMobCount == 0)
	{
		delete0(msTraits[0]);
		delete0(msTraits[1]);
		delete0(msTraits[2]);
		msTraits.clear();
	}
}

void Mob::move(float dt, ActionReactor* reactor)
{
	msTraits[mActiveTrait]->update(dt, this, reactor);
}
void Mob::die(ActionReactor* reactor)
{
#ifdef _DEBUG
	Framework::getSingleton().mLog->logMessage(
		"mob " + Ogre::StringConverter::toString(getID())
		+ " died!!"
		);
#endif
	// clear nest
	if(mSlot)
	{
		int i = 0;
		for(; i < (int)mSlot->items.size(); ++i) if(mSlot->items[i].ent == this) break;
		if(i < (int)mSlot->items.size())
		{
			mSlot->items[i].ent = 0;
			mSlot->capacity++;
		}
	}
	mSlot = 0;
	msMobCount--;

	reactor->createResource(mTilePosition, Room::FAUNAL_SOURCE);
}

void Mob::prepareForNewJob (int jobIndex)
{
    while (!mSubJobs[jobIndex].empty ()) mSubJobs[jobIndex].pop ();
    if (mJobs[jobIndex] >= 0)
    {
        mJobQuery->unAssignJob (this, mJobs[jobIndex]);
        mJobs[jobIndex] = -1;
        mJobFill--;
    }
}

IdleTrait<Mob>* Mob::getIdleTrait()
{
	return (IdleTrait<Mob>*)msTraits[0];
}
WarriorTrait<Mob>* Mob::getWarriorTrait()
{
	return (WarriorTrait<Mob>*)msTraits[1];
}
EscapeTrait<Mob>* Mob::getEscapeTrait()
{
	return (EscapeTrait<Mob>*)msTraits[2];
}
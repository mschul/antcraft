#include "AnPCH.h"
#include "AnEntityQuery.h"
#include "AnAnt.h"
using namespace Ants;

EntityQuery::EntityQuery (WorldMap* world, EntityMap* entities)
{
	mWorld = world;
	mEntities = entities;
}
EntityQuery::~EntityQuery ()
{
}

void EntityQuery::getEntities(const Ogre::Vector2& position, int radius, std::vector<Entity*>& ents)
{
	std::vector<Ogre::Vector2> positions;
	Tile::hexCircle(position, radius, positions);
	mEntities->get(positions, ents);
}

bool EntityQuery::enemyBlocking(const Ogre::Vector2& position, int clientID)
{
	std::vector<Entity*> ents = mEntities->get(position);
	for(Entity* e : ents)
	{
		if(e->isCharacter() && e->getClientID() != clientID)
		{
			Character* c = (Character*)e;
			if(c->isAlive())
				return true;
		}
	}
	return false;
}

void EntityQuery::findNextIdleWorker(int id, Entity*& targetEntity, int clientID)
{
	targetEntity = 0;
	int nextID = id;

	bool left = false;
	do
	{
		Entity* ent = mEntities->findNext(nextID);
		if(ent)
		{
			if(nextID > ent->getID())
				left = true;
			nextID = ent->getID();
			if(ent->isAnt())
			{
				Ant* ant = (Ant*)ent;
				if(ant->isIdle() && (clientID < 0 || ent->getClientID() == clientID))
				{
					targetEntity = ant;
					return;
				}
			}
		}
		else
		{
			return;
		}
	} while(!left || nextID < id);
}
void EntityQuery::findNextGrub(int id, Entity*& targetEntity, int clientID)
{
	targetEntity = 0;
	int nextID = id;

	bool left = false;
	do
	{
		Entity* ent = mEntities->findNext(nextID);
		if(ent)
		{
			if(nextID > ent->getID())
				left = true;
			nextID = ent->getID();
			if (ent->getType() == Entity::ET_GRUB &&
				(clientID < 0 || ent->getClientID() == clientID))
			{
				targetEntity = ent;
				return;
			}
		}
		else
		{
			return;
		}
	} while(!left || nextID < id);
}

int EntityQuery::getEntityFlag(const std::vector<Entity::EntityType>& types)
{
	int flags = 0;
	for (Entity::EntityType t : types)
	{
		flags |= (1 << (int)t);
	}
	return flags;
}

void EntityQuery::findNextEntity(const Ogre::Vector2& start,
	Ogre::Vector2& targetPosition, Entity*& targetEntity,
	int flags, int clientID, int depth) const
{
	targetEntity = 0;
	
	int d = depth;
	if(depth < 0)
		d = std::numeric_limits<int>::max();

	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::set<int> marks;
	marks.insert(integer(((int)start.y)*width + start.x));

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(start);

	while(!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		if(exploreTile(u, targetPosition, targetEntity, flags, clientID))
			break;

		// explore neighbours
		for(Ogre::Vector2 d : Tile::getNeighbours(u))
		{
			Ogre::Vector2 pos = u + d;
			if(pos.x < 0 || pos.x >= width
			|| pos.y < 0 || pos.y >= height)
				continue;

			Tile* v = mWorld->getTile(pos);
			if(v == 0 || v->isBlocking())
				continue;

			int p = integer(((int)pos.y)*width + pos.x);
			if(marks.find(p) == marks.end())
			{
				marks.insert(p);
				nextLevel.push_back(pos);
			}
		}

		if(currentLevel.empty())
		{
			if(--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}
}
void EntityQuery::findNextIdleEntity(const Ogre::Vector2& start,
	Ogre::Vector2& targetPosition, Entity*& targetEntity,
	int flags, int clientID, int depth) const
{
	targetEntity = 0;
	
	int d = depth;
	if(depth < 0)
		d = std::numeric_limits<int>::max();
	
	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::set<int> marks;
	marks.insert(((int)start.y)*width + start.x);

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(start);

	while(!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		if (exploreTileForIdle(u, targetPosition, targetEntity, flags, clientID))
			break;

		// explore neighbours
		for(Ogre::Vector2 d : Tile::getNeighbours(u))
		{
			Ogre::Vector2 pos = u + d;
			if(pos.x < 0 || pos.x >= width
			|| pos.y < 0 || pos.y >= height)
				continue;

			Tile* v = mWorld->getTile(pos);
			if(v == 0 || v->isBlocking())
				continue;

			int p = integer(((int)pos.y)*width + pos.x);
			if(marks.find(p) == marks.end())
			{
				marks.insert(p);
				nextLevel.push_back(pos);
			}
		}

		if(currentLevel.empty())
		{
			if(--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}
}
void EntityQuery::findNextFreelancer(const Ogre::Vector2& start,
	Ogre::Vector2& targetPosition, Entity*& targetEntity,
	int flags, int clientID, int depth) const
{
	targetEntity = 0;

	int d = depth;
	if (depth < 0)
		d = std::numeric_limits<int>::max();

	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::set<int> marks;
	marks.insert(integer(((int)start.y)*width + start.x));

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(start);

	while (!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		if (exploreTileForFreelancer(u, targetPosition, targetEntity, flags, clientID))
			break;

		// explore neighbours
		for (Ogre::Vector2 d : Tile::getNeighbours(u))
		{
			Ogre::Vector2 pos = u + d;
			if (pos.x < 0 || pos.x >= width
				|| pos.y < 0 || pos.y >= height)
				continue;

			Tile* v = mWorld->getTile(pos);
			if (v == 0 || v->isBlocking())
				continue;

			int p = integer(((int)pos.y)*width + pos.x);
			if (marks.find(p) == marks.end())
			{
				marks.insert(p);
				nextLevel.push_back(pos);
			}
		}

		if (currentLevel.empty())
		{
			if (--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}
}
void EntityQuery::findNextIdleEntityForJob(
	const Ogre::Vector2& start, Ogre::Vector2& targetPosition,
	Entity*& targetEntity, Job::JobType jobtype,
	int flags, int clientID, int depth) const
{
	targetEntity = 0;

	int d = depth;
	if(depth < 0)
		d = std::numeric_limits<int>::max();
	
	Ogre::Rect r = mWorld->getSize();
	int width = r.width();
	int height = r.height();

	std::set<int> marks;
	marks.insert(((int)start.y)*width + start.x);

	std::vector<Ogre::Vector2> currentLevel;
	std::vector<Ogre::Vector2> nextLevel;
	currentLevel.push_back(start);

	while(!currentLevel.empty())
	{
		Ogre::Vector2 u = currentLevel.back();
		currentLevel.pop_back();

		if (exploreTileForIdleAndJob(u, targetPosition, targetEntity, jobtype,
			flags, clientID))
			break;

		// explore neighbours
		for(Ogre::Vector2 d : Tile::getNeighbours(u))
		{
			Ogre::Vector2 pos = u + d;
			if(pos.x < 0 || pos.x >= width
			|| pos.y < 0 || pos.y >= height)
				continue;

			Tile* v = mWorld->getTile(pos);
			if(v == 0 || v->isBlocking())
				continue;

			int p = ((int)pos.y)*width + pos.x;
			if(marks.find(p) == marks.end())
			{
				marks.insert(p);
				nextLevel.push_back(pos);
			}
		}

		if(currentLevel.empty())
		{
			if(--d < 0)
				break;
			std::swap(currentLevel, nextLevel);
		}
	}
}


bool EntityQuery::exploreTile(const Ogre::Vector2& u,
	Ogre::Vector2& targetPosition, Entity*& targetEntity,
	int flag, int clientID) const
{
	std::vector<Entity*> ents = mEntities->get(u);
	std::vector<Entity*>::iterator iter, iend;
	if(ents.empty())
		return false;
	iter = ents.begin(); iend = ents.end();
	for(; iter != iend; ++iter)
	{
		Entity* ent = *iter;
		int t = (1 << (int)ent->getType());
		if (!(t & flag) || ent->getParent() || ent->getSlot())
			continue;

		if(clientID < 0 || clientID == ent->getClientID())
			break;
	}
	if(iter != iend)
		targetEntity = *iter;
	if(targetEntity)
		targetPosition = targetEntity->getTilePosition();
	return targetEntity != 0;
}
bool EntityQuery::exploreTileForIdle(const Ogre::Vector2& u,
	Ogre::Vector2& targetPosition, Entity*& targetEntity,
	int flag, int clientID) const
{
	std::vector<Entity*> ents = mEntities->get(u);
	std::vector<Entity*>::iterator iter, iend;
	if(ents.empty())
		return false;
	if(clientID < 0)
	{
		targetEntity = *ents.begin();
	}
	else
	{
		iter = ents.begin(); iend = ents.end();
		for(; iter != iend; ++iter)
		{
			Entity* ent = *iter;
			int t = (1 << (int)ent->getType());
			if (!(t & flag))
				continue;

			if(ent->isAnt())
			{
				Ant* ant = (Ant*)ent;
				if(ant->getClientID() == clientID && ant->isIdle())
					break;
			}
		}
		if(iter != iend)
			targetEntity = *iter;
	}
	if(targetEntity)
		targetPosition = targetEntity->getTilePosition();
	return targetEntity != 0;
}
bool EntityQuery::exploreTileForFreelancer(const Ogre::Vector2& u,
	Ogre::Vector2& targetPosition, Entity*& targetEntity,
	int flag, int clientID) const
{
	std::vector<Entity*> ents = mEntities->get(u);
	std::vector<Entity*>::iterator iter, iend;
	if (ents.empty())
		return false;
	if (clientID < 0)
	{
		targetEntity = *ents.begin();
	}
	else
	{
		iter = ents.begin(); iend = ents.end();
		for (; iter != iend; ++iter)
		{
			Entity* ent = *iter;
			int t = (1 << (int)ent->getType());
			if (!(t & flag))
				continue;

			if (ent->isAnt())
			{
				Ant* ant = (Ant*)ent;
				if (ant->getClientID() == clientID && ant->isFreelancer())
					break;
			}
		}
		if (iter != iend)
			targetEntity = *iter;
	}
	if (targetEntity)
		targetPosition = targetEntity->getTilePosition();
	return targetEntity != 0;
}
bool EntityQuery::exploreTileForIdleAndJob(const Ogre::Vector2& u,
	Ogre::Vector2& targetPosition, Entity*& targetEntity,
	Job::JobType jobtype, int flag, int clientID) const
{
	std::vector<Entity*> ents = mEntities->get(u);
	std::vector<Entity*>::iterator iter, iend;
	if(ents.empty())
		return false;
	if(clientID < 0)
	{
		targetEntity = *ents.begin();
	}
	else
	{
		iter = ents.begin(); iend = ents.end();
		for(; iter != iend; ++iter)
		{
			Entity* ent = *iter;
			int t = (1 << (int)ent->getType());
			if (!(t & flag))
				continue;

			if(ent->isAnt())
			{
				Ant* ant = (Ant*)ent;
				if(ant->getClientID() == clientID && ant->isIdle()
					&& ant->canWork(jobtype))
					break;
			}
		}
		if(iter != iend)
			targetEntity = *iter;
	}
	if(targetEntity)
		targetPosition = targetEntity->getTilePosition();
	return targetEntity != 0;
}
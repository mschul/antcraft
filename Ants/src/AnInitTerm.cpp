#include "AnPCH.h"
#include "AnInitTerm.h"
using namespace Ants;

int InitTerm::msNumInitializers = 0;
InitTerm::Initializer InitTerm::msInitializers[MAX_ELEMENTS];
int InitTerm::msNumTerminators = 0;
InitTerm::Terminator InitTerm::msTerminators[MAX_ELEMENTS];

//----------------------------------------------------------------------------
void InitTerm::addInitializer(Initializer function)
{
	if (msNumInitializers < MAX_ELEMENTS)
	{
		msInitializers[msNumInitializers++] = function;
	}
}
//----------------------------------------------------------------------------
void InitTerm::executeInitializers()
{
	for (int i = 0; i < msNumInitializers; ++i)
	{
		msInitializers[i]();
	}
}
//----------------------------------------------------------------------------
void InitTerm::addTerminator(Terminator function)
{
	if (msNumTerminators < MAX_ELEMENTS)
	{
		msTerminators[msNumTerminators++] = function;
	}
}
//----------------------------------------------------------------------------
void InitTerm::executeTerminators()
{
	for (int i = 0; i < msNumTerminators; ++i)
	{
		msTerminators[i]();
	}
}
//----------------------------------------------------------------------------
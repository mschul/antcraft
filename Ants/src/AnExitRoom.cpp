#include "AnPCH.h"
#include "AnExitRoom.h"
#include "AnAnt.h"
using namespace Ants;

ExitRoom::ExitRoom (int id)
	: Room(id, 2)
{
}
ExitRoom::~ExitRoom ()
{
}
		
Room::RoomType ExitRoom::getType() const
{
	return Room::EXIT_ROOM;
}
bool ExitRoom::isDistructible() const
{
	return false;
}

void ExitRoom::createTriangle(const Ogre::Vector2& pivot,
							  std::vector<Ogre::Vector2>& triangle)
{
	Ogre::Vector2 start = pivot - 3*Ogre::Vector2::UNIT_Y;
	Ogre::Vector2 border = pivot + 20*Ogre::Vector2::UNIT_Y;
	Tile::hexTriangle(start, border, triangle);
}
void ExitRoom::build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	std::vector<slot*> oldSlots;
	{ 
		// slot and workplace reset/transfer (there are no workplaces anyway)
		for(slot* s : mSlots)
		{
			oldSlots.push_back(s);
		}
		mSlots.clear();
		mFreeWorkplaces = 0;
	}



	mPivot.push_back(pivot);
	for (Ogre::Vector2 pos : positions)
	{
		bool skip = false;

		for (Ogre::Vector2 pivot : mPivot)
		{
			if (pos.x == pivot.x)
			{
				skip = true;
			}
		}

		if (skip)
			continue;

		slot* s = new0 slot();
		s->items.resize(mSlotCapacity);
		for (int i = 0; i < mSlotCapacity; ++i)
		{
			s->items[i].ent = 0;
			s->items[i].reservedFor = 0;
		}
		s->position = pos;
		s->reserved = 0;
		s->capacity = mSlotCapacity;

		for (slot* os : oldSlots)
		{
			if (os->position == pos)
			{
				s->reserved = os->reserved;
				s->capacity = os->capacity;
				s->items.assign(os->items.begin(), os->items.end());
				break;
			}
		}
		mSlots.push_back(s);
	}

	for (slot* s : oldSlots)
		SafeDelete0(s);

	/*
	std::vector<Ogre::Vector2> triangle;
	createTriangle(*mPivot.begin(), triangle);
	
	// calculate enclosing rectangle
	float minX = std::numeric_limits<float>::max();
	float maxX = -std::numeric_limits<float>::max();
	float minY = std::numeric_limits<float>::max();
	float maxY = -std::numeric_limits<float>::max();
	for(Ogre::Vector2 position : positions)
	{
		if(minX > position.x) minX = position.x;
		if(maxX < position.x) maxX = position.x;
		if(minY > position.y) minY = position.y;
		if(maxY < position.y) maxY = position.y;
	}
	int width = (maxX-minX+1);

	std::set<int> slotPositions;

	for(Ogre::Vector2 pos : positions)
	{
		int index = (pos.y-minY)*width + pos.x - minX;
		if (pos.x != mPivot.begin()->x)
		{
			slotPositions.insert(index);
		}
	}

	for(Ogre::Vector2 pos : triangle)
	{
		int index = (pos.y-minY)*width + pos.x - minX;
		if(slotPositions.find(index) != slotPositions.end())
		{
			slot* s = new0 slot();
			s->position = pos;
			s->reserved = 0;
			s->capacity = mSlotCapacity;
			mSlots.push_back(s);
		}
	}
	*/
}
void ExitRoom::update(float dt, ActionReactor* reactor)
{
    Room::update (dt, reactor);
	if(mChangedTiles.size() > 0)
	{
		reactor->setExitTiles(mChangedTiles);
		mChangedTiles.clear();
	}
	if(mRemovedEnts.size() > 0)
	{
		reactor->removeEntities(mRemovedEnts);
		mRemovedEnts.clear();
	}
}
void ExitRoom::getJob(std::vector<WorkplaceSubjobs>& jobs)
{
}

void ExitRoom::place(Entity* caller, Entity* item, slot*& reservedSlot)
{
	Room::place(caller, item, reservedSlot);
	if(reservedSlot->capacity == 0)
	{
		for(int i = 0; i < (int)reservedSlot->items.size(); ++i)
		{
			mRemovedEnts.push_back(reservedSlot->items[i].ent);
			reservedSlot->items[i].ent = 0;
		}
		mChangedTiles.push_back(std::make_pair(reservedSlot->position,
			Tile::msTiles[Tile::DIRT]));
	}

}
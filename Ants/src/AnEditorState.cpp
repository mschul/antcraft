#include "AnPCH.h"
#include "AnEditorState.h"
using namespace Ants;


EditorState::EditorState()
{
    mMoveSpeed			= 100;
    mRotateSpeed		= 0.3f;

    mLMouseDown       = false;
    mRMouseDown       = false;
    mQuit             = false;
	mFollowCam	 	  = false;
}

void EditorState::enter()
{
    Framework::getSingletonPtr()->mLog->logMessage("Entering EditorState...");
	
    mCurrentObject = 0;

    mSceneMgr = Framework::getSingletonPtr()->mRoot->createSceneManager(Ogre::ST_GENERIC, "GameSceneMgr");
    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));
	mSceneMgr->setShadowTechnique(Ogre::SHADOWTYPE_STENCIL_ADDITIVE);

    mSceneMgr->addRenderQueueListener(Framework::getSingletonPtr()->mOverlaySystem);
	
    mRSQ = mSceneMgr->createRayQuery(Ogre::Ray());
	
	mCameraNode = mSceneMgr->getRootSceneNode()->createChildSceneNode("CameraNode1");
    mCameraNode->setPosition(Ogre::Vector3(5, 60, 60));
    mCameraNode->lookAt(Ogre::Vector3(5, 20, 0), Ogre::Node::TS_WORLD);
	

    mCamera = mSceneMgr->createCamera("GameCamera");
    mCamera->setNearClipDistance(5);
    mCamera->setAspectRatio(Ogre::Real(Framework::getSingletonPtr()->mViewport->getActualWidth()) /
        Ogre::Real(Framework::getSingletonPtr()->mViewport->getActualHeight()));
    Framework::getSingletonPtr()->mViewport->setCamera(mCamera);
	mCameraNode->attachObject(mCamera);
	
    buildGUI();

    createScene();
}
bool EditorState::pause()
{
    Framework::getSingletonPtr()->mLog->logMessage("Pausing EditorState...");

    return true;
}
void EditorState::resume()
{
    Framework::getSingletonPtr()->mLog->logMessage("Resuming EditorState...");

    buildGUI();

    Framework::getSingletonPtr()->mViewport->setCamera(mCamera);
    mQuit = false;
}
void EditorState::exit()
{
    Framework::getSingletonPtr()->mLog->logMessage("Leaving EditorState...");

    mSceneMgr->destroyCamera(mCamera);
    mSceneMgr->destroyQuery(mRSQ);
    if(mSceneMgr)
        Framework::getSingletonPtr()->mRoot->destroySceneManager(mSceneMgr);
}
void EditorState::createScene()
{

}

void EditorState::updateCamera(float dT)
{
	if(!mFollowCam)
	{
		if(Framework::getSingletonPtr()->mKeyboard->isKeyDown(OIS::KC_LSHIFT))
			mCameraNode->translate(mTranslateVector, Ogre::Node::TS_LOCAL);
		mCameraNode->translate(mTranslateVector / 10, Ogre::Node::TS_LOCAL);
	}
	else
	{
	}
}
void EditorState::getInput()
{
    if(Framework::getSingletonPtr()->mKeyboard->isKeyDown(OIS::KC_A))
        mTranslateVector.x = -mMoveScale;

    if(Framework::getSingletonPtr()->mKeyboard->isKeyDown(OIS::KC_D))
        mTranslateVector.x = mMoveScale;

    if(Framework::getSingletonPtr()->mKeyboard->isKeyDown(OIS::KC_W))
        mTranslateVector.z = -mMoveScale;

    if(Framework::getSingletonPtr()->mKeyboard->isKeyDown(OIS::KC_S))
        mTranslateVector.z = mMoveScale;
}
void EditorState::buildGUI()
{
}

bool EditorState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    return true;
}
bool EditorState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    return true;
}

bool EditorState::mouseMoved(const OIS::MouseEvent &evt)
{
    return true;
}
bool EditorState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}
bool EditorState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
    return true;
}

void EditorState::onLeftPressed(const OIS::MouseEvent &evt)
{
    if(mCurrentObject)
    {
        mCurrentObject->showBoundingBox(false);
    }

    Ogre::Ray mouseRay = mCamera->getCameraToViewportRay(
		Framework::getSingletonPtr()->mMouse->getMouseState().X.abs / float(evt.state.width),
        Framework::getSingletonPtr()->mMouse->getMouseState().Y.abs / float(evt.state.height));
    mRSQ->setRay(mouseRay);
    mRSQ->setSortByDistance(true);

    Ogre::RaySceneQueryResult &result = mRSQ->execute();

	Ogre::SceneNode* tmp = 0;
    for(Ogre::RaySceneQueryResultEntry entry : result)
    {
        if(entry.movable)
        {
			Ogre::Entity* ent;
			try
			{
				ent = mSceneMgr->getEntity(entry.movable->getName());
			}
			catch(Ogre::Exception)
			{
				continue;
			}

            tmp = ent->getParentSceneNode();

			if(tmp != mCurrentObject)
				break;
        }
    }
	if(tmp != 0)
	{
		mCurrentObject = tmp;
        Framework::getSingletonPtr()->mLog->logMessage("MovableName: " + mCurrentObject->getName());
        Framework::getSingletonPtr()->mLog->logMessage("ObjName " + mCurrentObject->getName());
        mCurrentObject->showBoundingBox(true);
	}
}

void EditorState::handleInput(float dT)
{
}
void EditorState::update(float dT)
{
	mTranslateVector = Ogre::Vector3::ZERO;
	getInput();
	updateCamera(dT);

	handleInput(dT);
	
	mMoveScale = mMoveSpeed   * dT;
	mRotScale = mRotateSpeed * dT;

}


#include "AnPCH.h"
#include "AnCarrier.h"
using namespace Ants;

Ogre::String Carrier::msStandardMesh = "";
Ogre::String Carrier::msMaterialName = "";

Carrier::Carrier (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
	: Ant(id, clientID, position, tilePosition, visibilityRadius,
	worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mInventory.resize(4);
	mSubJobs.resize(4);
	mTargetRooms.resize(4);
	mAssignedSlots.resize(4);

	mJobs.resize(4);
	mJobs.assign(4, -1);

	mP1Jobs = Job::FETCH_JOB;
	mP2Jobs = Job::ROOM_CREATE_JOB | Job::ROOM_CREATE_JOB_EX;
	mP3Jobs = Job::DEFEND_JOB;

	mAOE = false;
	mAOEDamage = 0;
	mRanged = false;
	mRangedDamage = 0;
	mMeleeDamage = 12;

	mActiveTrait = 0;
}
Carrier::Carrier (const EntityDesc& desc)
	: Ant(desc)
{
}
Carrier::~Carrier ()
{
}

void Carrier::selectBehaviour(float dt)
{
	int newTrait = 0;
	if(!mEnemies.empty())
	{
		if(mHealth > 10)
		{
			newTrait = 1; // warrrior
		}
		else
		{
			newTrait = 2; // escape
		}
	}

	if(newTrait != mActiveTrait)
	{
		mCurrentState = FIND_JOB;
        mActiveTrait = newTrait;
#ifdef _DEBUG
        Framework::getSingleton ().mLog->logMessage (
            "carrier " + Ogre::StringConverter::toString (getID ())
            + " switched to mode " + Ogre::StringConverter::toString (mActiveTrait)
            );
#endif
	}
}

Entity::EntityType Carrier::getType() const
{
	return Entity::ET_CARRIER;
}

Ogre::String Carrier::getMaterialName() const
{
	return Carrier::msMaterialName;
}
Ogre::String Carrier::getStandardMesh() const
{
	return Carrier::msStandardMesh;
}
#include "AnPCH.h"
#include "AnWarriorTrait.h"
#include "AnMob.h"
#include "AnAnt.h"
using namespace Ants;

template class WarriorTrait<Mob>;
template class WarriorTrait<Ant>;

template<class T>
WarriorTrait<T>::WarriorTrait()
{
}

template<class T>
void WarriorTrait<T>::idleWalking(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::FIND_PATH;
}
template<class T>
void WarriorTrait<T>::idleStanding(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::FIND_PATH;
}
template<class T>
void WarriorTrait<T>::findJob(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::FIND_PATH;
}
template<class T>
void WarriorTrait<T>::getJobInfo(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::FIND_PATH;
}
template<class T>
void WarriorTrait<T>::findPath(float dt, T* character, ActionReactor* reactor)
{
	if(character->mWorldQuery->isBlocking(character->getTilePosition()))
		return;
	if(!character->mEnemies.empty())
	{
		character->clearPath();
		if(character->mRanged)
		{
			if(character->mWorldQuery->isShootable(
				character->mEnemies.front()->getTilePosition(),
				character->mTilePosition))
			{
				character->mCurrentState = Character::WORK_JOB;
				return;
			}
		}
		
		std::vector<Ogre::Vector2> path;
		character->mWorldQuery->findReachPath(character->mEnemies.front()->getTilePosition(),
			character->mTilePosition, path, 2*character->mVisibilityRadius);
		character->savePath(path);
		character->mCurrentState = Character::REACH_JOB;
	}
	else
	{
		character->mCurrentState = Character::IDLE_STANDING;
	}
}
template<class T>
void WarriorTrait<T>::reachJob(float dt, T* character, ActionReactor* reactor)
{
	if(!character->mPath.empty())
	{
		character->mChanged = true;
		character->walkWaypoint(dt);
		if (character->mRanged)
		{
			if (character->mWorldQuery->isShootable(
				character->mEnemies.front()->getTilePosition(),
				character->mTilePosition))
			{
				character->mCurrentState = Character::WORK_JOB;
				return;
			}
		}
	}
	else character->mCurrentState = Character::WORK_JOB;
}
template<class T>
void WarriorTrait<T>::workJob(float dt, T* character, ActionReactor* reactor)
{
	if(character->mWorldQuery->isBlocking(character->getTilePosition()))
		return;


	int dist = Tile::dist(character->mTilePosition,
		character->mEnemies.front()->getTilePosition());

	// find new path if not ranged and out of reach
	// ranged and randomly. this should solve this scenario:
	// multiple ranged units don't hit moving (escaping) target
	// because dist too high. This scenario can become an infinte loop.
	if ((!character->mRanged || (rand() % 4 == 0)) && dist > 1)
	{
		character->mCurrentState = Character::FIND_PATH;
		return;
	}
	
	if (character->mRanged)
	{
		// find new path if ranged and out of reach
		if (!character->mWorldQuery->isShootable(
			character->mEnemies.front()->getTilePosition(),
			character->mTilePosition))
		{
			character->mCurrentState = Character::FIND_PATH;
			return;
		}
	}

	if (character->mAOE)
	{
		float attack = character->aoeAttack(dt);
		if (attack > 0)
		{
			reactor->aoeAttack(character->getID(), character->mAOEDamage, character->mTilePosition, character->getVisibilityRadius());
			character->mHealth = -1;
			character->die(reactor);
		}
	}
	else
	{
		// ranged but close enough, do melee
		bool ranged = character->mRanged;
		if(character->mRanged && dist <= 1)
		{
			ranged = false;
		}

		Entity* target = character->mEnemies.front();
		if(ranged)
		{
			float attack = character->rangedAttack(dt, character->mEnemies.front());
			if (attack == 1)
			{
				// critical Miss -> Reposition Penalty
				character->mCurrentState = Character::FIND_PATH;
#ifdef _DEBUG
				Framework::getSingleton().mLog->logMessage(
					Ogre::StringConverter::toString(character->getID()) + " attacks " +
					Ogre::StringConverter::toString(target->getID()) + " ranged but MISSES");
#endif
			}
			else if (attack == 20)
			{
				// critical Hit
				float damage = 2 * character->mRangedDamage;
				reactor->rangedAttack(damage, character, character->mEnemies.front());

#ifdef _DEBUG
				Framework::getSingleton().mLog->logMessage(
					Ogre::StringConverter::toString(character->getID()) + " attacks " +
					Ogre::StringConverter::toString(target->getID()) + " ranged CRITICAL");
#endif
			}
			else if (attack >= character->mEnemies.front()->getArmor())
			{
				float damage = character->getRangedDamage(dt);
				reactor->rangedAttack(damage, character, character->mEnemies.front());

#ifdef _DEBUG
				Framework::getSingleton().mLog->logMessage(
					Ogre::StringConverter::toString(character->getID()) + " attacks " +
					Ogre::StringConverter::toString(target->getID()) + " ranged");
#endif
			}
		}
		else
		{
			float attack = character->meleeAttack(dt, character->mEnemies.front());
			if (attack == 1)
			{
				// critical Miss -> Reposition Penalty
				character->mCurrentState = Character::FIND_PATH;

#ifdef _DEBUG
				Framework::getSingleton().mLog->logMessage(
					Ogre::StringConverter::toString(character->getID()) + " attacks " +
					Ogre::StringConverter::toString(target->getID()) + " melee but MISSES");
#endif
			}
			else if (attack == 20)
			{
				// critical Hit
				float damage = 2 * character->mMeleeDamage;
				reactor->meleeAttack(damage, character, target);

#ifdef _DEBUG
				Framework::getSingleton().mLog->logMessage(
					Ogre::StringConverter::toString(character->getID()) + " attacks " +
					Ogre::StringConverter::toString(target->getID()) + " melee CRITICAL");
#endif
			}
			else if (attack >= character->mEnemies.front()->getArmor())
			{
				float damage = character->getMeleeDamage(dt);
				reactor->meleeAttack(damage, character, target);

#ifdef _DEBUG
				Framework::getSingleton().mLog->logMessage(
					Ogre::StringConverter::toString(character->getID()) + " attacks " +
					Ogre::StringConverter::toString(target->getID()) + " melee");
#endif
			}
		}
	}
}
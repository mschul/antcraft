#include "AnPCH.h"
#include "AnFloralFood.h"
using namespace Ants;

Ogre::String FloralFood::msStandardMesh = "";
Ogre::String FloralFood::msMaterialName = "";

FloralFood::FloralFood (int id, int clientID,
					  const Ogre::Vector2& position,
					  const Ogre::Vector2& tilePosition, 
					  int visibilityRadius)
	: Entity(id, clientID, position, tilePosition, visibilityRadius)
{
}
FloralFood::FloralFood (const EntityDesc& desc)
	: Entity(desc)
{
}
FloralFood::~FloralFood ()
{
}
Entity::EntityType FloralFood::getType() const
{
	return Entity::ET_FLORAL_FOOD;
}
bool FloralFood::isResource() const
{
	return true;
}

Ogre::String FloralFood::getMaterialName() const
{
	return FloralFood::msMaterialName;
}
Ogre::String FloralFood::getStandardMesh() const
{
	return FloralFood::msStandardMesh;
}
		
void FloralFood::update(float dt, ActionReactor* reactor)
{
}
		
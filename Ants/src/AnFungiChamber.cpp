#include "AnPCH.h"
#include "AnFungiChamber.h"
#include "AnAnt.h"
using namespace Ants;


FungiChamber::FungiChamber(int id)
	: Room(id, 1)
{
    mHygieneDec = 0.1f;
}
FungiChamber::~FungiChamber()
{
}

Room::RoomType FungiChamber::getType() const
{
	return Room::FUNGI_CHAMBER;
}

void FungiChamber::build(const std::vector<Ogre::Vector2>& positions,
	const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
	std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	mPivot.clear();
	mPivot.push_back(pivot);

	std::vector<float> tempAges;
	tempAges.assign(mAges.begin(), mAges.end());
	mAges.clear();

	float minX, maxX, minY, maxY;
	calculateEnclosingRectangle(positions, minX, maxX, minY, maxY);

	// setup assignment map
	std::vector<int> assignedTile;
	std::vector<int> allTileIndices;
	std::vector<int> tileIndices;
	std::vector<int> borderIndices;
	int width = integer(maxX - minX + 1);
	int height = integer(maxY - minY + 1);
	std::vector<Ogre::Vector2> oldSlots;
	std::map<int, slot*> positionedSlots;
	for (slot* s : mSlots)
	{
		Ogre::Vector2 p = s->position;
		const int i = (p.y - minY)*width + (p.x - minX);
		positionedSlots.insert(std::make_pair(i, s));
		oldSlots.push_back(p);
	}
	mSlots.clear();

	assignedTile.resize(width*height, -1);
	for (Ogre::Vector2 position : positions)
	{
		int index = integer((int)(position.y - minY)*width + position.x - minX);
		allTileIndices.push_back(index);
		assignedTile[index] = 0;
	}

	std::map<int, Entity*> positionedEnts;
	for (Entity* e : se)
	{
		Ogre::Vector2 p = e->getTilePosition();
		const int i = integer((int)(p.y - minY)*width + (p.x - minX));
		positionedEnts.insert(std::make_pair(i, e));
	}


	// find borders
	findBorder(width, height, integer(minX), integer(minY),
		allTileIndices, assignedTile, borderIndices, tileIndices);


	// check if old slot setup is still applicable to this room
	bool oldSlotsStillValid = true;
	std::vector<int> oldSlotTiles;
	std::vector<int> oldSlotTilesIndices;
	oldSlotTiles.resize(width*height, -1);
	for (Ogre::Vector2 s : oldSlots)
	{
		Ogre::Vector2 localP = s;
		localP.x -= minX;
		localP.y -= minY;

		// this slot is out of bounds
		if (localP.x < 0 || localP.x >= width
			|| localP.y < 0 || localP.y >= height)
		{
			oldSlotsStillValid = false;
			break;
		}

		// this slot is in a border now
		int index = integer(localP.y * width + localP.x);
		if (assignedTile[index] < 0)
		{
			oldSlotsStillValid = false;
			break;
		}

		oldSlotTilesIndices.push_back(index);
		oldSlotTiles[index] = 0;
	}

	// sort indices and create room tiles according to these rules
	//  ?  ^  ?
	// 2 | x |  ? -> 3
	//  ?  V  ?
	//
	//  ?  ^  ?
	// ? | x |  ? -> 2
	//  ?  V  1
	//
	//  ?  ^  ?
	// ? | x |  ? -> 4
	//  ?  V  3
	//
	//  ?  ^  ?
	// ? | x |  ? -> 1
	//  -1 V  -1
	//
	//  ?  ^  ?
	// ? | x |  ? -> 3
	//  1  V  ?
	// and create slot if x > 0
	// this is a very simple greedy bin packing with some simple constraints
	// so the solution might not be optimal!
	// this will hold the indices of the slots
	std::vector<int> slotTilesIndices;
	std::vector<int> slotTiles;
	slotTiles.resize(width*height, -1);
	std::sort(tileIndices.begin(), tileIndices.end());
	for (int i : tileIndices)
	{
		int currentValue = -1;
		Ogre::Vector2 localP(i % width, i / width);
		Ogre::Vector2 globalP = localP;
		globalP.x += minX;
		globalP.y += minY;
		{
			Ogre::Vector2 left = localP + Tile::getNeighbours(globalP)[Tile::LEFT];
			int l = assignedTile[(int)left.y * width + left.x];
			if (l == 2)
			{
				currentValue = 3;
			}
			else if (l == -1)
			{
				Ogre::Vector2 rightBottom = localP +
					Tile::getNeighbours(globalP)[Tile::BOTTOM_RIGHT];
				Ogre::Vector2 leftBottom = localP +
					Tile::getNeighbours(globalP)[Tile::BOTTOM_LEFT];
				int lb = assignedTile[(int)leftBottom.y * width + leftBottom.x];
				int rb = assignedTile[(int)rightBottom.y * width + rightBottom.x];
				if (rb == 1)
				{
					currentValue = 2;
				}
				else if (rb == 3)
				{
					currentValue = 4;
				}
				else if (lb == -1)
				{
					if (rb == -1) currentValue = 1;
				}
				else if (lb == 1)
				{
					currentValue = 3;
				}
			}
		}

		Ogre::Vector2 position(localP.x + minX, localP.y + minY);
		int index = (int)localP.y * width + localP.x;
		assignedTile[index] = currentValue;


		if (currentValue > 0)
		{
			slotTilesIndices.push_back(index);
			slotTiles[index] = 0;
		}
	}

	// if the solution is not as optimal as the last one (from a smaller room setup)
	// just create the slots from the old setup
	if (oldSlotsStillValid && oldSlotTilesIndices.size() >= slotTilesIndices.size())
	{
		slotTilesIndices.clear();
		slotTilesIndices.assign(oldSlotTilesIndices.begin(), oldSlotTilesIndices.end());
		slotTiles.clear();
		slotTiles.assign(oldSlotTiles.begin(), oldSlotTiles.end());
	}


	// create slots and capture slots that are empty
	std::vector<slot*> newSlots;
	for (int tile : allTileIndices)
	{
		Ogre::Vector2 localP(real(tile % width), real(tile / width));
		Ogre::Vector2 position(localP.x + minX, localP.y + minY);
		if (slotTiles[tile] == 0)
		{
			if (mSlots.size() % 5 == 0)
			{
				workplace* w = new0 workplace;
				w->worker = 0;
				w->position = position;
				mWorkPlaces.push_back(w);
				mFreeWorkplaces++;
			}

			std::map<int, slot*>::iterator slotIter = positionedSlots.find(tile);
			slot* s;
			if (slotIter == positionedSlots.end())
			{
				s = new0 slot;
				s->reserved = 0;
				s->capacity = mSlotCapacity;
				s->position = position;

				s->items.resize(mSlotCapacity);

				for (int i = 0; i < mSlotCapacity; ++i)
				{
					s->items[i].ent = 0;
					s->items[i].reservedFor = 0;
				}
				newSlots.push_back(s);
			}
			else
			{
				s = slotIter->second;

				Entity* e = s->items[0].ent;
				positionedSlots.erase(slotIter);
			}
			mSlots.push_back(s);
		}
		else
			tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::ROOM_FLOOR]));
	}

	while (!workers.empty() && mFreeWorkplaces > 0)
	{
		assignToWorkplace(workers.back());
		workers.pop_back();
	}
	workers.clear();

	std::map<int, slot*>::iterator iter, iend;
	iter = positionedSlots.begin(); iend = positionedSlots.end();
	for (; iter != iend; ++iter)
	{
		slot* s = iter->second;
		Entity* reserved = s->items[0].reservedFor;
		if (reserved)
		{
			((Ant*)reserved)->prepareForNewJob(s);
		}
		Entity* ent = s->items[0].ent;
		if (ent)
		{
			se.push_back(ent);
		}
		SafeDelete0(s);
	}

	// try to put loose entities into empty slots
	while (!se.empty() && !newSlots.empty())
	{
		slot* s = newSlots.back();
		Entity* e = se.back();

		// beam eggs only
		if (e->getType() == Entity::ET_EGG)
		{
			Ogre::Vector2 hp;
			Tile::toHexagonalPosition(s->position, hp);
			e->setPosition(hp);
			e->setTilePosition(s->position);
		}
		s->items[mSlotCapacity - s->capacity].ent = e;
		s->capacity--;
		newSlots.pop_back();
		se.pop_back();
	}
	se.clear();

	for (slot* s : mSlots)
	{
		Ogre::Vector2 position = s->position;

		if (!tempAges.empty())
		{
			if (s->items.empty())
			{
				// GROWING possible?
				float age = -1;
				for (int i = 0; i < tempAges.size(); ++i)
				{
					if (tempAges[i] >= 0)
					{
						age = tempAges[i];
						tempAges[i] = tempAges[tempAges.size() - 1];
						tempAges.pop_back();
						break;
					}
				}

				if (age >= 0)
				{
					s->capacity = 1;
					s->reserved = 1;
					tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::GROWING_FUNGUS]));
					mAges.push_back(age);
				}
				else
				{
					tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::FUNGAL_POD]));
					mAges.push_back(-1);
				}
			}
			else if (s->items[0].ent->getType() == Entity::ET_FAUNAL_FOOD)
			{
				tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::MATURED_FUNGUS]));
				mAges.push_back(-2);
			}
			else
			{

			}
		}
		else
		{
			// this is practicaly impossible but hey
			s->capacity = 1;
			s->reserved = 1;
			tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::FUNGAL_POD]));
			mAges.push_back(-1);
		}
	}

	mWorkerRefillRequested = true;
}
void FungiChamber::update(float dt, ActionReactor* reactor)
{
    Room::update (dt, reactor);
	for (int i = 0; i < (int)mSlots.size(); ++i)
	{
		// hidden state machine (BAD_SMELL)
		if (mAges[i] == -2)
		{
			if (mSlots[i]->capacity > 0)
			{
				reactor->setTile(mSlots[i]->position, Tile::FUNGAL_POD);
				mAges[i] = -1;
			}
		}
		else if (mAges[i] == -1)
		{
			if (!mSlots[i]->items.empty())
			{
				reactor->setTile(mSlots[i]->position, Tile::GROWING_FUNGUS);

				std::vector<Entity*> ents;
				ents.push_back(mSlots[i]->items[0].ent);
				reactor->removeEntities(ents);

				mSlots[i]->items[0].ent = 0;
				// reserve, so that this is not fetchable yet
				mSlots[i]->capacity = 1;
				mSlots[i]->reserved = 1;
				mAges[i] = 0;
			}
		}
		else if (mAges[i] >= 0)
		{
			if (mAges[i] >= 60)
			{
				reactor->setTile(mSlots[i]->position, Tile::MATURED_FUNGUS);
				Entity* res = reactor->spawn(mSlots[i]->position, Entity::ET_FAUNAL_FOOD);
				mSlots[i]->items[0].ent = res;
				// release reserve to make this fetchable
				mSlots[i]->capacity = 0;
				mSlots[i]->reserved = 0;
				// hidden state machine (BAD_SMELL)
				mAges[i] = -2;
			}
			else
			{
				mAges[i] += dt;
			}
		}
	}
}
void FungiChamber::getJob(std::vector<WorkplaceSubjobs>& jobs)
{
    float r = rand () % 100; r /= 10;
    bool hygieneNeeded = (r*r) > mHygiene;
    if (hygieneNeeded)
    {
        jobs.push_back (Room::FETCH_COPAL);
        jobs.push_back (Room::CLEAN_ROOM);
    }
	else if(freeSlots() > 0)
	{
		jobs.push_back(Room::FETCH_FLORAL_FOOD);
		jobs.push_back(Room::PLACE);
	}
}
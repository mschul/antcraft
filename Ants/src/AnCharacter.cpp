#include "AnPCH.h"
#include "AnCharacter.h"
#include "AnEntityDesc.h"
using namespace Ants;

Perlin* Character::msMovementPerlin = 0;

Character::Character (int id, int clientID,
					  const Ogre::Vector2& position,
					  const Ogre::Vector2& tilePosition, 
					  int visibilityRadius,
					  WorldQuery* worldQuery, JobQuery* jobQuery,
					  EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
		: Entity(id, clientID, position, tilePosition, visibilityRadius)
{
	mWorldQuery = worldQuery;
	mJobQuery = jobQuery;
	mEntityQuery = entityQuery;
	mRoomQuery = roomQuery;
	mPheromoneMap = pheromones;

	mHealth = 100;
	mMaxHealth = 100;
	mAttackTime = 0.7f;

	mSpeed = 0.f;
	mMaxSpeed = 2.5f;
	mStateUpdateDelta = 3.f + ((rand() % 3000) / 1000);
	mCurrentState = IDLE_STANDING;

	mArmor = 5;

	mAlive = true;
}
Character::Character (const EntityDesc& desc)
	: Entity(desc)
{
}
Character::~Character ()
{
}

void Character::set(const EntityDesc& desc)
{
	Entity::set(desc);
	mAlive = desc.getHealth() > 0;
}

void Character::updateEnvironment(float dt)
{
	std::vector<Entity*> ents;
	mEntityQuery->getEntities(mTilePosition, mVisibilityRadius, ents);

	mEnemies.clear();
	for(Entity* e : ents)
	{
		if(e->getClientID() != mClientID && e->isCharacter())
		{
			Character* c = (Character*)e;
			if (c->isAlive())
			{
				std::vector<Ogre::Vector2> path;
				mWorldQuery->findShortestPath(mTilePosition, e->getTilePosition(),
					path, mVisibilityRadius * 2.f);

				if (!path.empty())
				{
					mEnemies.push_back(c);
				}
			}
		}
	}
}

void Character::applyUpgrade(Upgrade u)
{
	switch (u)
	{
	case Entity::U_MOVEMENT_SPEED:
		mMaxSpeed *= 1.2f;
		break;
	case Entity::U_HORNS:
		mArmor += 2;
		break;
	}
}

bool Character::isCharacter() const
{
	return true;
}
bool Character::isAlive() const
{
	return mAlive;
}
void Character::setAlive(bool alive)
{
	mAlive = alive;
}
int Character::aoeAttack(float dt)
{
	mAttackTime -= dt;
	if (mAttackTime <= 0)
	{
		mAttackTime = 0.7f;
		return 30;
	}
	return 0;
}
int Character::meleeAttack(float dt, Character* enemy)
{
	mAttackTime -= dt;
	if(mAttackTime <= 0)
	{
		mAttackTime = 0.7f;
		return (rand() % 20) + 1;
	}
	return 0;
}
int Character::rangedAttack(float dt, Character* enemy)
{
	// ranged attack is slower (perhaps use seperate variables for this)
	mAttackTime -= (dt/2.f);
	if(mAttackTime <= 0)
	{
		int d = Tile::dist(mTilePosition, enemy->getTilePosition() - 2);
		mAttackTime = 0.7f;
		return (rand() % 20) + 1 - std::max(d, 0);
	}
	return 0;
}
int Character::getMeleeDamage(float dt)
{
	return (rand() % mMeleeDamage) + 1;
}
int Character::getRangedDamage(float dt)
{
	return (rand() % mRangedDamage) + 1;
}


void Character::applyDamage(Attack* a, ActionReactor* reactor)
{
	mChanged = true;
	mHealth -= a->getAttackPoints();

	if (mHealth <= 0)
	{
		die(reactor);
		reactor->unitDied(this, a);
		mAlive = false;
	}
}

void Character::update(float dt, ActionReactor* reactor)
{
	if(mAlive)
	{
		if(mHealth > 0)
		{
			updateEnvironment(dt);
			selectBehaviour(dt);
			move(dt, reactor);
			float dHealth = mWorldQuery->damagePerSecond(mTilePosition) * dt;
			// to non-ants healing effects dont apply
			if (isAnt() || dHealth > 0)
			{
				mHealth -= dHealth;
				mHealth = mHealth > mMaxHealth ? mMaxHealth : mHealth;
			}
		}
		else
		{
			die(reactor);
			reactor->unitDied(this);
			mAlive = false;
		}
	}
}

bool Character::walkWaypoint(float dt)
{
	const Ogre::Radian maxTurnSpeed = Ogre::Radian(Ogre::Degree(360.f));
	const float acc = 5.f;
	const float proximityRadius = 0.1f;
	const float arrivalRadius = 0.05f;

	Ogre::Vector2 target = mPath.front();

	if(mWorldQuery->isBlocking(target) || mEntityQuery->enemyBlocking(target, mClientID))
	{
		// this could also throw an exception
		clearPath();
		return false;
	}

	Ogre::Vector2 targetNoisy;
	Tile::toHexagonalPosition(target, targetNoisy);
	targetNoisy += mPathNoise.front();

	Ogre::Vector2 deltaNoisy = targetNoisy - mPosition;
	float dist = deltaNoisy.length();

	Ogre::Vector2 orientationVec = rotateVector2(Ogre::Vector2::UNIT_Y, mOrientation);

	bool cw = false;
	Ogre::Radian angleTo = orientationVec.angleTo(deltaNoisy);

	if(angleTo > Ogre::Radian(Ogre::Degree(180)))
	{
		cw = true;
		angleTo = angleTo - Ogre::Degree(360);
	}

	Ogre::Radian maxTurn = maxTurnSpeed * dt;

	//bool turned = false;
	//if(maxTurn.valueRadians() >= angleTo.valueRadians())
	//{
		mOrientation += angleTo;
	//}
	//else
	//{
	//	mOrientation += cw > 0 ? maxTurn : -maxTurn;
	//	turned = true;
	//}

	//if(turned)
	//{
	//	mSpeed = std::max(0.f, mSpeed - acc * dt);
	//}
	//else
	//{
		if (dist > proximityRadius)
		{
			mSpeed = std::min(mMaxSpeed, mSpeed + acc * dt);
		}
		else
		{
			float s = mMaxSpeed * (dist / proximityRadius);
			mSpeed = std::max(s, mMaxSpeed - acc * dt);
		}
	//}
	

	if (mSpeed*dt > dist || dist <= arrivalRadius)
	{
		mTilePosition = target;
		mPheromoneMap->increase(target);
		mPosition = targetNoisy;
		mPath.pop();
		mPathNoise.pop();
	}
	else
	{
		deltaNoisy /= dist;
		mPosition += deltaNoisy * mSpeed * dt;
	}
	return true;
}

bool Character::findPath(Room* room)
{
	std::vector<Ogre::Vector2> path;
	mWorldQuery->findShortestPath(room->getPivot(mTilePosition), mTilePosition, path);

	if(path.empty())
		return false;
	savePath(path);
	return true;
}
void Character::randomPath()
{
	std::vector<Ogre::Vector2> path;
	mWorldQuery->generateRandomWalk(mTilePosition, mVisibilityRadius, path, mPheromoneMap);
	savePath(path);
}
void Character::restrictedPath(const std::vector<Tile::TileType>& allowedTiles)
{
	std::vector<Ogre::Vector2> path;
	mWorldQuery->generateRestrictedPath(mTilePosition, mVisibilityRadius, path,
		mPheromoneMap, allowedTiles);
	savePath(path);
}
void Character::escape(const Ogre::Vector2& position)
{
	std::vector<Ogre::Vector2> path;
	mWorldQuery->findEscapePath(mTilePosition, position, path, mVisibilityRadius);
	std::vector<Ogre::Vector2> rpath;
	std::vector<Ogre::Vector2>::reverse_iterator riter, rend;
	riter = path.rbegin(); rend = path.rend();
	for (; riter != rend; ++riter)
		rpath.push_back(*riter);
	savePath(rpath);
}
void Character::approach(const Ogre::Vector2& position)
{
	std::vector<Ogre::Vector2> path;
	mWorldQuery->findReachPath(position, mTilePosition, path, mVisibilityRadius);
	savePath(path);

}
void Character::clearPath()
{
	while(!mPath.empty()) mPath.pop();
	while(!mPathNoise.empty()) mPathNoise.pop();
}
void Character::savePath(const std::vector<Ogre::Vector2>& path)
{
	if(path.empty())
		return;

#define NOISE_RESOULTION 10000
	Ogre::Vector2 minP(real(rand() % NOISE_RESOULTION), real(rand() % NOISE_RESOULTION));
	Ogre::Vector2 maxP(real(rand() % (NOISE_RESOULTION - (int)minP.x) + minP.x),
					   real(rand() % (NOISE_RESOULTION - (int)minP.y) + minP.y));
	minP /= NOISE_RESOULTION;
	maxP /= NOISE_RESOULTION;
#undef NOISE_RESOULTION
	Ogre::Vector2 step = (maxP - minP) / real(path.size());

	for(Ogre::Vector2 p : path)
	{
		mPath.push(p);
		Ogre::Vector2 noise(msMovementPerlin->get(minP.x, minP.y),
							msMovementPerlin->get(minP.y, minP.x));
		mPathNoise.push(noise);
		minP += step;
	}
}
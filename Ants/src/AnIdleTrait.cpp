#include "AnPCH.h"
#include "AnIdleTrait.h"
#include "AnMob.h"
#include "AnAnt.h"
using namespace Ants;

template class IdleTrait<Mob>;
template class IdleTrait<Ant>;

template<class T>
IdleTrait<T>::IdleTrait()
{
}

template<class T>
void IdleTrait<T>::idleWalking(float dt, T* character, ActionReactor* reactor)
{
	if(!character->mPath.empty())
	{
		character->mChanged = true;
		character->walkWaypoint(dt);
	}
	else
	{
		if(character->mWorldQuery->isBlocking(character->getTilePosition()))
			return;

		if(character->mStateUpdateDelta < 0)
		{
			character->mStateUpdateDelta = ((float)(rand() % 1000) / 1000 + 1);
			character->mCurrentState = Character::IDLE_STANDING;
		}
		else if (character->getType() == Entity::ET_GRUB)
		{
			character->restrictedPath(Tile::msAnthillTiles);
		}
		else
		{
			character->randomPath();
		}
	}
	character->mStateUpdateDelta -= dt;
}
template<class T>
void IdleTrait<T>::idleStanding(float dt, T* character, ActionReactor* reactor)
{
	if(character->mStateUpdateDelta < 0)
	{
		character->mStateUpdateDelta = ((float)(rand() % 1000) / 1000 + 1);
		character->mCurrentState = Character::IDLE_WALKING;
	}
	character->mStateUpdateDelta -= dt;
}
template<class T>
void IdleTrait<T>::findJob(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::IDLE_STANDING;
}
template<class T>
void IdleTrait<T>::getJobInfo(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::IDLE_STANDING;
}
template<class T>
void IdleTrait<T>::findPath(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::IDLE_STANDING;
}
template<class T>
void IdleTrait<T>::reachJob(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::IDLE_STANDING;
}
template<class T>
void IdleTrait<T>::workJob(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::IDLE_STANDING;
}
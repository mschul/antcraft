#include "AnPCH.h"
#include "AnClient.h"
#include "AnPuppetFactory.h"
using namespace Ants;

Client::Client ()
	: GameObject(-1)
{
	mStatistics = 0;
	mWorld = 0;
	mEntities = 0;
	mJobs = 0;
	mEntityQuery = 0;
	mJobQuery = 0;
	mWorldQuery = 0;
	mRooms = 0;
	mRoomQuery = 0;
	mPheromoneMap = 0;
}
Client::~Client ()
{
	SafeDelete0(mStatistics);
	SafeDelete0(mWorld);
	SafeDelete0(mEntities);
	SafeDelete0(mJobs);
	SafeDelete0(mRooms);
	SafeDelete0(mEntityQuery);
	SafeDelete0(mJobQuery);
	SafeDelete0(mWorldQuery);
	SafeDelete0(mPheromoneMap);
}

void Client::login(Server* server)
{
	mID = server->loginClient(this);
	mServer = server;
}
void Client::logoff()
{
	mServer->logoffClient(this, mID);
}

void Client::sendUpdates()
{
	std::vector<PositionTilePair> tiles;
	std::vector<EntityDesc> addedEnts;
	std::vector<EntityDesc> changedEnts;
	std::vector<EntityDesc> removedEnts;

	mEntities->getUpdates(addedEnts, changedEnts, removedEnts);

	mWorld->getUpdates(tiles);
	mServer->setUpdates(mID, tiles, addedEnts, changedEnts, removedEnts, mAttacks, mMessages, mAOEs);
	mWorld->clearUpdates();
	mEntities->clearUpdates();
	mAttacks.clear();
	mMessages.clear();
	mAOEs.clear();
}
void Client::receiveUpdates()
{
	std::vector<PositionTilePair> tiles;
	std::vector<EntityDesc> addedEnts;
	std::vector<EntityDesc> changedEnts;
	std::vector<EntityDesc> removedEnts;
	mServer->getUpdates(mID, tiles, addedEnts, changedEnts, removedEnts, mAttacks, mMessages);
	{
		std::vector<EntityDesc>::const_iterator iter, iend;
		iter = addedEnts.cbegin(); iend = addedEnts.cend();
		for (; iter != iend; ++iter)
			addEntity(*iter);
		iter = changedEnts.cbegin(); iend = changedEnts.cend();
		for (; iter != iend; ++iter)
			changeEntity(*iter);
		iter = removedEnts.cbegin(); iend = removedEnts.cend();
		for (; iter != iend; ++iter)
			removeEntity(*iter);
	}

	{ // create rooms for discovered resources
		std::set<int> roomIDs;
		for (PositionTilePair pair : tiles)
		{
			int roomID = -1;
			switch (pair.second->getType())
			{
			case Tile::GRASS:
				roomID = mRooms->assignRoom(Room::FLORAL_SOURCE, pair.first);
				break;
			case Tile::COPAL:
				roomID = mRooms->assignRoom(Room::COPAL_SOURCE, pair.first);
				break;
			}
			if (roomID >= 0 && roomIDs.find(roomID) == roomIDs.end()) roomIDs.insert(roomID);
		}

		for (int id : roomIDs)
		{
			std::vector<PositionTilePair> resource;
			mRooms->buildRoom(id, Ogre::Vector2::ZERO, resource);
		}
	}

	mWorld->setTiles(tiles);

    onUpdatesReceived (tiles, addedEnts, changedEnts, removedEnts);
}
void Client::onUpdatesReceived (std::vector<PositionTilePair> tiles,
                                std::vector<EntityDesc> addedEnts,
                                std::vector<EntityDesc> changedEnts,
                                std::vector<EntityDesc> removedEnts)
{

}
void Client::removeJobs (int tile, Job::JobType type)
{
    Ogre::Rect size = mWorld->getSize ();
    Ogre::Vector2 position (real (tile % size.width ()), real (tile / size.width ()));
    Ogre::Vector2 targetPosition;

    mJobs->removeJobs (position, type);
}
void Client::getDisplayableMessages(std::vector<DisplayMessage*>& messages)
{
	for (Message* m : mMessages)
	{
		switch (m->getType())
		{
		case Message::MT_ACHIEVEMENT_NOTIFICATION:
		{
			messages.push_back(new0 DisplayMessage(m->getDatum("MessageTitle")));
		}
		}
	}
}
void Client::processUpdates()
{
	{ // process messages BADSMELL: must be done before attacks, as they can create new Messages!
		for (Message* m : mMessages)
		{
			switch (m->getType())
			{
			case Message::MT_KILL_NOTIFICATION:
			{
				Entity::EntityType type = (Entity::EntityType)Ogre::StringConverter::parseInt(
					m->getDatum("killedType"), (int)Entity::ET_COUNT);
				mStatistics->killed((int)type);
			}
			}
			delete0(m);
		}
		mMessages.clear();
	}
	
	{ // do all attacks
        for (Attack* a : mAttacks)
        {

            Character* c = (Character*)mEntities->get (a->getTarget ());

            c->applyDamage (a, this);
            mEntities->change (c, true);
#ifdef _DEBUG
            Framework::getSingleton ().mLog->logMessage ("unit from client " +
                                                         Ogre::StringConverter::toString (a->getSourceClientID ()) + " attacks " +
                                                         Ogre::StringConverter::toString (a->getTarget ()) + " with " +
                                                         Ogre::StringConverter::toString (a->getAttackPoints ()) + " damage");
            Framework::getSingleton ().mLog->logMessage (
                Ogre::StringConverter::toString (c->getID ()) + " has  now has " +
                Ogre::StringConverter::toString (c->getHealth ()) + " hp ");
#endif
			delete0(a);
		}
		mAttacks.clear();
	}
}

void Client::init(unsigned int width, unsigned int height)
{
	mStatistics = new0 Statistics(mID);

	mWorld = new0 WorldMap();
	mWorld->init(width, height);
	mWorldQuery = new0 WorldQuery(mWorld);

	mEntities = new0 EntityMap();
	mEntities->init(width, height);
	mEntityQuery = new0 EntityQuery(mWorld, mEntities);

	mJobs = new0 JobMap();
	mJobs->init(width, height);
	mJobQuery = new0 JobQuery(mWorld, mJobs);

	mRooms = new0 RoomMap();
	mRooms->init(width, height);
	mRoomQuery = new0 RoomQuery(mRooms, mWorld);
	
	mPheromoneMap = new0 PheromoneMap();
	mPheromoneMap->init(width, height);
}
void Client::init(const Ogre::Rect& area)
{
	mStatistics = new0 Statistics(mID);

	mWorld = new0 WorldMap();
	mWorld->init(area);
	mWorldQuery = new0 WorldQuery(mWorld);

	mEntities = new0 EntityMap();
	mEntities->init(area);
	mEntityQuery = new0 EntityQuery(mWorld, mEntities);
	
	mJobs = new0 JobMap();
	mJobs->init(area);
	mJobQuery = new0 JobQuery(mWorld, mJobs);

	mRooms = new0 RoomMap();
	mRooms->init(area);
	mRoomQuery = new0 RoomQuery(mRooms, mWorld);

	mPheromoneMap = new0 PheromoneMap();
	mPheromoneMap->init(area);
}
void Client::addEntity(const EntityDesc& desc)
{
	Entity* ent = PuppetFactory::getSingletonPtr()->create(desc);
	mEntities->add(ent);
}
void Client::changeEntity(const EntityDesc& desc)
{
	Entity* ent = mEntities->get(desc.getEntityID());
	ent->set(desc);
	if(ent->isPuppet() && ent->isCharacter() && ent->getHealth() <= 0)
	{
		Character* c = (Character*)ent;
		c->setAlive(false);
		createResource(c->getTilePosition(), Room::FAUNAL_SOURCE, c);
	}
	if(desc.getParent() >= 0)
	{
		if(ent->getParent() != 0)
		{
			ent->setParent(mEntities->get(desc.getParent()));
		}
	}
	else
	{
		ent->setParent(0);
	}
	mEntities->change(ent);
}
void Client::removeEntity(const EntityDesc& desc)
{
	Entity* ent = mEntities->get(desc.getEntityID());
	mEntities->remove(ent);
}
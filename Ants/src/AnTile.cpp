#include "AnPCH.h"
#include "AnTile.h"
#include "AnProceduralGeometry.h"
using namespace Ants;

std::vector<Ogre::String> Tile::msMeshNames = std::vector<Ogre::String>();
std::vector<Tile::TileType> Tile::msAnthillTiles = std::vector<Tile::TileType>();
std::vector<Tile::TileType> Tile::msNonAnthillTiles = std::vector<Tile::TileType>();
std::vector<Tile*> Tile::msTiles;
std::vector<Ogre::Vector2> Tile::msDeltas[2];

Tile::Tile (int id, TileType type, const Ogre::String& meshName,
			const Ogre::String& material, const Ogre::ARGB& colour,
			float dps, const Ogre::Vector3 offset)
	: GameObject(id)
{
	mType = type;
	mMaterial = material;
	mOffset = offset;
	mColour = colour;
	mDPS = dps;

	mMeshID = (int)msMeshNames.size();
	msMeshNames.push_back(meshName);
}
Tile::~Tile ()
{
}

Tile::TileType Tile::getType() const
{
	return mType;
}
Ogre::Vector3 Tile::getOffset() const
{
	return mOffset;
}
bool Tile::isBlocking() const
{
	return mType == DIRT || mType == ROCK || mType == AIR || mType == QUEEN ||
		mType == WATER;
}
bool Tile::isDiggable() const
{
	return mType == DIRT;
}
bool Tile::isResource() const
{
	return mType == GRASS || mType == COPAL; // MATURED_FUNGUS is only a placeholder
}
bool Tile::isOverWorld() const
{
	return mType == AIR || mType == GROUND || mType == GRASS || mType == TREE || mType == HARVESTED_GRASS;
}
bool Tile::isAnthill() const
{
	return mType == FLOOR || mType == NEST || mType == ROOM_FLOOR || mType == QUEEN
		|| mType == FUNGAL_POD || mType == GROWING_FUNGUS || mType == MATURED_FUNGUS;
}
Ogre::ARGB Tile::getColour() const
{
	return mColour;
}

float Tile::getDamagePerSecond() const
{
	return mDPS;
}

std::vector<Ogre::Vector2>& Tile::getNeighbours(const Ogre::Vector2& p)
{
	return msDeltas[((int)p.y) & 1];
}

void Tile::init(int& lastID)
{
	if (msDeltas[0].empty())
	{
		msDeltas[0].push_back(Ogre::Vector2(1, 0));
		msDeltas[0].push_back(Ogre::Vector2(0, -1));
		msDeltas[0].push_back(Ogre::Vector2(-1, -1));
		msDeltas[0].push_back(Ogre::Vector2(-1, 0));
		msDeltas[0].push_back(Ogre::Vector2(-1, 1));
		msDeltas[0].push_back(Ogre::Vector2(0, 1));
	}
	if (msDeltas[1].empty())
	{
		msDeltas[1].push_back(Ogre::Vector2(1, 0));
		msDeltas[1].push_back(Ogre::Vector2(1, -1));
		msDeltas[1].push_back(Ogre::Vector2(0, -1));
		msDeltas[1].push_back(Ogre::Vector2(-1, 0));
		msDeltas[1].push_back(Ogre::Vector2(0, 1));
		msDeltas[1].push_back(Ogre::Vector2(1, 1));
	}

	if (msTiles.empty())
	{
		msTiles.resize(Tile::TILE_COUNT);
		Ogre::String meshName0 = ProceduralGeometry::getSingletonPtr()->getPrimitive("hexagonCylinder");
		Ogre::String meshName1 = ProceduralGeometry::getSingletonPtr()->getPrimitive("hexagon");
		Ogre::String meshName2 = ProceduralGeometry::getSingletonPtr()->getPrimitive("sphere");

		msAnthillTiles.push_back(FLOOR);
		msAnthillTiles.push_back(ROOM_FLOOR);

		msNonAnthillTiles.push_back(GROUND);
		msNonAnthillTiles.push_back(FLOOR);
		msNonAnthillTiles.push_back(ROUGH);
		msNonAnthillTiles.push_back(MUD);
		msNonAnthillTiles.push_back(GRASS);

		msTiles[DIRT] = new0 Tile(lastID++, DIRT, meshName0,
			"dirt", Ogre::ColourValue(0.612f, 0.54f, 0.42f).getAsARGB());
		msTiles[GROUND] = new0 Tile(lastID++, GROUND, "",
			"", Ogre::ColourValue(0.06f, 0.23f, 0.06f).getAsARGB());
		msTiles[ROUGH] = new0 Tile(lastID++, ROUGH, meshName1,
			"rough", Ogre::ColourValue(0.42f, 0.35f, 0.27f).getAsARGB(),
			0, Ogre::Vector3(0.f, 0.f, -0.5f));
		msTiles[FLOOR] = new0 Tile(lastID++, FLOOR, meshName1,
			"floor", Ogre::ColourValue(0.69f, 0.59f, 0.40f).getAsARGB(),
			-2.f, Ogre::Vector3(0.f, 0.f, -0.5f));
		msTiles[ROCK] = new0 Tile(lastID++, ROCK, meshName0,
			"rock", Ogre::ColourValue(0.45f, 0.48f, 0.42f).getAsARGB());
		msTiles[MUD] = new0 Tile(lastID++, MUD, meshName1,
			"mud", Ogre::ColourValue(0.40f, 0.35f, 0.30f).getAsARGB(),
			0, Ogre::Vector3(0, 0, -0.5));
		msTiles[AIR] = new0 Tile(lastID++, AIR, "", "",
			Ogre::ColourValue(0.f, 0.f, 0.f).getAsARGB());
		msTiles[GRASS] = new0 Tile(lastID++, GRASS, meshName0,
			"grass", Ogre::ColourValue(0.463f, 0.725f, 0.f).getAsARGB(),
			0, Ogre::Vector3(0, 0, -0.5));
        msTiles[HARVESTED_GRASS] = new0 Tile (lastID++, HARVESTED_GRASS, meshName1,
			"harvestedGrass", Ogre::ColourValue(0.37f, 0.38f, 0.f).getAsARGB(),
			0, Ogre::Vector3(0, 0, -0.5));
		msTiles[TREE] = new0 Tile(lastID++, TREE, meshName0,
			"tree", Ogre::ColourValue(0.36f, 0.29f, 0.15f).getAsARGB(),
			0, Ogre::Vector3(0, 0, -0.5));
		msTiles[QUEEN] = new0 Tile(lastID++, QUEEN, meshName0,
			"queen", Ogre::ColourValue(1.f, 0.54f, 0.f).getAsARGB());
		msTiles[ROOM_FLOOR] = new0 Tile(lastID++, ROOM_FLOOR, meshName1,
			"roomFloor", Ogre::ColourValue(0.75f, 0.68f, 0.52f).getAsARGB(),
			-5.f, Ogre::Vector3(0.f, 0.f, -0.5f));
		msTiles[COPAL] = new0 Tile(lastID++, COPAL, meshName0,
			"copal", Ogre::ColourValue(0.93f, 0.89f, 0.54f).getAsARGB(),
			0, Ogre::Vector3(0, 0, -0.5));
		msTiles[NEST] = new0 Tile(lastID++, NEST, meshName2,
			"nest", Ogre::ColourValue(0.29f, 0.30f, 0.29f).getAsARGB(),
			0, Ogre::Vector3(0, 0, -0.5));
		msTiles[WATER] = new0 Tile(lastID++, WATER, meshName0,
			"water", Ogre::ColourValue(0.f, 0.3f, 1.f).getAsARGB(), 5);
		
		msTiles[FUNGAL_POD] = new0 Tile(lastID++, FUNGAL_POD, meshName1,
			"fungalPod", Ogre::ColourValue(1.f, 0.85f, 0.5f).getAsARGB(),
			-0.1f, Ogre::Vector3(0, 0, -0.5));
		msTiles[GROWING_FUNGUS] = new0 Tile(lastID++, GROWING_FUNGUS, meshName2,
			"growingFungus", Ogre::ColourValue(0.9f, 0.74f, 0.6f).getAsARGB(),
			-0.1f, Ogre::Vector3(0, 0, -0.3f));
		msTiles[MATURED_FUNGUS] = new0 Tile(lastID++, MATURED_FUNGUS, meshName0,
			"maturedFungus", Ogre::ColourValue(0.75f, 0.45f, 0.2f).getAsARGB(),
			-0.1f, Ogre::Vector3(0, 0, 0));
	}
}
		
void Tile::toHexagonalPosition(const Ogre::Vector2& tilePosition, Ogre::Vector2& hexPosition)
{
	hexPosition = tilePosition;
	if((int)tilePosition.y % 2)
		hexPosition.x += 0.5;
	hexPosition.x *= 0.86602540378f;
	hexPosition.y *= 0.75f;
}
void Tile::toTilePosition(const Ogre::Vector2& hexPosition, Ogre::Vector2& tilePosition)
{
	tilePosition = hexPosition;
	tilePosition.x *= 1.15470053838f;
	tilePosition.y *= 1.33333333333f;
	if((int)tilePosition.y % 2)
		tilePosition.x -= 0.5;
	tilePosition.y = real(integer(tilePosition.y));
	tilePosition.x = real(integer(tilePosition.x));
}
void Tile::hexRound(const Ogre::Vector3& hex, Ogre::Vector3& result)
{
	result.x = floor(hex.x+0.5f);
	result.y = floor(hex.y+0.5f);
	result.z = floor(hex.z+0.5f);
	
    float dx = fabsf(result.x - hex.x);
    float dy = fabsf(result.y - hex.y);
    float dz = fabsf(result.z - hex.z);

	if(dx > dy && dx > dz)
		result.x = -result.y -result.z;
	else if (dy > dz)
		result.y = -result.x -result.z;
	else
		result.z = -result.x - result.y;
}
void Tile::toHexagonalCube(const Ogre::Vector2& tilePosition, Ogre::Vector3& result)
{
	result.x = tilePosition.x - (tilePosition.y - ((int)tilePosition.y%2)) / 2;
	result.z = tilePosition.y;
	result.y = -result.x -result.z;
}
void Tile::fromHexagonalCube(const Ogre::Vector3& hex, Ogre::Vector2& result)
{
	result.x = hex.x + (hex.z - ((int)hex.z%2))/2;
	result.y = hex.z;
} 
int Tile::dist(const Ogre::Vector3& a, const Ogre::Vector3& b)
{
	float d = std::fabsf(a.x - b.x) + std::fabsf(a.y - b.y) + std::fabsf(a.z - b.z);
	return integer(d/2);
}
int Tile::dist(const Ogre::Vector2& a, const Ogre::Vector2& b)
{
	Ogre::Vector3 ah, bh;
	toHexagonalCube(a, ah);
	toHexagonalCube(b, bh);
	float d = std::fabsf(ah.x - bh.x) + std::fabsf(ah.y - bh.y) + std::fabsf(ah.z - bh.z);
	return integer(d*0.5f);
}

void Tile::createUpPath(const Ogre::Vector2& tilePosition, int length,
	std::vector<Ogre::Vector2>& path)
{
	Ogre::Vector2 p = tilePosition;
	for(int i = 0; i < length; ++i)
	{
		path.push_back(p);
		p += msDeltas[((int)p.y)%2][rand()%2+4];
	}
}
void Tile::hexLine(const Ogre::Vector2& start, const Ogre::Vector2& target,
	std::vector<Ogre::Vector2>& path)
{
	float epsilon = 0.00001f;
	Ogre::Vector3 sHex, eHex;
	Tile::toHexagonalCube(start, sHex);
	sHex.x += epsilon; sHex.y += epsilon; sHex.x -= 2*epsilon;
	Tile::toHexagonalCube(target, eHex);
	float dx = sHex.x - eHex.x;
	float dy = sHex.y - eHex.y;
	float dz = sHex.z - eHex.z;
	float N = std::max(std::max(std::fabs(dx - dy), fabs(dy - dz)), fabs(dz - dx));
	float invN = 1.0f / N;

	Ogre::Vector3 prev = sHex;
	path.push_back(start);
	for(int i = 0; i <= N; ++i)
	{
		Ogre::Vector3 h;
		float lambda = i * invN;
		Tile::hexRound(sHex * (1 - lambda) + eHex * lambda, h);
		if(h != prev)
		{
			Ogre::Vector2 p;
			Tile::fromHexagonalCube(h, p);
			path.push_back(p);
		}
	}
}
void Tile::hexRect(const Ogre::Vector2& center, int width, int height,
	std::vector<Ogre::Vector2>& path)
{
	float w2 = ((float)width) * 0.5f;
	float h2 = ((float)height) * 0.5f;
	float minX = center.x + integer(-w2 + 0.5f);
	float maxX = center.x + integer(w2);
	float minY = center.y + integer(-h2 + 0.5f);
	float maxY = center.y + integer(h2);
	path.reserve((maxX-minX)*(maxY-minY));

	for (float y = minY; y < maxY; ++y)
	{
		for (float x = minY; x < maxY; ++x)
		{
			path.push_back(Ogre::Vector2(x, y));
		}
	}
}
void Tile::hexCircle(const Ogre::Vector2& center, const Ogre::Vector2& border,
	std::vector<Ogre::Vector2>& path)
{
	int d = Tile::dist(center, border);
	// area of regular hexagon with radius d
	// 6*sum{1->d}(i) + 1 = 6*(d(d+1)/2)+1 = 3*d(d+1)+1
	path.reserve(3 * d * (d + 1) + 1);

	Ogre::Vector2 current = center;
	path.push_back(center);
	bool finished = center == border;
	int k = 0;
	while(!finished)
	{
		++k;
		current += Tile::getNeighbours(current)[4];
		for(int i = 0; i < 6; ++i)
		{
			for(int j = 0; j < k; ++j)
			{
				path.push_back(current);
				if(current == border)
					finished = true;
				current += Tile::getNeighbours(current)[i];
			}
		}	
	}
}
void Tile::hexCircle(const Ogre::Vector2& center, int width,
	std::vector<Ogre::Vector2>& path)
{
	Ogre::Vector2 border = center + real(width) * Tile::getNeighbours(center)[Tile::LEFT];
	hexCircle(center, border, path);
}
void Tile::hexRing(const Ogre::Vector2& center, int width,
	std::vector<Ogre::Vector2>& path)
{
	path.reserve(6 * width);
	Ogre::Vector2 current = center + real(width) * Tile::getNeighbours(center)[Tile::LEFT];
	for (int i = 0; i < 6; ++i)
	{
		for (int j = 0; j < width; ++j)
		{
			path.push_back(current);
			current += Tile::getNeighbours(current)[i];
		}
	}
}
void Tile::hexTriangle(const Ogre::Vector2& start,  const Ogre::Vector2& border,
	std::vector<Ogre::Vector2>& path)
{
	path.push_back(start);
	bool finished = start == border;
	int k = 0;
	while(!finished)
	{
		++k;
		Ogre::Vector2 cr = start + real(k) * Tile::getNeighbours(start)[Tile::RIGHT];
		Ogre::Vector2 cl = start + real(k) * Tile::getNeighbours(start)[Tile::LEFT];
		for(int j = 0; j < 2*k; ++j)
		{
			path.push_back(cr);
			path.push_back(cl);
			if(cr == border || cl == border)
				finished = true;
			cr += Tile::getNeighbours(cr)[Tile::TOP_LEFT];
			cl += Tile::getNeighbours(cl)[Tile::TOP_RIGHT];
		}
		Ogre::Vector2 current = start;
		for(int j = 0; j < k; ++j)
		{
			current += Tile::getNeighbours(start)[Tile::TOP_LEFT];
			current += Tile::getNeighbours(current)[Tile::TOP_RIGHT];
		}
		path.push_back(current);
		if(current == border)
			finished = true;
	}
}
void Tile::hexLineEnvironment(const Ogre::Vector2& start,  const Ogre::Vector2& end, int width,
	std::vector<Ogre::Vector2>& path)
{
	std::vector<Ogre::Vector2> borderCircle;
	hexCircle(start, width, borderCircle);
	hexCircle(end, width, borderCircle);
	float minX = std::numeric_limits<float>::max();
	float minY = std::numeric_limits<float>::max();
	float maxX = -std::numeric_limits<float>::max();
	float maxY = -std::numeric_limits<float>::max();
	for(Ogre::Vector2 p : borderCircle)
	{
		if(p.x < minX) minX = p.x;
		if(p.y < minY) minY = p.y;
		if(p.x > maxX) maxX = p.x;
		if(p.y > maxY) maxY = p.y;
	}
	int w = integer(fabsf(maxX - minX + 1));

	std::set<int> marks;
	std::vector<Ogre::Vector2> line;
	hexLine(start, end, line);
	for(Ogre::Vector2 p : line)
	{
		std::vector<Ogre::Vector2> circle;
		hexCircle(p, width, circle);
		for(Ogre::Vector2 position : circle)
		{
			int i = integer((position.y-minY)*w + position.x - minX);
			if(marks.find(i) == marks.end())
			{
				marks.insert(i);
				path.push_back(position);
			}
		}
	}
}

Ogre::String Tile::getMaterialName() const
{
	return mMaterial;
}
int Tile::getMeshID() const
{
	return mMeshID;
}
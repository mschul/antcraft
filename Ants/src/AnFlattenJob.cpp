#include "AnPCH.h"
#include "AnFlattenJob.h"
#include "AnClient.h"
using namespace Ants;

FlattenJob::FlattenJob (int id, const Ogre::Vector2& position, double duration)
	: Job(id, 1, position)
{
	mT = duration;
}
FlattenJob::~FlattenJob ()
{
}

Job::JobType FlattenJob::getType() const
{
	return Job::FLATTEN_JOB;
}

bool FlattenJob::update(double dt, ActionReactor* reactor)
{
	mT -= dt;
	if(mT <= 0)
	{
		return true;
	}
	return false;
}
void FlattenJob::getSubJobs(std::vector<Job::SubJobs>& jobs) const
{
	jobs.push_back(Job::UPDATE);
}

#include "AnPCH.h"
#include "AnRoom.h"
#include "AnAnt.h"
#include "AnEntity.h"
using namespace Ants;

Room::Room(ObjectID id, int capacity)
	: GameObject(id)
{
	mID = id;
	mFreeWorkplaces = 0;
	mSlotCapacity = capacity;
	mWorkerRefillRequested = false;
    mHygiene = 100.f;
    mHygieneDec = 0.f;
}
Room::~Room ()
{
	for (Entity* g : mGuests)
	{
		Ant* ant = static_cast<Ant*>(g);
		ant->targetRoomDestroyed(this);
	}

	std::vector<Ant*> workers; std::vector<Entity*> se;
	reset(workers, se);
}

bool Room::isResource() const
{
	return false;
}
Ogre::Vector2 Room::getPivot(const Ogre::Vector2& nearest) const
{
	int dist = std::numeric_limits<int>::max();
	Ogre::Vector2 result;
	for (Ogre::Vector2 v : mPivot)
	{
		int d = Tile::dist(v, nearest);
		if (d < dist)
		{
			dist = d;
			result = v;
		}
	}
	return result;
}
Ogre::Vector2 Room::getFirstPivot() const
{
	return *mPivot.begin();
}


void Room::update (float dt, ActionReactor* reactor)
{
    mHygiene -= mHygieneDec * dt * mSlots.size ();
    mHygiene = std::max (mHygiene, 0.f);
    if (!mDeleteCache.empty ())
    {
        reactor->removeEntities (mDeleteCache);
        mDeleteCache.clear ();
    }
}
void Room::reset(std::vector<Ant*>& workers, std::vector<Entity*>& se)
{
	{ // slot and workplace reset/transfer
		for (slot* s : mSlots)
		{
			for (int i = 0; i < mSlotCapacity; ++i)
			{
				if (s->items[i].ent) se.push_back(s->items[i].ent);
				if (s->items[i].reservedFor)
				{
					((Ant*)s->items[i].reservedFor)->prepareForNewJob(s);
				}
			}
			SafeDelete0(s);
		}
		mSlots.clear();
		for (workplace* w : mWorkPlaces)
		{
			if (w->worker)
				workers.push_back((Ant*)w->worker);
			SafeDelete0(w);
		}
		mWorkPlaces.clear();
		mFreeWorkplaces = 0;
	}
}

void Room::calculateEnclosingRectangle(const std::vector<Ogre::Vector2>& positions,
	float& minX, float& maxX, float& minY, float& maxY)
{
	minX = std::numeric_limits<float>::max();
	maxX = -std::numeric_limits<float>::max();
	minY = std::numeric_limits<float>::max();
	maxY = -std::numeric_limits<float>::max();

	for (Ogre::Vector2 position : positions)
	{
		if (minX > position.x) minX = position.x;
		if (maxX < position.x) maxX = position.x;
		if (minY > position.y) minY = position.y;
		if (maxY < position.y) maxY = position.y;
	}
}

void Room::findBorder(int width, int height, int minX, int minY,
	const std::vector<int>& allTileIndices,
	std::vector<int>& assignedTile,
	std::vector<int>& borderIndices, std::vector<int>& tileIndices)
{
	for (int i : allTileIndices)
	{
		Ogre::Vector2 localP(i % width, i / width);
		Ogre::Vector2 globalP = localP;
		globalP.x += minX;
		globalP.y += minY;

		bool border = false;
		for (Ogre::Vector2 d : Tile::getNeighbours(globalP))
		{
			Ogre::Vector2 n = localP + d;
			if (n.x < 0 || n.y < 0 || n.x >= width || n.y >= height)
			{
				border = true;
				break;
			}
			int ni = ((int)n.y) * width + n.x;
			if (assignedTile[ni] == -1)
			{
				border = true;
				break;
			}
		}

		if (border)
		{
			borderIndices.push_back(i);
		}
		else
		{
			tileIndices.push_back(i);
		}
	}
	for (int i : borderIndices)
	{
		assignedTile[i] = -1;
	}
}

int Room::freeWorkplaces() const
{
	return mFreeWorkplaces;
}
int Room::workerCount() const
{
	return mWorkPlaces.size() - mFreeWorkplaces;
}
bool Room::workerRefillRequested() const
{
	return mWorkerRefillRequested;
}
void Room::setWorkersRefilled()
{
	mWorkerRefillRequested = false;
}
bool Room::isDistructible() const
{
	return true;
}

void Room::assignToWorkplace(Ant* ant)
{
	mWorkerRefillRequested = false;
	workplace* w = mWorkPlaces[mWorkPlaces.size() - mFreeWorkplaces];
	w->worker = ant;
	ant->assignToWorkplace(this);
	mFreeWorkplaces--;
}
void Room::unassignWorkers()
{
	mWorkerRefillRequested = true;
	for(workplace* w : mWorkPlaces)
	{
		if(w->worker)
		{
			Ant* a = (Ant*)w->worker;
			a->unAssignFromWorkplace();
			w->worker = 0;
			mFreeWorkplaces++;
		}
	}
}
void Room::unassignWorker(Ant* worker)
{
	mWorkerRefillRequested = true;
	for(workplace* w : mWorkPlaces)
	{
		if(w->worker == worker)
		{
			worker->unAssignFromWorkplace();
			w->worker = 0;
			mFreeWorkplaces++;
			break;
		}
	}
}
void Room::unassignLastWorker()
{
	mWorkerRefillRequested = true;
	for (workplace* w : mWorkPlaces)
	{
		if (w->worker != 0)
		{
			Ant* worker = (Ant*)w->worker;
			worker->unAssignFromWorkplace();
			w->worker = 0;
			mFreeWorkplaces++;
			break;
		}
	}
}

Entity* Room::fetch(Entity* caller, slot*& reservedSlot)
{
	int index = -1;
	for (int i = 0; i < mSlotCapacity; ++i)
	{
		if (reservedSlot->items[i].reservedFor == caller)
		{
			index = i;
			break;
		}
	}
	if (index < 0)
		return 0;

	Entity* result = reservedSlot->items[index].ent;

#ifdef _DEBUG
	Framework::getSingleton().mLog->logMessage("fetch " + Ogre::StringConverter::toString(result->getID())
		+ " from slot " + Ogre::StringConverter::toString(reservedSlot->position.x) + "," +
		Ogre::StringConverter::toString(reservedSlot->position.y));
#endif
	result->assignSlot(0);
	reservedSlot->items[index].reservedFor = 0;
	reservedSlot->items[index].ent = 0;
	reservedSlot->capacity++;
	reservedSlot->reserved--;
	return result;
}
void Room::place(Entity* caller, Entity* item, slot*& reservedSlot)
{
	int index = -1;
	for (int i = 0; i < mSlotCapacity; ++i)
	{
		if (reservedSlot->items[i].reservedFor == caller)
		{
			index = i;
			break;
		}
	}
	assert(index >= 0);

#ifdef _DEBUG
	Framework::getSingleton().mLog->logMessage("place " + Ogre::StringConverter::toString(item->getID())
		+ " in slot " + Ogre::StringConverter::toString(reservedSlot->position.x) + "," +
		Ogre::StringConverter::toString(reservedSlot->position.y));
#endif
	reservedSlot->items[index].reservedFor = 0;
	reservedSlot->items[index].ent = item;
	reservedSlot->reserved--;
	reservedSlot->capacity--;

	item->assignSlot(reservedSlot);
	Ogre::Vector2 p;
	Tile::toHexagonalPosition(reservedSlot->position, p);
	item->setPosition(p);
	item->setTilePosition(reservedSlot->position);
}
void Room::placeManual(const std::vector<Entity*>& items, const Ogre::Vector2& position)
{
	for(slot* s : mSlots)
	{
		if(s->position == position)
		{
			for(Entity* item : items)
			{
#ifdef _DEBUG
				Framework::getSingleton().mLog->logMessage("place manual " + Ogre::StringConverter::toString(item->getID()));
#endif
				int index = -1;
				for (int i = 0; i < mSlotCapacity; ++i)
				{
					if (s->items[i].ent == 0
					&& s->items[i].reservedFor == 0)
					{
						index = i;
						break;
					}
				}
				assert(index >= 0);

				s->items[index].ent = item;
				s->capacity--;

				//i->assignSlot(s);
				Ogre::Vector2 p;
				Tile::toHexagonalPosition(s->position, p);
				item->setPosition(p);
				item->setTilePosition(s->position);
			}
			break;
		}
	}
}
void Room::reserveFetch(Entity* caller, slot*& reservedSlot, const Ogre::Vector2& nearest)
{
	reservedSlot = 0;
	Ogre::Vector2 pivot = getPivot(nearest);
	int dist = std::numeric_limits<int>::max();
	int index = -1;
	for (slot* s : mSlots)
	{
		for (int i = 0; i < mSlotCapacity; ++i)
		{
			if (s->items[i].ent
				&& s->items[i].reservedFor == 0)
			{
				int d = Tile::dist(pivot, s->position);
				if (d < dist)
				{
					dist = d;
					reservedSlot = s;
					index = i;
				}
			}
		}
	}

	if (reservedSlot)
	{
		reservedSlot->items[index].reservedFor = caller;
		reservedSlot->reserved++;
	}
}
void Room::reserveFetch(Entity* caller, slot*& reservedSlot, int entitytype, const Ogre::Vector2& nearest)
{
	reservedSlot = 0;
	Ogre::Vector2 pivot = getPivot(nearest);
	int dist = std::numeric_limits<int>::max();
	int index = -1;
	for (slot* s : mSlots)
	{
		if (s->capacity + s->reserved < mSlotCapacity)
		{
			for (int i = 0; i < mSlotCapacity; ++i)
			{
				if (s->items[i].ent
				&& s->items[i].reservedFor == 0
				&& s->items[i].ent->getType() == entitytype)
				{
					int d = Tile::dist(pivot, s->position);
					if (d < dist)
					{
						dist = d;
						reservedSlot = s;
						index = i;
					}
				}
			}
		}
	}
	if (reservedSlot)
	{
		reservedSlot->items[index].reservedFor = caller;
		reservedSlot->reserved++;
	}
}
void Room::reservePlace(Entity* caller, slot*& reservedSlot, const Ogre::Vector2& nearest)
{
	reservedSlot = 0;
	Ogre::Vector2 pivot = getPivot(nearest);
	int dist = std::numeric_limits<int>::max();
	int index = -1;
	for(slot* s : mSlots)
	{
		if(s->capacity - s->reserved > 0)
		{
			int d = Tile::dist(pivot, s->position) + mSlotCapacity*s->capacity + 0.5*s->position.y;
			if (d < dist)
			{
				dist = d;
				reservedSlot = s;
				for (int i = 0; i < mSlotCapacity; ++i)
				{
					if (s->items[i].ent == 0
					&& s->items[i].reservedFor == 0)
						index = i;
				}
			}
		}
	}

	if (reservedSlot)
	{
		reservedSlot->items[index].reservedFor = caller;
		reservedSlot->reserved++;
	}
}
void Room::getWorkers(std::vector<Ant*>& ants)
{
	for(workplace* w : mWorkPlaces)
		if(w->worker)
			ants.push_back((Ant*)w->worker);
}
void Room::getSlotEntities(std::vector<Entity*>& se)
{
	for (slot* s : mSlots)
		if (s->capacity != mSlotCapacity)
			for (int i = 0; i < mSlotCapacity; ++i)
				if (s->items[i].ent)
					se.push_back(s->items[i].ent);
}
int Room::freeSlots() const
{
	int freeSlots = 0;
	for(slot* s : mSlots)
		if(s->capacity - s->reserved > 0)
			freeSlots++;
	return freeSlots;
}
int Room::slotCount() const
{
	return mSlots.size();
}
bool Room::slotEmpty(const Ogre::Vector2& position) const
{
	for (slot* s : mSlots)
		if (s->position == position && s->capacity == mSlotCapacity)
			return true;
	return false;
}
bool Room::isEmpty() const
{
	for(slot* s : mSlots)
		if(s->capacity != mSlotCapacity)
			return false;
	return true;
}
bool Room::isEmptyIncReservations() const
{
	for (slot* s : mSlots)
		if (s->capacity + s->reserved != mSlotCapacity)
			return false;
	return true;
}

std::vector<Entity*>& Room::getGuests()
{
	return mGuests;
}
void Room::bookGuests(const std::vector<Entity*>& guests)
{
	mGuests.insert(mGuests.end(), guests.begin(), guests.end());
}
void Room::bookGuest(Entity* guest)
{
	mGuests.push_back(guest);
}
void Room::unbookGuest(Entity* guest)
{
	int i = 0;
	int c = (int)mGuests.size()-1;
	for(; i <= c; ++i)
		if(mGuests[i] == guest)
			break;
	if (i <= c)
	{
		mGuests[i] = mGuests[c];
		mGuests.pop_back();
	}
}
void Room::clean (Entity* ent)
{
    mDeleteCache.push_back (ent);
    mHygiene += 50;
}
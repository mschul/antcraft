#include "AnPCH.h"
#include "AnVisibilityMap.h"
using namespace Ants;

VisibilityMap::VisibilityMap ()
{
	mMap = 0;
	mWidth = 0;
	mHeight = 0;
}
VisibilityMap::~VisibilityMap ()
{
	SafeDelete1(mMap);
}

void VisibilityMap::init(int width, int height, bool setAll)
{
	mWidth = width;
	mHeight = height;
	mMap = new1<int>(mWidth*mHeight);
	if (!setAll)
		memset(mMap, 0, mWidth*mHeight*sizeof(int));
	else
		memset(mMap, 1, mWidth*mHeight*sizeof(int));
}
void VisibilityMap::init(const Ogre::Rect& area, bool setAll)
{
	init(area.width(), area.height(), setAll);
}

void VisibilityMap::delta(const Ogre::Vector2 pos, unsigned int radius, int delta)
{
	std::vector<Ogre::Vector2> circle;
	Ogre::Vector2 border = pos;
	border += Tile::getNeighbours(pos)[Tile::LEFT] * radius;
	Tile::hexCircle(pos, border, circle);
	for(Ogre::Vector2 p : circle)
	{
		unsigned int i = mWidth*p.y + p.x;
		mMap[i] += delta;
	}
}
void VisibilityMap::increment(const Ogre::Vector2 pos, unsigned int radius)
{
	delta(pos, radius, 1);
}
void VisibilityMap::decrement(const Ogre::Vector2 pos, unsigned int radius)
{
	delta(pos, radius, -1);
}
void VisibilityMap::increment(Entity* ent)
{
	const Ogre::Vector2 pos = ent->getTilePosition();
	const unsigned int radius = ent->getVisibilityRadius();
	delta(pos, radius, 1);
}
void VisibilityMap::decrement(Entity* ent)
{
	const Ogre::Vector2 pos = ent->getTilePosition();
	const unsigned int radius = ent->getVisibilityRadius();
	delta(pos, radius, -1);
}
void VisibilityMap::set(const Ogre::Vector2 pos, unsigned int radius)
{
	std::vector<Ogre::Vector2> circle;
	Ogre::Vector2 border = pos;
	border += Tile::getNeighbours(pos)[Tile::LEFT] * radius;
	Tile::hexCircle(pos, border, circle);
	for (Ogre::Vector2 p : circle)
	{
		unsigned int i = mWidth*p.y + p.x;
		mMap[i] = 1;
	}
}
void VisibilityMap::set(Entity* ent)
{
	const Ogre::Vector2 pos = ent->getTilePosition();
	const unsigned int radius = ent->getVisibilityRadius();
	set(pos, radius);
}

bool VisibilityMap::isVisible(Ogre::Vector2 position) const
{
	const unsigned int i = mWidth*((int)position.y) + position.x;
	return mMap[i] > 0;
}
bool VisibilityMap::isVisible(Entity* ent) const
{
	return isVisible(ent->getTilePosition());
}
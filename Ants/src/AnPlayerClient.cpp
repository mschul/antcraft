#include "AnPCH.h"
#include "AnPlayerClient.h"
#include "AnEgg.h"
#include "AnSmallWorker.h"
#include "AnWorker.h"
#include "AnCarrier.h"
#include "AnSoldier.h"
#include "AnKamikaze.h"
#include "AnGrub.h"
#include "AnFlattenJob.h"
#include "AnGotoJob.h"
#include "AnFaunalFood.h"
#include "AnDigResourceJob.h"
#include "AnFloralFood.h"
#include "AnCopal.h"
#include "AnDebris.h"
#include "AnFetchJob.h"
#include "AnRoomCreateJob.h"
#include "AnRoomCreateJobEx.h"
#include "AnFaunalSource.h"
#include "AnProceduralGeometry.h"
#include "AnProjectile.h"
#include "AnDrawableWorldMap.h"
#include "AnDrawableEntityMap.h"
#include "AnAOE.h"
using namespace Ants;

PlayerClient::PlayerClient ()
{
	mUpgradeLevel = 0;

	mSceneManager = 0;
	mFOW = false;

	mWorld = 0;
	mWorldQuery = 0;

	mEntities = 0;
	mEntityQuery = 0;

	mJobs = 0;
	mJobQuery = 0;

	mRooms = 0;
	mRoomQuery = 0;

	mPheromoneMap = 0;
}
PlayerClient::~PlayerClient ()
{
	SafeDelete0(mStatistics);
	SafeDelete0(mWorld);
	SafeDelete0(mWorldQuery);
	SafeDelete0(mEntities);
	SafeDelete0(mEntityQuery);
	SafeDelete0(mJobs);
	SafeDelete0(mJobQuery);
	SafeDelete0(mRooms);
	SafeDelete0(mRoomQuery);
	SafeDelete0(mPheromoneMap);
}

void PlayerClient::init(unsigned int width, unsigned int height)
{
	mStatistics = new0 Statistics(mID);

	mWorld = new0 DrawableWorldMap();
	mWorld->init(width, height);
	mWorldQuery = new0 WorldQuery(mWorld);

	mEntities = new0 DrawableEntityMap();
	mEntities->init(width, height);
	mEntityQuery = new0 EntityQuery(mWorld, mEntities);

	mJobs = new0 JobMap();
	mJobs->init(width, height);
	mJobQuery = new0 JobQuery(mWorld, mJobs);

	mRooms = new0 RoomMap();
	mRooms->init(width, height);
	mRoomQuery = new0 RoomQuery(mRooms, mWorld);

	mPheromoneMap = new0 PheromoneMap();
	mPheromoneMap->init(width, height);
}
void PlayerClient::init(const Ogre::Rect& area)
{
	mStatistics = new0 Statistics(mID);

	mWorld = new0 DrawableWorldMap();
	mWorld->init(area);
	mWorldQuery = new0 WorldQuery(mWorld);

	mEntities = new0 DrawableEntityMap();
	mEntities->init(area);
	mEntityQuery = new0 EntityQuery(mWorld, mEntities);

	mJobs = new0 JobMap();
	mJobs->init(area);
	mJobQuery = new0 JobQuery(mWorld, mJobs);

	mRooms = new0 RoomMap();
	mRooms->init(area);
	mRoomQuery = new0 RoomQuery(mRooms, mWorld);

	mPheromoneMap = new0 PheromoneMap();
	mPheromoneMap->init(area);
}
void PlayerClient::initScene(Ogre::SceneManager* sceneMgr, Ogre::Camera* camera,
	int rwWidth, int rwHeight)
{
	mSceneManager = sceneMgr;
	((DrawableWorldMap*)mWorld)->initScene(sceneMgr);
	((DrawableEntityMap*)mEntities)->initScene(sceneMgr, mFOW);


	mOverlayRootNode = sceneMgr->getRootSceneNode()->createChildSceneNode("overlayRootNode");
	mWorldRootNode = sceneMgr->getRootSceneNode()->createChildSceneNode("worldRootNode");

	mHoverMeshName = ProceduralGeometry::getSingletonPtr()->getPrimitive("hexagon");
	mHoverTile = mOverlayRootNode->createChildSceneNode("hoverNodeTileNode");
	Ogre::Entity* ent = mSceneManager->createEntity(mHoverMeshName);
	ent->setVisibilityFlags(OVERLAY_VISIBILITY_FLAG);

	ent->setRenderQueueGroup(Ogre::RENDER_QUEUE_OVERLAY);
	mHoverTile->attachObject(ent);
	mHoverTile->setVisible(false);

	mSelectionMeshName = ProceduralGeometry::getSingletonPtr()->getPrimitive("hexagon");
	mSelectionMaterialName = "selection";
	mSelectedTilesGeom = sceneMgr->createStaticGeometry("sgSelectedTiles");

	mAssignMeshName = ProceduralGeometry::getSingletonPtr()->getPrimitive("hexagon");
	mAssignMaterialName = "assign";
	mAssignedTilesGeom = sceneMgr->createStaticGeometry("sgAssignedTiles");

	mAttackStrategyMaterialName = "attack";
	mDefendStrategyMaterialName = "defend";
	mStrategyMeshName = ProceduralGeometry::getSingletonPtr()->getPrimitive("hexagon");

	// set fog of war compositor
	Ogre::Viewport* vp = Framework::getSingleton().mViewport;
	Ogre::CompositorManager* cMgr = Ogre::CompositorManager::getSingletonPtr();
	if (mFOW)
	{
		Ogre::CompositorInstance *instance = cMgr->addCompositor(vp, "FOWB&W");
		cMgr->setCompositorEnabled(vp, "FOWB&W", true);
	}
	else
	{
		cMgr->setCompositorEnabled(vp, "FOWB&W", false);
	}
}

Statistics* PlayerClient::getStatistics() const
{
	mStatistics->AnthillArea = mWorld->getAnthillTileCount();
	mRooms->fillStats(mStatistics);
	return mStatistics;
}

void PlayerClient::assignToWorkplace(Room* room)
{
	Entity* ent;
	Ogre::Vector2 tp;

	room->setWorkersRefilled();
	if (room->freeWorkplaces() == 0)
		return;

	std::vector<Entity::EntityType> allowedTypes;
	allowedTypes.push_back(Entity::ET_SMALL_WORKER);
	allowedTypes.push_back(Entity::ET_WORKER);
	allowedTypes.push_back(Entity::ET_CARRIER);
	int flags = mEntityQuery->getEntityFlag(allowedTypes);
	mEntityQuery->findNextFreelancer(room->getFirstPivot(), tp, ent, flags, mID, 50);

	if(ent)
		room->assignToWorkplace((Ant*)ent);
}
void PlayerClient::assignWorkers(Room* room)
{

	room->setWorkersRefilled();
	while (room->freeWorkplaces() > 0)
	{
		Entity* ent;
		Ogre::Vector2 tp;
		if (room->getType() == Room::BROOD_CHAMBER && room->workerCount() == 0)
		{
			std::vector<Entity::EntityType> allowedTypes;
			allowedTypes.push_back(Entity::ET_SMALL_WORKER);
			allowedTypes.push_back(Entity::ET_WORKER);
			allowedTypes.push_back(Entity::ET_CARRIER);
			int flags = mEntityQuery->getEntityFlag(allowedTypes);
			mEntityQuery->findNextEntity(room->getFirstPivot(), tp, ent, flags, mID, 50);
		}
		else
		{
			std::vector<Entity::EntityType> allowedTypes;
			allowedTypes.push_back(Entity::ET_SMALL_WORKER);
			allowedTypes.push_back(Entity::ET_WORKER);
			allowedTypes.push_back(Entity::ET_CARRIER);
			int flags = mEntityQuery->getEntityFlag(allowedTypes);
			mEntityQuery->findNextFreelancer(room->getFirstPivot(), tp, ent, flags, mID, 50);
		}

		if (ent)
			room->assignToWorkplace((Ant*)ent);
		else
			break;
	}
}
void PlayerClient::updateWorkerAssignments(Room::RoomType type)
{
	roomIterator iter, iend;
	iter = mRooms->getRoomIter(type);
	iend = mRooms->getRoomIterEnd(type);
	for (; iter != iend; ++iter)
	{
		Room* room = mRooms->getRoom(*iter);
		if (room->workerRefillRequested())
			assignWorkers(room);
	}
}
void PlayerClient::unAssignFromWorkplace(Room* room)
{
	room->unassignLastWorker();
}

bool PlayerClient::isSelected(const Ogre::Vector2& position)
{
	typedef std::set<int>::iterator setIter;
	Ogre::Rect size = mWorld->getSize();
	Ogre::Vector2 p;
	Tile::toTilePosition(Ogre::Vector2(position.x / 2, position.y / 2), p);
	const int tile = integer(size.width()*((int)p.y) + p.x);
	return mSelectedTiles.find(tile) != mSelectedTiles.end();
}
void PlayerClient::preSelect(const Ogre::Vector2& position)
{
	std::vector<Ogre::Vector2> positions;
	Ogre::Vector2 p;
	Tile::toTilePosition(Ogre::Vector2(position.x / 2, position.y / 2), p);
	positions.push_back(p);
	preSelect(positions);
}
void PlayerClient::preSelectLine(const Ogre::Vector2 start, const Ogre::Vector2 end)
{
	std::vector<Ogre::Vector2> path;
	Ogre::Vector2 s, e;
	Tile::toTilePosition(Ogre::Vector2(start.x / 2, start.y / 2), s);
	Tile::toTilePosition(Ogre::Vector2(end.x / 2, end.y / 2), e);
	Tile::hexLine(s, e, path);
	preSelect(path);
}
void PlayerClient::preSelectCircle(const Ogre::Vector2 start, const Ogre::Vector2 end)
{
	std::vector<Ogre::Vector2> path;
	Ogre::Vector2 s, e;
	Tile::toTilePosition(Ogre::Vector2(start.x / 2, start.y / 2), s);
	Tile::toTilePosition(Ogre::Vector2(end.x / 2, end.y / 2), e);
	Tile::hexCircle(s, e, path);
	preSelect(path);
}
void PlayerClient::preSelect(const std::vector<Ogre::Vector2>& positions)
{
	clearPreselection();

	Ogre::Rect size = mWorld->getSize();
	std::vector<Ogre::Vector2>::const_iterator iter, iend;
	iter = positions.begin(); iend = positions.end();

	for(; iter != iend; ++iter)
	{
		Ogre::Vector2 position = *iter;
		if (position.x < 0 || position.x >= size.width()
			|| position.y < 0 || position.y >= size.height())
			continue;

		int tile = integer(size.width()*((int)position.y) + position.x);

		std::map<int, Ogre::SceneNode*>::iterator iter = mPreselectionNodes.find(tile);
		if(iter == mPreselectionNodes.end())
		{
			Ogre::String name = Ogre::String("preSelection");
			name += Ogre::StringConverter::toString(position);

			Ogre::Entity* ent = mSceneManager->createEntity(name, mSelectionMeshName);
			ent->setMaterialName(mSelectionMaterialName);
			ent->setVisibilityFlags(OVERLAY_VISIBILITY_FLAG);

			Ogre::SceneNode* n = mOverlayRootNode->createChildSceneNode();
			n->attachObject(ent);
			Ogre::Vector2 hp;
			Tile::toHexagonalPosition(position, hp);
			n->setPosition(Ogre::Vector3(2.0f * hp.x, 2.0f * hp.y, 1.3f));
			mPreselectionNodes.insert(std::make_pair(tile, n));
		}
	}
}
void PlayerClient::preUnSelect(const Ogre::Vector2& position)
{
	std::vector<Ogre::Vector2> positions;
	Ogre::Vector2 p;
	Tile::toTilePosition(Ogre::Vector2(position.x / 2, position.y / 2), p);
	positions.push_back(p);
	preUnSelect(positions);
}
void PlayerClient::preUnSelectLine(const Ogre::Vector2 start, const Ogre::Vector2 end)
{
	std::vector<Ogre::Vector2> path;
	Ogre::Vector2 s, e;
	Tile::toTilePosition(Ogre::Vector2(start.x / 2, start.y / 2), s);
	Tile::toTilePosition(Ogre::Vector2(end.x / 2, end.y / 2), e);
	Tile::hexLine(s, e, path);
	preUnSelect(path);
}
void PlayerClient::preUnSelectCircle(const Ogre::Vector2 start, const Ogre::Vector2 end)
{
	std::vector<Ogre::Vector2> path;
	Ogre::Vector2 s, e;
	Tile::toTilePosition(Ogre::Vector2(start.x / 2, start.y / 2), s);
	Tile::toTilePosition(Ogre::Vector2(end.x / 2, end.y / 2), e);
	Tile::hexCircle(s, e, path);
	preUnSelect(path);
}
void PlayerClient::preUnSelect(const std::vector<Ogre::Vector2>& positions)
{
	clearPreselection();

	Ogre::Rect size = mWorld->getSize();
	std::vector<Ogre::Vector2>::const_iterator iter, iend;
	iter = positions.begin(); iend = positions.end();

	for(; iter != iend; ++iter)
	{
		Ogre::Vector2 position = *iter;
		if (position.x < 0 || position.x >= size.width()
			|| position.y < 0 || position.y >= size.height())
			continue;

		int tile = integer(size.width()*((int)position.y) + position.x);

		std::map<int, Ogre::SceneNode*>::iterator iter = mPreunselectionNodes.find(tile);
		if(iter == mPreunselectionNodes.end())
		{
			Ogre::String name = Ogre::String("preUnSelection");
			name += Ogre::StringConverter::toString(position);

			Ogre::Entity* ent = mSceneManager->createEntity(name, mSelectionMeshName);
			ent->setVisibilityFlags(OVERLAY_VISIBILITY_FLAG);

			ent->setMaterialName(mSelectionMaterialName);
			Ogre::SceneNode* n = mOverlayRootNode->createChildSceneNode();
			n->attachObject(ent);
			Ogre::Vector2 hp;
			Tile::toHexagonalPosition(position, hp);
			n->setPosition(Ogre::Vector3(2.0f * hp.x, 2.0f * hp.y, 1.3f));
			mPreunselectionNodes.insert(std::make_pair(tile, n));
		}
	}
}
void PlayerClient::select()
{
	typedef std::map<int, Ogre::SceneNode*>::iterator mapIter;
	typedef std::set<int>::iterator setIter;
	Ogre::Rect size = mWorld->getSize();

	mapIter iter, iend;
	iter = mPreselectionNodes.begin(); iend = mPreselectionNodes.end();
	for(; iter != iend; ++iter)
	{
		int tile = iter->first;
		if(mSelectedTiles.find(tile) == mSelectedTiles.end())
		{
			Ogre::Vector2 pos(real(tile % size.width()), real(tile / size.width()));
			Room* r0 = mRooms->getResourceRoom(pos);
			FaunalSource* r1 = (FaunalSource*)mRooms->getFaunalRoom(pos);
			if(mWorld->isDiggable(tile))
			{
				addJob(tile, Job::DIG_JOB);
				mSelectedTiles.insert(tile);
			}
			else if (r0 && r0->slotEmpty(pos))
			{
				DigResourceJob* j = (DigResourceJob*)addJob(tile, Job::DIG_RESOURCE_JOB);
				j->setResourceID(r0->getID());
				mSelectedTiles.insert(tile);
			}
			else if (r1 && r1->getSource() != 0)
			{
				DigResourceJob* j = (DigResourceJob*)addJob(tile, Job::DIG_RESOURCE_JOB);
				j->setResourceID(r1->getID());
				mSelectedTiles.insert(tile);
			}
		}
	}
	
	iter = mPreunselectionNodes.begin(); iend = mPreunselectionNodes.end();
	for(; iter != iend; ++iter)
	{
		int tile = iter->first;
		if(mSelectedTiles.find(tile) != mSelectedTiles.end())
		{
			if(mWorld->isResource(tile))
			{
				removeJobs(tile, Job::DIG_RESOURCE_JOB);
			}
			else if(mWorld->isDiggable(tile))
			{
				removeJobs(tile, Job::DIG_JOB);
			}
			mSelectedTiles.erase(tile);
		}
	}
	
	rebuildSelection();

	clearPreselection();
}
void PlayerClient::unSelect()
{
	std::set<int> outSet;
	typedef std::map<int, Ogre::SceneNode*>::iterator mapIter;
	typedef std::set<int>::iterator setIter;
	
	mapIter iter, iend;
	iter = mPreunselectionNodes.begin(); iend = mPreunselectionNodes.end();
	for(; iter != iend; ++iter)
	{
		int tile = iter->first;
		if(mSelectedTiles.find(tile) != mSelectedTiles.end())
		{
			// actually unselected tile
			removeJobs(tile, Job::DIG_JOB);
			mSelectedTiles.erase(tile);
		}
	}

	rebuildSelection();
	
	clearPreselection();
	
}
void PlayerClient::rebuildSelection()
{
	typedef std::set<int>::iterator setIter;
	mSelectedTilesGeom->reset();
	Ogre::Rect size = mWorld->getSize();
	
	setIter siter, siend;
	siter = mSelectedTiles.begin(); siend = mSelectedTiles.end();
	Ogre::Entity* ent = mSceneManager->createEntity(mSelectionMeshName);
	for(; siter != siend; ++siter)
	{
		int i = *siter;
		Ogre::Vector2 p;
		Tile::toHexagonalPosition(Ogre::Vector2(real(i % size.width()),  real(i / size.width())), p);

		ent->setMaterialName(mSelectionMaterialName);
		ent->setVisibilityFlags(OVERLAY_VISIBILITY_FLAG);
		mSelectedTilesGeom->addEntity(ent, Ogre::Vector3(2.0f*p.x, 2.0f*p.y, 1.1f));
	}
	mSelectedTilesGeom->build();
	mSelectedTilesGeom->setVisibilityFlags(OVERLAY_VISIBILITY_FLAG);
}
void PlayerClient::hover(const Ogre::Vector2& position, SelectionMode mode)
{
	Ogre::Rect size = mWorld->getSize();
	Ogre::Vector2 p;
	Tile::toTilePosition(Ogre::Vector2(position.x / 2, position.y / 2), p);
	if(!mWorld->tileExists(p))
		return;

	const int tile = integer(size.width()*((int)p.y) + p.x);

	bool show = false;

	switch (mode)
	{
	case ROOMS:
		show = mWorld->isBuildable(tile);
		break;
	case DIG:
	{
		show = mWorld->isDiggable(tile);
		if (!show)
		{
			Room* r0 = mRooms->getResourceRoom(p);
			FaunalSource* r1 = (FaunalSource*)mRooms->getFaunalRoom(p);
			show = ((r0 && r0->slotEmpty(p)) || (r1 && r1->getSource() != 0));
		}
	}
	break;
	}


	if(show)
	{
		Ogre::Vector2 hp;
		Tile::toHexagonalPosition(p, hp);
		mHoverTile->setPosition(Ogre::Vector3(2*hp.x, 2*hp.y, 1.2f));
		mHoverTile->setVisible(true);
	}
	else
	{
		mHoverTile->setVisible(false);
	}
}
void PlayerClient::hideHover()
{
	mHoverTile->setVisible(false);
}
void PlayerClient::clearPreselection()
{
	for(std::pair<int, Ogre::SceneNode*> p : mPreselectionNodes)
	{
		Ogre::SceneNode* n = p.second;
		if(!n)
			continue;

		destroyAllAttachedMovableObjects(n);
		p.second->removeAndDestroyAllChildren();
		mSceneManager->destroySceneNode(n);
	}
	for(std::pair<int, Ogre::SceneNode*> p : mPreunselectionNodes)
	{
		Ogre::SceneNode* n = p.second;
		if(!n)
			continue;

		destroyAllAttachedMovableObjects(n);
		p.second->removeAndDestroyAllChildren();
		mSceneManager->destroySceneNode(n);
	}
	mPreselectionNodes.clear();
	mPreunselectionNodes.clear();
}

void PlayerClient::getRoomList(std::vector<Room*>& rooms)
{
	rooms.push_back(mRooms->getThroneChamber());

	roomIterator iter, iend;
	iter = mRooms->getRoomIter(Room::BROOD_CHAMBER);
	iend = mRooms->getRoomIterEnd(Room::BROOD_CHAMBER);
	for (; iter != iend; ++iter)
		rooms.push_back(mRooms->getRoom(*iter));
	iter = mRooms->getRoomIter(Room::STORAGE_CHAMBER);
	iend = mRooms->getRoomIterEnd(Room::STORAGE_CHAMBER);
	for (; iter != iend; ++iter)
		rooms.push_back(mRooms->getRoom(*iter));
	iter = mRooms->getRoomIter(Room::FUNGI_CHAMBER);
	iend = mRooms->getRoomIterEnd(Room::FUNGI_CHAMBER);
	for (; iter != iend; ++iter)
		rooms.push_back(mRooms->getRoom(*iter));

}
Room* PlayerClient::getRoomAt(const Ogre::Vector2& position)
{
	Ogre::Vector2 p;
	Tile::toTilePosition(Ogre::Vector2(position.x / 2, position.y / 2), p);
	return mRooms->getRoom(p);
}
Room* PlayerClient::getResourceAt(const Ogre::Vector2& position)
{
	Ogre::Vector2 p;
	Tile::toTilePosition(Ogre::Vector2(position.x / 2, position.y / 2), p);
	return mRooms->getResourceRoom(p);
}
Room* PlayerClient::getExitAt(const Ogre::Vector2& position)
{
	Ogre::Vector2 p;
	Tile::toTilePosition(Ogre::Vector2(position.x / 2, position.y / 2), p);
	return mRooms->getExit(p);
}
Room* PlayerClient::getRoom(int id)
{
	return mRooms->getRoom(id);
}
Entity* PlayerClient::getEntity(int id)
{
	return mEntities->get(id);
}
void PlayerClient::preAssign(const Ogre::Vector2& position)
{
	std::vector<Ogre::Vector2> positions;
	Ogre::Vector2 p;
	Tile::toTilePosition(Ogre::Vector2(position.x / 2, position.y / 2), p);
	positions.push_back(p);
	preAssign(positions);
}
void PlayerClient::preAssignLine(const Ogre::Vector2 start, const Ogre::Vector2 end)
{
	std::vector<Ogre::Vector2> path;
	Ogre::Vector2 s, e;
	Tile::toTilePosition(Ogre::Vector2(start.x / 2, start.y / 2), s);
	Tile::toTilePosition(Ogre::Vector2(end.x / 2, end.y / 2), e);
	Tile::hexLine(s, e, path);
	preAssign(path);
}
void PlayerClient::preAssignCircle(const Ogre::Vector2 start, const Ogre::Vector2 end)
{
	std::vector<Ogre::Vector2> path;
	Ogre::Vector2 s, e;
	Tile::toTilePosition(Ogre::Vector2(start.x / 2, start.y / 2), s);
	Tile::toTilePosition(Ogre::Vector2(end.x / 2, end.y / 2), e);
	Tile::hexCircle(s, e, path);
	preAssign(path);
}
void PlayerClient::preAssign(const std::vector<Ogre::Vector2>& positions)
{	
	clearPreassignment();

	Ogre::Rect size = mWorld->getSize();
	std::vector<Ogre::Vector2>::const_iterator iter, iend;
	iter = positions.begin(); iend = positions.end();

	for(; iter != iend; ++iter)
	{
		Ogre::Vector2 position = *iter;
		
		if (position.x < 0 || position.x >= size.width()
			|| position.y < 0 || position.y >= size.height())
			continue;

		const int tile = integer(size.width()*((int)position.y) + position.x);


		Room* r = mRooms->getRoom(position);
		if (r && r->getType() == Room::THRONE_CHAMBER)
			continue;

		if(mWorld->isBuildable(tile))
		{
			std::map<int, Ogre::SceneNode*>::iterator iter = mPreAssignedNodes.find(tile);
			if(iter == mPreAssignedNodes.end())
			{
				Ogre::String name = Ogre::String("preAssignment");
				name += Ogre::StringConverter::toString(position);

				Ogre::Entity* ent = mSceneManager->createEntity(name, mAssignMeshName);
				ent->setVisibilityFlags(OVERLAY_VISIBILITY_FLAG);

				ent->setMaterialName(mAssignMaterialName);
				Ogre::SceneNode* n = mOverlayRootNode->createChildSceneNode();
				n->attachObject(ent);
				Ogre::Vector2 hp;
				Tile::toHexagonalPosition(position, hp);
				n->setPosition(Ogre::Vector3(2.0f * hp.x, 2.0f * hp.y,
					mWorld->isBlocking(tile) ? 1.3f : 0.3f));
				mPreAssignedNodes.insert(std::make_pair(tile, n));
			}
		}
	}
}
void PlayerClient::preUnAssign(const Ogre::Vector2& position)
{
	std::vector<Ogre::Vector2> positions;
	Ogre::Vector2 p;
	Tile::toTilePosition(Ogre::Vector2(position.x / 2, position.y / 2), p);
	positions.push_back(p);
	preUnAssign(positions);
}
void PlayerClient::preUnAssignLine(const Ogre::Vector2 start, const Ogre::Vector2 end)
{
	std::vector<Ogre::Vector2> path;
	Ogre::Vector2 s, e;
	Tile::toTilePosition(Ogre::Vector2(start.x / 2, start.y / 2), s);
	Tile::toTilePosition(Ogre::Vector2(end.x / 2, end.y / 2), e);
	Tile::hexLine(s, e, path);
	preUnAssign(path);
}
void PlayerClient::preUnAssignCircle(const Ogre::Vector2 start, const Ogre::Vector2 end)
{
	std::vector<Ogre::Vector2> path;
	Ogre::Vector2 s, e;
	Tile::toTilePosition(Ogre::Vector2(start.x / 2, start.y / 2), s);
	Tile::toTilePosition(Ogre::Vector2(end.x / 2, end.y / 2), e);
	Tile::hexCircle(s, e, path);
	preUnAssign(path);
}
void PlayerClient::preUnAssign(const std::vector<Ogre::Vector2>& positions)
{	
	clearPreassignment();

	Ogre::Rect size = mWorld->getSize();
	std::vector<Ogre::Vector2>::const_iterator iter, iend;
	iter = positions.begin(); iend = positions.end();

	for(; iter != iend; ++iter)
	{
		Ogre::Vector2 position = *iter;
		if (position.x < 0 || position.x >= size.width()
			|| position.y < 0 || position.y >= size.height())
			continue;

		int tile = integer(size.width()*((int)position.y) + position.x);

		Room* r = mRooms->getRoom(position);
		if (r && r->getType() == Room::THRONE_CHAMBER)
			continue;

		std::map<int, Ogre::SceneNode*>::iterator iter = mPreUnassignedNodes.find(tile);
		if(iter == mPreUnassignedNodes.end())
		{
			Ogre::String name = Ogre::String("preUnAssignment");
			name += Ogre::StringConverter::toString(position);

			Ogre::Entity* ent = mSceneManager->createEntity(name, mAssignMeshName);
			ent->setVisibilityFlags(OVERLAY_VISIBILITY_FLAG);

			ent->setMaterialName(mAssignMaterialName);
			Ogre::SceneNode* n = mOverlayRootNode->createChildSceneNode();
			n->attachObject(ent);
			Ogre::Vector2 hp;
			Tile::toHexagonalPosition(position, hp);
			n->setPosition(Ogre::Vector3(2.0f * hp.x, 2.0f * hp.y,
				mWorld->isBlocking(tile) ? 1.3f : 0.3f));
			mPreUnassignedNodes.insert(std::make_pair(tile, n));
		}
	}
}
void PlayerClient::rebuildAssignment()
{
	typedef std::set<int>::iterator setIter;
	mAssignedTilesGeom->reset();
	Ogre::Rect size = mWorld->getSize();
	
	setIter siter, siend;
	siter = mAssignedTiles.begin(); siend = mAssignedTiles.end();
	Ogre::Entity* ent = mSceneManager->createEntity(mAssignMeshName);
	for(; siter != siend; ++siter)
	{
		int i = *siter;
		Ogre::Vector2 p;
		Tile::toHexagonalPosition(Ogre::Vector2(real(i % size.width()), real(i / size.width())), p);

		ent->setMaterialName(mAssignMaterialName);
		mAssignedTilesGeom->addEntity(ent, Ogre::Vector3(2.0f*p.x, 2.0f*p.y,
			mWorld->isBlocking(i) ? 1.3f : 0.3f));
	}
	mAssignedTilesGeom->build();
	mAssignedTilesGeom->setVisibilityFlags(OVERLAY_VISIBILITY_FLAG);
}
int PlayerClient::assignRoom(Room::RoomType type, const Ogre::Vector2& position, Tile* t)
{
	int roomID = -1;
	if(t)
	{
		if(t->isAnthill())
		{
			bool clear = true;
			for(Entity* e : mEntities->get(position))
			{
				if (e->getType() == Entity::ET_DEBRIS && e->getParent() == 0)
				{
					clear = false;
					break;
				}
			}
			if(clear) roomID = mRooms->assignRoom(type, position);
		}
		else if ((t->getType() == Tile::ROUGH || t->getType() ==Tile::MUD)
		&&	!mJobQuery->hasJob(position, Job::ROOM_CREATE_JOB))
		{

			Room* r = mRoomQuery->findNearestRoom(Room::EXIT_ROOM, position);
			if(r)
			{
				int jobID = mJobs->add(Job::ROOM_CREATE_JOB, position);
				RoomCreateJob* j = (RoomCreateJob*)mJobs->get(jobID);
				j->setRoomType(type);
				j->setTargetRoom(r);
			}
		}
		else if (t->getType() == Tile::DIRT
			&& !mJobQuery->hasJob(position, Job::ROOM_CREATE_JOB_EX))
		{
			Room* r = mRoomQuery->findNearestRoom(Room::EXIT_ROOM, position);
			if (r)
			{
				int jobID = mJobs->add(Job::ROOM_CREATE_JOB_EX, position);
				RoomCreateJobEx* j = (RoomCreateJobEx*)mJobs->get(jobID);
				j->setRoomType(type);
				j->setTargetRoom(r);
			}
		}
	}
	return roomID;
}
void PlayerClient::assignRoom(Room::RoomType type, const std::vector<Ogre::Vector2>& positions)
{
	std::vector<Ogre::Vector2> filteredPositions;
	int roomID = -1;
	for(Ogre::Vector2 p : positions)
	{
		Room* r = mRooms->getRoom(p);
		if (r && r->getType() == Room::THRONE_CHAMBER)
			continue;

		Tile* t = mWorld->getTile(p);
		if(t)
		{
			int id = assignRoom(type, p, t);
			if(id >= 0) roomID = id;
			if(t->isAnthill())
				filteredPositions.push_back(p);
		}
	}
	if(roomID >= 0)
	{
		std::vector<PositionTilePair> positionedTiles;
		mRooms->buildRoom(roomID, filteredPositions[0], positionedTiles);
		mWorld->setTiles(positionedTiles, true);
	}
}
void PlayerClient::assignRoom(Room::RoomType type, const Ogre::Vector2& position)
{
	std::vector<Ogre::Vector2> filteredPositions;

	Room* r = mRooms->getRoom(position);
	if (r && r->getType() == Room::THRONE_CHAMBER)
		return;

	Tile* t = mWorld->getTile(position);
	int roomID = -1;
	if(t)
	{

		int id = assignRoom(type, position, t);
		if(id >= 0) roomID = id;
		if(t->isAnthill())
			filteredPositions.push_back(position);
	}
	if(roomID >= 0)
	{
		std::vector<PositionTilePair> positionedTiles;
		mRooms->buildRoom(roomID, position, positionedTiles);
		mWorld->setTiles(positionedTiles, true);
	}
}
void PlayerClient::assign(Room::RoomType type)
{
	Ogre::Rect size = mWorld->getSize();
	unsigned int width = size.width();
	std::vector<Ogre::Vector2> filteredPositions;
	int roomID = -1;
	for(std::pair<int, Ogre::SceneNode*> p : mPreAssignedNodes)
	{
		if(p.second)
		{
			Ogre::Vector2 pos(real(p.first%width), real(p.first/width));
			mAssignedTiles.insert(p.first);
			Tile* t = mWorld->getTile(pos);
			if(t)
			{
				int id = assignRoom(type, pos, t);
				if(id >= 0) roomID = id;
				if(t->isAnthill())
					filteredPositions.push_back(pos);
			}
		}
	}
	for(std::pair<int, Ogre::SceneNode*> p : mPreUnassignedNodes)
	{
		if(p.second)
		{
			Ogre::Vector2 pos(real(p.first%width), real(p.first/width));
			mAssignedTiles.erase(p.first);
			Tile* t = mWorld->getTile(pos);
			if(t)
			{
				removeJobs(p.first, Job::ROOM_CREATE_JOB);
				removeJobs(p.first, Job::ROOM_CREATE_JOB_EX);

				// unassigning has a built-in build function
				std::vector<PositionTilePair> tiles;
				mRooms->unAssignRoom(pos, tiles);
				mWorld->setTiles(tiles, true);
			}
		}
	}
	
	if(roomID >= 0)
	{
		for(Ogre::Vector2 p : filteredPositions)
		{
			int tile = integer(size.width()*((int)p.y) + p.x);
			mAssignedTiles.erase(tile);
		}

		std::vector<PositionTilePair> positionedTiles;
		mRooms->buildRoom(roomID, filteredPositions[0], positionedTiles);
		mWorld->setTiles(positionedTiles, true);
	}
	
	rebuildAssignment();
	clearPreassignment();
}
void PlayerClient::clearPreassignment()
{
	for(std::pair<int, Ogre::SceneNode*> p : mPreAssignedNodes)
	{
		Ogre::SceneNode* n = p.second;
		if(!n)
			continue;

		destroyAllAttachedMovableObjects(n);
		p.second->removeAndDestroyAllChildren();
		mSceneManager->destroySceneNode(n);
	}
	for(std::pair<int, Ogre::SceneNode*> p : mPreUnassignedNodes)
	{
		Ogre::SceneNode* n = p.second;
		if(!n)
			continue;

		destroyAllAttachedMovableObjects(n);
		p.second->removeAndDestroyAllChildren();
		mSceneManager->destroySceneNode(n);
	}
	mPreAssignedNodes.clear();
	mPreUnassignedNodes.clear();
}
void PlayerClient::toggleHealthBarsVisible()
{
	((DrawableEntityMap*)mEntities)->toggleHealthBarsVisible();
}

void PlayerClient::grow(Job::JobType type, const Ogre::Vector2& position)
{
	Ogre::Vector2 tp;
	Tile::toTilePosition(Ogre::Vector2(position.x/2, position.y/2), tp);
	Ogre::Rect size = mWorld->getSize();
	std::vector<Job*> jobs;
	
	Job::JobType otherType = type == Job::HOLD_JOB ? Job::DEFEND_JOB : Job::HOLD_JOB;
	Tile* t = mWorld->getTile(tp);
	if (!t)
		return;

	if(!t->isBlocking())
	{
		std::vector<Job*> allJobs = mJobs->get(tp);
		for(Job* j : allJobs)
		{
			if(j->getType() == type)
				jobs.push_back(j);
			else if(j->getType() == otherType)
				removeStrategy(j);
		}
	}
	
	int currentKernel = 0;
	int nextKernel = 0;
	int start = integer(size.width()*((int)tp.y) + tp.x);
	if(jobs.empty() && !mWorld->getTile(tp)->isBlocking())
	{
		addJob(start, type);
		addStrategyNode(1, tp, type);
	}
	else
	{
		std::vector<Job*> jobBuckets[2];
		for(Job* j : jobs)
		{
			if(j->getType() == type && j->getCapacity() < 3)
			{
				currentKernel = j->getCapacity();
				jobBuckets[j->getCapacity()-1].push_back(j);
				break;
			}
		}

		std::vector<Ogre::Vector2> nextLevel;
		std::vector<Ogre::Vector2> currentLevel;
		std::set<int> marks;
		marks.insert(start);
		Job* increasable = 0;
		currentLevel.push_back(tp);
		for(;;)
		{
			std::vector<Job*> newJobBuckets[2];
			nextKernel = 3;
			bool finished = false;
			while(!currentLevel.empty() && !finished)
			{
				// calculate next kernel layer
				Ogre::Vector2 pos = currentLevel.back();
				currentLevel.pop_back();
				Ogre::Vector2 lastPos;
				for(Ogre::Vector2& delta : Tile::getNeighbours(pos))
				{
					Ogre::Vector2 p = pos + delta;
					int tile = integer(size.width()*((int)p.y) + p.x);
					if(marks.find(tile) != marks.end())
						continue;
					marks.insert(tile);
					Tile* t = mWorld->getTile(p);
					if (t && !t->isBlocking())
					{
						nextLevel.push_back(p);
						jobs.clear();
						{
							std::vector<Job*> allJobs = mJobs->get(p);
							for(Job* j : allJobs)
							{
								if(j->getType() == type)
									jobs.push_back(j);
								else if(j->getType() == otherType)
									removeStrategy(j);
							}
						}
						if(jobs.empty())
						{
							lastPos = p;
							nextKernel = 0;
							finished = true;
							addJob(tile, type);
							addStrategyNode(1, p, type);
							break;
						}
						else
						{
							for(Job* j : jobs)
							{
								if(j->getType() == type && j->getCapacity() < 3)
								{
									if(!increasable)
									{
										increasable = j;
									}
									lastPos = p;
									nextKernel = std::min(j->getCapacity(), nextKernel);
									newJobBuckets[j->getCapacity()-1].push_back(j);
								}
							}
						}
					}
				}
			}
			
			if(finished)
			{
				// already handled
				break;
			}

			// if no place left to grow, increase capacity of first increasable job
			if(nextLevel.empty())
			{
				if(increasable)
				{
					increasable->setCapacity(increasable->getCapacity()+1);
					addStrategyNode(increasable);
				}
				break;
			}
			// if jobs of current level are in same kernel as all of their neighbours
			// increase one of them
			else if(nextKernel < 3 && nextKernel == currentKernel)
			{
				Job* j = jobBuckets[currentKernel-1].back();
				Ogre::Vector2 p = j->getPosition();
				j->setCapacity(j->getCapacity()+1);
				addStrategyNode(j);
				break;
			}

			std::swap(nextLevel, currentLevel);
			std::swap(jobBuckets, newJobBuckets);
			std::swap(nextKernel, currentKernel);
		}
	}
}
void PlayerClient::shrink(Job::JobType type, const Ogre::Vector2& position)
{
	Ogre::Vector2 tp;
	Tile::toTilePosition(Ogre::Vector2(position.x/2, position.y/2), tp);
	Ogre::Rect size = mWorld->getSize();
	std::vector<Job*> jobs;
	
	int start = integer(size.width()*((int)tp.y) + tp.x);

	Tile* t = mWorld->getTile(tp);
	if (!t)
		return;

	if (!t->isBlocking())
	{
		std::vector<Job*> allJobs = mJobs->get(tp);
		for(Job* j : allJobs)
			if(j->getType() == type)
				jobs.push_back(j);
	}
	
	int currentKernel = 0;
	int nextKernel = 0;
	if(jobs.empty())
	{
		return;
	}
	else
	{
		std::vector<Job*> jobBuckets[3];
		for(Job* j : jobs)
		{
			if(j->getType() == type)
			{
				currentKernel = j->getCapacity();
				jobBuckets[j->getCapacity()-1].push_back(j);
				break;
			}
		}

		std::vector<Ogre::Vector2> nextLevel;
		std::vector<Ogre::Vector2> currentLevel;
		currentLevel.push_back(tp);
		std::set<int> marks;
		marks.insert(start);
		for(;;)
		{
			std::vector<Job*> newJobBuckets[3];
			nextKernel = 0;
			while(!currentLevel.empty())
			{
				// calculate next kernel layer
				Ogre::Vector2 pos = currentLevel.back();
				currentLevel.pop_back();
				Ogre::Vector2 lastPos;
				for(Ogre::Vector2& delta : Tile::getNeighbours(pos))
				{
					Ogre::Vector2 p = pos + delta;
					int tile = integer(size.width()*((int)p.y) + p.x);
					if(marks.find(tile) != marks.end())
						continue;
					marks.insert(tile);
					Tile* t = mWorld->getTile(p);
					if (t && !t->isBlocking())
					{
						jobs = mJobs->get(p);
						nextLevel.push_back(p);
						if(!jobs.empty())
						{
							for(Job* j : jobs)
							{
								if(j->getType() == type)
								{
									lastPos = p;
									nextKernel = std::max(j->getCapacity(), nextKernel);
									newJobBuckets[j->getCapacity()-1].push_back(j);
									break;
								}
							}
						}
					}
				}
			}
			
			// if no place left to grow, decrease capacity of last job
			if(nextKernel == 0)
			{
				Job* j = jobBuckets[currentKernel-1].back();
				removeStrategyNode(j);
				j->setCapacity(j->getCapacity() - 1);
				if(j->getCapacity() == 0)
					removeJob(j);
				return;
			}

			std::swap(nextLevel, currentLevel);
			std::swap(jobBuckets, newJobBuckets);
			std::swap(nextKernel, currentKernel);
		}
	}
}
void PlayerClient::addStrategyNode(Job* j)
{
	addStrategyNode(j->getCapacity(), j->getPosition(), j->getType());
}
void PlayerClient::addStrategyNode(int capacity, const Ogre::Vector2& position, Job::JobType type)
{
	Ogre::Rect size = mWorld->getSize();
	if (position.x < 0 || position.x >= size.width()
		|| position.y < 0 || position.y >= size.height())
		return;

	int tile = integer(size.width()*((int)position.y) + position.x);

	Ogre::String name = (type == Job::HOLD_JOB ? Ogre::String("hold") : Ogre::String("defend"));
	name += Ogre::StringConverter::toString(capacity);
	name += Ogre::String("p");
	name += Ogre::StringConverter::toString(position);

	Ogre::Entity* ent = mSceneManager->createEntity(name, mStrategyMeshName);
	ent->setMaterialName((type == Job::HOLD_JOB ? mAttackStrategyMaterialName : mDefendStrategyMaterialName));
	ent->setVisibilityFlags(OVERLAY_VISIBILITY_FLAG);
	Ogre::SceneNode* n = mOverlayRootNode->createChildSceneNode();
	n->attachObject(ent);
	Ogre::Vector2 hp;
	Tile::toHexagonalPosition(position, hp);
	n->setPosition(Ogre::Vector3(2.0f * hp.x, 2.0f * hp.y, 0.1f + (float)capacity/20.f));
}
void PlayerClient::removeStrategyNode(Job* j)
{
	removeStrategyNode(j->getCapacity(), j->getPosition(), j->getType());
}
void PlayerClient::removeStrategyNode(int capacity, const Ogre::Vector2& position, Job::JobType type)
{
	Ogre::Rect size = mWorld->getSize();
	if (position.x < 0 || position.x >= size.width()
		|| position.y < 0 || position.y >= size.height())
		return;

	Ogre::String name = (type == Job::HOLD_JOB ? Ogre::String("hold") : Ogre::String("defend"));
	name += Ogre::StringConverter::toString(capacity);
	name += Ogre::String("p");
	name += Ogre::StringConverter::toString(position);

	Ogre::Entity* e = mSceneManager->getEntity(name);
	Ogre::SceneNode* n = e->getParentSceneNode();
	if(!n)
		return;

	destroyAllAttachedMovableObjects(n);
	n->removeAndDestroyAllChildren();
	mSceneManager->destroySceneNode(n);
}
void PlayerClient::removeStrategy(Job* j)
{
	while(j->getCapacity() > 0)
	{
		removeStrategyNode(j);
		j->setCapacity(j->getCapacity() -1);
	}
	removeJob(j);
}

Ogre::Rect PlayerClient::getViewRect() const
{
	return mViewRect;
}
void PlayerClient::setViewRect(const Ogre::Rect& rect)
{
	mViewRect = rect;
}

void PlayerClient::createThroneChamber(const Ogre::Vector2& position,
						const std::vector<Ogre::Vector2>& throneCircle)
{
	int roomID = mRooms->assignRoom(Room::THRONE_CHAMBER, throneCircle);

	std::vector<PositionTilePair> positionedTiles;
	mRooms->buildRoom(roomID, position, positionedTiles);

	mWorld->setTiles(positionedTiles, true);

	Ogre::Rect size = mWorld->getSize();
	std::set<int> fixedTile;
	std::vector<PositionTilePair> fixes;
	for (PositionTilePair pair : positionedTiles)
	{
		Ogre::Vector2 p = pair.first;
		for (Ogre::Vector2 d : Tile::getNeighbours(p))
		{
			Ogre::Vector2 position = p + d;
			if (position.x < 0 || position.x >= size.width()
				|| position.y < 0 || position.y >= size.height())
				continue;

			int tile = integer(size.width()*((int)position.y) + position.x);
			Tile* t = mWorld->getTile(position);
			if (t)
			{
				Tile::TileType tp = t->getType();
				if ((tp == Tile::ROUGH || tp == Tile::MUD || tp == Tile::WATER)
					&& fixedTile.find(tile) == fixedTile.end())
				{
					fixedTile.insert(tile);
					fixes.push_back(std::make_pair(position, Tile::msTiles[Tile::DIRT]));
				}
			}
		}
	}
	mWorld->setTiles(fixes, true);

}
void PlayerClient::createExit(const Ogre::Vector2& exitPosition,
						const std::vector<Ogre::Vector2>& triangle)
{
	int roomID = mRooms->assignExitRoom(triangle);

	std::vector<PositionTilePair> positionedTiles;
	mRooms->buildRoom(roomID, exitPosition, positionedTiles);
	mWorld->setTiles(positionedTiles, true);

}
void PlayerClient::connect(const Ogre::Vector2& thronePosition,
					 const Ogre::Vector2& exitPosition,
					 std::vector<Ogre::Vector2>& path)
{
	Tile::hexLine(thronePosition, exitPosition, path);

	std::vector<PositionTilePair> positionedTiles;
	for(Ogre::Vector2 p : path)
		positionedTiles.push_back(std::make_pair(p, Tile::msTiles[Tile::FLOOR]));
	positionedTiles.pop_back();
	mWorld->setTiles(positionedTiles, true);

	Ogre::Rect size = mWorld->getSize();
	std::set<int> fixedTile;
	std::vector<PositionTilePair> fixes;
	for (Ogre::Vector2 p : path)
	{
		for (Ogre::Vector2 d : Tile::getNeighbours(p))
		{
			Ogre::Vector2 position = p + d;
			if (position.x < 0 || position.x >= size.width()
				|| position.y < 0 || position.y >= size.height())
				continue;

			int tile = integer(size.width()*((int)position.y) + position.x);
			Tile::TileType t = mWorld->getTile(position)->getType();
			if ((t == Tile::ROUGH || t == Tile::MUD || t == Tile::WATER)
			&& fixedTile.find(tile) == fixedTile.end())
			{
				fixedTile.insert(tile);
				fixes.push_back(std::make_pair(position, Tile::msTiles[Tile::DIRT]));
			}
		}
	}
	mWorld->setTiles(fixes, true);

}
void PlayerClient::createFlattenJob(const Ogre::Vector2& pos)
{
	Tile* t = mWorld->getTile(pos);

	if (t && (t->getType() == Tile::ROUGH || t->getType() == Tile::MUD))
	{
		if(mJobs->get(pos).empty())
		{
			mJobs->add(Job::FLATTEN_JOB, pos);
		}
	}
}
void PlayerClient::createFlattenJobEnvironment(const Ogre::Vector2& pos)
{
	for(Ogre::Vector2 d : Tile::getNeighbours(pos))
		createFlattenJob(pos + d);
}
void PlayerClient::createFlattenJobsEnvironment(const std::vector<Ogre::Vector2>& positions)
{
	for(Ogre::Vector2 pos : positions)
		createFlattenJobEnvironment(pos);
}

Job* PlayerClient::addJob(int tile, Job::JobType type)
{
	// find ents and path for dig command
	Ogre::Rect size = mWorld->getSize();
	Ogre::Vector2 position(real(tile % size.width()), real(tile / size.width()));
	Ogre::Vector2 targetPosition;
	std::vector<Entity::EntityType> allowedTypes;
	allowedTypes.push_back(Entity::ET_SMALL_WORKER);
	allowedTypes.push_back(Entity::ET_WORKER);
	allowedTypes.push_back(Entity::ET_CARRIER);
	int flags = mEntityQuery->getEntityFlag(allowedTypes);
	Entity* targetEntity = 0;
	mEntityQuery->findNextIdleEntityForJob(position, targetPosition,
		targetEntity, type, flags, mID, 50);

	int id = mJobs->add(type, position);
	Job* job = (Job*)mJobs->get(id);
	if(targetEntity && targetEntity->isAnt())
	{
		Ant* ant = (Ant*)targetEntity;
		ant->enqueueJob(job);
	}
	return job;
}
void PlayerClient::removeJob(Job* job)
{
	mJobs->remove(job->getID());
}

void PlayerClient::addUpgrade(Entity::Upgrade u)
{
	mUpgrades.push_back(u);
	mEntities->applyUpgrade(mID, u);
	mJobs->applyUpgrade(u);
}
void PlayerClient::getAvailableUpgrade(std::vector<bool>& u)
{
	std::vector<bool> upgrades(Entity::U_COUNT, false);
	if(getAvailableUpgradePoint() > 0)
	{
		upgrades[Entity::U_HORNS] = true;
		upgrades[Entity::U_MOVEMENT_SPEED] = true;
		upgrades[Entity::U_HARVEST_SPEED] = true;
		if(mUpgradeLevel > 0)
		{
			for (Entity::Upgrade up : mUpgrades)
			{
				switch(up)
				{
				case Entity::U_HORNS:
					upgrades[Entity::U_ACID] = true;
					upgrades[Entity::U_CARRIER] = true;
					break;
				case Entity::U_MOVEMENT_SPEED:
					upgrades[Entity::U_ACID] = true;
					upgrades[Entity::U_CARRIER] = true;
					break;
				case Entity::U_HARVEST_SPEED:
					upgrades[Entity::U_ACID] = true;
					upgrades[Entity::U_CARRIER] = true;
					upgrades[Entity::U_ANTIBIOTICS] = true;
					break;
				}
			}
		}
		if(mUpgradeLevel >= 1)
		{
			for (Entity::Upgrade up : mUpgrades)
			{
				switch(up)
				{
				case Entity::U_ACID:
					upgrades[Entity::U_HONEYDEW] = true;
					break;
				}
			}
		}
		if(mUpgradeLevel >= 2)
		{
			for (Entity::Upgrade up : mUpgrades)
			{
				switch(up)
				{
				case Entity::U_ANTIBIOTICS:
					upgrades[Entity::U_KAMIKAZE] = true;
					break;
				case Entity::U_KAMIKAZE:
					upgrades[Entity::U_FUNGI] = true;
					break;
				}
			}
		}
		if(mUpgradeLevel >= 3)
		{
			for (Entity::Upgrade up : mUpgrades)
			{
				switch(up)
				{
				case Entity::U_CARRIER:
					upgrades[Entity::U_BULLDOGS] = true;
					upgrades[Entity::U_FUNGI] = true;
					break;
				}
			}
		}
		u = upgrades;
	}
	else
	{
		getUpgrades(u);
	}
}
int PlayerClient::getAvailableUpgradePoint() const
{
	return mUpgradeLevel - mUpgrades.size();
}
void PlayerClient::setUpgradeLevel(int level)
{
	mUpgradeLevel = level;
}
void PlayerClient::getUpgrades(std::vector<bool>& u)
{
	std::vector<bool> upgrades(Entity::U_COUNT, false);
	for (Entity::Upgrade up : mUpgrades)
	{
		upgrades[up] = true;
	}
	u = upgrades;
}
int PlayerClient::getUnitsNeededForUpgrade() const
{
	int score = mStatistics->getScore();

	int result;
	if (score > 2500)
	{
		result = -1;
	}
	else if (score > 1000)
	{
		result = 2500 - score;
	}
	else if (score > 400)
	{
		result = 1000 - score;
	}
	else if (score > 150)
	{
		result = 400 - score;
	}
	else
	{
		result = 150 - score;
	}
	return result;
}
void PlayerClient::updateAchievements()
{
	std::vector<bool> upgrades;
	getUpgrades(upgrades);
	Framework::getSingleton().mAchievements->reportStatistics(
		mStatistics, Framework::getSingleton().mAllTimeStats, upgrades, mMessages);
}

// PUBLIC INTERFACES
void PlayerClient::update(float dt)
{
	mStatistics->TimePassed += dt;
	int score = mStatistics->getScore();
	if (mUpgradeLevel == 0 && score > 150)
	{
		mUpgradeLevel = 1;
	}
	else if (mUpgradeLevel == 1 && score > 400)
	{
		mUpgradeLevel = 2;
	}
	else if (mUpgradeLevel == 2 && score > 1000)
	{
		mUpgradeLevel = 3;
	}
	else if (mUpgradeLevel == 3 && score > 2500)
	{
		mUpgradeLevel = 4;
	}

	mRooms->update(dt, this);
	updateWorkerAssignments(Room::BROOD_CHAMBER);
	updateWorkerAssignments(Room::FUNGI_CHAMBER);
	updateWorkerAssignments(Room::STORAGE_CHAMBER);

	mEntities->update(dt, mID, this);

	Ogre::Vector2 lt;
	Ogre::Vector2 rb;
	Tile::toTilePosition(Ogre::Vector2(real(mViewRect.left / 2), real(mViewRect.top / 2)), lt);
	Tile::toTilePosition(Ogre::Vector2(real(mViewRect.right / 2), real(mViewRect.bottom / 2)), rb);
	((DrawableWorldMap*)mWorld)->setViewRect(Ogre::Rect(integer(lt.x), integer(lt.y),
		integer(rb.x), integer(rb.y)));
	((DrawableWorldMap*)mWorld)->updatePages();
}
void PlayerClient::settle(std::vector<Ogre::Vector2>& locations)
{
	Ogre::Vector2 tp;
	Ogre::Vector2 ep;
	std::vector<Ogre::Vector2> exitTriangle;
	std::vector<Ogre::Vector2> throneCircle;
	std::vector<PositionTilePair> environment;
	mServer->settleClient(mID, environment, tp, throneCircle, ep, exitTriangle);
	mWorld->setTiles(environment, false);
	{ // create rooms for discovered resources
		std::set<int> roomIDs;
		for (PositionTilePair pair : environment)
		{
			int roomID = -1;
			switch (pair.second->getType())
			{
			case Tile::GRASS:
				roomID = mRooms->assignRoom(Room::FLORAL_SOURCE, pair.first);
				break;
			case Tile::COPAL:
				roomID = mRooms->assignRoom(Room::COPAL_SOURCE, pair.first);
				break;
			}
			if (roomID >= 0 && roomIDs.find(roomID) == roomIDs.end()) roomIDs.insert(roomID);
		}

		for (int id : roomIDs)
		{
			std::vector<PositionTilePair> resource;
			mRooms->buildRoom(id, Ogre::Vector2::ZERO, resource);
		}
	}


	// connection of rooms must be before room creation for now,
	// there should be a way to determine exact room boundaries for
	// connections. now i just exploit the order of tile updates (overwriting)
	std::vector<Ogre::Vector2> path;
	connect(tp, ep, path);
	createThroneChamber(tp, throneCircle);
	createExit(ep, exitTriangle);

	std::vector<Entity*> ents;
	Ogre::Vector2 p;
	Tile::toHexagonalPosition(Ogre::Vector2(tp.x + 2, tp.y - 2), p);
	ents.push_back(new0 Worker(mServer->createEntityID(), mID,
		p, Ogre::Vector2(tp.x + 2, tp.y - 2),
		7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap));
	Tile::toHexagonalPosition(Ogre::Vector2(tp.x + 2, tp.y + 2), p);
	ents.push_back(new0 Worker(mServer->createEntityID(), mID,
		p, Ogre::Vector2(tp.x + 2, tp.y + 2),
		7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap));
	Tile::toHexagonalPosition(Ogre::Vector2(tp.x - 2, tp.y + 2), p);
	ents.push_back(new0 Worker(mServer->createEntityID(), mID,
		p, Ogre::Vector2(tp.x + 2, tp.y + 2),
		7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap));

	mStatistics->Worker.Alive += 3;

	mEntities->add(ents, true);
	for (Entity* ent : ents)
		for (Entity::Upgrade u : mUpgrades)
			ent->applyUpgrade(u);

	sendUpdates();
	
	createFlattenJobsEnvironment(throneCircle);
	createFlattenJobsEnvironment(exitTriangle);
	createFlattenJobsEnvironment(path);
	
	locations.push_back(tp);
}

void PlayerClient::jobMilestoneReached(Job* job)
{
	switch (job->getType())
	{
		case Job::ROOM_CREATE_JOB:
		{
			RoomCreateJob* j = (RoomCreateJob*)job;
			Ogre::Vector2 targetPosition = job->getPosition();

			Room::RoomType t = (Room::RoomType)j->getRoomType();
			assignRoom(t, targetPosition);

			// this is expensive as fuck (no static geometry instead?)
			Ogre::Rect size = mWorld->getSize();
			int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
			mAssignedTiles.erase(tile);
			rebuildAssignment();
			break;
		}
		case Job::ROOM_CREATE_JOB_EX:
		{
			RoomCreateJobEx* j = (RoomCreateJobEx*)job;
			Ogre::Vector2 targetPosition = job->getPosition();

			Room::RoomType t = (Room::RoomType)j->getRoomType();
			assignRoom(t, targetPosition);

			// this is expensive as fuck (no static geometry instead?)
			Ogre::Rect size = mWorld->getSize();
			int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
			mAssignedTiles.erase(tile);
			rebuildAssignment();
			break;
		}
	}
}
void PlayerClient::jobFinished(Job* job)
{
	switch(job->getType())
	{
	case Job::ROOM_CREATE_JOB:
		{
			RoomCreateJob* j = (RoomCreateJob*)job;
			Ogre::Vector2 targetPosition = j->getPosition();

			Ogre::Rect size = mWorld->getSize();
			int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
			removeJobs(tile, Job::ROOM_CREATE_JOB);
		}
		break;
	case Job::ROOM_CREATE_JOB_EX:
	{
		RoomCreateJobEx* j = (RoomCreateJobEx*)job;
		Ogre::Vector2 targetPosition = j->getPosition();

		Ogre::Rect size = mWorld->getSize();
		int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
		removeJobs(tile, Job::ROOM_CREATE_JOB_EX);
	}
		break;
	case Job::DIG_JOB:
		{
			DigJob* j = (DigJob*)job;
			Ogre::Vector2 targetPosition = j->getPosition();
			mWorld->setTile(targetPosition, Tile::msTiles[Tile::ROUGH], true);

			for(Ogre::Vector2 d : Tile::getNeighbours(targetPosition))
			{
				Ogre::Vector2 p = targetPosition + d;
				Tile* t = mWorld->getTile(p);
				if(t && t->isOverWorld())
				{
					std::vector<Ogre::Vector2> exitTriangle;
					mServer->createExit(p, exitTriangle);
					createExit(p, exitTriangle);
				}
			}

			Ogre::Rect size = mWorld->getSize();
			int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
			removeJobs(tile, Job::DIG_JOB);

			bool createFlatten = false;
			for(Ogre::Vector2 d : Tile::getNeighbours(targetPosition))
			{
				Ogre::Vector2 p = targetPosition + d;
				Tile* t = mWorld->getTile(p);
				if(t && t->isAnthill())
				{
					createFlatten = true;
					break;
				}
			}
			if(createFlatten) createFlattenJob(targetPosition);

			
			// this is expensive as fuck (no static geometry instead?)
			mSelectedTiles.erase(tile);
			rebuildSelection();
		}
		break;
	case Job::FLATTEN_JOB:
		{
			FlattenJob* j = (FlattenJob*)job;
			Ogre::Vector2 targetPosition = j->getPosition();
			mWorld->setTile(targetPosition, Tile::msTiles[Tile::FLOOR], true);

			// this is expensive as fuck (no static geometry instead?)
			Ogre::Rect size = mWorld->getSize();
			int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
			removeJobs(tile, Job::FLATTEN_JOB);

			// create debris
			Ogre::Vector2 hex;
			Tile::toHexagonalPosition(targetPosition, hex);
			Entity* ent = new0 Debris(mServer->createEntityID(), mID, hex, targetPosition, 0);
			mEntities->add(ent, true);
			for (Entity::Upgrade u : mUpgrades)
				ent->applyUpgrade(u);

			Room* r = mRoomQuery->findNearestRoom(Room::EXIT_ROOM, targetPosition);
			if(r)
			{
				int jobID = mJobs->add(Job::FETCH_JOB, targetPosition);
				FetchJob* job = (FetchJob*)mJobs->get(jobID);
				job->setFetchEntity(ent);
				job->setTargetRoom(r);
			}

			createFlattenJobEnvironment(targetPosition);
		}
		break;
	case Job::GOTO_JOB:
		removeJob(job);
		break;

	case Job::DIG_RESOURCE_JOB:
		{
			DigResourceJob* j = (DigResourceJob*)job;
			Ogre::Vector2 targetPosition = j->getPosition();
			
			std::vector<Entity*> ents;

			int roomID = j->getResourceID();
			Room* r = mRooms->getRoom(roomID);
			if (r)
			{
				if (r->getType() == Room::FLORAL_SOURCE)
				{
					{
						Ogre::Vector2 hex;
						Tile::toHexagonalPosition(targetPosition, hex);
						Entity* ent = new0 FloralFood(mServer->createEntityID(),
							mID, hex, targetPosition, 0);
						mEntities->add(ent, true);
						for (Entity::Upgrade u : mUpgrades)
							ent->applyUpgrade(u);
						ents.push_back(ent);
					}
					mWorld->setTile(targetPosition, Tile::msTiles[Tile::HARVESTED_GRASS], true);
				}
				else if (r->getType() == Room::COPAL_SOURCE)
				{
					{
						Ogre::Vector2 hex;
						Tile::toHexagonalPosition(targetPosition, hex);
						Entity* ent = new0 Copal(mServer->createEntityID(),
							mID, hex, targetPosition, 0);
						mEntities->add(ent, true);
						for (Entity::Upgrade u : mUpgrades)
							ent->applyUpgrade(u);
						ents.push_back(ent);
					}
					mWorld->setTile(targetPosition, Tile::msTiles[Tile::TREE], true);
				}
				else
				{
					{
						Ogre::Vector2 hex;
						Tile::toHexagonalPosition(targetPosition, hex);
						Entity* ent = new0 FaunalFood(mServer->createEntityID(),
							mID, hex, targetPosition, 0);
						mEntities->add(ent, true);
						for (Entity::Upgrade u : mUpgrades)
							ent->applyUpgrade(u);
						ents.push_back(ent);
					}

					FaunalSource* source = (FaunalSource*)r;

					// delete harvested entity
					Character* resource = source->getSource();
					mEntities->remove(resource, true);

					source->setSource(0);
				}
				r->placeManual(ents, targetPosition);
			}


			Ogre::Rect size = mWorld->getSize();
			int tile = integer(size.width()*((int)targetPosition.y) + targetPosition.x);
			removeJobs(tile, Job::DIG_RESOURCE_JOB);
			mSelectedTiles.erase(tile);
			rebuildSelection();

		}
		break;
	}
}
void PlayerClient::setExitTiles(const std::vector<std::pair<Ogre::Vector2, Tile*>>& tiles)
{
	std::vector<PositionTilePair> border;
	for(PositionTilePair p : tiles)
	{
		if(p.second->isBlocking())
		{
			for(Ogre::Vector2 d : Tile::getNeighbours(p.first))
			{
				Ogre::Vector2 pos = p.first + d;
				Tile* neighbour = mWorld->getTile(pos);
				if(neighbour && neighbour->isOverWorld() && neighbour->isBlocking())
				{
					border.push_back(std::make_pair(pos, Tile::msTiles[Tile::GROUND]));
				}
			}

			// beam entities out of tight spot
			std::vector<Entity*> ents = mEntities->get(p.first);
			if(!ents.empty())
			{
				Ogre::Vector2 beamTo = p.first;
				for(Ogre::Vector2 delta : Tile::getNeighbours(p.first))
				{
					beamTo = delta + p.first;
					if(!mWorld->isBlocking(beamTo))
						break;
				}
				for(Entity* e : ents)
				{
					e->setTilePosition(beamTo);
					Ogre::Vector2 hex;
					Tile::toHexagonalPosition(beamTo, hex);
					e->setPosition(hex);
					mEntities->change(e, true);
				}
			}

			// kill all jobs residing there
			for(Job* j : mJobs->get(p.first))
			{
				if (j->getType() == Job::HOLD_JOB || j->getType() == Job::DEFEND_JOB)
					removeStrategy(j);
				else
					removeJob(j);
			}
		}
	}
	mWorld->setTiles(border, true);
	mWorld->setTiles(tiles, true);
}
void PlayerClient::setTile(const Ogre::Vector2& position, int type)
{
	mWorld->setTile(position, Tile::msTiles[type], true);
}
void PlayerClient::createResource(const Ogre::Vector2& position, int roomType,
	Character* resource)
{
	int id = mRooms->assignRoom((Room::RoomType)roomType, position);

	if(id >= 0)
	{
		std::vector<PositionTilePair> positionedTiles;
		mRooms->buildRoom(id, position, positionedTiles);
		mWorld->setTiles(positionedTiles, true);

		if (resource && roomType == Room::FAUNAL_SOURCE)
		{
			FaunalSource* source = (FaunalSource*)mRooms->getRoom(id);
			source->setSource(resource);
		}
	}
}
void PlayerClient::removeResource(Room* r)
{
	mRooms->remove(r->getID());
}
Egg* PlayerClient::spawnEgg(const Ogre::Vector2& position)
{
	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(position, hex);
	Egg* egg = new0 Egg(mServer->createEntityID(), mID, hex, position, 5);
	mEntities->add(egg, true);
	for (Entity::Upgrade u : mUpgrades)
		egg->applyUpgrade(u);
	return egg;
}
Grub* PlayerClient::hatchEgg(Egg* egg)
{
	if(egg->getParent() != 0)
		((Ant*)egg->getParent())->drop(egg);
	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(egg->getTilePosition(), hex);
	Grub* grub = new0 Grub(mServer->createEntityID(), mID,
		hex, egg->getTilePosition(),
		5, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
	mEntities->add(grub, true);
	for (Entity::Upgrade u : mUpgrades)
		grub->applyUpgrade(u);
	mEntities->remove(egg, true);
	return grub;
}
void PlayerClient::unitDied(Character* unit, Attack* a)
{
	switch (unit->getType())
	{
	case Entity::ET_SMALL_WORKER:
		mStatistics->SmallWorker.Alive--;
		break;
	case Entity::ET_WORKER:
		mStatistics->Worker.Alive--;
		break;
	case Entity::ET_CARRIER:
		mStatistics->Carrier.Alive--;
		break;
	case Entity::ET_SOLDIER:
		mStatistics->Soldier.Alive--;
		break;
	case Entity::ET_KAMIKAZE:
		mStatistics->Kamikaze.Alive--;
		break;
	}

	if (a)
	{
	}
}
Ant* PlayerClient::spawnAnt(Grub* grub, int type)
{
	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(grub->getTilePosition(), hex);

	Ant* ant = 0;
	switch((Entity::EntityType)type)
	{
	case Entity::ET_SMALL_WORKER:
		ant = new0 SmallWorker(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->SmallWorker.Produced++;
		mStatistics->SmallWorker.Alive++;
		break;
	case Entity::ET_WORKER:
		ant = new0 Worker(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->Worker.Produced++;
		mStatistics->Worker.Alive++;
		break;
	case Entity::ET_CARRIER:
		ant = new0 Carrier(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->Carrier.Produced++;
		mStatistics->Carrier.Alive++;
		break;
	case Entity::ET_SOLDIER:
		ant = new0 Soldier(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->Soldier.Produced++;
		mStatistics->Soldier.Alive++;
		break;
	case Entity::ET_KAMIKAZE:
		ant = new0 Kamikaze(mServer->createEntityID(), mID,
			hex, grub->getTilePosition(),
			7, mWorldQuery, mJobQuery, mEntityQuery, mRoomQuery, mPheromoneMap);
		mStatistics->Kamikaze.Produced++;
		mStatistics->Kamikaze.Alive++;
		break;
	}
	mEntities->remove(grub, true);
	mEntities->add(ant, true);
	for (Entity::Upgrade u : mUpgrades)
		ant->applyUpgrade(u);
	return ant;
}
Entity* PlayerClient::spawn(const Ogre::Vector2& position, int type)
{
	if (mWorld->isBlocking(position))
		return 0;

	Ogre::Vector2 hex;
	Tile::toHexagonalPosition(position, hex);
	Entity* ent = 0;
	switch((Entity::EntityType)type)
	{
	case Entity::ET_DEBRIS:
		ent = new0 Debris(mServer->createEntityID(), mID, hex, position, 0);
		break;
	case Entity::ET_FAUNAL_FOOD:
		ent = new0 FaunalFood(mServer->createEntityID(), mID, hex, position, 0);
		break;
	}
	mEntities->add(ent, true);
	for (Entity::Upgrade u : mUpgrades)
		ent->applyUpgrade(u);
	return ent;
}
void PlayerClient::removeEntities(const std::vector<Entity*>& ents)
{
	mEntities->remove(ents, true);
}
void PlayerClient::aoeAttack(int sourceID, float attackPoints, const Ogre::Vector2& position, float radius)
{
	mAOEs.push_back(new0 AOE(sourceID, mID, attackPoints, position, radius));
}
void PlayerClient::meleeAttack(float attackPoints, Character* source, Entity* target)
{
	mAttacks.push_back(new0 Attack(attackPoints, source->getID(), target->getID(),
		source->getClientID(), target->getClientID()));
}
void PlayerClient::rangedAttack(float attackPoints, Character* source, Entity* target)
{
	Projectile* proj = new0 Projectile(mServer->createEntityID(), attackPoints,
		source, target->getPosition(), target->getTilePosition());
	mEntities->add(proj, true);
	for (Entity::Upgrade u : mUpgrades)
		proj->applyUpgrade(u);
}
void PlayerClient::setFOW(bool fow)
{
	mFOW = fow;
}

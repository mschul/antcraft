#include "AnPCH.h"
#include "AnProceduralGeometry.h"
using namespace Ants;

#include <OgreManualObject.h>
#include <OgreMeshManager.h>


template<> ProceduralGeometry* Ogre::Singleton<ProceduralGeometry>::msSingleton = 0;

ProceduralGeometry::ProceduralGeometry(Ogre::String standardMaterial)
{
	mStandardMaterial = standardMaterial;
}
ProceduralGeometry::~ProceduralGeometry()
{
}

Ogre::Vector3 getNormal(const Ogre::Vector3& v0,
						const Ogre::Vector3& v1,
						const Ogre::Vector3& v2)
{
	Ogre::Vector3 v10 = v1 - v0;
	Ogre::Vector3 v20 = v2 - v0;
	Ogre::Vector3 n = v10.crossProduct(v20);
	n.normalise();
	return n;
}
void createQuad(Ogre::ManualObject* obj, int j,
				const Ogre::Vector3& v0, const Ogre::Vector3& v1,
				const Ogre::Vector3& v2, const Ogre::Vector3& v3)
{
	Ogre::Vector3 n = getNormal(v0, v1, v2);
	obj->position(v0);	obj->normal(n); obj->textureCoord(0,0);
	obj->position(v1);	obj->normal(n); obj->textureCoord(1,0);
	obj->position(v2);	obj->normal(n); obj->textureCoord(0,1);
	obj->triangle(j,j+1,j+2);
	obj->position(v3);	obj->normal(n); obj->textureCoord(1,1);
	obj->triangle(j+2,j+1,j+3);
}
void createTriangle(Ogre::ManualObject* obj, int j,
				const Ogre::Vector3& v0, const Ogre::Vector3& v1,
				const Ogre::Vector3& v2)
{
	Ogre::Vector3 n = getNormal(v0, v1, v2);
	obj->position(v0);	obj->normal(n); obj->textureCoord(0,0);
	obj->position(v1);	obj->normal(n); obj->textureCoord(1,0);
	obj->position(v2);	obj->normal(n); obj->textureCoord(0,1);
	obj->triangle(j,j+1,j+2);
}
Ogre::String ProceduralGeometry::getPrimitive(const Ogre::String& name)
{
	Ogre::String mesh = name + "Mesh";

	if(!Ogre::MeshManager::getSingleton().resourceExists(mesh))
	{
		if(Ogre::StringUtil::match(name, "cube", false))
		{
			Ogre::ManualObject* obj = new Ogre::ManualObject(name);
			obj->begin(mStandardMaterial);

			// LEFT SIDE
			int j = 0;
			createQuad(obj, j,
				Ogre::Vector3(-1.0,-1.0,-1.0), Ogre::Vector3(-1.0,-1.0, 1.0),
				Ogre::Vector3(-1.0, 1.0,-1.0), Ogre::Vector3(-1.0, 1.0, 1.0));

			// DOWN SIDE
			j+= 4;
			createQuad(obj, j,
				Ogre::Vector3(-1.0,-1.0, 1.0), Ogre::Vector3(-1.0,-1.0,-1.0),
				Ogre::Vector3( 1.0,-1.0, 1.0), Ogre::Vector3( 1.0,-1.0,-1.0));
		
			// BACK SIDE
			j+= 4;
			createQuad(obj, j,
				Ogre::Vector3(-1.0,-1.0,-1.0), Ogre::Vector3(-1.0, 1.0,-1.0),
				Ogre::Vector3( 1.0,-1.0,-1.0), Ogre::Vector3( 1.0, 1.0,-1.0));
		
			// FRONT SIDE
			j+= 4;
			createQuad(obj, j,
				Ogre::Vector3( 1.0,-1.0, 1.0), Ogre::Vector3( 1.0, 1.0, 1.0),
				Ogre::Vector3(-1.0,-1.0, 1.0), Ogre::Vector3(-1.0, 1.0, 1.0));
		
			// UP SIDE
			j+= 4;
			createQuad(obj, j,
				Ogre::Vector3(-1.0, 1.0, 1.0), Ogre::Vector3( 1.0, 1.0, 1.0),
				Ogre::Vector3(-1.0, 1.0,-1.0), Ogre::Vector3( 1.0, 1.0,-1.0));
		
			// RIGHT SIDE
			j+= 4;
			createQuad(obj, j,
				Ogre::Vector3( 1.0,-1.0,-1.0), Ogre::Vector3( 1.0, 1.0,-1.0),
				Ogre::Vector3( 1.0,-1.0, 1.0), Ogre::Vector3( 1.0, 1.0, 1.0));

			obj->end(); 
			obj->convertToMesh(mesh);
		}
		else if(Ogre::StringUtil::match(name, "hexagonCylinder", false))
		{
			Ogre::ManualObject* obj = new Ogre::ManualObject(name);
			obj->begin(mStandardMaterial);

			const float t = 0.86602540378f;
			
			Ogre::Vector3 fc(0,   0,  1.f);
			Ogre::Vector3 f1(0,   -1, 1.f);
			Ogre::Vector3 f2(t, -.5f, 1.f);
			Ogre::Vector3 f3(t,  .5f, 1.f);
			Ogre::Vector3 f4( 0,   1, 1.f);
			Ogre::Vector3 f5(-t, .5f, 1.f);
			Ogre::Vector3 f6(-t,-.5f, 1.f);
			
			Ogre::Vector3 bc(0,   0,  -1.f);
			Ogre::Vector3 b1(0,   -1, -1.f);
			Ogre::Vector3 b2(t, -.5f, -1.f);
			Ogre::Vector3 b3(t,  .5f, -1.f);
			Ogre::Vector3 b4( 0,   1, -1.f);
			Ogre::Vector3 b5(-t, .5f, -1.f);
			Ogre::Vector3 b6(-t,-.5f, -1.f);

			// BACK SIDE
			int j = 0;

			// FRONT SIDE
			createTriangle(obj, j, fc, f1, f2);  j+= 3;
			createTriangle(obj, j, fc, f2, f3);  j+= 3;
			createTriangle(obj, j, fc, f3, f4);  j+= 3;
			createTriangle(obj, j, fc, f4, f5);  j+= 3;
			createTriangle(obj, j, fc, f5, f6);  j+= 3;
			createTriangle(obj, j, fc, f6, f1);  j+= 3;

			// BORDER
			createQuad(obj, j, f1, b1, f2, b2); j+= 4;
			createQuad(obj, j, f2, b2, f3, b3); j+= 4;
			createQuad(obj, j, f3, b3, f4, b4); j+= 4;
			createQuad(obj, j, f4, b4, f5, b5); j+= 4;
			createQuad(obj, j, f5, b5, f6, b6); j+= 4;
			createQuad(obj, j, f6, b6, f1, b1); j+= 4;


			obj->end(); 
			obj->convertToMesh(mesh);
		}
		else if(Ogre::StringUtil::match(name, "hexagon", false))
		{
			Ogre::ManualObject* obj = new Ogre::ManualObject(name);
			obj->begin(mStandardMaterial);

			const float t = 0.86602540378f;
			
			Ogre::Vector3 fc(0,   0,  0.f);
			Ogre::Vector3 f1(0,   -1, 0.f);
			Ogre::Vector3 f2(t, -.5f, 0.f);
			Ogre::Vector3 f3(t,  .5f, 0.f);
			Ogre::Vector3 f4( 0,   1, 0.f);
			Ogre::Vector3 f5(-t, .5f, 0.f);
			Ogre::Vector3 f6(-t,-.5f, 0.f);
			// BACK SIDE
			int j = 0;

			// FRONT SIDE
			createTriangle(obj, j, fc, f1, f2);  j+= 3;
			createTriangle(obj, j, fc, f2, f3);  j+= 3;
			createTriangle(obj, j, fc, f3, f4);  j+= 3;
			createTriangle(obj, j, fc, f4, f5);  j+= 3;
			createTriangle(obj, j, fc, f5, f6);  j+= 3;
			createTriangle(obj, j, fc, f6, f1);  j+= 3;


			obj->end(); 
			obj->convertToMesh(mesh);
		}
		else if(Ogre::StringUtil::match(name, "rect", false))
		{
			Ogre::ManualObject* obj =  new Ogre::ManualObject(name);
			Ogre::String materialName = name + "Material";
			// NOTE: The second parameter to the create method is the resource group the material will be added to.
			// If the group you name does not exist (in your resources.cfg file) the library will assert() and your program will crash
			Ogre::MaterialPtr mat = Ogre::MaterialManager::getSingleton().create(materialName, "General"); 
			mat->setReceiveShadows(false); 
			mat->getTechnique(0)->setLightingEnabled(true); 
			mat->getTechnique(0)->getPass(0)->setDiffuse(1,0,0,0); 
			mat->getTechnique(0)->getPass(0)->setAmbient(1,0,0); 
			mat->getTechnique(0)->getPass(0)->setSelfIllumination(1,0,0); 
			//mat->dispose();  // dispose pointer, not the material
 
			obj->begin(materialName, Ogre::RenderOperation::OT_LINE_LIST); 
			obj->position(-1.0f, -1.0f, 0.0f);  obj->position(-1.0f, 1.0f, 0.0f);
			obj->position(-1.0f, 1.0f, 0.0f);  obj->position(1.0f, 1.0f, 0.0f); 
			obj->position(1.0f, 1.0f, 0.0f);  obj->position(1.0f, -1.0f, 0.0f); 
			obj->position(1.0f, -1.0f, 0.0f);  obj->position(-1.0f, -1.0f, 0.0f); 

			obj->end(); 
			obj->convertToMesh(mesh);
		}
		else if(Ogre::StringUtil::match(name, "plane", false))
		{
			Ogre::MeshManager::getSingleton().createPlane(mesh, Ogre::ResourceGroupManager::DEFAULT_RESOURCE_GROUP_NAME,
				Ogre::Plane(0, 0, 1, 0), 2, 2, 1, 1);
		}
		else if(Ogre::StringUtil::match(name, "sphere", false))
		{
			const int nRings = 8;
			const int nSegments = 8;
			const int r = 1;

			Ogre::ManualObject* obj = new Ogre::ManualObject(name);
			obj->begin(mStandardMaterial, Ogre::RenderOperation::OT_TRIANGLE_LIST);
 
			float fDeltaRingAngle = (Ogre::Math::PI / nRings);
			float fDeltaSegAngle = (2 * Ogre::Math::PI / nSegments);
			unsigned short wVerticeIndex = 0 ;
 
			// Generate the group of rings for the sphere
			for( int ring = 0; ring <= nRings; ring++ )
			{
				float r0 = r * sinf (ring * fDeltaRingAngle);
				float y0 = r * cosf (ring * fDeltaRingAngle);
 
				// Generate the group of segments for the current ring
				for(int seg = 0; seg <= nSegments; seg++)
				{
					float x0 = r0 * sinf(seg * fDeltaSegAngle);
					float z0 = r0 * cosf(seg * fDeltaSegAngle);
 
					// Add one vertex to the strip which makes up the sphere
					obj->position( x0, y0, z0);
					obj->normal(Ogre::Vector3(x0, y0, z0).normalisedCopy());
					obj->textureCoord((float) seg / (float) nSegments, (float) ring / (float) nRings);
 
					if (ring != nRings)
					{
						// each vertex (except the last) has six indicies pointing to it
						obj->index(wVerticeIndex + nSegments + 1);
						obj->index(wVerticeIndex);               
						obj->index(wVerticeIndex + nSegments);
						obj->index(wVerticeIndex + nSegments + 1);
						obj->index(wVerticeIndex + 1);
						obj->index(wVerticeIndex);
						wVerticeIndex ++;
					}
				};
			}
			obj->end();
			Ogre::MeshPtr m = obj->convertToMesh(mesh);
			m->_setBounds(Ogre::AxisAlignedBox(Ogre::Vector3(-r, -r, -r), Ogre::Vector3(r, r, r) ), false);
 
			m->_setBoundingSphereRadius(r);
			unsigned short src, dest;
			if (!m->suggestTangentVectorBuildParams(Ogre::VES_TANGENT, src, dest))
			{
				m->buildTangentVectors(Ogre::VES_TANGENT, src, dest);
			}
		}
	}

	return mesh;
}
Ogre::String ProceduralGeometry::getPlaneStrip(const Ogre::String& name,
											   const std::vector<Ogre::Vector2>& vertices)
{
	Ogre::String mesh = name + "Mesh";

	if(!Ogre::MeshManager::getSingleton().resourceExists(mesh))
	{
		Ogre::ManualObject* obj = new Ogre::ManualObject(name);
		obj->begin(mStandardMaterial);

		for(int i = 0, j = 0; i < (int)vertices.size()-1; ++i, j += 4)
		{
			Ogre::Vector2 v0 = vertices[i];
			Ogre::Vector2 v1 = vertices[i+1];
			
			createQuad(obj, j,
				Ogre::Vector3(v0.x, v0.y,-1.0), Ogre::Vector3(v0.x, v0.y, 1.0),
				Ogre::Vector3(v1.x, v1.y,-1.0), Ogre::Vector3(v1.x, v1.y, 1.0));
		}

		obj->end(); 
		obj->convertToMesh(mesh);
	}

	return mesh;

}
std::vector<Ogre::Vector2> ProceduralGeometry::getCubeStripPolygon(
	const std::vector<Ogre::Vector2>& vertices, float thickness)
{
	std::vector<Ogre::Vector2> shape;
	for(int i = 0; i < (int)vertices.size(); ++i)
	{
		shape.push_back(vertices[i]);
	}
	for(int i = (int)vertices.size()-1; i >= 0; --i)
	{
		Ogre::Vector2 v0 = vertices[vertices.size()-2];
		Ogre::Vector2 v1 = vertices[vertices.size()-1];

		Ogre::Vector3 n = getNormal(
			Ogre::Vector3(v0.x, v0.y,-1.0),
			Ogre::Vector3(v0.x, v0.y, 1.0),
			Ogre::Vector3(v1.x, v1.y,-1.0));

		Ogre::Vector2 v2 = v1 - Ogre::Vector2(n.x, n.y)*thickness;
		shape.push_back(v2);
	}
	return shape;
}
Ogre::String ProceduralGeometry::getCubeStrip(const Ogre::String& name,
											  const std::vector<Ogre::Vector2>& vertices,
											  float thickness)
{
	Ogre::String mesh = name + "Mesh";

	if(!Ogre::MeshManager::getSingleton().resourceExists(mesh))
	{
		Ogre::ManualObject* obj = new Ogre::ManualObject(name);
		obj->begin(mStandardMaterial);
		
		int j = 0;
		{
			Ogre::Vector2 v0 = vertices[0];
			Ogre::Vector2 v1 = vertices[1];

			Ogre::Vector3 n = getNormal(
				Ogre::Vector3(v0.x, v0.y,-1.0),
				Ogre::Vector3(v0.x, v0.y, 1.0),
				Ogre::Vector3(v1.x, v1.y,-1.0));

			Ogre::Vector2 v2 = v0 - Ogre::Vector2(n.x, n.y)*thickness;
		
			createQuad(obj, j,
				 Ogre::Vector3(v0.x,v0.y, 1.0), Ogre::Vector3(v0.x,v0.y,-1.0),
				 Ogre::Vector3(v2.x,v2.y, 1.0), Ogre::Vector3(v2.x,v2.y,-1.0));
			j+=4;
		}

		for(int i = 0; i < (int)vertices.size()-1; ++i)
		{
			Ogre::Vector2 v0 = vertices[i];
			Ogre::Vector2 v1 = vertices[i+1];

			Ogre::Vector3 n = getNormal(
				Ogre::Vector3(v0.x, v0.y,-1.0),
				Ogre::Vector3(v0.x, v0.y, 1.0),
				Ogre::Vector3(v1.x, v1.y,-1.0));

			Ogre::Vector2 v2 = v0 - Ogre::Vector2(n.x, n.y)*thickness;
			Ogre::Vector2 v3 = v1 - Ogre::Vector2(n.x, n.y)*thickness;

			createQuad(obj, j,
				Ogre::Vector3(v0.x, v0.y,-1.0), Ogre::Vector3(v0.x, v0.y, 1.0),
				Ogre::Vector3(v1.x, v1.y,-1.0), Ogre::Vector3(v1.x, v1.y, 1.0));
			j+=4;
			
			createQuad(obj, j,
				Ogre::Vector3(v2.x, v2.y, 1.0), Ogre::Vector3(v2.x, v2.y,-1.0),
				Ogre::Vector3(v3.x, v3.y, 1.0), Ogre::Vector3(v3.x, v3.y,-1.0));
			j+=4;
			
			createQuad(obj, j,
				Ogre::Vector3(v0.x, v0.y, 1.0), Ogre::Vector3(v2.x, v2.y, 1.0),
				Ogre::Vector3(v1.x, v1.y, 1.0), Ogre::Vector3(v3.x, v3.y, 1.0));
			j+=4;
			
			createQuad(obj, j,
				Ogre::Vector3(v1.x, v1.y,-1.0), Ogre::Vector3(v3.x, v3.y,-1.0),
				Ogre::Vector3(v0.x, v0.y,-1.0), Ogre::Vector3(v2.x, v2.y, -1.0));
			j+=4;
		}
		
		{
			Ogre::Vector2 v0 = vertices[vertices.size()-2];
			Ogre::Vector2 v1 = vertices[vertices.size()-1];
			
			Ogre::Vector3 n = getNormal(
				Ogre::Vector3(v0.x, v0.y,-1.0),
				Ogre::Vector3(v0.x, v0.y, 1.0),
				Ogre::Vector3(v1.x, v1.y,-1.0));

			Ogre::Vector2 v2 = v1 - Ogre::Vector2(n.x, n.y)*thickness;
		
			createQuad(obj, j,
				 Ogre::Vector3(v1.x,v1.y,-1.0), Ogre::Vector3(v1.x,v1.y,1.0),
				 Ogre::Vector3(v2.x,v2.y,-1.0), Ogre::Vector3(v2.x,v2.y,1.0));
		}

		obj->end(); 
		obj->convertToMesh(mesh);
	}

	return mesh;

}
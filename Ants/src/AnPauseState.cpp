#include "AnPCH.h"
#include "AnPauseState.h"
#include "AnGameState.h"

using namespace Ants;

PauseState::PauseState()
{
    mQuit             = false;
}

void PauseState::enter()
{
    Framework::getSingletonPtr()->mLog->logMessage("Entering PauseState...");

    mSceneMgr = Framework::getSingletonPtr()->mRoot->createSceneManager(Ogre::ST_GENERIC, "PauseSceneMgr");
    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    mSceneMgr->addRenderQueueListener(Framework::getSingletonPtr()->mOverlaySystem);

    mCamera = mSceneMgr->createCamera("PauseCam");
    mCamera->setPosition(Ogre::Vector3(0, 25, -50));
    mCamera->lookAt(Ogre::Vector3(0, 0, 0));
    mCamera->setNearClipDistance(1);

    mCamera->setAspectRatio(Ogre::Real(Framework::getSingletonPtr()->mViewport->getActualWidth()) /
        Ogre::Real(Framework::getSingletonPtr()->mViewport->getActualHeight()));

    Framework::getSingletonPtr()->mViewport->setCamera(mCamera);

    mQuit = false;

	CEGUI::Window* wnd = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("PauseGame.layout");
	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(wnd);
	wnd->getChild("BackToGameBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&PauseState::backToGame, this));
	wnd->getChild("BackToMenuBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&PauseState::backToMenu, this));

    createScene();
}
void PauseState::createScene()
{
}
void PauseState::exit()
{
    Framework::getSingletonPtr()->mLog->logMessage("Leaving PauseState...");

    mSceneMgr->destroyCamera(mCamera);
    if(mSceneMgr)
        Framework::getSingletonPtr()->mRoot->destroySceneManager(mSceneMgr);
}

bool PauseState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	if (Framework::getSingletonPtr()->mKeyboard->isKeyDown(OIS::KC_ESCAPE))
	{
		mQuit = true;
		return true;
	}

	Framework::getSingletonPtr()->keyPressed(keyEventRef);
	return true;
}
bool PauseState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	Framework::getSingletonPtr()->keyReleased(keyEventRef);
	return true;
}

bool PauseState::mouseMoved(const OIS::MouseEvent &evt)
{
	Framework::getSingletonPtr()->mouseMoved(evt);
	return true;
}
bool PauseState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	Framework::getSingletonPtr()->mousePressed(evt, id);
	return true;
}
bool PauseState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	Framework::getSingletonPtr()->mouseReleased(evt, id);
	return true;
}

bool PauseState::backToMenu(const CEGUI::EventArgs& /*e*/)
{
	GameState::LastGameStatistics.GameCancelled = 1;
	GameState::LastGameStatistics.GameWon = 0;
	popAllAndPushAppState(findByName("MenuState"));
	return true;
}
bool PauseState::backToGame(const CEGUI::EventArgs& /*e*/)
{
	mQuit = true;
	return true;
}

void PauseState::update(float dT)
{
    if(mQuit == true)
    {
        popAppState();
        return;
    }
}

#include "AnPCH.h"
#include "AnKamikaze.h"
using namespace Ants;

Ogre::String Kamikaze::msStandardMesh = "";
Ogre::String Kamikaze::msMaterialName = "";

Kamikaze::Kamikaze(int id, int clientID,
	const Ogre::Vector2& position,
	const Ogre::Vector2& tilePosition,
	int visibilityRadius,
	WorldQuery* worldQuery, JobQuery* jobQuery,
	EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
	: Ant(id, clientID, position, tilePosition, visibilityRadius,
	worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mInventory.resize(1);
	mSubJobs.resize(1);
	mTargetRooms.resize(1);
	mAssignedSlots.resize(1);
	mJobs.resize(1);
	mJobs.assign(1, -1);

	mP1Jobs = Job::HOLD_JOB;
	mP2Jobs = 0;
	mP3Jobs = Job::DEFEND_JOB;

	mRanged = false;
	mAOE = true;
	mAOEDamage = 150;
	mAttackTime = 3.0f;
	mMeleeDamage = 20;
	mRangedDamage = 0;
	mActiveTrait = 0;
	mArmor = 0;
}
Kamikaze::Kamikaze(const EntityDesc& desc)
: Ant(desc)
{
}
Kamikaze::~Kamikaze()
{
}

void Kamikaze::applyUpgrade(Entity::Upgrade u)
{
	if (u == Entity::U_BULLDOGS)
	{
		mMeleeDamage = 25;
		mArmor += 2;
		mMaxSpeed /= 2;
	}
	else
	{
		Ant::applyUpgrade(u);
	}
}

void Kamikaze::selectBehaviour(float dt)
{
	int newTrait = 0;
	if (!mEnemies.empty())
	{
		newTrait = 1; // warrrior
	}

	if (newTrait != mActiveTrait)
	{
		mCurrentState = FIND_JOB;
        mActiveTrait = newTrait;
#ifdef _DEBUG
        Framework::getSingleton ().mLog->logMessage (
            "kamikaze " + Ogre::StringConverter::toString (getID ())
            + " switched to mode " + Ogre::StringConverter::toString (mActiveTrait)
            );
#endif
	}
}

Entity::EntityType Kamikaze::getType() const
{
	return Entity::ET_KAMIKAZE;
}

Ogre::String Kamikaze::getMaterialName() const
{
	return Kamikaze::msMaterialName;
}
Ogre::String Kamikaze::getStandardMesh() const
{
	return Kamikaze::msStandardMesh;
}
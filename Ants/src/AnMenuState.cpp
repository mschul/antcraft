#include "AnPCH.h"
#include "AnMenuState.h"
#include "AnGameState.h"
#include "AnDeserializer.h"
#include "AnSerializer.h"
#include "AnOptions.h"
using namespace Ants;

MenuState::MenuState()
{
    mQuit         = false;
}

void MenuState::enter()
{
    Framework::getSingletonPtr()->mLog->logMessage("Entering MenuState...");

    mSceneMgr = Framework::getSingletonPtr()->mRoot->createSceneManager(Ogre::ST_GENERIC, "MenuSceneMgr");
    mSceneMgr->setAmbientLight(Ogre::ColourValue(0.7f, 0.7f, 0.7f));

    mSceneMgr->addRenderQueueListener(Framework::getSingletonPtr()->mOverlaySystem);

    mCamera = mSceneMgr->createCamera("MenuCam");
    mCamera->setPosition(Ogre::Vector3(0, 25, -50));
    mCamera->lookAt(Ogre::Vector3(0, 0, 0));
    mCamera->setNearClipDistance(1);

    mCamera->setAspectRatio(Ogre::Real(Framework::getSingletonPtr()->mViewport->getActualWidth()) /
        Ogre::Real(Framework::getSingletonPtr()->mViewport->getActualHeight()));

    Framework::getSingletonPtr()->mViewport->setCamera(mCamera);


	CEGUI::Window* rootWindow = CEGUI::WindowManager::getSingleton().loadLayoutFromFile("MainMenu.layout");

	mMainMenuWnd = rootWindow->getChild("MainFrame");
	mMainMenuWnd->moveToFront();
	mCurrentWnd = mMainMenuWnd;
	CEGUI::Window* wnd = mMainMenuWnd->getChild("NewGameBtn");

	mCustomGameWnd = rootWindow->getChild("CustomFrame");
	mCustomGameWnd->setAlpha(0.f); mCustomGameWnd->setDisabled(true);

	mHighscoresWnd = rootWindow->getChild("HighscoresFrame");
	mHighscoresWnd->setAlpha(0.f); mHighscoresWnd->setDisabled(true);

	mAchievementsWnd = rootWindow->getChild("AchievementsFrame");
	mAchievementsWnd->setAlpha(0.f); mAchievementsWnd->setDisabled(true);


	mOptionsWnd = rootWindow->getChild("OptionsFrame");
	mOptionsWnd->setAlpha(0.f); mOptionsWnd->setDisabled(true);

	mTutorialWnd = rootWindow->getChild("TutorialFrame");
	mTutorialWnd->setAlpha(0.f); mTutorialWnd->setDisabled(true);
	for (int i = 0; i < 7; ++i)
	{
		Ogre::String name = "TutorialPage" + Ogre::StringConverter::toString(i) + ".layout";
		mTutorialPages.push_back(CEGUI::WindowManager::getSingleton().loadLayoutFromFile(name));
	}
	mCurrentTutorialPage = 0;
	mTutorialWnd->getChild("TutorialContainer")->addChild(mTutorialPages[mCurrentTutorialPage]);

	mEnterNameWnd = rootWindow->getChild("EnterNameFrame");
	mEnterNameWnd->setAlpha(0.f); mEnterNameWnd->setDisabled(true);

	mGameOverWnd = rootWindow->getChild("GameOverFrame");
	if (!GameState::GameStatisticsSynched)
	{
		if (Framework::getSingleton().mHighscores->isInTopTen(&GameState::LastGameStatistics))
		{
			// show enter name screen
			mCurrentWnd->setAlpha(0.f);
			mCurrentWnd->setDisabled(true);
			mEnterNameWnd->setAlpha(1.f);
			mEnterNameWnd->setEnabled(true);
			mEnterNameWnd->moveToFront();
			mCurrentWnd = mEnterNameWnd;
			
			CEGUI::Window* wnd = mEnterNameWnd->getChild("HighscoreScoreLabel");
			wnd->setText("Score: " + Ogre::StringConverter::toString(GameState::LastGameStatistics.getScore()));
		}
		else
		{
			GameState::GameStatisticsSynched = true;
			showGameOver(CEGUI::EventArgs());
		}
	}
	else
	{
		mGameOverWnd->setAlpha(0.f);
		mGameOverWnd->setDisabled(true);
	}


	CEGUI::System::getSingleton().getDefaultGUIContext().setRootWindow(rootWindow);

	mMainMenuWnd->getChild("NewGameBtn")->subscribeEvent(CEGUI::PushButton::EventMouseEntersArea,
		CEGUI::Event::Subscriber(&MenuState::enters, this));
	mMainMenuWnd->getChild("NewGameBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::newGame, this));
	mMainMenuWnd->getChild("CustomGameBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::showCustomGame, this));
	mMainMenuWnd->getChild("HighscoresBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::showHighscores, this));
	mMainMenuWnd->getChild("AchievementsBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::showAchievements, this));
	mMainMenuWnd->getChild("TutorialBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::showTutorial, this));
	mMainMenuWnd->getChild("OptionsBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::showOptions, this));
	mMainMenuWnd->getChild("QuitBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::quit, this));
	
	mCustomGameWnd->getChild("StartBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::newCustomGame, this));
	mOptionsWnd->getChild("SaveBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::saveOptions, this));

	mTutorialWnd->getChild("NextPageBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::nextTutorialPage, this));
	mTutorialWnd->getChild("PrevPageBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::prevTutorialPage, this));

	mCustomGameWnd->getChild("BackBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::back, this));
	mHighscoresWnd->getChild("BackBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::back, this));
	mAchievementsWnd->getChild("BackBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::back, this));
	mTutorialWnd->getChild("BackBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::back, this));
	mGameOverWnd->getChild("BackBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::back, this));
	mOptionsWnd->getChild("BackBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::back, this));
	
	mEnterNameWnd->getChild("BackBtn")->subscribeEvent(CEGUI::PushButton::EventClicked,
		CEGUI::Event::Subscriber(&MenuState::saveHighscore, this));

	updateUI();

	{
		CEGUI::Combobox* combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("RendererOptions"));
		combobox->setReadOnly(true);
		Ogre::RenderSystemList renderers = Ogre::Root::getSingleton().getAvailableRenderers();
		Ogre::RenderSystemList::iterator iter, iend;
		iter = renderers.begin(); iend = renderers.end();
		Ogre::RenderSystem* renderSystem = 0;
		for (; iter != iend; ++iter)
		{
			Ogre::String name = (*iter)->getName();
			CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(name);
			combobox->addItem(item);

			if (name == Options::getSingleton().Renderer())
			{
				combobox->setItemSelectState(item, true);
			}
			if (name == Options::getSingleton().StandardRenderer())
			{
				renderSystem = *iter;
			}
		}

		Ogre::ConfigOptionMap options = renderSystem->getConfigOptions();
		{
			combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("FSAAOptions"));
			Ogre::StringVector items = options["FSAA"].possibleValues;
			for (Ogre::String name : items)
			{
				CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(name);
				combobox->addItem(item);

				if (name == Options::getSingleton().FSAA())
				{
					combobox->setItemSelectState(item, true);
				}

			}
		}
		{
			combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("VideoModeOptions"));
			Ogre::StringVector items = options["Video Mode"].possibleValues;
			for (Ogre::String name : items)
			{
				CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(name);
				combobox->addItem(item);

				if (name == Options::getSingleton().VideoMode())
				{
					combobox->setItemSelectState(item, true);
				}

			}
		}
		{
			combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("ColourDepthOptions"));
			Ogre::StringVector items = options["Colour Depth"].possibleValues;
			for (Ogre::String name : items)
			{
				CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(name);
				combobox->addItem(item);

				if (name == Options::getSingleton().ColourDepth())
				{
					combobox->setItemSelectState(item, true);
				}

			}
		}
		{
			combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("FullScreenOptions"));
			Ogre::StringVector items = options["Full Screen"].possibleValues;
			for (Ogre::String name : items)
			{
				CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(name);
				combobox->addItem(item);

				if (name == Options::getSingleton().FullScreen())
				{
					combobox->setItemSelectState(item, true);
				}

			}
		}
		{
			combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("VSyncOptions"));
			Ogre::StringVector items = options["VSync"].possibleValues;
			for (Ogre::String name : items)
			{
				CEGUI::ListboxTextItem* item = new CEGUI::ListboxTextItem(name);
				combobox->addItem(item);

				if (name == Options::getSingleton().VSync())
				{
					combobox->setItemSelectState(item, true);
				}

			}
		}
	}
	
	createHighscoreList();
	createAchievementsList();
}
bool MenuState::enters(const CEGUI::EventArgs& e)
{
	const CEGUI::MouseEventArgs* evt = dynamic_cast<const CEGUI::MouseEventArgs*>(&e);
	
	return true;
}

void MenuState::createHighscoreList()
{
	for (int i = 0; i < Framework::getSingleton().mHighscores->size(); ++i)
	{
		Statistics* stat = (*Framework::getSingleton().mHighscores)[i];
		CEGUI::DefaultWindow* wnd;

		Ogre::String caption = Ogre::StringConverter::toString(i + 1) + ". "
			+ stat->Name + " :: " + Ogre::StringConverter::toString(stat->getScore()) + " Points";
		wnd = (CEGUI::DefaultWindow*)mHighscoresWnd->createChild("OgreTray/Label",
			"points" + Ogre::StringConverter::toString(i));
		wnd->setText(caption);
		wnd->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f),
			CEGUI::UDim(((float)i) / 12, 0.0f)));
		wnd->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f),
			CEGUI::UDim(0.1f, 0.0f)));
	}


	Statistics* stat = Framework::getSingleton().mAllTimeStats;
	Ogre::String caption = "All Time "
		+ stat->getTimePassed() + " :: " + Ogre::StringConverter::toString(stat->getScore()) + " Points";
	CEGUI::DefaultWindow* wnd = (CEGUI::DefaultWindow*)mHighscoresWnd->createChild("OgreTray/Label",
		"allTime");
	wnd->setText(caption);
	wnd->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f),
		CEGUI::UDim(((float)10) / 12, 0.0f)));
	wnd->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f),
		CEGUI::UDim(0.1f, 0.0f)));
}
void MenuState::createAchievementsList()
{
	std::vector<Message*> achievements;
	Framework::getSingleton().mAchievements->getAchievements(achievements);

	CEGUI::ScrollablePane* root = (CEGUI::ScrollablePane*)mAchievementsWnd->createChild("OgreTray/ScrollablePane", "pane");

	for (int i = 0; i < achievements.size(); ++i)
	{
		Message* m = achievements[i];
		CEGUI::DefaultWindow* wnd;

		bool achievementGot = m->getDatum("AchievementGot") == "True";

		wnd = (CEGUI::DefaultWindow*)root->createChild("OgreTray/Title",
			"ach" + Ogre::StringConverter::toString(i));
		wnd->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f),
			CEGUI::UDim(((float)i) / 5, 0.0f)));
		wnd->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f),
			CEGUI::UDim(0.2f, 0.0f)));

		if (!achievementGot)
			wnd->setDisabled(true);

		{
			CEGUI::DefaultWindow* lbl = (CEGUI::DefaultWindow*)wnd->createChild("OgreTray/Label");
			lbl->setText(m->getDatum("MessageTitle"));
			lbl->setSize(CEGUI::USize(CEGUI::UDim(0.9f, 0.0f),
				CEGUI::UDim(0.15f, 0.0f)));
			lbl->setPosition(CEGUI::UVector2(CEGUI::UDim(0.05f, 0.0f),
				CEGUI::UDim(0.1f, 0.0f)));
		}
		{
			CEGUI::DefaultWindow* lbl = (CEGUI::DefaultWindow*)wnd->createChild("OgreTray/Label");
			if (achievementGot)
				lbl->setText(m->getDatum("MessageBody"));
			else
				lbl->setText("You don't have this Achievement.");
			lbl->setSize(CEGUI::USize(CEGUI::UDim(0.9f, 0.0f),
				CEGUI::UDim(0.7f, 0.0f)));
			lbl->setPosition(CEGUI::UVector2(CEGUI::UDim(0.05f, 0.0f),
				CEGUI::UDim(0.15f, 0.0f)));
		}
		{
			CEGUI::DefaultWindow* lbl = (CEGUI::DefaultWindow*)wnd->createChild("OgreTray/Label");
			lbl->setText(m->getDatum("MessageFooter"));
			lbl->setSize(CEGUI::USize(CEGUI::UDim(0.9f, 0.0f),
				CEGUI::UDim(0.15f, 0.0f)));
			lbl->setPosition(CEGUI::UVector2(CEGUI::UDim(0.05f, 0.0f),
				CEGUI::UDim(0.72f, 0.0f)));
		}

		delete0(m);
	}
	root->setSize(CEGUI::USize(CEGUI::UDim(1.0f, 0.0f),
		CEGUI::UDim(0.9f, 0.0f)));
	root->setPosition(CEGUI::UVector2(CEGUI::UDim(0.0f, 0.0f),
		CEGUI::UDim(0.0f, 0.0f)));

}
void MenuState::destroyHighscoreList()
{
	for (int i = 0; i < Framework::getSingleton().mHighscores->size(); ++i)
	{
		mHighscoresWnd->destroyChild("points" + Ogre::StringConverter::toString(i));
	}
	mHighscoresWnd->destroyChild("allTime");
}
void MenuState::destroyAchievementsList()
{
	mAchievementsWnd->destroyChild("pane");
}
void MenuState::exit()
{
    Framework::getSingletonPtr()->mLog->logMessage("Leaving MenuState...");

    mSceneMgr->destroyCamera(mCamera);
    if(mSceneMgr)
        Framework::getSingletonPtr()->mRoot->destroySceneManager(mSceneMgr);
}

bool MenuState::keyPressed(const OIS::KeyEvent &keyEventRef)
{
    if(Framework::getSingletonPtr()->mKeyboard->isKeyDown(OIS::KC_ESCAPE))
    {
		if (mCurrentWnd != mMainMenuWnd)
		{
			{
				CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeOut");
				instance->setTargetWindow(mCurrentWnd);
				instance->start();
			}
			{
				CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeIn");
				instance->setTargetWindow(mMainMenuWnd);
				instance->start();
			}

			mCurrentWnd->setDisabled(true);
			mMainMenuWnd->setEnabled(true);
			mMainMenuWnd->moveToFront();
			mCurrentWnd = mMainMenuWnd;
		}
		else
		{
			mQuit = true;
		}
        return true;
    }

    Framework::getSingletonPtr()->keyPressed(keyEventRef);
    return true;
}
bool MenuState::keyReleased(const OIS::KeyEvent &keyEventRef)
{
    Framework::getSingletonPtr()->keyReleased(keyEventRef);
    return true;
}

bool MenuState::mouseMoved(const OIS::MouseEvent &evt)
{
    Framework::getSingletonPtr()->mouseMoved(evt);
    return true;
}
bool MenuState::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	Framework::getSingletonPtr()->mousePressed(evt, id);
    return true;
}
bool MenuState::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	Framework::getSingletonPtr()->mouseReleased(evt, id);
    return true;
}

bool MenuState::newGame(const CEGUI::EventArgs& /*e*/)
{
	changeAppState(findByName("GameState"));
	return true;
}
bool MenuState::saveOptions(const CEGUI::EventArgs& /*e*/)
{
	CEGUI::Combobox* combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("RendererOptions"));
	Options::getSingleton().setRenderer(Ogre::String(combobox->getSelectedItem()->getText().c_str()));
	combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("FSAAOptions"));
	Options::getSingleton().setFSAA(Ogre::String(combobox->getSelectedItem()->getText().c_str()));
	combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("VideoModeOptions"));
	Options::getSingleton().setVideoMode(Ogre::String(combobox->getSelectedItem()->getText().c_str()));
	combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("ColourDepthOptions"));
	Options::getSingleton().setColourDepth(Ogre::String(combobox->getSelectedItem()->getText().c_str()));
	combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("FullScreenOptions"));
	Options::getSingleton().setFullScreen(Ogre::String(combobox->getSelectedItem()->getText().c_str()));
	combobox = static_cast<CEGUI::Combobox*>(mOptionsWnd->getChild("VSyncOptions"));
	Options::getSingleton().setVSync(Ogre::String(combobox->getSelectedItem()->getText().c_str()));
	mQuit = true;
	return true;

}
bool MenuState::saveHighscore(const CEGUI::EventArgs& e)
{
	if (!GameState::GameStatisticsSynched)
	{
		if (Framework::getSingleton().mHighscores->isInTopTen(&GameState::LastGameStatistics))
		{
			// synch last game
			Statistics* stat = new Statistics(Framework::getSingleton().mHighscores->size());
			GameState::GameStatisticsSynched = true;
			stat->set(GameState::LastGameStatistics);
			stat->Name = Ogre::String(mEnterNameWnd->getChild("EnterNameTb")->getText().c_str());
			if (stat->Name == "") stat->Name = "No Name";

			destroyHighscoreList();
			Framework::getSingleton().mHighscores->addEntry(stat);
			createHighscoreList();
		}

		// show game over screen
		showGameOver(e);
	}
	return true;
}
bool MenuState::resetUserData(const CEGUI::EventArgs&)
{
	Framework::getSingleton().mHighscores->clear();
	Framework::getSingleton().mAchievements->clear();
	return true;
}
bool MenuState::back(const CEGUI::EventArgs& /*e*/)
{
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeOut");
		instance->setTargetWindow(mCurrentWnd);
		instance->start();
	}
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeIn");
		instance->setTargetWindow(mMainMenuWnd);
		instance->start();
	}

	mCurrentWnd->setDisabled(true);
	mMainMenuWnd->setEnabled(true);
	mMainMenuWnd->moveToFront();
	mCurrentWnd = mMainMenuWnd;
	return true;
}
bool MenuState::showCustomGame(const CEGUI::EventArgs& /*e*/)
{
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeOut");
		instance->setTargetWindow(mCurrentWnd);
		instance->start();
	}
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeIn");
		instance->setTargetWindow(mCustomGameWnd);
		instance->start();
	}

	mCurrentWnd->setDisabled(true);
	mCustomGameWnd->setEnabled(true);
	mCustomGameWnd->moveToFront();
	mCurrentWnd = mCustomGameWnd;
	return true;
}
bool MenuState::showHighscores(const CEGUI::EventArgs& /*e*/)
{
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeOut");
		instance->setTargetWindow(mCurrentWnd);
		instance->start();
	}
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeIn");
		instance->setTargetWindow(mHighscoresWnd);
		instance->start();
	}

	mCurrentWnd->setDisabled(true);
	mHighscoresWnd->setEnabled(true);
	mHighscoresWnd->moveToFront();
	mCurrentWnd = mHighscoresWnd;
	return true;
}
bool MenuState::showAchievements(const CEGUI::EventArgs& /*e*/)
{
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeOut");
		instance->setTargetWindow(mCurrentWnd);
		instance->start();
	}
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeIn");
		instance->setTargetWindow(mAchievementsWnd);
		instance->start();
	}

	mCurrentWnd->setDisabled(true);
	mAchievementsWnd->setEnabled(true);
	mAchievementsWnd->moveToFront();
	mCurrentWnd = mAchievementsWnd;
	return true;
}
bool MenuState::showTutorial(const CEGUI::EventArgs& /*e*/)
{
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeOut");
		instance->setTargetWindow(mCurrentWnd);
		instance->start();
	}
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeIn");
		instance->setTargetWindow(mTutorialWnd);
		instance->start();
	}

	mCurrentWnd->setDisabled(true);
	mTutorialWnd->setEnabled(true);
	mTutorialWnd->moveToFront();
	mCurrentWnd = mTutorialWnd;
	return true;
}
bool MenuState::nextTutorialPage(const CEGUI::EventArgs& /*e*/)
{
	mCurrentTutorialPage++;
	if (mCurrentTutorialPage > 6)
	{
		mCurrentTutorialPage = 6;
	}
	else
	{
		mTutorialWnd->getChild("TutorialContainer")->removeChild("TutorialPage");
		mTutorialWnd->getChild("TutorialContainer")->addChild(mTutorialPages[mCurrentTutorialPage]);
	}
	return true;
}
bool MenuState::prevTutorialPage(const CEGUI::EventArgs& /*e*/)
{
	mCurrentTutorialPage--;
	if (mCurrentTutorialPage < 0)
	{
		mCurrentTutorialPage = 0;
	}
	else
	{
		mTutorialWnd->getChild("TutorialContainer")->removeChild("TutorialPage");
		mTutorialWnd->getChild("TutorialContainer")->addChild(mTutorialPages[mCurrentTutorialPage]);
	}
	return true;
}
bool MenuState::showOptions(const CEGUI::EventArgs& /*e*/)
{
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeOut");
		instance->setTargetWindow(mCurrentWnd);
		instance->start();
	}
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeIn");
		instance->setTargetWindow(mOptionsWnd);
		instance->start();
	}

	mCurrentWnd->setDisabled(true);
	mOptionsWnd->setEnabled(true);
	mOptionsWnd->moveToFront();
	mCurrentWnd = mOptionsWnd;
	return true;
}
bool MenuState::showGameOver(const CEGUI::EventArgs& /*e*/)
{
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeOut");
		instance->setTargetWindow(mCurrentWnd);
		instance->start();
	}
	{
		CEGUI::AnimationInstance* instance = CEGUI::AnimationManager::getSingleton().instantiateAnimation("MyFadeIn");
		instance->setTargetWindow(mGameOverWnd);
		instance->start();
	}

	mGameOverWnd->getChild("Points")->setText(Ogre::StringConverter::toString(GameState::LastGameStatistics.getScore()));
	mGameOverWnd->getChild("PlayTime")->setText(GameState::LastGameStatistics.getTimePassed());
	mGameOverWnd->getChild("Rooms")->setText(Ogre::StringConverter::toString(GameState::LastGameStatistics.getRoomCount()));
	mGameOverWnd->getChild("Area")->setText(Ogre::StringConverter::toString(GameState::LastGameStatistics.AnthillArea));
	mGameOverWnd->getChild("RoomsArea")->setText(Ogre::StringConverter::toString(GameState::LastGameStatistics.getRoomArea()));
	mGameOverWnd->getChild("Produced")->setText(Ogre::StringConverter::toString(GameState::LastGameStatistics.getUnitsProduced()));
	mGameOverWnd->getChild("Killed")->setText(Ogre::StringConverter::toString(GameState::LastGameStatistics.getUnitsKilled()));


	mCurrentWnd->setDisabled(true);
	mGameOverWnd->setEnabled(true);
	mGameOverWnd->moveToFront();
	mCurrentWnd = mGameOverWnd;
	return true;
}
bool MenuState::quit(const CEGUI::EventArgs& /*e*/)
{
	mQuit = true;
	return true;
}
bool MenuState::newCustomGame(const CEGUI::EventArgs& /*e*/)
{
	GameState::Settings.setRockAmount(integer(((CEGUI::Slider*)mCustomGameWnd->getChild("RockSlider"))->getCurrentValue()));
	GameState::Settings.setWaterAmount(integer(((CEGUI::Slider*)mCustomGameWnd->getChild("WaterSlider"))->getCurrentValue()));
	GameState::Settings.setCaveAmount(integer(((CEGUI::Slider*)mCustomGameWnd->getChild("CaveSlider"))->getCurrentValue()));
	GameState::Settings.setGrassAmount(integer(((CEGUI::Slider*)mCustomGameWnd->getChild("GrassSlider"))->getCurrentValue()));
	GameState::Settings.setMobAmount(integer(((CEGUI::Slider*)mCustomGameWnd->getChild("MobSlider"))->getCurrentValue()));

	GameState::Settings.setFOW(((CEGUI::ToggleButton*)mCustomGameWnd->getChild("FOWCB"))->isSelected());
	GameState::Settings.setRevealMap(((CEGUI::ToggleButton*)mCustomGameWnd->getChild("RevealMapCB"))->isSelected());
	GameState::Settings.setUpgrades(((CEGUI::ToggleButton*)mCustomGameWnd->getChild("AllUpgradesCB"))->isSelected());

	changeAppState(findByName("GameState"));
	return true;
}
void MenuState::updateUI()
{
	((CEGUI::Slider*)mCustomGameWnd->getChild("RockSlider"))->setCurrentValue(real(GameState::Settings.getRockAmount()));
	((CEGUI::Slider*)mCustomGameWnd->getChild("WaterSlider"))->setCurrentValue(real(GameState::Settings.getWaterAmount()));
	((CEGUI::Slider*)mCustomGameWnd->getChild("CaveSlider"))->setCurrentValue(real(GameState::Settings.getCaveAmount()));
	((CEGUI::Slider*)mCustomGameWnd->getChild("GrassSlider"))->setCurrentValue(real(GameState::Settings.getGrassAmount()));
	((CEGUI::Slider*)mCustomGameWnd->getChild("MobSlider"))->setCurrentValue(real(GameState::Settings.getMobAmount()));

	((CEGUI::ToggleButton*)mCustomGameWnd->getChild("FOWCB"))->setSelected(GameState::Settings.getFOW());
	((CEGUI::ToggleButton*)mCustomGameWnd->getChild("RevealMapCB"))->setSelected(GameState::Settings.getRevealMap());
	((CEGUI::ToggleButton*)mCustomGameWnd->getChild("AllUpgradesCB"))->setSelected(GameState::Settings.getUpgrades());
}


void MenuState::update(float dT)
{
    if(mQuit == true)
    {
        shutdown();
        return;
    }
}


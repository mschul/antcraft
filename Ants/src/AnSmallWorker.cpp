#include "AnPCH.h"
#include "AnSmallWorker.h"
using namespace Ants;

Ogre::String SmallWorker::msStandardMesh = "";
Ogre::String SmallWorker::msMaterialName = "";

SmallWorker::SmallWorker (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
		: Ant(id, clientID, position, tilePosition, visibilityRadius,
		worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mInventory.resize(1);
	mSubJobs.resize(1);
	mTargetRooms.resize(1);
	mAssignedSlots.resize(1);
	mJobs.resize(1);
	mJobs.assign(1, -1);

	mP1Jobs = Job::FETCH_JOB | Job::ROOM_CREATE_JOB | Job::ROOM_CREATE_JOB_EX;
	mP2Jobs = Job::DIG_JOB | Job::DIG_RESOURCE_JOB;
	mP3Jobs = 0;

	mAOE = false;
	mAOEDamage = 0;
	mRanged = false;
	mRangedDamage = 0;
	mMeleeDamage = 0;

	mArmor = 2;
	mActiveTrait = 0;
}
SmallWorker::SmallWorker (const EntityDesc& desc)
	: Ant(desc)
{
}
SmallWorker::~SmallWorker ()
{
}

void SmallWorker::selectBehaviour(float dt)
{
	int newTrait = 0;
	if (!mEnemies.empty() && mHealth < 10)
	{
		newTrait = 2; // escape
	}

	if(newTrait != mActiveTrait)
	{
		mCurrentState = FIND_JOB;
        mActiveTrait = newTrait;
#ifdef _DEBUG
        Framework::getSingleton ().mLog->logMessage (
            "smallworker " + Ogre::StringConverter::toString (getID ())
            + " switched to mode " + Ogre::StringConverter::toString (mActiveTrait)
            );
#endif
	}
}

Entity::EntityType SmallWorker::getType() const
{
	return Entity::ET_SMALL_WORKER;
}

Ogre::String SmallWorker::getMaterialName() const
{
	return SmallWorker::msMaterialName;
}
Ogre::String SmallWorker::getStandardMesh() const
{
	return SmallWorker::msStandardMesh;
}
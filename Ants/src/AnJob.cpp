#include "AnPCH.h"
#include "AnJob.h"
#include "AnEntity.h"
using namespace Ants;

Job::Job(ObjectID id, int capacity, const Ogre::Vector2& position)
	: GameObject(id)
{
	mPosition = position;
	mCapacity = capacity;
	mMaximumCapacity = capacity;
	mID = id;
}
Job::~Job ()
{
}

Ogre::Vector2 Job::getPosition() const
{
	return mPosition;
}

void Job::assign(Entity* ent)
{
	AN_UNUSED(ent);
	mCapacity--;
}
void Job::unAssign(Entity* ent)
{
	AN_UNUSED(ent);
	mCapacity++;
}
bool Job::isAvailable() const
{
	return mCapacity > 0;
}
void Job::finish(ActionReactor* reactor)
{
	reactor->jobFinished(this);
}
void Job::setCapacity(int capacity)
{
	mCapacity += capacity - mMaximumCapacity;
	mMaximumCapacity = capacity;
}
int Job::getCapacity() const
{
	return mMaximumCapacity;
}
int Job::getRemainingCapacity() const
{
	return mCapacity;
}
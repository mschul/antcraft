#include "AnPCH.h"
#include "AnEscapeTrait.h"
#include "AnMob.h"
#include "AnAnt.h"
using namespace Ants;

template class EscapeTrait<Mob>;
template class EscapeTrait<Ant>;

template<class T>
EscapeTrait<T>::EscapeTrait()
{
}

template<class T>
void EscapeTrait<T>::idleWalking(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::FIND_PATH;
}
template<class T>
void EscapeTrait<T>::idleStanding(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::FIND_PATH;
}
template<class T>
void EscapeTrait<T>::findJob(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::FIND_PATH;
}
template<class T>
void EscapeTrait<T>::getJobInfo(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::FIND_PATH;
}
template<class T>
void EscapeTrait<T>::findPath(float dt, T* character, ActionReactor* reactor)
{
	if(character->mWorldQuery->isBlocking(character->getTilePosition()))
		return;

	character->clearPath();
	character->escape(character->mEnemies.front()->getTilePosition());
	character->mCurrentState = Character::REACH_JOB;
}
template<class T>
void EscapeTrait<T>::reachJob(float dt, T* character, ActionReactor* reactor)
{
	bool success = false;

	if(!character->mPath.empty())
	{
		character->mChanged = true;
		success = character->walkWaypoint(dt);
	}

	if(!success)
	{
		character->mCurrentState = Character::FIND_PATH;
	}
}
template<class T>
void EscapeTrait<T>::workJob(float dt, T* character, ActionReactor* reactor)
{
	character->mCurrentState = Character::FIND_PATH;
}
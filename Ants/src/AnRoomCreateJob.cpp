#include "AnPCH.h"
#include "AnRoomCreateJob.h"
#include "AnClient.h"
using namespace Ants;

RoomCreateJob::RoomCreateJob (int id, const Ogre::Vector2& position, double duration)
	: Job(id, 1, position)
{
	mT = duration;
	mRoomType = Room::NONE;
	mFetchEnt = 0;
	mTargetRoom = 0;
}
RoomCreateJob::~RoomCreateJob ()
{
}

Job::JobType RoomCreateJob::getType() const
{
	return Job::ROOM_CREATE_JOB;
}

bool RoomCreateJob::update(double dt, ActionReactor* reactor)
{
	mT -= dt;
	if(mT <= 0)
	{
		if(!mFetchEnt)
		{
			reactor->setTile(mPosition, Tile::FLOOR);
			mFetchEnt = reactor->spawn(mPosition, Entity::ET_DEBRIS);
		}
		return true;
	}
	return false;
}
void RoomCreateJob::getSubJobs(std::vector<Job::SubJobs>& jobs) const
{
	if(!mFetchEnt)
		jobs.push_back(Job::UPDATE);
	jobs.push_back(Job::FETCH);
	jobs.push_back(Job::WALK_TO_TARGET);
	jobs.push_back(Job::PLACE);
}

Entity* RoomCreateJob::getFetchEntity() const
{
	return mFetchEnt;
}
void RoomCreateJob::setFetchEntity(Entity* fetchEnt)
{
	mFetchEnt = fetchEnt;
}

Room* RoomCreateJob::getTargetRoom() const
{
	return mTargetRoom;
}
void RoomCreateJob::setTargetRoom(Room* targetRoom)
{
	mTargetRoom = targetRoom;
}

Room::RoomType RoomCreateJob::getRoomType() const
{
	return mRoomType;
}
void RoomCreateJob::setRoomType(Room::RoomType roomType)
{
	mRoomType = roomType;
}

#include "AnPCH.h"
#include "AnEntity.h"
#include "AnEntityDesc.h"
using namespace Ants;

Entity::Entity (int id, int clientID,
				const Ogre::Vector2& position,
				const Ogre::Vector2& tilePosition, 
				int visibilityRadius)
	: GameObject(id)
{
	mClientID = clientID;
	mPosition = position;
	mTilePosition = tilePosition;
	mVisibilityRadius = visibilityRadius;
	mOrientation = Ogre::Radian();
	mSlot = 0;
	mChanged = false;
	mPuppet = false;
	mParent = 0;
}
Entity::Entity (const EntityDesc& desc)
	: GameObject(desc.getEntityID())
{
	mSlot = 0;
	mChanged = false;
	mParent = 0;
	mPuppet = true;
	set(desc);
}
Entity::~Entity ()
{
}

void Entity::set(const EntityDesc& desc)
{
	mClientID = desc.getClientID();
	mPosition = desc.getPosition();
	mTilePosition = desc.getTilePosition();
	mOrientation = desc.getOrientation();
	mHealth = desc.getHealth();
	mVisibilityRadius = desc.getVisibilityRadius();
	mArmor = desc.getArmor();
}
		
int Entity::getClientID() const
{
	return mClientID;
}
int Entity::getVisibilityRadius() const
{
	return mVisibilityRadius;
}
bool Entity::isAnt() const
{
	return false;
}
bool Entity::isCharacter() const
{
	return false;
}
bool Entity::isResource() const
{
	return false;
}
		
Ogre::Radian Entity::getOrientation() const
{
	return mOrientation;
}
Ogre::Vector2 Entity::getPosition() const
{
	return mPosition;
}
Ogre::Vector2 Entity::getTilePosition() const
{
	return mTilePosition;
}
void Entity::setPosition(const Ogre::Vector2& position)
{
	mPosition = position;
	mChanged = true;
}
void Entity::setTilePosition(const Ogre::Vector2& position)
{
	mTilePosition = position;
	mChanged = true;
}

void Entity::applyUpgrade(Upgrade u)
{
}

void Entity::setHealth(float health)
{
	mHealth = health;
}
float Entity::getHealth() const
{
	return mHealth;
}
int Entity::getArmor() const
{
	return mArmor;
}

void Entity::assignSlot(Room::slot* slot)
{
	mSlot = slot;
}
Room::slot* Entity::getSlot() const
{
	return mSlot;
}

bool Entity::isPuppet() const
{
	return mPuppet;
}
bool Entity::isChanged() const
{
	return mChanged;
}
void Entity::resetChangedFlag()
{
	mChanged = false;
}

Entity* Entity::getParent() const
{
	return mParent;
}
void Entity::setParent(Entity* parent)
{
	mParent = parent;
}

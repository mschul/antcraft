#include "AnPCH.h"
#include "AnSpider.h"
using namespace Ants;

Ogre::String Spider::msStandardMesh = "";
Ogre::String Spider::msMaterialName = "";


Spider::Spider (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition,
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones)
	: Mob(id, clientID, position, tilePosition, visibilityRadius,
	worldQuery, jobQuery, entityQuery, roomQuery, pheromones)
{
	mActiveTrait = 0;

	mRanged = true;
	mAOE = false;
	mAOEDamage = 0;
	mRangedDamage = 15;
	mMeleeDamage = 12;
    mArmor = 6;

    mSubJobs.resize (1);
    mJobs.resize (1);
    mJobs.assign (1, -1);

    mP1Jobs = Job::DESTROY_ANTHILL_JOB;
    mP2Jobs = 0;
    mP3Jobs = 0;
}
Spider::Spider (const EntityDesc& desc)
	: Mob(desc)
{
}
Spider::~Spider ()
{
}
		
Entity::EntityType Spider::getType() const
{
	return Entity::ET_SPIDER;
}

void Spider::selectBehaviour (float dt)
{
    int newTrait = 3;
    if (!mEnemies.empty ())
    {
        newTrait = 1; // warrrior
    }

    if (newTrait != mActiveTrait)
    {
        mCurrentState = FIND_JOB;
        mActiveTrait = newTrait;
#ifdef _DEBUG
        Framework::getSingleton ().mLog->logMessage (
            "spider " + Ogre::StringConverter::toString (getID ())
            + " switched to mode " + Ogre::StringConverter::toString (mActiveTrait)
            );
#endif
    }
}

Ogre::String Spider::getMaterialName() const
{
	return Spider::msMaterialName;
}
Ogre::String Spider::getStandardMesh() const
{
	return Spider::msStandardMesh;
}
#include "AnPCH.h"
#include "AnAttack.h"
using namespace Ants;

Attack::Attack(float attackPoints, ObjectID source, ObjectID target,
	int sourceClientID, int targetClientID)
{
	mAttackPoints = attackPoints;
	mSource = source;
	mSourceClientID = sourceClientID;
	mTarget = target;
	mTargetClientID = targetClientID;
}
Attack::~Attack ()
{
}
		
float Attack::getAttackPoints() const
{
	return mAttackPoints;
}
ObjectID Attack::getSource() const
{
	return mSource;
}
int Attack::getSourceClientID() const
{
	return mSourceClientID;
}
ObjectID Attack::getTarget() const
{
	return mTarget;
}
int Attack::getTargetClientID() const
{
	return mTargetClientID;
}
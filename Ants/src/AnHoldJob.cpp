#include "AnPCH.h"
#include "AnHoldJob.h"
using namespace Ants;

HoldJob::HoldJob(int id, const Ogre::Vector2& position, double duration)
	: Job(id, 1, position)
{
	mDuration = duration;
	mT = duration;
}
HoldJob::~HoldJob()
{
}
		
bool HoldJob::update(double dt, ActionReactor* reactor)
{
	mT -= dt;
	if(mT <= 0)
	{
		mT = mDuration;
		return true;
	}
	return false;
}
Job::JobType HoldJob::getType() const
{
	return Job::HOLD_JOB;
}
void HoldJob::getSubJobs(std::vector<Job::SubJobs>& jobs) const
{
	jobs.push_back(Job::UPDATE);
}
#include "AnPCH.h"
#include "AnPheromoneMap.h"
using namespace Ants;

PheromoneMap::PheromoneMap ()
{
	mWidth = 0;
	mHeight = 0;
	mMap = 0;
}
PheromoneMap::~PheromoneMap ()
{
	SafeDelete1<unsigned char>(mMap);
}
		
void PheromoneMap::init(unsigned int width, unsigned int height)
{
	mWidth = width;
	mHeight = height;
	mMap = new1<unsigned char>(width*height);
	memset(mMap, 1, sizeof(unsigned char)*width*height);
}
void PheromoneMap::init(const Ogre::Rect& area)
{
	init(area.width(), area.height());
}

unsigned char PheromoneMap::getValue(const Ogre::Vector2& pos, Tile::Direction dir)
{
	Ogre::Vector2 n = pos + Tile::getNeighbours(pos)[dir];
	unsigned int i = ((int)n.y)*(mWidth) + n.x;
	return mMap[i];
}

void PheromoneMap::increase(const Ogre::Vector2& pos)
{
	unsigned int index = ((int)pos.y)*(mWidth) + pos.x;
	unsigned char val = mMap[index];
	float lambda = (float)(255 - val) / 255;
	mMap[index] += (unsigned char)(8*lambda);
}
void PheromoneMap::decrease(const Ogre::Vector2& pos)
{
	unsigned int index = ((int)pos.y)*(mWidth) + pos.x;
	mMap[index] = mMap[index] > 1 ? mMap[index] - 1 : 1;
}
void PheromoneMap::getDistribution(const Ogre::Vector2& pos, Tile::Direction dir,
	unsigned char* distro, unsigned int& sum, unsigned int sigma)
{
	// init with 0;
	for(int i = 0; i < 6; ++i)
		distro[i] = 0;

	// fill distro
	int d = dir;
	switch(sigma)
	{
	case 0:
		distro[d] = 16 * getValue(pos, (Tile::Direction)d);
		sum = distro[d];
		break;
	case 1:
		distro[d] = 8 * getValue(pos, (Tile::Direction)d);
		sum = distro[d];
		
		d = (dir+1)%6;
		distro[d] = 4  * getValue(pos, (Tile::Direction)d);
		sum += distro[d];

		d = (dir+5)%6;
		distro[d] = 4 * getValue(pos, (Tile::Direction)d);
		sum += distro[d];
		break;
	case 2:
		distro[d] = 6 * getValue(pos, (Tile::Direction)d);
		sum = distro[d];
		
		d = (dir+1)%6;
		distro[d] = 4 *getValue(pos, (Tile::Direction)d);
		sum += distro[d];

		d = (dir+2)%6;
		distro[d] = 1 * getValue(pos, (Tile::Direction)d);
		sum += distro[d];

		d = (dir+5)%6;
		distro[d] = 4 * getValue(pos, (Tile::Direction)d);
		sum += distro[d];
		
		d = (dir+4)%6;
		distro[d] = 1 * getValue(pos, (Tile::Direction)d);
		sum += distro[d];
		break;
	}
}
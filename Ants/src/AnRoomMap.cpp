#include "AnPCH.h"
#include "AnRoomMap.h"
#include "AnThroneChamber.h"
#include "AnStorageChamber.h"
#include "AnFungiChamber.h"
#include "AnBroodChamber.h"
#include "AnFaunalSource.h"
#include "AnFloralSource.h"
#include "AnCopalSource.h"
#include "AnExitRoom.h"
#include "AnAnt.h"
#include "AnSpiderSpawner.h"
#include "AnBombardierBeetleSpawner.h"
#include "AnLadybugSpawner.h"
#include "AnEarthwormSpawner.h"

using namespace Ants;

RoomMap::RoomMap ()
{
	mWidth = 0;
	mHeight = 0;
	mRoomMap = 0;
	mLastRoomID = 0;
}
RoomMap::~RoomMap ()
{
	SafeDelete1(mRoomMap);
	SafeDelete1(mResourceMap);
	SafeDelete1(mExitMap);
}
		
void RoomMap::init(unsigned int width, unsigned int height)
{
	mWidth = width;
	mHeight = height;
	mRoomMap = new1<int>(width*height);
	mResourceMap = new1<int>(width*height);
	mExitMap = new1<int>(width*height);
	memset(mRoomMap, -1, width*height*sizeof(int));
	memset(mResourceMap, -1, width*height*sizeof(int));
	memset(mExitMap, -1, width*height*sizeof(int));
}
void RoomMap::init(const Ogre::Rect& area)
{
	init(area.width(), area.height());
}

void RoomMap::update(float dt, ActionReactor* reactor)
{
	std::map<int, RoomInfo>::const_iterator iter, iend;
	iter = mRooms.begin(); iend = mRooms.end();
	for(; iter != iend; ++iter)
		iter->second.room->update(dt, reactor);
}
void RoomMap::fillStats(Statistics* stats)
{
	stats->AStorageFull = false;
	stats->FaunalStored = 0;
	stats->FloralStored = 0;
	stats->CopalStored = 0;

	stats->Brood.Slots = 0;
	stats->Brood.SlotsFree = 0;
	stats->Brood.Area = 0;
	stats->Storage.Slots = 0;
	stats->Storage.SlotsFree = 0;
	stats->Storage.Area = 0;
	stats->Fungi.Slots = 0;
	stats->Fungi.SlotsFree = 0;
	stats->Fungi.Area = 0;

	stats->Brood.Count = mRoomTypes[Room::BROOD_CHAMBER].size();
	stats->Storage.Count = mRoomTypes[Room::STORAGE_CHAMBER].size();
	stats->Fungi.Count = mRoomTypes[Room::FUNGI_CHAMBER].size();

	std::set<int>::iterator iter, iend;

	iter = mRoomTypes[Room::BROOD_CHAMBER].begin();
	iend = mRoomTypes[Room::BROOD_CHAMBER].end();
	for (; iter != iend; ++iter)
	{
		RoomInfo r = mRooms[*iter];
		stats->Brood.Area += r.tiles.size();
		stats->Brood.Slots += r.room->slotCount();
		stats->Brood.SlotsFree += r.room->freeSlots();
	}
	iter = mRoomTypes[Room::STORAGE_CHAMBER].begin();
	iend = mRoomTypes[Room::STORAGE_CHAMBER].end();
	for (; iter != iend; ++iter)
	{
		RoomInfo r = mRooms[*iter];

		int area = r.tiles.size();
		int slots = r.room->slotCount();
		int free = r.room->freeSlots();
		if (area > 5 && free == 0)
			stats->AStorageFull = true;

		StorageChamber* s = static_cast<StorageChamber*>(r.room);
		
		stats->FaunalStored += s->getFaunalCount();
		stats->FloralStored += s->getFloralCount();
		stats->CopalStored += s->getCopalCount();
		stats->Storage.Area += area;
		stats->Storage.Slots += slots;
		stats->Storage.SlotsFree += free;
	}
	iter = mRoomTypes[Room::FUNGI_CHAMBER].begin();
	iend = mRoomTypes[Room::FUNGI_CHAMBER].end();
	for (; iter != iend; ++iter)
	{
		RoomInfo r = mRooms[*iter];
		stats->Fungi.Area += r.tiles.size();
		stats->Fungi.Slots += r.room->slotCount();
		stats->Fungi.SlotsFree += r.room->freeSlots();
	}
}

void RoomMap::buildRoom(int roomID, const Ogre::Vector2& pivot,
						std::vector<PositionTilePair>& tiles)
{
	std::map<int, RoomInfo>::const_iterator iter = mRooms.find(roomID);
	if(iter == mRooms.end())
		return;
	
	RoomInfo info = iter->second;

	std::vector<Ogre::Vector2> positions;
	for(int p : info.tiles)
		positions.push_back(Ogre::Vector2(p % mWidth, p / mWidth));

	info.room->build(positions, pivot, tiles, mPendingWorkers, mPendingSlotEntities);

	for(Ant* a : mPendingWorkers)
		a->unAssignFromWorkplace();
	mPendingWorkers.clear();
	mPendingSlotEntities.clear();
}

void RoomMap::remove(int id)
{
	std::map<int, RoomInfo>::const_iterator iter = mRooms.find(id);
	if (iter == mRooms.end())
		return;
	Room* room = iter->second.room;

	// remove tile information
	for (int p : iter->second.tiles)
	{
		switch (room->getType())
		{
		case Room::EXIT_ROOM:
			mExitMap[p] = -1;
			break;
		case Room::FAUNAL_SOURCE:
		{
			typedef std::multimap<int, int>::const_iterator iterator;
			std::pair<iterator, iterator> ret = mFaunalMap.equal_range(p);

			iterator it = ret.first;
			for (; it != ret.second; ++it)
			{
				if (it->second == id)
				{
					mFaunalMap.erase(it);
					break;
				}
			}
			break;
		}
		case Room::COPAL_SOURCE:
		case Room::FLORAL_SOURCE:
			mResourceMap[p] = -1;
			break;
		default:
			mRoomMap[p] = -1;
			break;
		}
	}

	room->getWorkers(mPendingWorkers);
	room->getSlotEntities(mPendingSlotEntities);
	mRoomTypes[room->getType()].erase(id);
	SafeDelete0(room);
	mRooms.erase(iter);
}
		
void RoomMap::reportTile(const Ogre::Vector2& position, Tile* tile)
{
	Room* room = getRoom(position);
	if(room)
	{
		if(!room->isDistructible())
			return;

		switch(tile->getType())
		{
		case Tile::DIRT:
		case Tile::WATER:
		case Tile::ROUGH:
		case Tile::MUD:
			assignRoom(Room::NONE, position);
			break;
		}
	}

	Room* resource = getResourceRoom(position);
	if (resource)
	{
		if (resource->getType() == Room::FLORAL_SOURCE)
		{
			switch (tile->getType())
            {
            case Tile::HARVESTED_GRASS:
				FloralSource* r = (FloralSource*)resource;
				r->grow(position);
				break;
			}
		}
		else if (resource->getType() == Room::COPAL_SOURCE)
		{
			switch (tile->getType())
			{
			case Tile::TREE:
				CopalSource* r = (CopalSource*)resource;
				r->grow(position);
				break;
			}
		}
	}
}

void RoomMap::unAssignRoom(const Ogre::Vector2& position,
	std::vector<PositionTilePair>& tiles)
{
	const unsigned int tile = mWidth*((int)position.y) + position.x;
	// defaults to any user created room
	int oldID = getRoomID(tile, Room::STORAGE_CHAMBER);
	if(oldID >= 0)
	{
		Room* room = mRooms[oldID].room;
		setRoomID(tile, room->getType(), -1);
		removeTile(Room::NONE, position, oldID, tiles);
	}
}
int RoomMap::assignRoom(Room::RoomType type, const std::vector<Ogre::Vector2>& positions)
{
	int roomID = -1;
	for(Ogre::Vector2 p : positions)
		roomID = assignRoom(type, p);
	return roomID;
}
int RoomMap::assignExitRoom(const std::vector<Ogre::Vector2>& positions)
{
	std::set<int> overlappingRooms;
	std::vector<int> filteredTiles;
	for (Ogre::Vector2 pos : positions)
	{
		const unsigned int tile = mWidth*((int)pos.y) + pos.x;
		int id = getRoomID(tile, Room::EXIT_ROOM);
		if (id >= 0)
		{
			if (overlappingRooms.find(id) == overlappingRooms.end())
				overlappingRooms.insert(id);
		}
		else
		{
			filteredTiles.push_back(tile);
		}
	}

	int roomID = -1;
	if (overlappingRooms.empty())
	{
		for (Ogre::Vector2 pos : positions)
			roomID = assignRoom(Room::EXIT_ROOM, pos);
	}
	else
	{
		roomID = *overlappingRooms.begin();
		for (int id : overlappingRooms)
		{
			if (roomID != id)
				roomID = mergeRooms(roomID, id);
		}
		std::map<int, RoomInfo>::iterator iter = mRooms.find(roomID);
		for (int tile : filteredTiles)
		{
			iter->second.tiles.insert(tile);
			setRoomID(tile, Room::EXIT_ROOM, roomID);
		}
	}
	return roomID;
}
int RoomMap::assignRoom(Room::RoomType type, const Ogre::Vector2& position)
{
	const unsigned int tile = mWidth*((int)position.y) + position.x;
	if(type != Room::FAUNAL_SOURCE)
	{
		// remove this tile from its room. if this creates empty room, remove it.
		Room* oldRoom = 0;
		int oldID = getRoomID(tile, type);

		if (oldID > 0 && type == Room::EXIT_ROOM)
			return -1;

		// tile updates are handled by standard procedure
		std::vector<PositionTilePair> tiles;
		if(removeTile(type, position, oldID, tiles) < 0)
			return -1;
	}

		
	int roomID = -1;
	std::vector<int> n;
	int i = 0;

	if(type == Room::FAUNAL_SOURCE)
	{
		// these don't merge
		i = 6;
	}
	else
	{
		// search for neighbour with same roomtype and assign this tile to it
		for(Ogre::Vector2 d : Tile::getNeighbours(position))
		{
			Room* r = getRoom(position + d, type);
			if (r) n.push_back(r->getID());
			else n.push_back(-1);
		}

		assert(n.size() == 6);

		for(; i < 6; ++i)
		{
			if(n[i] >= 0)
			{
				Room* r = getRoom(n[i]);
				if (r->getType() == type)
				{
					roomID = n[i];
					std::map<int, RoomInfo>::iterator iter = mRooms.find(n[i]);
					iter->second.tiles.insert(tile);
					setRoomID(tile, type, n[i]);
					break;
				}
			}
		}
	}

	if(i == 6)
	{
		// if no neighbour with this roomtype found or not merging create a new one
		roomID = mLastRoomID;
		Room* newRoom = 0;
		switch(type)
		{
		case Room::THRONE_CHAMBER:
			newRoom = new0 ThroneChamber(roomID);
			break;
		case Room::BROOD_CHAMBER:
			newRoom = new0 BroodChamber(roomID);
			break;
		case Room::STORAGE_CHAMBER:
			newRoom = new0 StorageChamber(roomID);
			break;
		case Room::FUNGI_CHAMBER:
			newRoom = new0 FungiChamber(roomID);
			break;
		case Room::FAUNAL_SOURCE:
			newRoom = new0 FaunalSource(roomID);
			break;
		case Room::FLORAL_SOURCE:
			newRoom = new0 FloralSource(roomID);
			break;
		case Room::COPAL_SOURCE:
			newRoom = new0 CopalSource(roomID);
			break;
		case Room::EXIT_ROOM:
			newRoom = new0 ExitRoom(roomID);
			break;
		case Room::SPIDER_SPAWNER:
			newRoom = new0 SpiderSpawner(roomID);
			break;
		case Room::BOMBARDIER_BEETLE_SPAWNER:
			newRoom = new0 BombardierBeetleSpawner(roomID);
			break;
		case Room::LADYBUG_SPAWNER:
			newRoom = new0 LadybugSpawner(roomID);
			break;
		case Room::EARTHWORM_SPAWNER:
			newRoom = new0 EarthwormSpawner(roomID);
			break;
		default:
			break;
		}
		setRoomID(tile, type, roomID);
		addRoom(newRoom);
		std::map<int, RoomInfo>::iterator iter = mRooms.find(roomID);
		iter->second.tiles.insert(tile);
		mLastRoomID++;
	}
	else
	{
		// search remaining neighbours to merge this room into
		for(int j = i + 1; j < 6; ++j)
		{
			if (n[j] >= 0)
			{
				Room* r = getRoom(n[j]);

				if (r->getType() == type)
				{
					int otherID = n[j];
					int currentID = roomID;
					if (otherID != roomID)
						roomID = mergeRooms(otherID, currentID);

					// clear this handled room from the array
					for (int k = j + 1; k < 6; ++k)
					{
						if (n[k] == otherID || n[k] == currentID)
							n[k] = -1;
					}
				}
			}
		}
	}
	return roomID;
}

int RoomMap::getRoomID(int tile, Room::RoomType type) const
{
	switch(type)
	{
	case Room::EXIT_ROOM:
		return mExitMap[tile];
	case Room::FAUNAL_SOURCE:
		{
			std::map<int,int>::const_iterator iter = mFaunalMap.find(tile);
			return (iter != mFaunalMap.end()) ? iter->second : -1;
		}
	case Room::COPAL_SOURCE:
	case Room::FLORAL_SOURCE:
		return mResourceMap[tile];
	default:
		return mRoomMap[tile];
	}
}
void RoomMap::setRoomID(int tile, Room::RoomType type, int id)
{
	switch(type)
	{
	case Room::EXIT_ROOM:
		mExitMap[tile] = id;
		break;
	case Room::FAUNAL_SOURCE:
		if(id >=0)
		{
			mFaunalMap.insert(std::make_pair(tile, id));
		}
		else
		{
			std::map<int,int>::const_iterator iter = mFaunalMap.find(tile);
			if(iter != mFaunalMap.end()) mFaunalMap.erase(iter);
		}
		break;
	case Room::COPAL_SOURCE:
	case Room::FLORAL_SOURCE:
		mResourceMap[tile] = id;
		break;
	default:
		mRoomMap[tile] = id;
		break;
	}
}
Room* RoomMap::getRoom(int roomID) const
{
	std::map<int, RoomInfo>::const_iterator iter = mRooms.find(roomID);
	return iter != mRooms.end() ? iter->second.room : 0;
}
Room* RoomMap::getRoom(const Ogre::Vector2& position, Room::RoomType type) const
{
	switch(type)
	{
	case Room::EXIT_ROOM:
		return getExit(position);
	case Room::FAUNAL_SOURCE:
		return getFaunalRoom(position);
	case Room::COPAL_SOURCE:
	case Room::FLORAL_SOURCE:
		return getResourceRoom(position);
	default:
		return getRoom(position);
	}
}
Room* RoomMap::getRoom(const Ogre::Vector2& position) const
{
	if(position.y < 0 || position.x < 0 || position.x >= mWidth || position.y >= mHeight)
		return 0;
	const unsigned int tile = mWidth*((int)position.y) + position.x;
	int id = mRoomMap[tile];
	if(id < 0)
		return 0;
	return getRoom(id);
}
Room* RoomMap::getExit(const Ogre::Vector2& position) const
{
	if(position.y < 0 || position.x < 0 || position.x >= mWidth || position.y >= mHeight)
		return 0;
	const unsigned int tile = mWidth*((int)position.y) + position.x;
	int id = mExitMap[tile];
	if(id < 0)
		return 0;
	return getRoom(id);
}
Room* RoomMap::getResourceRoom(const Ogre::Vector2& position) const
{
	if(position.y < 0 || position.x < 0 || position.x >= mWidth || position.y >= mHeight)
		return 0;
	const unsigned int tile = mWidth*((int)position.y) + position.x;
	int id = mResourceMap[tile];
	if(id < 0)
		return 0;
	return getRoom(id);
}
Room* RoomMap::getFaunalRoom(const Ogre::Vector2& position) const
{
	if(position.y < 0 || position.x < 0 || position.x >= mWidth || position.y >= mHeight)
		return 0;
	const unsigned int tile = mWidth*((int)position.y) + position.x;
	typedef std::multimap<int, int>::const_iterator iterator;
	std::pair<iterator, iterator> ret = mFaunalMap.equal_range(tile);
	if (ret.first != ret.second)
	{
		FaunalSource* room = 0;
		for (iterator iter = ret.first; iter != ret.second; ++iter)
		{
			room = (FaunalSource*)getRoom(iter->second);
			if (room->getSource() != 0)
				break;
		}
		return room;
	}
	else
		return 0;
}
Room* RoomMap::getFirstRoom(Room::RoomType type) const
{
	if(!mRoomTypes[type].empty())
	{
		return getRoom(*mRoomTypes[type].begin());
	}
	else return 0;
}
bool RoomMap::hasRooms(Room::RoomType type) const
{
	return !mRoomTypes[type].empty();
}
roomIterator RoomMap::getRoomIter(Room::RoomType type) const
{
	return mRoomTypes[type].begin();
}
roomIterator RoomMap::getRoomIterEnd(Room::RoomType type) const
{
	return mRoomTypes[type].end();
}
ThroneChamber* RoomMap::getThroneChamber() const
{
	int i = *mRoomTypes[Room::THRONE_CHAMBER].begin();
	return (ThroneChamber*)getRoom(i);
}

void RoomMap::findDisconnectedComponents(int roomID, std::vector<std::set<int>>& components)
{
	RoomInfo info = mRooms.find(roomID)->second;
	std::set<int> allTiles;
	std::set<int>::iterator iter, iend;
	iter = info.tiles.begin(); iend = info.tiles.end();
	for (; iter != iend; ++iter) allTiles.insert(*iter);

	while (!allTiles.empty())
	{
		std::set<int> currentComp;
		std::vector<int> nextLevel;
		std::vector<int> currentLevel;
		currentLevel.push_back(*allTiles.begin());

		while (!currentLevel.empty())
		{
			int tile = currentLevel.back();
			currentLevel.pop_back();

			if (allTiles.find(tile) != allTiles.end() &&
				currentComp.find(tile) == currentComp.end())
			{
				currentComp.insert(tile);
				allTiles.erase(tile);

				// explore neighbours
				Ogre::Vector2 u(tile % mWidth, tile / mWidth);
				for (Ogre::Vector2 d : Tile::getNeighbours(u))
				{
					Ogre::Vector2 pos = u + d;
					if (pos.x < 0 || pos.x >= mWidth
						|| pos.y < 0 || pos.y >= mHeight)
						continue;

					int p = ((int)pos.y)*mWidth + pos.x;
					if (info.tiles.find(p) != info.tiles.end())
					{
						nextLevel.push_back(p);
					}
				}
			}

			if (currentLevel.empty()) std::swap(currentLevel, nextLevel);
		}

		if (!currentComp.empty())
			components.push_back(currentComp);
	}
}
int RoomMap::removeTile(Room::RoomType type, const Ogre::Vector2& position,
	int roomID, std::vector<PositionTilePair>& tiles)
{
	const unsigned int tile = mWidth*((int)position.y) + position.x;
	// remove this tile from its room. if this creates empty room, remove it.
	if(roomID >= 0)
	{
		RoomInfo info = mRooms.find(roomID)->second;
		info.tiles.erase(tile);
		mRooms[roomID] = info;
		tiles.push_back(std::make_pair(position, Tile::msTiles[Tile::FLOOR]));
		if(info.tiles.empty())
		{
			info.room->getWorkers(mPendingWorkers);
			info.room->unassignWorkers();

			info.room->getSlotEntities(mPendingSlotEntities);
			mRoomTypes[info.room->getType()].erase(roomID);
			SafeDelete0(info.room);
			mRooms.erase(roomID);
			roomID = -1;
		}
		else
		{
			bool potentialDisconnect;
			{
				int up = 0;
				int down = 0;
				bool first = true;
				bool lastUp = false;
				for (Ogre::Vector2 delta : Tile::getNeighbours(position))
				{
					Ogre::Vector2 p = position + delta;
					const unsigned int neighbour = mWidth*((int)p.y) + p.x;
					int nID = mRoomMap[neighbour];

					if (first)
					{
						first = false;
					}
					else
					{
						if (nID == roomID && !lastUp)
						{
							up++;
						}
						if (nID != roomID && lastUp)
						{
							down++;
						}
					}
					lastUp = nID == roomID;
				}
				potentialDisconnect = down > 1 || up > 1;
			}

			if (potentialDisconnect)
			{
				std::vector<std::set<int>> components;
				findDisconnectedComponents(roomID, components);
				if (components.size() > 1)
				{
					for (int i = 1; i < (int)components.size(); ++i)
					{
						int newRoomID = mLastRoomID;
						Room* newRoom = 0;
						Room::RoomType newRoomType = info.room->getType();
						switch (newRoomType)
						{
						case Room::THRONE_CHAMBER:
							newRoom = new0 ThroneChamber(newRoomID);
							break;
						case Room::BROOD_CHAMBER:
							newRoom = new0 BroodChamber(newRoomID);
							break;
						case Room::STORAGE_CHAMBER:
							newRoom = new0 StorageChamber(newRoomID);
							break;
						case Room::FUNGI_CHAMBER:
							newRoom = new0 FungiChamber(newRoomID);
							break;
						case Room::FAUNAL_SOURCE:
							newRoom = new0 FaunalSource(newRoomID);
							break;
						case Room::FLORAL_SOURCE:
							newRoom = new0 FloralSource(newRoomID);
							break;
						case Room::COPAL_SOURCE:
							newRoom = new0 CopalSource(newRoomID);
							break;
						case Room::EXIT_ROOM:
							newRoom = new0 ExitRoom(newRoomID);
							break;
						case Room::SPIDER_SPAWNER:
							newRoom = new0 SpiderSpawner(newRoomID);
							break;
						case Room::BOMBARDIER_BEETLE_SPAWNER:
							newRoom = new0 BombardierBeetleSpawner(newRoomID);
							break;
						case Room::LADYBUG_SPAWNER:
							newRoom = new0 LadybugSpawner(newRoomID);
							break;
						case Room::EARTHWORM_SPAWNER:
							newRoom = new0 EarthwormSpawner(newRoomID);
							break;
						default:
							break;
						}
						addRoom(newRoom);
						std::map<int, RoomInfo>::iterator rIter = mRooms.find(newRoomID);
						
						std::set<int>::iterator iter, iend;
						iter = components[i].begin(); iend = components[i].end();
						Ogre::Vector2 newPivot(*iter % mWidth, *iter / mWidth);
						for (; iter != iend; ++iter)
						{
							setRoomID(*iter, newRoomType, newRoomID);
							info.tiles.erase(*iter);
							rIter->second.tiles.insert(*iter);
						}
						mRooms[roomID] = info;
						buildRoom(newRoomID, newPivot, tiles);
						mLastRoomID++;
					}
				}
			}
		}
	}
	if(type == Room::NONE)
	{
		if(roomID > 0)
		{
			buildRoom(roomID, position, tiles);
		}
		return -1;
	}
	return 0;
}
int RoomMap::mergeRooms(int roomID0, int roomID1)
{
	std::map<int, RoomInfo>::iterator targetIter = mRooms.find(roomID0);
	std::map<int, RoomInfo>::iterator sourceIter = mRooms.find(roomID1);

	// merge smaller into bigger room
	int targetID = roomID0;
	int sourceID = roomID1;
	if(targetIter->second.tiles.size() < sourceIter->second.tiles.size())
	{
		std::swap(targetIter, sourceIter);
		std::swap(targetID, sourceID);
	}

	Room* targetRoom = targetIter->second.room;
	Room* sourceRoom = sourceIter->second.room;

	// add tiles to target room and mark them in map
	for(int t : sourceIter->second.tiles)
	{
		setRoomID(t, targetRoom->getType(), targetID);
		targetIter->second.tiles.insert(t);
	}
	
	// transfer guests to targetRoom
	targetRoom->bookGuests(sourceRoom->getGuests());

	// remove other room
	sourceRoom->getWorkers(mPendingWorkers);
	sourceRoom->unassignWorkers();
	sourceRoom->getSlotEntities(mPendingSlotEntities);
	mRoomTypes[sourceRoom->getType()].erase(sourceID);
	SafeDelete0(sourceRoom);
	mRooms.erase(sourceIter);
	return targetID;
}

void RoomMap::addRoom(Room* room)
{
	int id = room->getID();
	RoomInfo info;
	info.room = room;

	mRooms.insert(std::make_pair(id, info));
	mRoomTypes[room->getType()].insert(id);
}
#include "AnPCH.h"
#include "AnFramework.h"
#include "AnProceduralGeometry.h"
#include "AnOptions.h"
#include "AnDeserializer.h"
using namespace Ants;

template<> Framework* Ogre::Singleton<Framework>::msSingleton = 0;

Framework::Framework()
{
    mRoot			= 0;
    mRenderWnd		= 0;
    mViewport		= 0;
    mLog			= 0;

    mInputMgr		= 0;
    mKeyboard		= 0;
    mMouse			= 0;

	mFSLayer = OGRE_NEW_T(Ogre::FileSystemLayer, Ogre::MEMCATEGORY_GENERAL)("Antcraft");
	WritableFolder = mFSLayer->getWritablePath(Ogre::BLANKSTRING);

	mMouseOff = false;

	new0 Options();
}
Framework::~Framework()
{
    Framework::getSingletonPtr()->mLog->logMessage("Shutdown OGRE...");

	Ogre::String userDataFile = Framework::getSingleton().mFSLayer->getWritablePath("userData.ants");
	Serializer serializer;
	serializer.insert(mHighscores);
	serializer.insert(mAchievements);
	serializer.insert(mAllTimeStats);
	serializer.toFile(userDataFile);
	delete0(mHighscores);
	delete0(mAchievements);
	delete0(mAllTimeStats);


	ProceduralGeometry* pmgr = ProceduralGeometry::getSingletonPtr();
	SafeDelete0(pmgr);

    if(mInputMgr)	OIS::InputManager::destroyInputSystem(mInputMgr);
	SafeDelete0(mRoot);
	SafeDelete0(mLog);

	Options* opt = Options::getSingletonPtr();
	opt->save(mCfgPath);
	SafeDelete0(opt);
}

bool Framework::initRenderer()
{
	Ogre::RenderSystemList renderers = Ogre::Root::getSingleton().getAvailableRenderers();

	if (renderers.empty())
		return false;

	Ogre::RenderSystemList::iterator iter, iend;
	iter = renderers.begin(); iend = renderers.end();
	Ogre::RenderSystem* renderSystem = *iter;
	for (; iter != iend; ++iter)
	{
		Ogre::String name = (*iter)->getName();
		if (name == Options::getSingleton().Renderer())
		{
			renderSystem = *iter;
			break;
		}
	}

	Ogre::ConfigOptionMap options = renderSystem->getConfigOptions();
	if (options.find("Colour Depth") != options.end())
	{
		renderSystem->setConfigOption("Video Mode", Options::getSingleton().VideoMode());
		renderSystem->setConfigOption("Colour Depth", Options::getSingleton().ColourDepth());
	}
	else
	{
		Ogre::String videoMode = Options::getSingleton().VideoMode() +
			" @ " + Options::getSingleton().ColourDepth() + "-bit colour";
		renderSystem->setConfigOption("Video Mode", videoMode);
	}
	renderSystem->setConfigOption("Full Screen", Options::getSingleton().FullScreen());
	renderSystem->setConfigOption("VSync", Options::getSingleton().VSync());
	renderSystem->setConfigOption("FSAA", Options::getSingleton().FSAA());

	Ogre::Root::getSingleton().setRenderSystem(renderSystem);
	return true;
}
bool Framework::init(Ogre::String wndTitle, OIS::KeyListener *pKeyListener, OIS::MouseListener *pMouseListener)
{
	srand(time(0));

    OGRE_NEW Ogre::LogManager();
    mLog = Ogre::LogManager::getSingleton().createLog("OgreLogfile.log", true, true, false);
	mLog->setDebugOutputEnabled(true);
	mCfgPath = mFSLayer->getWritablePath("Ants.cfg");
	Ogre::String logPath = mFSLayer->getWritablePath("ogre.log");
	Ogre::String pluginsPath = mFSLayer->getConfigFilePath("plugins.cfg");
	Ogre::String resourcesPath = mFSLayer->getConfigFilePath("resources.cfg");
	Ogre::String ceguiResPath = mFSLayer->getConfigFilePath("ceguiRes.cfg");
	
	mRoot = OGRE_NEW Ogre::Root(pluginsPath, mCfgPath, logPath);

	Options::getSingleton().load(mCfgPath);

	if (!initRenderer())
        return false;

    mRenderWnd = mRoot->initialise(true, wndTitle);

    mViewport = mRenderWnd->addViewport(0);
    mViewport->setBackgroundColour(Ogre::ColourValue(0.15f, 0.15f, 0.15f, 1.0f));

    mViewport->setCamera(0);

    mOverlaySystem = new Ogre::OverlaySystem();

    size_t hWnd = 0;
    OIS::ParamList paramList;
    mRenderWnd->getCustomAttribute("WINDOW", &hWnd);

    paramList.insert(OIS::ParamList::value_type("WINDOW", Ogre::StringConverter::toString(hWnd)));

    mInputMgr = OIS::InputManager::createInputSystem(paramList);

    mKeyboard = static_cast<OIS::Keyboard*>(mInputMgr->createInputObject(OIS::OISKeyboard, true));
    mMouse = static_cast<OIS::Mouse*>(mInputMgr->createInputObject(OIS::OISMouse, true));

    mMouse->getMouseState().height = mRenderWnd->getHeight();
    mMouse->getMouseState().width	 = mRenderWnd->getWidth();

    if(pKeyListener == 0)
        mKeyboard->setEventCallback(this);
    else
        mKeyboard->setEventCallback(pKeyListener);

    if(pMouseListener == 0)
        mMouse->setEventCallback(this);
    else
        mMouse->setEventCallback(pMouseListener);

	{
		Ogre::String secName, typeName, archName;
		Ogre::ConfigFile cf;
		cf.load(resourcesPath);

		Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
		while (seci.hasMoreElements())
		{
			secName = seci.peekNextKey();
			const Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
			for (std::pair<Ogre::String, Ogre::String> entry : *settings)
			{
				typeName = entry.first;
				archName = entry.second;
				Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
			}
		}
	}
    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();

    mRenderWnd->setActive(true);
	
	new0 ProceduralGeometry();

	mRenderer = &CEGUI::OgreRenderer::create();
	CEGUI::System::create(*mRenderer);
#ifdef _DEBUG
	CEGUI::Logger::getSingleton().setLoggingLevel(CEGUI::Insane);
#endif
	mGuiContext = &CEGUI::System::getSingleton().getDefaultGUIContext();

	{
		Ogre::String secName, typeName, archName;
		Ogre::ConfigFile cf;
		cf.load(ceguiResPath);

		Ogre::ConfigFile::SectionIterator seci = cf.getSectionIterator();
		CEGUI::DefaultResourceProvider* rp = static_cast<CEGUI::DefaultResourceProvider*>
			(CEGUI::System::getSingleton().getResourceProvider());
		while (seci.hasMoreElements())
		{
			secName = seci.peekNextKey();
			const Ogre::ConfigFile::SettingsMultiMap* settings = seci.getNext();
			for (std::pair<Ogre::String, Ogre::String> entry : *settings)
			{
				typeName = entry.first;
				archName = entry.second;
				rp->setResourceGroupDirectory(typeName, archName);
			}
		}
		CEGUI::ImageManager::setImagesetDefaultResourceGroup("imagesets");
		CEGUI::Font::setDefaultResourceGroup("fonts");
		CEGUI::Scheme::setDefaultResourceGroup("schemes");
		CEGUI::WidgetLookManager::setDefaultResourceGroup("looknfeels");
		CEGUI::WindowManager::setDefaultResourceGroup("layouts");
		CEGUI::ScriptModule::setDefaultResourceGroup("lua_scripts");
		CEGUI::XMLParser* parser = CEGUI::System::getSingleton().getXMLParser();
	}

	CEGUI::SchemeManager::getSingleton().createFromFile("OgreTray.scheme");
	CEGUI::FontManager::getSingleton().createFromFile("DejaVuSans-10.font");
	CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultFont("DejaVuSans-10");
	CEGUI::System::getSingleton().getDefaultGUIContext().
		getMouseCursor().setDefaultImage("TaharezLook/MouseArrow");
	CEGUI::Vector2f mousePos = mGuiContext->getMouseCursor().getPosition();
	mGuiContext->injectMouseMove(-mousePos.d_x, -mousePos.d_y);
	CEGUI::System::getSingleton().getDefaultGUIContext().setDefaultTooltipType((CEGUI::utf8*)"OgreTray/Tooltip");
	CEGUI::Tooltip* tooltip = CEGUI::System::getSingleton().getDefaultGUIContext().getDefaultTooltipObject();
	tooltip->setHoverTime(0.5f); // Display the tooltip after the mouse has been hovering over the widget for half a second
	tooltip->setDisplayTime(10.0f); // Display for 15 seconds then disappear

	// FADEOUT ANIMATION
	if (!CEGUI::AnimationManager::getSingleton().isAnimationPresent("MyFadeOut"))
	{
		CEGUI::Animation* anim = CEGUI::AnimationManager::getSingleton().createAnimation("MyFadeOut");
		anim->setDuration(0.3f);
		anim->setReplayMode(CEGUI::Animation::RM_Once);
		CEGUI::Affector* affector = anim->createAffector("Alpha", "float");
		affector->createKeyFrame(0.0f, "1.0");
		affector->createKeyFrame(0.3f, "0.0", CEGUI::KeyFrame::P_QuadraticDecelerating);
	}
	// FADEIN ANIMATION
	if (!CEGUI::AnimationManager::getSingleton().isAnimationPresent("MyFadeIn"))
	{
		CEGUI::Animation* anim = CEGUI::AnimationManager::getSingleton().createAnimation("MyFadeIn");
		anim->setDuration(0.3f);
		anim->setReplayMode(CEGUI::Animation::RM_Once);
		CEGUI::Affector* affector = anim->createAffector("Alpha", "float");
		affector->createKeyFrame(0.0f, "0.0");
		affector->createKeyFrame(0.3f, "1.0", CEGUI::KeyFrame::P_QuadraticDecelerating);
	}

	// Load Highscores and Achievements
	Ogre::String dataFile = Framework::getSingleton().mFSLayer->getWritablePath("userData.ants");
	Deserializer deserializer;
	deserializer.fromFile(dataFile);
	if (!deserializer.empty())
	{
		mHighscores = static_cast<Highscores*>(deserializer[0]);
		mAchievements = static_cast<Achievements*>(deserializer[1]);
		mAllTimeStats = static_cast<Statistics*>(deserializer[2]);
	}
	else
	{
		mHighscores = new0 Highscores(0);
		mAchievements = new0 Achievements(0);
		mAllTimeStats = new0 Statistics(0);
	}

    return true;
}

bool Framework::keyPressed(const OIS::KeyEvent &keyEventRef)
{
	mGuiContext->injectKeyDown((CEGUI::Key::Scan)keyEventRef.key);
	mGuiContext->injectChar(keyEventRef.text);

    if(mKeyboard->isKeyDown(OIS::KC_SYSRQ))
    {
        mRenderWnd->writeContentsToTimestampedFile("Antcraft_Screenshot_", ".jpg");
        return true;
    }

    return true;
}
bool Framework::keyReleased(const OIS::KeyEvent &keyEventRef)
{
	return mGuiContext->injectKeyUp((CEGUI::Key::Scan)keyEventRef.key);
}

bool Framework::mouseMoved(const OIS::MouseEvent &evt)
{
	return mGuiContext->injectMouseMove(evt.state.X.rel, evt.state.Y.rel);
}
bool Framework::mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	return mGuiContext->injectMouseButtonDown((CEGUI::MouseButton)id);
}
bool Framework::mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id)
{
	return mGuiContext->injectMouseButtonUp((CEGUI::MouseButton)id);
}

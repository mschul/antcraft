#ifndef STORAGE_CHAMBER_H
#define STORAGE_CHAMBER_H

#include "AnLIB.h"
#include "AnTile.h"
#include "AnEntity.h"
#include "AnRoom.h"

namespace Ants
{
	class AN_ITEM StorageChamber : public Room
	{
	public:
		StorageChamber (int id);
		~StorageChamber ();
		
		RoomType getType() const;

		void build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se);
		void update(float dt, ActionReactor* reactor);
		void getJob(std::vector<WorkplaceSubjobs>& jobs);

		bool has(Entity::EntityType type) const;

		Entity* fetch(Entity* caller, slot*& reservedSlot);
		void place(Entity* caller, Entity* item, slot*& reservedSlot);

		int getFloralCount() const;
		int getFaunalCount() const;
		int getCopalCount() const;

	private:
		int mFloral;
		int mFaunal;
		int mCopal;
	};
}

#endif
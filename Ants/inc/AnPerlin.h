#ifndef PERLIN_H
#define PERLIN_H

#include "AnLIB.h"

#define SAMPLE_SIZE 1024

namespace Ants
{
	class AN_ITEM Perlin
	{
	public:
		// standard values
		// octaves [4,8]
		// freq [1, 8]
		// amp -> result [-amp, amp]
		Perlin(int octaves, float freq, float amp, int seed);

		float get(const Ogre::Vector2& position);
		float get(float x,float y);
		float get(float x);
		float getAmplitude();

	private:
		float perlinNoise2D(float vec[2]);
		float perlinNoise1D(float arg);

		float noise1(float arg);
		float noise2(float vec[2]);
		float noise3(float vec[3]);
		void normalize2(float v[2]);
		void normalize3(float v[3]);
		void init(void);

		int   mOctaves;
		float mFrequency;
		float mAmplitude;
		int   mSeed;

		int p[SAMPLE_SIZE + SAMPLE_SIZE + 2];
		float g3[SAMPLE_SIZE + SAMPLE_SIZE + 2][3];
		float g2[SAMPLE_SIZE + SAMPLE_SIZE + 2][2];
		float g1[SAMPLE_SIZE + SAMPLE_SIZE + 2];
		bool  mStart;

	};
}

#endif
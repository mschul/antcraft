#ifndef EARTHWORM_H
#define EARTHWORM_H

#include "AnLIB.h"
#include "AnMob.h"

namespace Ants
{

	class AN_ITEM Earthworm : public Mob
	{
	public:
		Earthworm (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Earthworm (const EntityDesc& desc);
		~Earthworm ();

        virtual void selectBehaviour (float dt);
		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
#ifndef EDITOR_STATE_H
#define EDITOR_STATE_H

#include "AnLIB.h"

#include "AnAppState.h"

namespace Ants
{

	class EditorState : public AppState
	{
	public:
		EditorState();

		DECLARE_GAME_STATE_CLASS(EditorState)

		void enter();
		bool pause();
		void resume();
		void exit();
		void createScene();

		void updateCamera(float dT);
		void getInput();
		void buildGUI();

		bool keyPressed(const OIS::KeyEvent &keyEventRef);
		bool keyReleased(const OIS::KeyEvent &keyEventRef);

		bool mouseMoved(const OIS::MouseEvent &arg);
		bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

		void onLeftPressed(const OIS::MouseEvent &evt);
	
		void handleInput(float dT);
		void update(float dT);

	private:
		bool						mQuit;

		Ogre::Vector3				mTranslateVector;
		Ogre::Real					mMoveSpeed;
		Ogre::Degree				mRotateSpeed;
		float						mMoveScale;
		Ogre::Degree				mRotScale;

		Ogre::RaySceneQuery*		mRSQ;
		Ogre::SceneNode*			mCurrentObject;
		bool						mLMouseDown, mRMouseDown;
		bool						mFollowCam;

		Ogre::SceneNode* mCameraNode;
	};
}

#endif

#ifndef AOE_H
#define AOE_H

#include "AnLIB.h"
#include "AnCharacter.h"

namespace Ants
{

	class AN_ITEM AOE
	{
	public:
		AOE(ObjectID source, int sourceClientID,
			float attackPoints, const Ogre::Vector2& pos, unsigned int radius);
		AOE(ObjectID source, int sourceClientID,
			float attackPoints, const Ogre::Vector2& pos,
			const std::vector<Ogre::Vector2>& positions);
		~AOE();

		float getAttackPoints() const;
		Ogre::Vector2 getSourcePosition() const;
		int getSourceClientID() const;
		ObjectID getSourceID() const;
		std::vector<Ogre::Vector2>& getPositions();

	private:
		std::vector<Ogre::Vector2> mPositions;
		float mAttackPoints;
		Ogre::Vector2 mSourcePosition;
		int mSourceClientID;
		ObjectID mSourceID;
	};
}

#endif
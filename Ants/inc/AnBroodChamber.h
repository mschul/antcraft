#ifndef BROOD_CHAMBER_H
#define BROOD_CHAMBER_H

#include "AnLIB.h"
#include "AnTile.h"
#include "AnEntity.h"
#include "AnRoom.h"

namespace Ants
{
	class AN_ITEM BroodChamber : public Room
	{
	public:
		BroodChamber (int id);
		~BroodChamber ();
		
		RoomType getType() const;

		void build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se);
		void update(float dt, ActionReactor* reactor);
		void getJob(std::vector<WorkplaceSubjobs>& jobs);

		void setProduction(Entity::EntityType type);
		Entity::EntityType getProduction() const;

		void getDemand(int& floral, int& faunal, int& copal);
		void getReserve(int& floral, int& faunal, int& copal);

		void feed(Entity* ent);

	private:
		Entity::EntityType mProduction;

		void checkValid(const Ogre::String& caption);

		int mGrubCount;
		int mFloralReserve;
		int mFaunalReserve;
		int mCopalReserve;
	};
}

#endif
#ifndef ANGAME_H
#define ANGAME_H

#include "AnFramework.h"
#include "AnAppStateManager.h"

namespace Ants
{
	class GameApplication
	{
	public:
		GameApplication();
		~GameApplication();

		void start();
		Ogre::String getWritableFolder() const;

	private:
		AppStateManager*	mAppStateManager;
	};
}

#endif
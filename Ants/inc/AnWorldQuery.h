#ifndef WORLD_QUERY_H
#define WORLD_QUERY_H

#include "AnLIB.h"

#include "AnWorldMap.h"
#include "AnPerlin.h"
#include "AnPheromoneMap.h"
#include "AnJobQuery.h"
#include <boost/heap/fibonacci_heap.hpp>

namespace Ants
{
	struct score
	{
		float fscore;
		float gscore;
		float randomizer;
		int cameFrom;
		int length;
		Ogre::Vector2 position;

		struct compareScore
		{
			bool operator()(const score* l, const score* r) const
			{
				if(l->fscore == r->fscore)
					return l->randomizer > r->randomizer;
				return l->fscore > r->fscore;  
			}
		};

		typedef boost::heap::fibonacci_heap<score*, boost::heap::compare<score::compareScore>> priorityqueue;
		priorityqueue::handle_type handle;
	};

	class AN_ITEM WorldQuery
	{
	public:
		WorldQuery(const WorldMap* world);
		~WorldQuery();
		
		bool isBlocking(const Ogre::Vector2& pos) const;
		bool isBlocking(const Ogre::Vector2& pos, int radius) const;

		bool isRoomPlacable(const Ogre::Vector2& pos) const;

		float damagePerSecond(const Ogre::Vector2& position) const;

		// astar
		void findShortestPath(const Ogre::Vector2& start,
			const Ogre::Vector2& target, std::vector<Ogre::Vector2>& path, float maxLength = 100000.f) const;
		void findEscapePath(const Ogre::Vector2& start,
			const Ogre::Vector2& enemy, std::vector<Ogre::Vector2>& path, int length) const;
		void findReachPath(const Ogre::Vector2& start,
			const Ogre::Vector2& targetPosition, std::vector<Ogre::Vector2>& path, int depth) const;
		bool isShootable(const Ogre::Vector2& start, const Ogre::Vector2& targetPosition) const;

		void findNextTile(const Ogre::Vector2& start,
			const std::vector<Tile::TileType>& allowedTiles,
			Ogre::Vector2& targetPosition, int depth = 40) const;
		bool findNextZeroTile(const Ogre::Vector2& start,
			Ogre::Vector2& targetPosition, int depth = -1) const;
		void findNextJob(const Ogre::Vector2& start,
			JobQuery* jobQuery,
			Ogre::Vector2& targetPosition, int depth = 40) const;

		void generateRandomWalk(const Ogre::Vector2& start,
			int length, std::vector<Ogre::Vector2>& path) const;
		void generateRandomWalk(const Ogre::Vector2& start,
			int length, std::vector<Ogre::Vector2>& path, Perlin* noise) const;
		void generateRandomWalk(const Ogre::Vector2& start,
			int length, std::vector<Ogre::Vector2>& path, PheromoneMap* pheromones) const;
		void generateRestrictedPath(const Ogre::Vector2& start,
			int length, std::vector<Ogre::Vector2>& path, PheromoneMap* pheromones,
			const std::vector<Tile::TileType>& allowedTiles) const;
		void generateRandomWalkOnJobs(const Ogre::Vector2& start,
			int length, std::vector<Ogre::Vector2>& path,
			PheromoneMap* pheromones, JobQuery* jobQuery) const;
		
		void hexLine(const Ogre::Vector2& start, const Ogre::Vector2& target,
			std::vector<Ogre::Vector2>& path) const;
		void hexCircle(const Ogre::Vector2& start, const Ogre::Vector2& border, 
			std::vector<Ogre::Vector2>& path);

	private:

		void backtrack(const Ogre::Vector2& u, const Ogre::Vector2& v, 
			const std::map<int, score*>& scoreMap, std::vector<Ogre::Vector2>& path) const;
		void backtrack(const Ogre::Vector2& u, const Ogre::Vector2& v, 
			const std::map<int, int>& cameFromMarks, std::vector<Ogre::Vector2>& path) const;
		void backtrack(const Ogre::Vector2& u, 
			const std::map<int, score*>& scoreMap, std::vector<Ogre::Vector2>& path) const;

		void exploreNeighbours(std::vector<Ogre::Vector2>& neighbours, const Ogre::Vector2& position) const;
		void exploreNeighbours(std::vector<Ogre::Vector2>& neighbours, const Ogre::Vector2& position,
			const Ogre::Vector2& exclude) const;
		void exploreNeighboursGrid(std::vector<Ogre::Vector2>& neighbours, const Ogre::Vector2& position) const;
		void exploreNeighboursGrid(std::vector<Ogre::Vector2>& neighbours, const Ogre::Vector2& position,
			const Ogre::Vector2& exclude) const;

		const WorldMap* mWorld;
	};
}

#endif
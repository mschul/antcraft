#ifndef SMALL_WORKER_H
#define SMALL_WORKER_H

#include "AnLIB.h"
#include "AnAnt.h"

namespace Ants
{
	class AN_ITEM SmallWorker : public Ant
	{
	public:
		SmallWorker (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		SmallWorker (const EntityDesc& desc);
		~SmallWorker ();
		
		void selectBehaviour(float dt);

		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
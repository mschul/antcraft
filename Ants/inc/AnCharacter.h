#ifndef CHARACTER_H
#define CHARACTER_H

#include "AnLIB.h"
#include "AnEntity.h"
#include "AnAttack.h"
#include "AnDigJob.h"
#include "AnGotoJob.h"
#include "AnJobQuery.h"
#include "AnEntityQuery.h"
#include "AnWorldQuery.h"
#include "AnRoomQuery.h"
#include "AnPheromoneMap.h"
#include "AnPerlin.h"

namespace Ants
{
	class AN_ITEM Character : public Ants::Entity
	{
	public:

		enum State
		{
			IDLE_STANDING,
			IDLE_WALKING,
			FIND_JOB,
			GET_JOBINFORMATION,
			FIND_PATH,
			REACH_JOB,
			WORK_JOB,
		};

		Character (int id, int clientID,
					  const Ogre::Vector2& position,
					  const Ogre::Vector2& tilePosition, 
					  int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Character (const EntityDesc& desc);
		virtual ~Character();
		
		static Perlin* msMovementPerlin;

		virtual void set(const EntityDesc& desc);
		virtual void update(float dt, ActionReactor* reactor);
		virtual void selectBehaviour(float dt) = 0;
		virtual void die(ActionReactor* reactor) = 0;
		virtual void move(float dt, ActionReactor* reactor) = 0;

		// note: this is only used to kill puppets (and mask them from updateEnvironment)
		// use die for your own entities
		void setAlive(bool alive);
		void updateEnvironment(float dt);

		virtual void applyUpgrade(Upgrade u);

		virtual bool isCharacter() const;
		bool isAlive() const;

		int meleeAttack(float dt, Character* enemy);
		int rangedAttack(float dt, Character* enemy);
		int aoeAttack(float dt);
		int getMeleeDamage(float dt);
		int getRangedDamage(float dt);

		void applyDamage(Attack* a, ActionReactor* reactor);

	protected:
		bool walkWaypoint(float dt);

		bool findPath(Room* room);
		void randomPath();
		void restrictedPath(const std::vector<Tile::TileType>& allowedTiles);
		void escape(const Ogre::Vector2& position);
		void approach(const Ogre::Vector2& position);
		void clearPath();
		void savePath(const std::vector<Ogre::Vector2>& path);
		
		bool mAlive;

		bool mRanged;
		bool mAOE;
		int mAOEDamage;
		int mMeleeDamage;
		int mRangedDamage;
		float mAttackTime;

		// query datastructures
		WorldQuery* mWorldQuery;
		JobQuery* mJobQuery;
		EntityQuery* mEntityQuery;
		RoomQuery* mRoomQuery;
		PheromoneMap* mPheromoneMap;

		// environment
		std::vector<Character*> mEnemies;

		// movement system / state machine
		float mSpeed;
		float mMaxSpeed;
		std::queue<Ogre::Vector2> mPath;
		std::queue<Ogre::Vector2> mPathNoise;
		float mStateUpdateDelta;
		State mCurrentState;
	};
}

#endif
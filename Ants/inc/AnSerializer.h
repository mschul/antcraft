#ifndef SERIALIZER_H
#define SERIALIZER_H

#include "AnLIB.h"
#include "AnIO.h"

namespace Ants
{
	class GameObject;
	class AN_ITEM Serializer
	{
	public:
		Serializer();
		virtual ~Serializer();
		
		bool insert(GameObject* obj);

		void toMemory(int& size, char*& buffer);
		bool toFile(const Ogre::String& filename);

		template<typename T> bool write(T& datum);
		bool writeString(const Ogre::String& datum);
		bool writeBool(const bool datum);
		template <typename T> bool writePointer(const T* object);

		bool registerRoot(const GameObject* obj);
		template <typename T> void registerObject(const T* obj);

		void writeUniqueID(const GameObject* obj);

	private:
		bool isTopLevel(const GameObject* object) const;

		typedef std::map<const GameObject*, unsigned int> RegisterMap;
		typedef std::vector<const GameObject*> RegisterArray;

		std::vector<GameObject*> mObjects;

		RegisterMap mRegistered;
		RegisterArray mOrdered;
		IO mIO;
	};

	template<typename T> bool Serializer::write(T& datum)
	{
		return mIO.write(sizeof(T), &datum);
	}

	template <typename T>
	bool Serializer::writePointer(const T* object)
	{
		RegisterMap::iterator iter = mRegistered.find(object);
		if (iter != mRegistered.end())
		{
			unsigned int uniqueID = iter->second;
			mIO.write(sizeof(unsigned int), &uniqueID);
			return true;
		}
		return false;
	}
	
	template <typename T>
	void Serializer::registerObject(const T* obj)
	{
		if (obj)
		{
			obj->registerObjects(*this);
		}
	}
}

#endif
#ifndef WATER_MAP_H
#define WATER_MAP_H

#include "AnLIB.h"
#include "AnWorldMap.h"

namespace Ants
{
	class AN_ITEM WaterMap
	{
	public:
		WaterMap (WorldMap* world);
		~WaterMap ();
		
		void build();
		void update(std::vector<PositionTilePair>& tiles);
		void remove(const std::vector<Ogre::Vector2>& tiles);
		void remove(const Ogre::Vector2& tile);

	protected:
		Tile::Direction fallWater(int index, std::vector<PositionTilePair>& tiles,
							 bool* boundaries, bool* water, int* pressures);
		Tile::Direction freeFallWater(int index, std::vector<PositionTilePair>& tiles,
							 bool* boundaries, bool* water, int* pressures);
		void flowWater(int index, std::vector<PositionTilePair>& tiles);

		WorldMap* mWorld;
		std::vector<int> mPressure;
		std::vector<bool> mCurrentInactive;
		std::vector<bool> mNextInactive;
		int mWaterCount;
		bool mBuilt;
	};
}

#endif
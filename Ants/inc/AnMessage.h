#ifndef MESSAGE_H
#define MESSAGE_H

#include "AnLIB.h"

namespace Ants
{

	class AN_ITEM DisplayMessage
	{
	public:
		DisplayMessage(const Ogre::String& title);
		~DisplayMessage();

		void update(float dt);
		bool isAlive() const;

		Ogre::String getTitle() const;

	private:
		Ogre::String mTitle;
		float mDisplayTime;
	};

	class AN_ITEM Message
	{
	public:
		enum MessageType
		{
			MT_KILL_NOTIFICATION,
			MT_ACHIEVEMENT_NOTIFICATION,
			MT_CHAT_MESSAGE
		};

		Message(MessageType type, int receiverID);
		~Message();

		void addDatum(const Ogre::String& name, const Ogre::String& value);

		Ogre::String getDatum(const Ogre::String& name) const;
		MessageType getType() const;
		int getReceiver() const;

	private:
		MessageType mType;
		int mReceiverID;
		std::map<Ogre::String, Ogre::String> mData;
	};
}

#endif
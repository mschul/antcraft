#ifndef DRAWABLE_WORLD_MAP_H
#define DRAWABLE_WORLD_MAP_H

#include "AnLIB.h"
#include "AnWorldMap.h"

namespace Ants
{

	class AN_ITEM DrawableWorldMap : public WorldMap
	{
	public:
		DrawableWorldMap();
		~DrawableWorldMap();

		void initScene(Ogre::SceneManager* sceneMgr);

		void updatePages();

		void setVisible(bool visibility);

		bool setTile(const Ogre::Vector2& position, Tile* tile, bool tracking = false);
		bool setTiles(const std::vector<Ogre::Vector2>& positions,
			const std::vector<Tile*>& tiles, bool tracking = false);
		bool setTiles(const std::vector<PositionTilePair>& tiles, bool tracking = false);

		Ogre::Rect getViewRect() const;
		void setViewRect(const Ogre::Rect& rect);

		void redrawMinimap();

	private:
		Ogre::Rect mViewRect;

		int minX, minY, maxX, maxY;

		unsigned int mPageSize;
		WorldPage** mPages;

		Ogre::SceneManager* mSceneManager;
		Ogre::TexturePtr mMiniMapTexture;
		Ogre::MaterialPtr mMiniMapMaterial;
		std::vector<Ogre::InstanceManager*> mInstanceManagers;

		bool mUseInstancing;
	};
}

#endif
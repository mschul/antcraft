#ifndef ANT_IDLE_TRAIT_H
#define ANT_IDLE_TRAIT_H

#include "AnLIB.h"
#include "AnBehaviourTrait.h"

namespace Ants
{
	template<class Character>
	class AN_ITEM IdleTrait : public BehaviourTrait<Character>
	{
	public:
		IdleTrait();
		
		virtual void idleWalking(float dt, Character* character, ActionReactor* reactor);
		virtual void idleStanding(float dt, Character* character, ActionReactor* reactor);
		virtual void findJob(float dt, Character* character, ActionReactor* reactor);
		virtual void getJobInfo(float dt, Character* character, ActionReactor* reactor);
		virtual void findPath(float dt, Character* character, ActionReactor* reactor);
		virtual void reachJob(float dt, Character* character, ActionReactor* reactor);
		virtual void workJob(float dt, Character* character, ActionReactor* reactor);
	};
}

#endif
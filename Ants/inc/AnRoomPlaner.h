#ifndef ROOM_PLANER_H
#define ROOM_PLANER_H

#include "AnRoomQuery.h"
#include "AnWorldQuery.h"
#include "AnJobQuery.h"

namespace Ants
{
	class RoomPlaner
	{
		enum RoomGeom
		{
			RG_ROUND,
			RG_RECT
		};
		enum RoomSize
		{
			RS_SMALL,
			RS_MEDIUM,
			RS_LARGE
		};

		RoomPlaner(RoomQuery* rooms, WorldQuery* world, JobQuery* jobs);

		bool findBestSite(Ogre::Vector2& bestSite, RoomGeom geom,
			RoomSize size, Room::RoomType type, int distFromTC = 1);

	private:
		void createMask(const Ogre::Vector2& origin, RoomGeom geom, RoomSize size,
			std::vector<Ogre::Vector2>& mask);
		float evalSiteScore(const Ogre::Vector2& origin, RoomGeom geom, RoomSize size, Room::RoomType type);
		bool isValidSite(const std::vector<Ogre::Vector2>& mask);
		void findBestSite();

		RoomQuery* mRooms;
		WorldQuery* mWorld;
		JobQuery* mJobs;
	};
}

#endif
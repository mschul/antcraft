#ifndef HIGHSCORES_H
#define HIGHSCORES_H

#include "AnLIB.h"
#include "AnGameObject.h"
#include "AnStatistics.h"

namespace Ants
{
	class AN_ITEM Highscores : public GameObject
	{
	public:
		Highscores();
		Highscores(ObjectID id);
		~Highscores();

		void clear();
		void addEntry(Statistics* entry);
		bool isInTopTen(Statistics* entry) const;
		int size() const;
		Statistics* operator[] (const int index);
	private:
		std::vector<Statistics*> mEntries;
		void sort();

		// factory and streaming frontend
	public:
		static const Ogre::String TYPE;
		virtual const Ogre::String& getClassType() const;
		static bool registerFactory();
		static GameObject* Factory(Deserializer& deserializer);
		virtual int getStreamingSize() const;
		static void initializeFactory();
		virtual bool registerObjects(Serializer& serializer) const;
		virtual void serialize(Serializer& serializer) const;
		virtual void resolvePointers(Deserializer& deserializer);
		virtual void deserialize(Deserializer& deserializer);
	private:
		static bool msStreamRegistered;
	};

	static bool gsStreamRegistered_Highscores = Highscores::registerFactory();
}

#endif
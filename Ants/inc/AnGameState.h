#ifndef GAME_STATE_H
#define GAME_STATE_H

#include "AnLIB.h"
#include "AnPlayerClient.h"
#include "AnMobClient.h"
#include "AnServer.h"
#include "AnEntity.h"
#include "AnRoom.h"
#include "AnAppState.h"
#include "AnGameSettings.h"

namespace Ants
{

	class GameState : public AppState
	{
	public:
		static GameSettings Settings;
		static bool GameStatisticsSynched;
		static Statistics LastGameStatistics;

		GameState();

		enum UserTool
		{
			DIG,
			ASSIGN_ROOM,
			ASSIGN_BROOD_CHAMBER,
			ASSIGN_STORAGE_CHAMBER,
			ASSIGN_FUNGI_CHAMBER,
			HOLD,
			DEFEND,
			NONE
		};

		DECLARE_GAME_STATE_CLASS(GameState)

		void enter();
		bool pause();
		void resume();
		void exit();
		void createScene();

		// INPUT ->
		bool keyPressed(const OIS::KeyEvent &keyEventRef);
		bool keyReleased(const OIS::KeyEvent &keyEventRef);

		bool mouseMoved(const OIS::MouseEvent &arg);
		bool mousePressed(const OIS::MouseEvent &arg, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &arg, OIS::MouseButtonID id);

		// callbacks for right, left and right+left mouse button events
		void onLeftPressed(const OIS::MouseEvent &evt, bool handled);
		void onLeftDrag(const OIS::MouseEvent &evt);
		void onLeftReleased(const OIS::MouseEvent &evt, bool handled);

		void onRightPressed(const OIS::MouseEvent &evt, bool handled);
		void onRightDrag(const OIS::MouseEvent &evt);
		void onRightReleased(const OIS::MouseEvent &evt, bool handled);

		void onLRPressed(const OIS::MouseEvent &evt, bool handled);
		void onLRDrag(const OIS::MouseEvent &evt);
		void onLRReleased(const OIS::MouseEvent &evt, bool handled);
		// <-- INPUT

		// GUI -->
		void buildGUI();

		void switchInfoPage(CEGUI::Window* page);
		void showRoom(int roomID);

		// functions to feed ui with data
		void updateStatsPanel();
		void updateInfoLabel();
		void updateNotifications(float dt);

		// callbacks for ui events
		bool onUpgradeShowButtonClicked(const CEGUI::EventArgs &e);
		bool onRoomButtonClicked(const CEGUI::EventArgs &e);
		bool onStatsButtonClicked(const CEGUI::EventArgs &e);
		bool onUpgradeButtonClicked(const CEGUI::EventArgs &e);
		bool onPowerWheelEnter(const CEGUI::EventArgs &e);
		bool onRoomListClicked(const CEGUI::EventArgs &e);
		bool onUnitProductionClicked(const CEGUI::EventArgs &e);
		bool onWorkerBehaviourClicked(const CEGUI::EventArgs &e);
		bool onBackToRoomListButtonClicked(const CEGUI::EventArgs &e);
		bool onHideInfoButtonClicked(const CEGUI::EventArgs &e);
		// <-- GUI

		void takeUpgrade(Entity::Upgrade u);

		bool pickTile(const OIS::MouseEvent &evt, Ogre::Vector2& position);
		bool pickEntity(const OIS::MouseEvent &evt, Entity*& ent);

		// per frame updates and input processing (for camera control)
		void updateCamera(double dT);
		void processInput();
		void update(float dT);

	private:
		bool						mQuit;

		Ogre::Vector3				mTranslateVector;
		Ogre::Real					mMoveSpeed;
		Ogre::Degree				mRotateSpeed;
		float						mMoveScale;
		Ogre::Degree				mRotScale;
		
		Ogre::Vector2				mLastMousePos;
		Ogre::Vector2				mLastMouseDown;
		Ogre::Vector2				mLastPickedPos;
		UserTool					mCurrentUserTool;
		bool						mLMouseDown, mRMouseDown, mCtrlDown, mShiftDown;
		
		bool						mDragCam;

		float						mZoomSpeed;
		float						mZoomFriction;

		Ogre::SceneNode*			mCameraNode;
		Ogre::Vector3				mCameraTargetPos;

		Ogre::RaySceneQuery*		mRSQ;
		int							mCurrentRoomID;
		Ogre::String				mCurrentNodeName;

		Ogre::Vector3				mHomePosition;

		float mNextGuiUpdate;
		float mNextMouseHoldAction;
		bool mMouseHoldActionActive;

		PlayerClient* mPlayer;
		MobClient* mMobs;
		Server* mServer;

		//UI
		CEGUI::Window *mMiniMap;

		std::map<int, CEGUI::PushButton*> mRoomButtons;

		CEGUI::FrameWindow* mInfoTab;

		CEGUI::Window* mPowerWheel;
		CEGUI::AnimationInstance* mPowerWheelFader;

		CEGUI::Window* mCurrentPage;
		CEGUI::Window* mRoomsPage;
		CEGUI::Window* mUpgradesPage;
		CEGUI::Window* mStatsPage;

		CEGUI::Window* mThronePage;
		CEGUI::Window* mStoragePage;
		CEGUI::Window* mBroodPage;
		CEGUI::Window* mFungiPage;
		CEGUI::Window* mCopalPage;
		CEGUI::Window* mFloralPage;
		CEGUI::Window* mExitPage;

		struct Notification { DisplayMessage* m; CEGUI::PushButton* b; };
		std::vector<Notification*> mNotifications;

	};
}

#endif

#ifndef DRAWABLE_ENTITY_MAP_H
#define DRAWABLE_ENTITY_MAP_H

#include "AnLIB.h"
#include "AnEntityMap.h"

namespace Ants
{

	class AN_ITEM DrawableEntityMap : public EntityMap
	{
	public:
		DrawableEntityMap();
		~DrawableEntityMap();

		void initScene(Ogre::SceneManager* sceneMgr, bool fow);

		void add(Entity* ent, bool tracking = false);
		void add(const std::vector<Entity*>& entities, bool tracking = false);
		void change(Entity* ent, bool tracking = false);
		void change(const std::vector<Entity*>& entities, bool tracking = false);
		void remove(Entity* ent, bool tracking = false);
		void remove(const std::vector<Entity*>& entities, bool tracking = false);
		
		void onEntityChange(Entity* ent);

		void setEntityVisible(bool visiblity);
		void setHealthBarsVisible(bool visiblity);
		void toggleHealthBarsVisible();
		void setVisiblityMaskVisible(bool visiblity);

	private:
		FowMaskMap* mFowMaskMap;

		Ogre::SceneManager* mSceneMgr;
		Ogre::SceneNode* mRootNode;
		Ogre::BillboardSet* mHealthBars;
	};
}

#endif
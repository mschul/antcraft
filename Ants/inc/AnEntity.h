#ifndef ENTITY_H
#define ENTITY_H

#include "AnLIB.h"
#include "AnGameObject.h"
#include "AnDigJob.h"
#include "AnRoom.h"
#include "AnPerlin.h"

namespace Ants
{
	class EntityDesc;
	class AN_ITEM Entity : public GameObject
	{
	public:
		enum Upgrade
		{
			U_HORNS = 0,
			U_MOVEMENT_SPEED = 1,
			U_HARVEST_SPEED = 2,
			U_ACID = 3,
			U_CARRIER = 4,
			U_ANTIBIOTICS = 5,
			U_HONEYDEW = 6,
			U_KAMIKAZE = 7,
			U_BULLDOGS = 8,
			U_FUNGI = 9,
			U_COUNT = 10
		};

		enum EntityType
		{
			ET_SMALL_WORKER = 0,
			ET_WORKER = 1,
			ET_CARRIER = 2,
			ET_SOLDIER = 3,
			ET_KAMIKAZE = 4,

			ET_EGG = 5,
			ET_GRUB = 6,
			ET_FLORAL_FOOD = 7,
			ET_FAUNAL_FOOD = 8,
			ET_COPAL = 9,
			ET_DEBRIS = 10,

			ET_LADYBUG = 11,
			ET_EARTHWORM = 12,
			ET_SPIDER = 13,
			ET_BOMBARDIER_BEETLE = 14,

			ET_PROJECTILE = 15,
			ET_COUNT = 16
		};

		Entity (int id, int clientID,
				const Ogre::Vector2& position,
				const Ogre::Vector2& tilePosition, 
				int visibilityRadius);
		Entity (const EntityDesc& desc);
		~Entity ();

		virtual void set(const EntityDesc& desc);

		virtual EntityType getType() const = 0;
		
		virtual Ogre::String getMaterialName() const = 0;
		virtual Ogre::String getStandardMesh() const = 0;

		virtual void update(float dt, ActionReactor* reactor) = 0;
		virtual bool isCharacter() const;
		virtual bool isAnt() const;
		virtual bool isResource() const;

		int getClientID() const;
		int getVisibilityRadius() const;

		Ogre::Radian getOrientation() const;
		Ogre::Vector2 getPosition() const;
		Ogre::Vector2 getTilePosition() const;
		void setPosition(const Ogre::Vector2& position);
		void setTilePosition(const Ogre::Vector2& position);
		
		virtual void applyUpgrade(Upgrade u);

		void setHealth(float health);
		float getHealth() const;
		int getArmor() const;

		void assignSlot(Room::slot* slot);
		Room::slot* getSlot() const;
		bool isPuppet() const;
		bool isChanged() const;
		void resetChangedFlag();

		Entity* getParent() const;
		void setParent(Entity* parent);

	protected:
		bool mChanged;
		bool mPuppet;
		int mClientID;
		int mVisibilityRadius;
		Room::slot* mSlot;

		Entity* mParent;
		Ogre::Radian mOrientation;
		Ogre::Vector2 mPosition;
		Ogre::Vector2 mTilePosition;

		float mHealth;		
		int mMaxHealth;
		int mArmor;
	};
}

#endif
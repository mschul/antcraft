#ifndef JOB_H
#define JOB_H

#include "AnLIB.h"
#include "AnActionReactor.h"

namespace Ants
{
	class Entity;
	class AN_ITEM Job : public GameObject
	{
	public:
		enum JobType
		{
			DIG_JOB = 1,
			DIG_RESOURCE_JOB = 2,
			GOTO_JOB = 4,
			FETCH_JOB = 8,
			FLATTEN_JOB = 16,
			ROOM_CREATE_JOB = 32,
			HOLD_JOB = 64,
			DEFEND_JOB = 128,
            ROOM_CREATE_JOB_EX = 256,
            DESTROY_ANTHILL_JOB = 512
		};
		enum SubJobs
		{
			UPDATE, // just work it
			FETCH, // fetch something
			WALK_TO_TARGET,
			CHECK, // check if there are other jobs available
			PLACE // place something
		};

		Job(ObjectID id, int capacity, const Ogre::Vector2& position);
		virtual ~Job();

		virtual bool update(double dt, ActionReactor* reactor) = 0;

		Ogre::Vector2 getPosition() const;
		
		virtual JobType getType() const = 0;
		virtual void getSubJobs(std::vector<SubJobs>& jobs) const = 0;

		void finish(ActionReactor* reactor);
		bool isAvailable() const;

		void setCapacity(int capacity);
		int getCapacity() const;
		int getRemainingCapacity() const;

	protected:
		friend class JobMap;

		void assign(Entity* ent);
		void unAssign(Entity* ent);

		int mCapacity;
		int mMaximumCapacity;
		Ogre::Vector2 mPosition;
	};
}

#endif
#ifndef FLATTEN_JOB_H
#define FLATTEN_JOB_H

#include "AnLIB.h"

#include "AnJob.h"
#include "AnTile.h"

namespace Ants
{
	class Client;
	class AN_ITEM FlattenJob : public Job
	{
	public:
		FlattenJob (int id, const Ogre::Vector2& position, double duration);
		~FlattenJob ();
		
		bool update(double dt, ActionReactor* reactor);
		JobType getType() const;
		void getSubJobs(std::vector<Job::SubJobs>& jobs) const;

	private:
		double mT;
	};
}

#endif
#ifndef GOTO_JOB_H
#define GOTO_JOB_H

#include "AnLIB.h"

#include "AnJob.h"
#include "AnTile.h"

namespace Ants
{
	class Client;
	class AN_ITEM GotoJob : public Job
	{
	public:
		GotoJob (int id, const Ogre::Vector2& position, const Ogre::Vector2& targetPosition);
		GotoJob (int id, const Ogre::Vector2& position);
		~GotoJob ();
		
		bool update(double dt, ActionReactor* reactor);

		JobType getType() const;
		void getSubJobs(std::vector<Job::SubJobs>& jobs) const;

		Ogre::Vector2 getTargetPosition() const;

	private:
		Ogre::Vector2 mTargetPosition;
	};
}

#endif
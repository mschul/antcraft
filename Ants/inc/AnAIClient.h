#ifndef AI_CLIENT_H
#define AI_CLIENT_H

#include "AnLIB.h"

#include "AnClient.h"
#include "AnStatistics.h"

namespace Ants
{
	class AN_ITEM AIClient : public Client
	{
	public:
		AIClient();
		~AIClient();

		void init(unsigned int width, unsigned int height);
		void init(const Ogre::Rect& area);

		void createThroneChamber(const Ogre::Vector2& position,
			const std::vector<Ogre::Vector2>& throneCircle);
		void createExit(const Ogre::Vector2& exitPosition,
			const std::vector<Ogre::Vector2>& triangle);
		void connect(const Ogre::Vector2& thronePosition,
			const Ogre::Vector2& exitPosition,
			std::vector<Ogre::Vector2>& path);

		void assignWorkers(Room* room);
		void updateWorkerAssignments(Room::RoomType type);

		int assignRoom(Room::RoomType type, const Ogre::Vector2& position, Tile* t);
		void assignRoom(Room::RoomType type, const Ogre::Vector2& position);

		void removeJob(Job* job);
		void removeJobs(int tile, Job::JobType type);
		void createFlattenJob(const Ogre::Vector2& pos);
		void createFlattenJobEnvironment(const Ogre::Vector2& pos);
		void removeStrategy(Job* j);

		// -> Client Impl
		void update(float dt);
		void settle(std::vector<Ogre::Vector2>& locations);
		// <- Client Impl

		// -> Client:ActionReactor Impl
		void jobMilestoneReached(Job* job);
		void jobFinished(Job* job);
		void setExitTiles(const std::vector<std::pair<Ogre::Vector2, Tile*>>& tiles);
		void setTile(const Ogre::Vector2& position, int type);
		void createResource(const Ogre::Vector2& position, int roomType, Character* source = 0);
		void removeResource(Room* r);
		Egg* spawnEgg(const Ogre::Vector2& position);
		Grub* hatchEgg(Egg* egg);
		void unitDied(Character* unit, Attack* a = 0);
		Ant* spawnAnt(Grub* grub, int type);
		Entity* spawn(const Ogre::Vector2& position, int type);
		void removeEntities(const std::vector<Entity*>& ents);
		void aoeAttack(int sourceID, float attackPoints, const Ogre::Vector2& position, float radius);
		void meleeAttack(float attackPoints, Character* source, Entity* target);
		void rangedAttack(float attackPoints, Character* source, Entity* target);
		// <- Client:ActionReactor Impl

	private:
		Statistics* mStatistics;
	};
}

#endif
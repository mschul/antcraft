#ifndef ANFRAMEWORK_H
#define ANFRAMEWORK_H

#include "AnLIB.h"
#include "AnHighscores.h"
#include "AnAchievements.h"

namespace Ants
{

	class Framework : public Ogre::Singleton<Framework>, OIS::KeyListener, OIS::MouseListener
	{
	public:
		Framework();
		~Framework();

		bool initRenderer();
		bool init(Ogre::String wndTitle, OIS::KeyListener *pKeyListener = 0, OIS::MouseListener *pMouseListener = 0);

		bool keyPressed(const OIS::KeyEvent &keyEventRef);
		bool keyReleased(const OIS::KeyEvent &keyEventRef);

		bool mouseMoved(const OIS::MouseEvent &evt);
		bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

		Ogre::Root*					mRoot;
		Ogre::RenderWindow*			mRenderWnd;
		Ogre::Viewport*				mViewport;
		Ogre::Log*					mLog;
		Ogre::FileSystemLayer*		mFSLayer;

		OIS::InputManager*			mInputMgr;
		OIS::Keyboard*				mKeyboard;
		OIS::Mouse*					mMouse;
		bool mMouseOff;

		Ogre::OverlaySystem*        mOverlaySystem;

		Highscores* mHighscores;
		Achievements* mAchievements;
		Statistics* mAllTimeStats;

		Ogre::String WritableFolder;
		Ogre::String mCfgPath;
		CEGUI::GUIContext* mGuiContext;
		CEGUI::OgreRenderer* mRenderer;

	private:
		Framework(const Framework&);
		Framework& operator= (const Framework&);
	};

}

#endif

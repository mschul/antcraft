#ifndef PUPPET_FACTORY_H
#define PUPPET_FACTORY_H

#include "AnLIB.h"
#include "AnEntity.h"
#include "AnEntityDesc.h"

namespace Ants
{
	class AN_ITEM PuppetFactory : public Ogre::Singleton<PuppetFactory>
	{
	public:
		PuppetFactory();
		~PuppetFactory();
		
		Entity* create(const EntityDesc& desc);

	private:
		PuppetFactory(const Framework&);
		PuppetFactory& operator= (const Framework&);
	};
}

#endif
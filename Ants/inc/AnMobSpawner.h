#ifndef MOB_SPAWNER_H
#define MOB_SPAWNER_H

#include "AnLIB.h"
#include "AnRoom.h"

namespace Ants
{
	class AN_ITEM MobSpawner : public Room
	{
	public:
		struct SpawnerInfo
		{
			SpawnerInfo(Room::RoomType t, bool r, const std::vector<Tile::TileType>& a)
			: type(t), randomPositionAllowed(r), allowedTiles(a)
			{
			}

			Room::RoomType type;
			bool randomPositionAllowed;
			std::vector<Tile::TileType> allowedTiles;
		};

		MobSpawner (int id);
		virtual ~MobSpawner();
		
		static void init();
		static std::vector<SpawnerInfo*> msSpawnerInfos;
		static int msSpawnerCount;

		bool isDistructible() const;


		void update(float dt, ActionReactor* reactor);
		void getJob(std::vector<WorkplaceSubjobs>& jobs);
		
		virtual Entity* spawn(ActionReactor* reactor) = 0;

	protected:
		float mInitialSpawnDelta;
		int mSpawnCount;
		float mNextSpawnDelta;
	};

	typedef std::pair<int, Ogre::Vector2> SpawnerIdPositionPair;
}

#endif
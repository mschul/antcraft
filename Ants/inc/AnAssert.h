#ifndef ANASSERT_H
#define ANASSERT_H

#include "AnLIB.h"

#ifdef AN_USE_ASSERT
namespace Ants
{

	class AN_ITEM Assert
	{
	public:
		// Construction and destruction.
		Assert (bool condition, const char* file, int line, const char* format, ...);
		~Assert ();

	private:
		enum { MAX_MESSAGE_BYTES = 1024 };
		static const char* msDebugPrompt;
		static const size_t msDebugPromptLength;
		static const char* msMessagePrefix;

	#ifdef AN_USE_ASSERT_WRITE_TO_MESSAGE_BOX
		static const char* msMessageBoxTitle;
	#endif
	};

}

#define assertion(condition, format, ...) \
    Ants::Assert(condition, __FILE__, __LINE__, format, __VA_ARGS__)
#else
//----------------------------------------------------------------------------
// Use standard asserts.
//----------------------------------------------------------------------------
#define assertion(condition, format, ...) assert(condition)
//----------------------------------------------------------------------------
#endif

#endif

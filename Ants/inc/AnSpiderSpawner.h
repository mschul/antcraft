#ifndef SPIDER_SPAWNER_H
#define SPIDER_SPAWNER_H

#include "AnLIB.h"
#include "AnMobSpawner.h"

namespace Ants
{
	class AN_ITEM SpiderSpawner : public MobSpawner
	{
	public:
		SpiderSpawner (int id);
		~SpiderSpawner ();
		
		RoomType getType() const;

		void build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se);
		Entity* spawn(ActionReactor* reactor);
	};
}

#endif
#ifndef EARTHWORM_SPAWNER_H
#define EARTHWORM_SPAWNER_H

#include "AnLIB.h"
#include "AnMobSpawner.h"

namespace Ants
{
	class AN_ITEM EarthwormSpawner : public MobSpawner
	{
	public:
		EarthwormSpawner (int id);
		~EarthwormSpawner ();
		
		RoomType getType() const;

		void build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se);
		Entity* spawn(ActionReactor* reactor);
	};
}

#endif
#ifndef THRONE_CHAMBER_H
#define THRONE_CHAMBER_H

#include "AnLIB.h"
#include "AnTile.h"
#include "AnRoom.h"

namespace Ants
{
	class AN_ITEM ThroneChamber : public Room
	{
	public:
		enum WorkerBehaviour
		{
			PROTECT,
			EXPAND,
			DISCOVER
		};

		ThroneChamber (int id);
		~ThroneChamber ();
		
		RoomType getType() const;
		bool isDistructible() const;

		void build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se);
		void update(float dt, ActionReactor* reactor);
		void getJob(std::vector<WorkplaceSubjobs>& jobs);

		void setWorkerBehaviour(WorkerBehaviour wb);
		WorkerBehaviour getWorkerBehaviour() const;

	private:
		WorkerBehaviour mWorkerBehaviour;
		float mNextSpawnDelta;
	};
}

#endif
#ifndef ANMUTEX_H
#define ANMUTEX_H

#include "AnLIB.h"
#include "AnMutexType.h"

namespace Ants
{
	class AN_ITEM Mutex
	{
	public:
		Mutex ();
		~Mutex ();

		void Enter ();
		void Leave ();

	private:
		MutexType mMutex;
	};
}

#endif
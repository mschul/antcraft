#ifndef SPIDER_H
#define SPIDER_H

#include "AnLIB.h"
#include "AnMob.h"

namespace Ants
{

	class AN_ITEM Spider : public Mob
	{
	public:
		Spider (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Spider (const EntityDesc& desc);
		~Spider ();

        virtual void selectBehaviour (float dt);
		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
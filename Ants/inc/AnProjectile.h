#ifndef PROJECTILE_H
#define PROJECTILE_H

#include "AnLIB.h"
#include "AnCharacter.h"

namespace Ants
{

	class AN_ITEM Projectile : public Entity
	{
	public:
		Projectile(int id, float attackPoints, Character* source,
			const Ogre::Vector2& targetPosition, const Ogre::Vector2& targeTiletPosition);
		Projectile (const EntityDesc& desc);
		~Projectile ();
		
		float getAttackPoints() const;
		ObjectID getSource() const;
		
		EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		void update(float dt, ActionReactor* reactor);
		
		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;

	private:
		Ogre::Vector2 mTargetPosition;
		Ogre::Vector2 mTargetTilePosition;
		ObjectID mSourceID;

		float mAttackPoints;
	};
}

#endif
#ifndef ACHIEVEMENTS_H
#define ACHIEVEMENTS_H

#include "AnLIB.h"
#include "AnGameObject.h"
#include "AnMessage.h"

namespace Ants
{
	class AN_ITEM Achievements : public GameObject
	{

	public:
		Achievements();
		Achievements(ObjectID id);
		~Achievements();

		void clear();

		void getAchievements(std::vector<Message*>& messages);

		void reportStatistics(Statistics* currentStats, Statistics* allTimeStats,
			const std::vector<bool>& upgrades, std::vector<Message*>& messages);

		bool UnitsKilled1;
		bool UnitsKilled2;
		bool UnitsKilled3;

		bool CopalHoarder;
		bool Over9000;
		bool NoRoomChallenge;
		bool AllRooms;
		bool AllUnits;
		bool BigBreeder;
		bool StorageFull;
		bool Ranged;
		bool Bulldog;
		bool Tile2048;

		// factory and streaming frontend
	public:
		static const Ogre::String TYPE;
		virtual const Ogre::String& getClassType() const;
		static bool registerFactory();
		static GameObject* Factory(Deserializer& deserializer);
		virtual int getStreamingSize() const;
		static void initializeFactory();
		virtual bool registerObjects(Serializer& serializer) const;
		virtual void serialize(Serializer& serializer) const;
		virtual void resolvePointers(Deserializer& deserializer);
		virtual void deserialize(Deserializer& deserializer);
	private:
		static bool msStreamRegistered;
	};

	static bool gsStreamRegistered_Achievements = Achievements::registerFactory();
}

#endif
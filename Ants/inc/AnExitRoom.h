#ifndef EXIT_ROOM_H
#define EXIT_ROOM_H

#include "AnLIB.h"
#include "AnTile.h"
#include "AnRoom.h"

namespace Ants
{
	class AN_ITEM ExitRoom : public Room
	{
	public:
		ExitRoom (int id);
		~ExitRoom ();
		
		RoomType getType() const;
		bool isDistructible() const;

		static void createTriangle(const Ogre::Vector2& pivot,
							  std::vector<Ogre::Vector2>& triangle);

		void build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se);
		void update(float dt, ActionReactor* reactor);
		void getJob(std::vector<WorkplaceSubjobs>& jobs);
		
		void place(Entity* caller, Entity* item, slot*& reservedSlot);

	private:
		std::vector<PositionTilePair> mChangedTiles;
		std::vector<Entity*> mRemovedEnts;
	};
}

#endif
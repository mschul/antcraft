#ifndef ROOM_CREATE_JOB_H
#define ROOM_CREATE_JOB_H

#include "AnLIB.h"

#include "AnJob.h"
#include "AnTile.h"
#include "AnRoom.h"

namespace Ants
{
	class Client;
	class AN_ITEM RoomCreateJob : public Job
	{
	public:
		RoomCreateJob (int id, const Ogre::Vector2& position, double duration);
		~RoomCreateJob ();
		
		bool update(double dt, ActionReactor* reactor);

		Entity* getFetchEntity() const ;
		void setFetchEntity(Entity* fetchEnt);

		Room* getTargetRoom() const;
		void setTargetRoom(Room* mTargetRoom);

		JobType getType() const;
		void getSubJobs(std::vector<Job::SubJobs>& jobs) const;

		Room::RoomType getRoomType() const;
		void setRoomType(Room::RoomType roomType);

	private:
		Entity* mFetchEnt;
		Room* mTargetRoom;
		Room::RoomType mRoomType;
		double mT;
	};
}

#endif
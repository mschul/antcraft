#ifndef TILE_H
#define TILE_H

#include "AnLIB.h"
#include "AnGameObject.h"

namespace Ants
{
	class AN_ITEM Tile : public GameObject
	{
	public:
		enum TileType
		{
			DIRT = 0,
			ROUGH = 1,
			FLOOR = 2,
			GROUND = 3,
			AIR = 4,
			WATER = 5,
			TREE = 6,
			ROCK = 7,
			MUD = 8,

			QUEEN = 9,
			NEST = 10,
			ROOM_FLOOR = 11,
			
			GRASS = 12,
			HARVESTED_GRASS = 13,
			COPAL = 14,
			FUNGAL_POD = 15,
			GROWING_FUNGUS = 16,
			MATURED_FUNGUS = 17,
			TILE_COUNT = 18
		};

		enum Direction
		{
			RIGHT = 0,
			BOTTOM_RIGHT = 1,
			BOTTOM_LEFT = 2,
			LEFT = 3,
			TOP_LEFT = 4,
			TOP_RIGHT = 5,
			NONE = 6
		};

		Tile(int id, TileType type, const Ogre::String& meshName,
			const Ogre::String& material, const Ogre::BGRA& colour,
			float dps = 0.f, const Ogre::Vector3 offset = Ogre::Vector3::ZERO);
		~Tile ();

		bool isBlocking() const;
		bool isDiggable() const;
		bool isResource() const;
		bool isOverWorld() const;
		bool isAnthill() const;
		TileType getType() const;
		float getDamagePerSecond() const;
		static std::vector<Ogre::Vector2>& getNeighbours(const Ogre::Vector2& p);

		Ogre::String getMaterialName() const;
		Ogre::Vector3 getOffset() const;
		Ogre::BGRA getColour() const;
		int getMeshID() const;

		static void toHexagonalPosition(const Ogre::Vector2& tilePosition, Ogre::Vector2& hexPosition);
		static void toTilePosition(const Ogre::Vector2& hexPosition, Ogre::Vector2& tilePosition);
		static void hexRound(const Ogre::Vector3& hex, Ogre::Vector3& result);
		static void toHexagonalCube(const Ogre::Vector2& tilePosition, Ogre::Vector3& result);
		static void fromHexagonalCube(const Ogre::Vector3& hex, Ogre::Vector2& result);
		static int dist(const Ogre::Vector3& a, const Ogre::Vector3& b);
		static int dist(const Ogre::Vector2& a, const Ogre::Vector2& b);
		static void hexLine(const Ogre::Vector2& start, const Ogre::Vector2& target,
			std::vector<Ogre::Vector2>& path);
		static void hexRect(const Ogre::Vector2& center, int width, int height,
			std::vector<Ogre::Vector2>& path);
		static void hexCircle(const Ogre::Vector2& center, const Ogre::Vector2& border,
			std::vector<Ogre::Vector2>& path);
		static void hexCircle(const Ogre::Vector2& center, int width,
			std::vector<Ogre::Vector2>& path);
		static void hexRing(const Ogre::Vector2& center, int width,
			std::vector<Ogre::Vector2>& path);
		static void hexTriangle(const Ogre::Vector2& start,  const Ogre::Vector2& border,
			std::vector<Ogre::Vector2>& path);
		static void hexLineEnvironment(const Ogre::Vector2& start, const Ogre::Vector2& end,
			int width, std::vector<Ogre::Vector2>& path);

		static void createUpPath(const Ogre::Vector2& tilePosition, int length,
			std::vector<Ogre::Vector2>& path);

		static std::vector<Ogre::String> msMeshNames;
		static std::vector<Tile*> msTiles;
		static std::vector<TileType> msAnthillTiles;
		static std::vector<TileType> msNonAnthillTiles;
		static std::vector<Ogre::Vector2> msDeltas[2];
		static void init(int& lastID);

	protected:
		TileType mType;

		Ogre::String mMaterial;
		Ogre::Vector3 mOffset;
		Ogre::BGRA mColour;
		float mDPS;
		int mMeshID;
	};
	typedef std::pair<Ogre::Vector2, Tile*> PositionTilePair; 
}

#endif
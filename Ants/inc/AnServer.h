#ifndef SERVER_H
#define SERVER_H

#include "AnLIB.h"

#include "AnTile.h"
#include "AnWorldMap.h"
#include "AnEntityMap.h"
#include "AnVisibilityMap.h"
#include "AnRoomMap.h"
#include "AnWaterMap.h"
#include "AnAttack.h"
#include "AnMobSpawner.h"
#include "AnEntityDesc.h"
#include "AnAOE.h"
#include "AnMessage.h"

namespace Ants
{
	class Client;
	struct ClientInfo
	{
		WorldMap* World;
		EntityMap* Entities;
		VisibilityMap* Visibilities;
		std::vector<Attack*> Attacks;
		std::vector<Message*> Messages;
		std::vector<Ogre::Vector2> ChangedTiles;
	};

	class AN_ITEM Server : public ActionReactor
	{
	public:
		Server ();
		virtual ~Server();
		
		void init(unsigned int width, unsigned int height, bool fogOfWar, bool revealMap,
			Ogre::SceneManager* sceneMgr);
		void init(const Ogre::Rect& area, bool fogOfWar, bool revealMap,
			Ogre::SceneManager* sceneMgr);
		
		void update(float dt);

		int loginClient(Client* client);
		void logoffClient(Client* client, int clientID);

		void createSpawners(int count, std::vector<SpawnerIdPositionPair>& spawners);
		void settleClient(int clientId, std::vector<PositionTilePair>& environment,
			Ogre::Vector2& thronePosition, std::vector<Ogre::Vector2>& throneCircle,
			Ogre::Vector2& exitPosition, std::vector<Ogre::Vector2>& exitTriangle);
		void createExit(const Ogre::Vector2& exitPosition, std::vector<Ogre::Vector2>& exitTriangle);

		void getUpdates(int clientID,
			std::vector<PositionTilePair>& tiles,
			std::vector<EntityDesc>& addedEnts,
			std::vector<EntityDesc>& changedEnts,
			std::vector<EntityDesc>& removedEnts,
			std::vector<Attack*>& attacks,
			std::vector<Message*>& messages);

		void setUpdates(int clientID,
			const std::vector<PositionTilePair>& tiles,
			const std::vector<EntityDesc>& addedEnts,
			const std::vector<EntityDesc>& changedEnts,
			const std::vector<EntityDesc>& removedEnts,
			const std::vector<Attack*>& attacks,
			const std::vector<Message*>& messages,
			const std::vector<AOE*>& aoes);

		int createEntityID();
		void createEntityIDs(int count, std::vector<int>& ids);

		void initTiles(Ogre::SceneManager* sceneMgr);
		void createWorld(int rock, int water, int cave, int grass);
		
		// -> ACTION REACTOR IMPL
		void jobMilestoneReached(Job* job);
		void jobFinished(Job* job);
		void setExitTiles(const std::vector<std::pair<Ogre::Vector2, Tile*>>& tiles);
		void setTile(const Ogre::Vector2& position, int type);
		void createResource(const Ogre::Vector2& position, int roomType, Character* source = 0);
		void removeResource(Room* r);
		Egg* spawnEgg(const Ogre::Vector2& position);
		Grub* hatchEgg(Egg* egg);
		void unitDied(Character* unit, Attack* a = 0);
		Ant* spawnAnt(Grub* grub, int type);
		Entity* spawn(const Ogre::Vector2& position, int type);
		void removeEntities(const std::vector<Entity*>& ents);
		void aoeAttack(int sourceID, float attackPoints, const Ogre::Vector2& position, float radius);
		void meleeAttack(float attackPoints, Character* source, Entity* target);
		void rangedAttack(float attackPoints, Character* source, Entity* target);
		// <- ACTION REACTOR IMPL

	protected:
		void setTile(int clientID, const Ogre::Vector2& position, Tile* tile);
		void setTiles(int clientID, const std::vector<Ogre::Vector2>& positions, const std::vector<Tile*>& tiles);
		void setTiles(int clientID, const std::vector<PositionTilePair>& tiles);

		void addEntity(const EntityDesc& desc, int clientID);
		void changeEntity(const EntityDesc& desc, int clientID);
		void removeEntity(const EntityDesc& desc, int clientID);

		int mLastEntityID;
		int mFirstTileID;
		int mNextClientID;
		WorldMap* mWorld;
		RoomMap* mRooms;

		float mWaterUpdateDelta;
		WaterMap* mWater;
		
		bool mFogOfWar;
		bool mRevealMap;
		std::vector<ClientInfo*> mClientInfos;
	};
}

#endif
#ifndef DESTROY_ANTHILL_JOB_H
#define DESTROY_ANTHILL_JOB_H

#include "AnLIB.h"

#include "AnJob.h"
#include "AnTile.h"

namespace Ants
{
    class Client;
    class AN_ITEM DestroyAnthillJob : public Job
    {
    public:
        DestroyAnthillJob (int id, const Ogre::Vector2& position, double duration);
        ~DestroyAnthillJob ();

        bool update (double dt, ActionReactor* reactor);
        JobType getType () const;
        void getSubJobs (std::vector<Job::SubJobs>& jobs) const;

    private:
        double mT;
    };
}

#endif
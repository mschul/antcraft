#ifndef ENTITY_DESC_H
#define ENTITY_DESC_H

#include "AnLIB.h"
#include "AnEntity.h"

namespace Ants
{
	class AN_ITEM EntityDesc
	{
	public:
		EntityDesc (Entity* ent);
		~EntityDesc ();
		
		Entity::EntityType getEntityType() const;
		ObjectID getEntityID() const;
		int getClientID() const;
		
		int getVisibilityRadius() const;

		Ogre::Radian getOrientation() const;
		Ogre::Vector2 getPosition() const;
		Ogre::Vector2 getTilePosition() const;
		
		ObjectID getParent() const;
		
		float getHealth() const;
		int getArmor() const;

	protected:
		Entity::EntityType mEntityType;
		ObjectID mID;
		int mClientID;

		int mVisibilityRadius;

		ObjectID mParent;
		Ogre::Radian mOrientation;
		Ogre::Vector2 mPosition;
		Ogre::Vector2 mTilePosition;

		float mHealth;
		int mArmor;
	};
}

#endif
#ifndef SOLDIER_H
#define SOLDIER_H

#include "AnLIB.h"
#include "AnAnt.h"

namespace Ants
{
	class AN_ITEM Soldier : public Ant
	{
	public:
		Soldier (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Soldier (const EntityDesc& desc);
		~Soldier ();

		void applyUpgrade(Entity::Upgrade u);
		void selectBehaviour(float dt);

		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
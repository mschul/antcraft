#ifndef FAUNAL_SOURCE_H
#define FAUNAL_SOURCE_H

#include "AnLIB.h"
#include "AnTile.h"
#include "AnEntity.h"
#include "AnRoom.h"
#include "AnCharacter.h"

namespace Ants
{
	class AN_ITEM FaunalSource : public Room
	{
	public:
		FaunalSource (int id);
		~FaunalSource ();
		
		RoomType getType() const;
		bool isResource() const;
		bool isDistructible() const;

		void build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se);
		void update(float dt, ActionReactor* reactor);
		void getJob(std::vector<WorkplaceSubjobs>& jobs);

		void setSource(Character* source);
		Character* getSource() const;

	private:
		Character* mSource;
	};
}

#endif
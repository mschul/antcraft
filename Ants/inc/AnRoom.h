#ifndef ROOM_H
#define ROOM_H

#include "AnLIB.h"
#include "AnTile.h"
#include "AnJob.h"
#include "AnActionReactor.h"

namespace Ants
{

	class Server;
	class Ant;
	typedef std::set<int>::const_iterator roomIterator;

	class AN_ITEM Room : public GameObject
	{
	public:
		enum RoomType
		{
			THRONE_CHAMBER = 0,
			STORAGE_CHAMBER = 1,
			BROOD_CHAMBER = 2,
			FUNGI_CHAMBER = 3,

			COPAL_SOURCE = 4,
			FLORAL_SOURCE = 5,
			FAUNAL_SOURCE = 6,
			HONEYDEW_SOURCE = 7,

			EXIT_ROOM = 8,

			SPIDER_SPAWNER = 9,
			LADYBUG_SPAWNER = 10,
			BOMBARDIER_BEETLE_SPAWNER = 11,
			EARTHWORM_SPAWNER = 12,

			ROOM_COUNT = 13,
			NONE = 14
		};

		enum WorkplaceSubjobs
		{
			FETCH_FAUNAL_FOOD,
			FETCH_FLORAL_FOOD,
			FETCH_COPAL,
			FETCH_RESOURCE,
            FETCH_EGGS,
            CLEAN_ROOM,
			PLACE,
			FEED
		};

		struct workplace
		{
			Entity* worker;
			Ogre::Vector2 position;
		};

		struct slotItem
		{
			Entity* ent;
			Entity* reservedFor;
		};

		struct slot
		{
			Ogre::Vector2 position;
			std::vector<slotItem> items;
			int capacity;
			int reserved;
		};

		Room(ObjectID id, int slotCapacity);
		virtual ~Room();
		
		Ogre::Vector2 getPivot(const Ogre::Vector2& nearest) const;
		Ogre::Vector2 getFirstPivot() const;
		virtual bool isResource() const;
		virtual RoomType getType() const = 0;
		virtual bool isDistructible() const;

		virtual void build(const std::vector<Ogre::Vector2>& positions,
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se)  = 0;

		virtual void update(float dt, ActionReactor* reactor);
		virtual void getJob(std::vector<WorkplaceSubjobs>& jobs) = 0;

		virtual Entity* fetch(Entity* caller, slot*& reservedSlot);
		virtual void place(Entity* caller, Entity* item, slot*& reservedSlot);
		void placeManual(const std::vector<Entity*>& items, const Ogre::Vector2& position);
		void getSlotEntities(std::vector<Entity*>& se);
		void reserveFetch(Entity* caller, slot*& reservedSlot, const Ogre::Vector2& nearest);
		void reserveFetch(Entity* caller, slot*& reservedSlot, int entitytype, const Ogre::Vector2& nearest);
		void reservePlace(Entity* caller, slot*& reservedSlot, const Ogre::Vector2& nearest);
		
		int freeWorkplaces() const;
		int workerCount() const;
		bool workerRefillRequested() const;
		void setWorkersRefilled();
		void assignToWorkplace(Ant* ant);
		void getWorkers(std::vector<Ant*>& ants);
		void unassignLastWorker();
		void unassignWorker(Ant* worker);
		void unassignWorkers();

		int freeSlots() const;
		int slotCount() const;
		virtual bool slotEmpty(const Ogre::Vector2& position) const;
		bool isEmpty() const;
		bool isEmptyIncReservations() const;

		std::vector<Entity*>& getGuests();
		void bookGuests(const std::vector<Entity*>& guests);
		void bookGuest(Entity* guest);
		void unbookGuest(Entity* guest);

        void clean (Entity* ent);

	protected:
		void reset(std::vector<Ant*>& workers, std::vector<Entity*>& se);
		void calculateEnclosingRectangle(const std::vector<Ogre::Vector2>& positions,
			float& minX, float& maxX, float& minY, float& maxY);
		void findBorder(int width, int height, int minX, int minY,
			const std::vector<int>& allTileIndices,
			std::vector<int>& assignedTile,
			std::vector<int>& borderIndices, std::vector<int>& tileIndices);


        std::vector<Entity*> mDeleteCache;
		std::vector<Entity*> mGuests;
		std::vector<workplace*> mWorkPlaces;
		int mFreeWorkplaces;
		std::vector<slot*> mSlots;
		bool mWorkerRefillRequested;
		int mSlotCapacity;
		std::vector<Ogre::Vector2> mPivot;
        float mHygiene;
        float mHygieneDec;
	};
}

#endif
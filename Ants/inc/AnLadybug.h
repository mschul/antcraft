#ifndef LADYBUG_H
#define LADYBUG_H

#include "AnLIB.h"
#include "AnMob.h"

namespace Ants
{

	class AN_ITEM Ladybug : public Mob
	{
	public:
		Ladybug (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Ladybug (const EntityDesc& desc);
		~Ladybug ();

        virtual void selectBehaviour (float dt);
		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
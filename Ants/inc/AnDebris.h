#ifndef DEBRIS_H
#define DEBRIS_H

#include "AnLIB.h"
#include "AnEntity.h"

namespace Ants
{

	class AN_ITEM Debris : public Ants::Entity
	{
	public:
		Debris (int id, int clientID,
				const Ogre::Vector2& position,
				const Ogre::Vector2& tilePosition, 
				int visibilityRadius);
		Debris (const EntityDesc& desc);
		~Debris ();
		
		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		void update(float dt, ActionReactor* reactor);

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
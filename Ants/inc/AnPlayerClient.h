#ifndef PLAYER_CLIENT_H
#define PLAYER_CLIENT_H

#include "AnLIB.h"

#include "AnClient.h"
#include "AnGameObject.h"
#include "AnWorldMap.h"
#include "AnEntityMap.h"
#include "AnJobMap.h"
#include "AnServer.h"
#include "AnDigJob.h"
#include "AnEntityQuery.h"
#include "AnJobQuery.h"
#include "AnWorldQuery.h"
#include "AnRoomQuery.h"
#include "AnRoomMap.h"
#include "AnPheromoneMap.h"
#include "AnGrub.h"
#include "AnEgg.h"
#include "AnAttack.h"
#include "AnStatistics.h"

namespace Ants
{
	class AN_ITEM PlayerClient : public Client//, public Ogre::RenderTargetListener
	{
	public:
		enum SelectionMode
		{
			DIG,
			ROOMS,
			BOTH
		};
		
		PlayerClient ();
		~PlayerClient ();

		void init(unsigned int width, unsigned int height);
		void init(const Ogre::Rect& area);
		void initScene(Ogre::SceneManager* sceneMgr, Ogre::Camera* camera,
			int rwWidth, int rwHeight);

		//void preRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);
		//void postRenderTargetUpdate(const Ogre::RenderTargetEvent& evt);

		Statistics* getStatistics() const;

		// tile system
		Ogre::Rect getViewRect() const;
		void setViewRect(const Ogre::Rect& rect);

		// world creation
		void createThroneChamber(const Ogre::Vector2& position,
						const std::vector<Ogre::Vector2>& throneCircle);
		void createExit(const Ogre::Vector2& exitPosition,
						const std::vector<Ogre::Vector2>& triangle);
		void connect(const Ogre::Vector2& thronePosition,
					const Ogre::Vector2& exitPosition,
					std::vector<Ogre::Vector2>& path);
		
		void createFlattenJob(const Ogre::Vector2& pos);
		void createFlattenJobEnvironment(const Ogre::Vector2& pos);
		void createFlattenJobsEnvironment(const std::vector<Ogre::Vector2>& positions);

		// object actions
		void assignToWorkplace(Room* room);
		void assignWorkers(Room* room);
		void updateWorkerAssignments(Room::RoomType type);
		void unAssignFromWorkplace(Room* room);
		Entity* getEntity(int id);
		
		// job selection
		bool isSelected(const Ogre::Vector2& position);
		void preSelect(const Ogre::Vector2& position);
		void preSelectLine(const Ogre::Vector2 start, const Ogre::Vector2 end);
		void preSelectCircle(const Ogre::Vector2 start, const Ogre::Vector2 end);
		void preUnSelect(const Ogre::Vector2& position);
		void preUnSelectLine(const Ogre::Vector2 start, const Ogre::Vector2 end);
		void preUnSelectCircle(const Ogre::Vector2 start, const Ogre::Vector2 end);

		void select();
		void unSelect();
		void hover(const Ogre::Vector2& position, SelectionMode mode);
		void hideHover();
		void clearPreselection();
		void toggleHealthBarsVisible();

		// room system
		void getRoomList(std::vector<Room*>& rooms);
		Room* getRoomAt(const Ogre::Vector2& position);
		Room* getResourceAt(const Ogre::Vector2& position);
		Room* getExitAt(const Ogre::Vector2& position);
		Room* getRoom(int id);
		void preAssign(const Ogre::Vector2& position);
		void preAssignLine(const Ogre::Vector2 start, const Ogre::Vector2 end);
		void preAssignCircle(const Ogre::Vector2 start, const Ogre::Vector2 end);
		void preUnAssign(const Ogre::Vector2& position);
		void preUnAssignLine(const Ogre::Vector2 start, const Ogre::Vector2 end);
		void preUnAssignCircle(const Ogre::Vector2 start, const Ogre::Vector2 end);
		void assign(Room::RoomType type);
		int assignRoom(Room::RoomType type, const Ogre::Vector2& position, Tile* t);
		void assignRoom(Room::RoomType type, const std::vector<Ogre::Vector2>& positions);
		void assignRoom(Room::RoomType type, const Ogre::Vector2& position);
		void clearPreassignment();

		// strategy system
		void grow(Job::JobType type, const Ogre::Vector2& position);
		void shrink(Job::JobType type, const Ogre::Vector2& position);
		void addStrategyNode(Job* j);
		void addStrategyNode(int capacity, const Ogre::Vector2& position, Job::JobType type);
		void removeStrategyNode(Job* j);
		void removeStrategyNode(int capacity, const Ogre::Vector2& position, Job::JobType type);
		void removeStrategy(Job* j);

		// upgrade system
		void addUpgrade(Entity::Upgrade u);
		void getAvailableUpgrade(std::vector<bool>& u);
		void getUpgrades(std::vector<bool>& u);
		int getAvailableUpgradePoint() const;
		int getUnitsNeededForUpgrade() const;
		void setUpgradeLevel(int level);

		void updateAchievements();

		void setFOW(bool fow);

	protected:
		void preSelect(const std::vector<Ogre::Vector2>& positions);
		void preUnSelect(const std::vector<Ogre::Vector2>& positions);
		void preAssign(const std::vector<Ogre::Vector2>& positions);
		void preUnAssign(const std::vector<Ogre::Vector2>& positions);

		void rebuildSelection();
		void rebuildAssignment();
		Job* addJob(int tile, Job::JobType type);
		void removeJob(Job* job);
		
		Ogre::SceneManager* mSceneManager;
		Ogre::Rect mViewRect;

		Ogre::SceneNode* mOverlayRootNode;
		Ogre::SceneNode* mWorldRootNode;

		Ogre::String mHoverMeshName;
		Ogre::SceneNode* mHoverTile;

		// job selection
		std::map<int, Ogre::SceneNode*> mPreselectionNodes;
		std::map<int, Ogre::SceneNode*> mPreunselectionNodes;
		Ogre::String mSelectionMeshName;
		Ogre::String mSelectionMaterialName;
		std::set<int> mSelectedTiles;
		Ogre::StaticGeometry* mSelectedTilesGeom;
		
		// room assignement
		std::map<int, Ogre::SceneNode*> mPreAssignedNodes;
		std::map<int, Ogre::SceneNode*> mPreUnassignedNodes;
		Ogre::String mAssignMeshName;
		Ogre::String mAssignMaterialName;
		std::set<int> mAssignedTiles;
		Ogre::StaticGeometry* mAssignedTilesGeom;

		// job system
		std::list<Job*> mJobDeleteList;

		Ogre::String mStrategyMeshName;
		Ogre::String mAttackStrategyMaterialName;
		Ogre::String mDefendStrategyMaterialName;

		// upgrade system
		std::vector<Entity::Upgrade> mUpgrades;
		int mUpgradeLevel;

		bool mFOW;

	public: // INTERFACES

		// -> Client Impl
		void update(float dt);
		void settle(std::vector<Ogre::Vector2>& locations);
		// <- Client Impl
		
		// -> Client:ActionReactor Impl
		void jobMilestoneReached(Job* job);
		void jobFinished(Job* job);
		void setExitTiles(const std::vector<std::pair<Ogre::Vector2, Tile*>>& tiles);
		void setTile(const Ogre::Vector2& position, int type);
		void createResource(const Ogre::Vector2& position, int roomType, Character* source = 0);
		void removeResource(Room* r);
		Egg* spawnEgg(const Ogre::Vector2& position);
		Grub* hatchEgg(Egg* egg);
		void unitDied(Character* unit, Attack* a = 0);
		Ant* spawnAnt(Grub* grub, int type);
		Entity* spawn(const Ogre::Vector2& position, int type);
		void removeEntities(const std::vector<Entity*>& ents);
		void aoeAttack(int sourceID, float attackPoints, const Ogre::Vector2& position, float radius);
		void meleeAttack(float attackPoints, Character* source, Entity* target);
		void rangedAttack(float attackPoints, Character* source, Entity* target);
		// <- Client:ActionReactor Impl
	};
}

#endif
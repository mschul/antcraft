#ifndef FLORAL_FOOD_H
#define FLORAL_FOOD_H

#include "AnLIB.h"
#include "AnEntity.h"

namespace Ants
{

	class AN_ITEM FloralFood : public Ants::Entity
	{
	public:
		FloralFood (int id, int clientID,
					  const Ogre::Vector2& position,
					  const Ogre::Vector2& tilePosition, 
					  int visibilityRadius);
		FloralFood (const EntityDesc& desc);
		~FloralFood ();
		
		Entity::EntityType getType() const;
		bool isResource() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		void update(float dt, ActionReactor* reactor);

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
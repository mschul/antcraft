#ifndef ROOM_MAP_H
#define ROOM_MAP_H

#include "AnLIB.h"

#include "AnRoom.h"
#include "AnThroneChamber.h"
#include "AnTile.h"

namespace Ants
{
	class AN_ITEM RoomMap
	{
	public:
		RoomMap ();
		~RoomMap ();
		
		void init(unsigned int width, unsigned int height);
		void init(const Ogre::Rect& area);
		
		void update(float dt, ActionReactor* reactor);

		void fillStats(Statistics* stats);

		int assignExitRoom(const std::vector<Ogre::Vector2>& positions);
		int assignRoom(Room::RoomType type, const std::vector<Ogre::Vector2>& positions);
		int assignRoom(Room::RoomType type, const Ogre::Vector2& position);
		void unAssignRoom(const Ogre::Vector2& position,
			std::vector<PositionTilePair>& tiles);

		void buildRoom(int roomID, const Ogre::Vector2& pivot,
			std::vector<PositionTilePair>& tiles);

		void remove(int id);

		void reportTile(const Ogre::Vector2& position, Tile* tile);
		
		int getRoomID(int tile, Room::RoomType type) const;
		void setRoomID(int tile, Room::RoomType type, int id);
		Room* getRoom(int roomID) const;
		Room* getRoom(const Ogre::Vector2& position, Room::RoomType type) const;
		Room* getRoom(const Ogre::Vector2& position) const;
		Room* getExit(const Ogre::Vector2& position) const;
		Room* getResourceRoom(const Ogre::Vector2& position) const;
		Room* getFaunalRoom(const Ogre::Vector2& position) const;
		Room* getFirstRoom(Room::RoomType type) const;
		
		bool hasRooms(Room::RoomType type) const;
		roomIterator getRoomIter(Room::RoomType type) const;
		roomIterator getRoomIterEnd(Room::RoomType type) const;

		ThroneChamber* getThroneChamber() const;

	protected:
		void findDisconnectedComponents(int romID, std::vector<std::set<int>>& components);
		int removeTile(Room::RoomType type, const Ogre::Vector2& position,
			int roomID, std::vector<PositionTilePair>& tiles);
		void addRoom(Room* room);
		int mergeRooms(int roomID0, int roomID1);

		unsigned int mWidth;
		unsigned int mHeight;
		int mLastRoomID;
		int* mRoomMap;
		int* mResourceMap;
		std::multimap<int, int> mFaunalMap;
		int* mExitMap;
		
		struct RoomInfo
		{
			Room* room;
			std::set<int> tiles;
		};

		std::map<int, RoomInfo> mRooms;
		std::set<int> mRoomTypes[Room::ROOM_COUNT];

		std::vector<Ant*> mPendingWorkers;
		std::vector<Entity*> mPendingSlotEntities;
	};
}

#endif
#ifndef ANMUTEXTYPE_H
#define ANMUTEXTYPE_H

#include "AnLIB.h"

#if defined(WIN32)

namespace Ants
{
	typedef void* MutexType;
}

#elif defined(__LINUX__) || defined(__APPLE__)

#include <pthread.h>
namespace Ants
{
	typedef struct
	{
		pthread_mutexattr_t Attribute;
		pthread_mutex_t Mutex;
	}
	MutexType;
}

#else
#error Platform not supported!
#endif

#endif
#ifndef OPTIONS_H
#define OPTIONS_H

#include "AnLIB.h"

namespace Ants
{
	class AN_ITEM Options : public Ogre::Singleton<Options>
	{
	public:
		Options();

		void save(const Ogre::String& filename);
		void load(const Ogre::String& filename);

		Ogre::String Renderer() const;
		Ogre::String StandardRenderer() const;
		Ogre::String FullScreen() const;
		Ogre::String VideoMode() const;
		Ogre::String ColourDepth() const;
		Ogre::String FSAA() const;
		Ogre::String VSync() const;

		void setRenderer(const Ogre::String& value);
		void setFullScreen(const Ogre::String& value);
		void setVideoMode(const Ogre::String& value);
		void setColourDepth(const Ogre::String& value);
		void setFSAA(const Ogre::String& value);
		void setVSync(const Ogre::String& value);

	private:
		typedef std::map<std::string, std::string> optionMap;
		typedef std::pair<std::string, std::string> option;
		optionMap mOptions;
		Ogre::ConfigFile mConfig;

		Options(const Options&);
		Options& operator= (const Options&);
	};
}

#endif
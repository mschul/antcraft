#ifndef JOB_MAP_H
#define JOB_MAP_H

#include "AnLIB.h"

#include "AnEntity.h"
#include "AnJob.h"

namespace Ants
{
	class Entity;
	class AN_ITEM JobMap
	{
	public:
		JobMap ();
		~JobMap ();
		
		void init(unsigned int width, unsigned int height);
		void init(const Ogre::Rect& area);

		int add(Job::JobType type, Ogre::Vector2 position);
		void remove(int id);
		void removeJobs(const Ogre::Vector2& position, Job::JobType type);
		
		bool assign(Entity* ent, int id);
		bool unAssign(Entity* ent, int id);

		bool jobsAvailable() const;

		void applyUpgrade(Entity::Upgrade u);

		std::vector<Job*> get(const Ogre::Vector2& position) const;
		Job* get(int id) const;
		
		int mLastJobID;

	protected:
		unsigned int mWidth;
		unsigned int mHeight;
		float mHarvestSpeed;
		std::map<int, Job*> mJobs;
		std::vector<std::vector<Job*>> mMap;

		unsigned int mAvailableJobs;
	};
}

#endif
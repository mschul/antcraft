#include "AnPCH.h"
#include "AnAchievements.h"
#include "AnEntity.h"
using namespace Ants;

Achievements::Achievements()
{
	clear();
}
Achievements::Achievements(ObjectID id)
	: GameObject(id)
{
	clear();
}
Achievements::~Achievements()
{

}

void Achievements::clear()
{
	UnitsKilled1 = false;
	UnitsKilled2 = false;
	UnitsKilled3 = false;

	CopalHoarder = false;
	Over9000 = false;
	NoRoomChallenge = false;
	AllRooms = false;
	AllUnits = false;
	BigBreeder = false;
	StorageFull = false;

	Ranged = false;
	Bulldog = false;
	Tile2048 = false;
}

void Achievements::getAchievements(std::vector<Message*>& messages)
{
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Bug Crusher.");
		m->addDatum("AchievementGot", UnitsKilled1 ? "True" : "False");
		m->addDatum("MessageBody", "No bug stands in your way.");
		m->addDatum("MessageFooter", "Kill 100 Bugs (in Alltime Stats)");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Mass Murder!");
		m->addDatum("AchievementGot", UnitsKilled2 ? "True" : "False");
		m->addDatum("MessageBody", "It's okay to kill bugs, they don't have any feelings, right? Right???");
		m->addDatum("MessageFooter", "Kill 500 Bugs (in Alltime Stats))");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Insecticide!");
		m->addDatum("AchievementGot", UnitsKilled3 ? "True" : "False");
		m->addDatum("MessageBody", "KILL THEM, KILL, KiLL, kiIl1l1.");
		m->addDatum("MessageFooter", "Kill 1000 Bugs (in Alltime Stats)");
		messages.push_back(m);
	}

	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Copal Hoarder!");
		m->addDatum("AchievementGot", CopalHoarder ? "True" : "False");
		m->addDatum("MessageBody", "Now, why would you need Copal anyway?");
		m->addDatum("MessageFooter", "Store 20 Copal");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Three Musketeers.");
		m->addDatum("AchievementGot", NoRoomChallenge ? "True" : "False");
		m->addDatum("MessageBody", "Tous pour un, un pour tous.");
		m->addDatum("MessageFooter", "Kill 42 Bugs without building a single Room");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Master Architect!");
		m->addDatum("AchievementGot", AllRooms ? "True" : "False");
		m->addDatum("MessageBody", "Hey, you built all types of Rooms. Here have an Achievement.");
		m->addDatum("MessageFooter", "Built each type of room");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Gotta Breed 'em All!");
		m->addDatum("AchievementGot", AllUnits ? "True" : "False");
		m->addDatum("MessageBody", "I bet this took you a while.");
		m->addDatum("MessageFooter", "Produce every unit type (in Alltime Stats)");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Big Breeder!");
		m->addDatum("AchievementGot", BigBreeder ? "True" : "False");
		m->addDatum("MessageBody", "Now let the breeding start.");
		m->addDatum("MessageFooter", "Have 100 Nests");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Full Stock!");
		m->addDatum("AchievementGot", StorageFull ? "True" : "False");
		m->addDatum("MessageBody", "How about a bigger Storage Room?");
		m->addDatum("MessageFooter", "Completely fill a Storage room");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Sniper!");
		m->addDatum("AchievementGot", Ranged ? "True" : "False");
		m->addDatum("MessageBody", "DID HE SAY SNI-.");
		m->addDatum("MessageFooter", "Build a ranged unit");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Can't touch me!");
		m->addDatum("AchievementGot", Ranged ? "True" : "False");
		m->addDatum("MessageBody", "Now this is some armor.");
		m->addDatum("MessageFooter", "Upgrade your Soldiers");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Over 9000!");
		m->addDatum("AchievementGot", Over9000 ? "True" : "False");
		m->addDatum("MessageBody", "I know it's an ancient meme, but I couldn't resist. Deal with it.");
		m->addDatum("MessageFooter", "Get a score over 9000 in a single game");
		messages.push_back(m);
	}
	{
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Got the 2048 Tile!");
		m->addDatum("AchievementGot", Tile2048 ? "True" : "False");
		m->addDatum("MessageBody", "Now this is an Anthive!");
		m->addDatum("MessageFooter", "Get the 2048 tile.");
		messages.push_back(m);
	}
}
void Achievements::reportStatistics(Statistics* currentStats, Statistics* allTimeStats,
	const std::vector<bool>& upgrades, std::vector<Message*>& messages)
{
	int uc = allTimeStats->getUnitsKilled() + currentStats->getUnitsKilled();
	if (!UnitsKilled1 && uc >= 100)
	{
		UnitsKilled1 = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Bug Crusher.");
		m->addDatum("MessageBody", "No bug stands in your way.");
		m->addDatum("MessageFooter", "Kill 100 Bugs (in Alltime Stats)");
		messages.push_back(m);
	}
	if (!UnitsKilled2 && uc >= 500)
	{
		UnitsKilled2 = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Mass Murder!");
		m->addDatum("MessageBody", "It's okay to kill bugs, they don't have any feelings, right? Right???");
		m->addDatum("MessageFooter", "Kill 500 Bugs (in Alltime Stats))");
		messages.push_back(m);
	}
	if (!UnitsKilled3 && uc >= 1000)
	{
		UnitsKilled3 = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Insecticide!");
		m->addDatum("MessageBody", "KILL THEM, KILL, KiLL, kiIl1l1.");
		m->addDatum("MessageFooter", "Kill 1000 Bugs (in Alltime Stats)");
		messages.push_back(m);
	}

	if (!CopalHoarder
		&& currentStats->CopalStored >= 20)
	{
		CopalHoarder = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Copal Hoarder!");
		m->addDatum("MessageBody", "Now, why would you need Copal anyway?");
		m->addDatum("MessageFooter", "Store 20 Copal");
		messages.push_back(m);
	}
	if (!NoRoomChallenge
		&& currentStats->getRoomCount() == 0 && currentStats->getUnitsKilled() > 20)
	{
		NoRoomChallenge = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Three Musketeers.");
		m->addDatum("MessageBody", "Tous pour un, un pour tous.");
		m->addDatum("MessageFooter", "Kill 42 Bugs without building a single Room");
		messages.push_back(m);
	}
	if (!AllRooms
		&& currentStats->Fungi.Count > 0 && currentStats->Storage.Count > 0
		&& currentStats->Brood.Count > 0)
	{
		AllRooms = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Master Architect!");
		m->addDatum("MessageBody", "Hey, you built all types of Rooms. Here have an Achievement.");
		m->addDatum("MessageFooter", "Built each type of room");
		messages.push_back(m);
	}

	if (!AllUnits
		&& (currentStats->Carrier.Produced + allTimeStats->Carrier.Produced) > 0
		&& (currentStats->Soldier.Produced + allTimeStats->Soldier.Produced) > 0
		&& (currentStats->Worker.Produced + allTimeStats->Worker.Produced) > 0
		&& (currentStats->SmallWorker.Produced + allTimeStats->SmallWorker.Produced) > 0
		&& (currentStats->Kamikaze.Produced + allTimeStats->Kamikaze.Produced) > 0)
	{
		AllUnits = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Gotta Breed 'em All!");
		m->addDatum("MessageBody", "I bet this took you a while.");
		m->addDatum("MessageFooter", "Produce every unit type (in Alltime Stats)"); 
		messages.push_back(m);
	}
	
	if (!BigBreeder && currentStats->Brood.Slots > 100)
	{
		BigBreeder = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Big Breeder!");
		m->addDatum("MessageBody", "Now let the breeding start.");
		m->addDatum("MessageFooter", "Have 100 Nests");
		messages.push_back(m);
	}

	if (!StorageFull && currentStats->AStorageFull)
	{
		StorageFull = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Full Stock!");
		m->addDatum("MessageBody", "How about a bigger Storage Room?");
		m->addDatum("MessageFooter", "Completely fill a Storage room");
		messages.push_back(m);
	}

	if (!Ranged && upgrades[Entity::U_ACID] && currentStats->Worker.Alive > 0)
	{
		Ranged = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Sniper!");
		m->addDatum("MessageBody", "DID HE SAY SNI-.");
		m->addDatum("MessageFooter", "Build a ranged unit");
		messages.push_back(m);
	}

	if (!Bulldog && upgrades[Entity::U_BULLDOGS] && currentStats->Soldier.Alive > 0)
	{
		Ranged = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Can't touch me!");
		m->addDatum("MessageBody", "Now this is some armor.");
		m->addDatum("MessageFooter", "Upgrade your Soldiers");
		messages.push_back(m);
	}

	if (!Over9000 && currentStats->getScore() > 9000)
	{
		Over9000 = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Over 9000!");
		m->addDatum("MessageBody", "I know it's an ancient meme, but I couldn't resist. Deal with it.");
		m->addDatum("MessageFooter", "Get a score over 9000 in a single game");
		messages.push_back(m);
	}

	if (!Tile2048 && currentStats->AnthillArea >= 2048)
	{
		Tile2048 = true;
		Message* m = new0 Message(Message::MT_ACHIEVEMENT_NOTIFICATION, -1);
		m->addDatum("MessageTitle", "Got the 2048 Tile!");
		m->addDatum("MessageBody", "Now this is an Anthive!");
		m->addDatum("MessageFooter", "Get the 2048 tile.");
		messages.push_back(m);
	}
}


//--------------------------------------------------------
const Ogre::String Achievements::TYPE("Achievements");
const Ogre::String& Achievements::getClassType() const
{
	return TYPE;
}

bool Achievements::msStreamRegistered = false;
bool Achievements::registerFactory()
{
	if (!msStreamRegistered)
	{
		InitTerm::addInitializer(Achievements::initializeFactory);
		msStreamRegistered = true;
	}
	return msStreamRegistered;
}

GameObject* Achievements::Factory(Deserializer& deserializer)
{
	Achievements* object = new0 Achievements();
	object->deserialize(deserializer);
	return object;
}
void Achievements::initializeFactory()
{
	if (!msFactories)
	{
		msFactories = new0 GameObject::FactoryMap();
	}
	msFactories->insert(std::make_pair(TYPE, Factory));
}

bool Achievements::registerObjects(Serializer& serializer) const
{
	if (GameObject::registerObjects(serializer))
	{
		return true;
	}
	return false;
}

int Achievements::getStreamingSize() const
{
	int size = GameObject::getStreamingSize();

	size += sizeof(UnitsKilled1);
	size += sizeof(UnitsKilled2);
	size += sizeof(UnitsKilled3);

	size += sizeof(CopalHoarder);
	size += sizeof(Over9000);
	size += sizeof(NoRoomChallenge);

	size += sizeof(AllRooms);
	size += sizeof(AllUnits);
	size += sizeof(BigBreeder);
	size += sizeof(StorageFull);

	size += sizeof(Ranged);
	size += sizeof(Bulldog);
	size += sizeof(Tile2048);

	return size;
}

void Achievements::serialize(Serializer& serializer) const
{
	GameObject::serialize(serializer);
	serializer.writeBool(UnitsKilled1);
	serializer.writeBool(UnitsKilled2);
	serializer.writeBool(UnitsKilled3);

	serializer.writeBool(CopalHoarder);
	serializer.writeBool(Over9000);
	serializer.writeBool(NoRoomChallenge);

	serializer.writeBool(AllRooms);
	serializer.writeBool(AllUnits);
	serializer.writeBool(BigBreeder);
	serializer.writeBool(StorageFull);

	serializer.writeBool(Ranged);
	serializer.writeBool(Bulldog);
	serializer.writeBool(Tile2048);
}
void Achievements::deserialize(Deserializer& deserializer)
{
	GameObject::deserialize(deserializer);
	deserializer.readBool(UnitsKilled1);
	deserializer.readBool(UnitsKilled2);
	deserializer.readBool(UnitsKilled3);

	deserializer.readBool(CopalHoarder);
	deserializer.readBool(Over9000);
	deserializer.readBool(NoRoomChallenge);

	deserializer.readBool(AllRooms);
	deserializer.readBool(AllUnits);
	deserializer.readBool(BigBreeder);
	deserializer.readBool(StorageFull);

	deserializer.readBool(Ranged);
	deserializer.readBool(Bulldog);
	deserializer.readBool(Tile2048);
}
void Achievements::resolvePointers(Deserializer& deserializer)
{
	GameObject::resolvePointers(deserializer);
}

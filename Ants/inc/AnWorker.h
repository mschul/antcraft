#ifndef WORKER_H
#define WORKER_H

#include "AnLIB.h"
#include "AnAnt.h"

namespace Ants
{
	class AN_ITEM Worker : public Ant
	{
	public:
		Worker (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Worker (const EntityDesc& desc);
		~Worker ();
		
		void selectBehaviour(float dt);

		void applyUpgrade(Entity::Upgrade u);

		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
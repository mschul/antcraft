#ifndef FUNGI_CHAMBER_H
#define FUNGI_CHAMBER_H

#include "AnLIB.h"
#include "AnTile.h"
#include "AnEntity.h"
#include "AnRoom.h"

namespace Ants
{
	class AN_ITEM FungiChamber : public Room
	{
	public:
		FungiChamber(int id);
		~FungiChamber();

		RoomType getType() const;

		void build(const std::vector<Ogre::Vector2>& positions,
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se);
		void update(float dt, ActionReactor* reactor);
		void getJob(std::vector<WorkplaceSubjobs>& jobs);

		void reserveFetch(slot*& reservedSlot);

	private:
		std::vector<float> mAges;
	};
}

#endif
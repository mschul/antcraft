#ifndef INITTERM_H
#define INITTERM_H

#include "AnLIB.h"

namespace Ants
{

	class AN_ITEM InitTerm
	{
	public:
		typedef void(*Initializer)(void);
		static void addInitializer(Initializer function);
		static void executeInitializers();

		typedef void(*Terminator)(void);
		static void addTerminator(Terminator function);
		static void executeTerminators();

	private:
		// This number must be large enough to support your application.  If your
		// application triggers an assert about exceeding this, change this number
		// and recompile.  The goal is to avoid dynamic allocation during premain
		// and postmain execution, thus making it easier to manage and track
		// memory usage.
		enum
		{
			MAX_ELEMENTS = 512
		};

		static int msNumInitializers;
		static Initializer msInitializers[MAX_ELEMENTS];

		static int msNumTerminators;
		static Terminator msTerminators[MAX_ELEMENTS];
	};

}

#endif
#ifndef KAMIKAZE_H
#define KAMIKAZE_H

#include "AnLIB.h"
#include "AnAnt.h"

namespace Ants
{
	class AN_ITEM Kamikaze : public Ant
	{
	public:
		Kamikaze(int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition,
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Kamikaze(const EntityDesc& desc);
		~Kamikaze();

		void applyUpgrade(Entity::Upgrade u);
		void selectBehaviour(float dt);

		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
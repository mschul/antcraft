#ifndef ANT_H
#define ANT_H

#include "AnLIB.h"
#include "AnCharacter.h"
#include "AnDigJob.h"
#include "AnGotoJob.h"
#include "AnBehaviourTrait.h"

namespace Ants
{
	class AN_ITEM Ant : public Character
	{
	protected:
		static int msAntCount;

		// traits
		template<class Ant> friend class BehaviourTrait;
		template<class Ant> friend class IdleTrait;
		template<class Ant> friend class WorkerTrait;
		template<class Ant> friend class WarriorTrait;
        template<class Ant> friend class EscapeTrait;
        template<class Ant> friend class RansackTrait;
		static std::vector<BehaviourTrait<Ant>*> msTraits;
		int mActiveTrait;

	public:
		static WorkerTrait<Ant>* getWorkerTrait();
		static WarriorTrait<Ant>* getWarriorTrait();
		static EscapeTrait<Ant>* getEscapeTrait();

	public:
		Ant (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Ant (const EntityDesc& desc);
		virtual ~Ant();

		void move(float dt, ActionReactor* reactor);

		void die(ActionReactor* reactor);

		virtual void applyUpgrade(Entity::Upgrade u);
		virtual bool isAnt() const;

		void prepareForNewJob(Room::slot* s);
		void prepareForNewJob(int jobIndex);
		void enqueueJob(Job* job);
		void assignToWorkplace(Room* room);
		void unAssignFromWorkplace();
		void targetRoomDestroyed(Room* room);

		bool isIdle() const;
		bool isFreelancer() const;
		bool canWork(Job::JobType type) const;

		void drop(Entity* item);
		void drop(Entity* item, int inventoryIndex);
		void dropAll();
		void pick(Entity* item, int inventoryIndex);
		

	protected:
		bool findPathToWorkplace();
		
		int mActiveJobIndex;
		std::vector<Room*> mTargetRooms;
		std::vector<Room::slot*> mAssignedSlots;
		bool mFetching;
		
		int mInventoryFill;
		std::vector<Entity*> mInventory;

		int mP1Jobs;
		int mP2Jobs;
		int mP3Jobs;
		
		bool mAntibiotics;

		// job system
		int mJobFill;
		bool mFreelancing;
		Room::RoomType mHomeRoomType;
		Room* mHomeRoom;		
		std::vector<int> mJobs;
		std::vector<std::queue<int>> mSubJobs;
	};
}

#endif
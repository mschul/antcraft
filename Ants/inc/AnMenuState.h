#ifndef MENU_STATE_H
#define MENU_STATE_H

#include "AnLIB.h"

#include "AnAppState.h"
#include "AnHighscores.h"

namespace Ants
{

	class MenuState : public AppState
	{
	public:
		MenuState();

		DECLARE_GAME_STATE_CLASS(MenuState)

		void enter();
		void exit();

		bool keyPressed(const OIS::KeyEvent &keyEventRef);
		bool keyReleased(const OIS::KeyEvent &keyEventRef);

		bool mouseMoved(const OIS::MouseEvent &evt);
		bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

		bool newGame(const CEGUI::EventArgs& /*e*/);
		bool quit(const CEGUI::EventArgs& /*e*/);
		bool saveOptions(const CEGUI::EventArgs& /*e*/);
		bool resetUserData(const CEGUI::EventArgs& /*e*/);
		bool saveHighscore(const CEGUI::EventArgs& e);

		bool enters(const CEGUI::EventArgs& /*e*/);

		bool back(const CEGUI::EventArgs& /*e*/);
		bool showHighscores(const CEGUI::EventArgs& /*e*/);
		bool showAchievements(const CEGUI::EventArgs& /*e*/);
		bool showCustomGame(const CEGUI::EventArgs& /*e*/);
		bool showOptions(const CEGUI::EventArgs& /*e*/);
		bool showGameOver(const CEGUI::EventArgs& /*e*/);
		bool showTutorial(const CEGUI::EventArgs& /*e*/);

		bool nextTutorialPage(const CEGUI::EventArgs& /*e*/);
		bool prevTutorialPage(const CEGUI::EventArgs& /*e*/);

		bool newCustomGame(const CEGUI::EventArgs& /*e*/);
		void updateUI();
		void createHighscoreList();
		void createAchievementsList();
		void destroyHighscoreList();
		void destroyAchievementsList();

		void update(float dT);

	private:
		bool mQuit;

		CEGUI::Window* mCurrentWnd;
		CEGUI::Window* mMainMenuWnd;
		CEGUI::Window* mCustomGameWnd;
		CEGUI::Window* mHighscoresWnd;
		CEGUI::Window* mAchievementsWnd;
		CEGUI::Window* mOptionsWnd;
		CEGUI::Window* mGameOverWnd;
		CEGUI::Window* mEnterNameWnd;

		CEGUI::Window* mTutorialWnd;
		std::vector<CEGUI::Window*> mTutorialPages;
		int mCurrentTutorialPage;
	};

}

#endif

#ifndef ENTITY_MAP_H
#define ENTITY_MAP_H

#include "AnLIB.h"
#include "AnEntity.h"
#include "AnFowMaskMap.h"

namespace Ants
{
	class AN_ITEM EntityMap
	{
	public:
		EntityMap ();
		virtual ~EntityMap();
		
		void init(unsigned int width, unsigned int height);
		void init(const Ogre::Rect& area);
		
		virtual void onEntityChange(Entity* ent);
		void update(float dt, int clientID, ActionReactor* reactor);

		void getUpdates(std::vector<EntityDesc>& addedEnts,
				std::vector<EntityDesc>& changedEnts,
				std::vector<EntityDesc>& removedEnts);
		void clearUpdates();

		virtual void add(Entity* ent, bool tracking = false);
		virtual void add(const std::vector<Entity*>& entities, bool tracking = false);
		virtual void add(EntityDesc desc, bool tracking = false);
		virtual void change(Entity* ent, bool tracking = false);
		virtual void change(const std::vector<Entity*>& entities, bool tracking = false);
		virtual void change(EntityDesc desc, bool tracking = false);
		virtual void remove(Entity* ent, bool tracking = false);
		virtual void remove(const std::vector<Entity*>& entities, bool tracking = false);
		virtual void remove(EntityDesc desc, bool tracking = false);

		void applyUpgrade(int clientID, Entity::Upgrade u);
		
		std::vector<Entity*>& get(const Ogre::Vector2& position) const;
		void get(const std::vector<Ogre::Vector2>& positions, std::vector<Entity*>& ents) const;
		Entity* findNext(int id) const;
		Entity* get(int id) const;

	protected:
		bool mBusy;

		unsigned int mWidth;
		unsigned int mHeight;
		std::vector<Entity*>* mMap;
		std::map<int, Entity*> mEntities;
		std::vector<Ogre::Vector2> mInverseMap;

		std::vector<EntityDesc> mAddedEnts;
		std::vector<EntityDesc> mChangedEnts;
		std::vector<EntityDesc> mRemovedEnts;

		std::vector<std::pair<Entity*, bool>> mAddCache;
		std::vector<std::pair<Entity*, bool>> mChangeCache;
		std::vector<std::pair<Entity*, bool>> mRemoveCache;
	};
}

#endif
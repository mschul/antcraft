#ifndef APP_STATE_MANAGER_H
#define APP_STATE_MANAGER_H

#include "AnLIB.h"

#include "AnAppState.h"

namespace Ants
{

	class AppStateManager : public Ogre::FrameListener, public AppStateListener
	{
	public:
		typedef struct
		{
			Ogre::String name;
			AppState* state;
		} state_info;

		AppStateManager();
		~AppStateManager();

		void manageAppState(Ogre::String stateName, AppState* state);

		AppState* findByName(Ogre::String stateName);

		void changeAppState(AppState* state);
		bool pushAppState(AppState* state);
		void popAppState();
		void pauseAppState();
		void shutdown();
		void popAllAndPushAppState(AppState* state);

	
		bool frameStarted(const Ogre::FrameEvent& evt);
		bool frameRenderingQueued(const Ogre::FrameEvent& evt);
		bool frameEnded(const Ogre::FrameEvent& evt);


	protected:
		void init(AppState *state);

		std::vector<AppState*>		mActiveStateStack;
		std::vector<state_info>		mStates;
		bool						mShutdown;
	};

}

#endif

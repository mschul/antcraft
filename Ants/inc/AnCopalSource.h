#ifndef COPAL_SOURCE_H
#define COPAL_SOURCE_H

#include "AnLIB.h"
#include "AnTile.h"
#include "AnEntity.h"
#include "AnRoom.h"

namespace Ants
{
	class AN_ITEM CopalSource : public Room
	{
	public:
		CopalSource (int id);
		~CopalSource ();
		
		RoomType getType() const;
		bool isResource() const;
		bool isDistructible() const;

		void build(const std::vector<Ogre::Vector2>& positions, 
			const Ogre::Vector2& pivot, std::vector<PositionTilePair>& tiles,
			std::vector<Ant*>& workers, std::vector<Entity*>& se);
		void update(float dt, ActionReactor* reactor);
		void getJob(std::vector<WorkplaceSubjobs>& jobs);

		void grow(const Ogre::Vector2& position);

	private:
		std::vector<bool> mGrowingSlots;
		bool mRegrowing;
		float mGrowthDelta;
	};
}

#endif
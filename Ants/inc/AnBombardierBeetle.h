#ifndef BOMBARDIERBEETLE_H
#define BOMBARDIERBEETLE_H

#include "AnLIB.h"
#include "AnMob.h"

namespace Ants
{

	class AN_ITEM BombardierBeetle : public Mob
	{
	public:
		BombardierBeetle (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		BombardierBeetle (const EntityDesc& desc);
		~BombardierBeetle ();

        virtual void selectBehaviour (float dt);
		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
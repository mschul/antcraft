#ifndef DEFEND_JOB_H
#define DEFEND_JOB_H

#include "AnLIB.h"

#include "AnJob.h"
#include "AnTile.h"

namespace Ants
{
	class Client;
	class AN_ITEM DefendJob : public Job
	{
	public:
		DefendJob (int id, const Ogre::Vector2& position, double duration);
		~DefendJob ();
		
		bool update(double dt, ActionReactor* reactor);
		JobType getType() const;
		void getSubJobs(std::vector<Job::SubJobs>& jobs) const;
		
	private:
		double mDuration;
		double mT;
	};
}

#endif
#ifndef WORKER_TRAIT_H
#define WORKER_TRAIT_H

#include "AnLIB.h"
#include "AnBehaviourTrait.h"

namespace Ants
{
	template<class Character>
	class AN_ITEM WorkerTrait : public BehaviourTrait<Character>
	{
	public:
		WorkerTrait();
		
		virtual void idleWalking(float dt, Character* character, ActionReactor* reactor);
		virtual void idleStanding(float dt, Character* character, ActionReactor* reactor);
		virtual void findJob(float dt, Character* character, ActionReactor* reactor);
		virtual void getJobInfo(float dt, Character* character, ActionReactor* reactor);
		virtual void findPath(float dt, Character* character, ActionReactor* reactor);
		virtual void reachJob(float dt, Character* character, ActionReactor* reactor);
		virtual void workJob(float dt, Character* character, ActionReactor* reactor);

	private:
		bool findJobWorker(float dt, Character* character, ActionReactor* reactor, int jobIndex);
		bool findJobFreelancer(float dt, Character* character, ActionReactor* reactor, int jobIndex);
		bool getJobInfoWorker(float dt, Character* character, ActionReactor* reactor, int jobIndex);
		bool getJobInfoFreelancer(float dt, Character* character, ActionReactor* reactor, int jobIndex);
		bool findPathWorker(float dt, Character* character, ActionReactor* reactor, int jobIndex);
		bool findPathFreelancer(float dt, Character* character, ActionReactor* reactor, int jobIndex);
		bool workJobWorker(float dt, Character* character, ActionReactor* reactor, int jobIndex);
		bool workJobFreelancer(float dt, Character* character, ActionReactor* reactor, int jobIndex);
	};
}

#endif
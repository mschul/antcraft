#ifndef COPAL_H
#define COPAL_H

#include "AnLIB.h"
#include "AnEntity.h"

namespace Ants
{

	class AN_ITEM Copal : public Ants::Entity
	{
	public:
		Copal(int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition,
			int visibilityRadius);
		Copal(const EntityDesc& desc);
		~Copal();

		Entity::EntityType getType() const;
		bool isResource() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		void update(float dt, ActionReactor* reactor);

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
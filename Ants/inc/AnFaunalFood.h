#ifndef FAUNAL_FOOD_H
#define FAUNAL_FOOD_H

#include "AnLIB.h"
#include "AnEntity.h"

namespace Ants
{

	class AN_ITEM FaunalFood : public Ants::Entity
	{
	public:
		FaunalFood(int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition,
			int visibilityRadius);
		FaunalFood(const EntityDesc& desc);
		~FaunalFood();

		Entity::EntityType getType() const;
		bool isResource() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		void update(float dt, ActionReactor* reactor);

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
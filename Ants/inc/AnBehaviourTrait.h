#ifndef BEHAVIOURTRAIT_H
#define BEHAVIOURTRAIT_H

#include "AnLIB.h"
#include "AnActionReactor.h"
#include "AnCharacter.h"

namespace Ants
{
	template<class T>
	class BehaviourTrait
	{
	public:
		void update(float dt, T* mob, ActionReactor* reactor);

		virtual void idleWalking(float dt, T* character, ActionReactor* reactor) = 0;
		virtual void idleStanding(float dt, T* character, ActionReactor* reactor) = 0;
		virtual void findJob(float dt, T* character, ActionReactor* reactor) = 0;
		virtual void getJobInfo(float dt, T* character, ActionReactor* reactor) = 0;
		virtual void findPath(float dt, T* character, ActionReactor* reactor) = 0;
		virtual void reachJob(float dt, T* character, ActionReactor* reactor) = 0;
		virtual void workJob(float dt, T* character, ActionReactor* reactor) = 0;
	};

	template<class T>
	void BehaviourTrait<T>::update(float dt, T* character, ActionReactor* reactor)
	{
		switch(character->mCurrentState)
		{
		case Character::IDLE_WALKING: idleWalking(dt, character, reactor); break;
		case Character::IDLE_STANDING: idleStanding(dt, character, reactor); break;
		case Character::FIND_JOB: findJob(dt, character, reactor); break;
		case Character::GET_JOBINFORMATION: getJobInfo(dt, character, reactor); break;
		case Character::FIND_PATH: findPath(dt, character, reactor); break;
		case Character::REACH_JOB: reachJob(dt, character, reactor); break;
		case Character::WORK_JOB: workJob(dt, character, reactor); break;
		}
	}
}

#endif
#ifndef DESERIALIZER_H
#define DESERIALIZER_H

#include "AnLIB.h"
#include "AnIO.h"

namespace Ants
{
	class GameObject;
	class AN_ITEM Deserializer
	{
	public:
		Deserializer();
		virtual ~Deserializer();

		bool empty() const;
		GameObject* operator[] (const int index);

		void fromMemory(int& size, char*& buffer);
		bool fromFile(const Ogre::String& filename);

		template<typename T> bool read(T& datum);
		bool readString(Ogre::String& datum);

		void readUniqueID(GameObject* obj);

		template <typename T> bool readPointer(T*& object);
		template <typename T> bool readPointerVV(int numElements, T** objects);
		bool readBool(bool& datum);
		template <typename T> void resolvePointer(T*& object);

	private:
		typedef std::map<uint64_t, GameObject*> LinkMap;
		typedef std::vector<GameObject*> LinkArray;

		std::vector<GameObject*> mObjects;

		LinkMap mLinked;
		LinkArray mOrdered;
		IO mIO;
	};

	template<typename T> bool Deserializer::read(T& datum)
	{
		return mIO.read(sizeof(T), &datum);
	}

	template <typename T>
	bool Deserializer::readPointer(T*& object)
	{
		unsigned int uniqueID;
		mIO.read(sizeof(unsigned int), &uniqueID);
		object = reinterpret_cast<T*>(uniqueID);
		return true;
	}
	template <typename T>
	bool Deserializer::readPointerVV(int numElements, T** objects)
	{
		if (numElements > 0)
		{
			for (int i = 0; i < numElements; ++i)
			{
				if (!readPointer(objects[i]))
				{
					return false;
				}
			}
		}
		return true;
	}

	template <typename T>
	void Deserializer::resolvePointer(T*& object)
	{
		if (object)
		{
			uint64_t uniqueID = reinterpret_cast<uint64_t>((void*)object);
			LinkMap::iterator iter = mLinked.find(uniqueID);
			if (iter != mLinked.end())
			{
				object = (T*)iter->second;
			}
			else
			{
				object = 0;
			}
		}
	}
}

#endif
#ifndef JOB_QUERY_H
#define JOB_QUERY_H

#include "AnLIB.h"

#include "AnWorldMap.h"
#include "AnJobMap.h"
#include "AnDigJob.h"
#include "AnTile.h"

namespace Ants
{
	class AN_ITEM JobQuery
	{
	public:
		JobQuery(const WorldMap* world, JobMap* jobs);
		~JobQuery();
		
		void findNextJob(const Ogre::Vector2& start, Job*& job, int filter, int depth = -1) const;

		Job* getJob(int id) const;
		bool hasJob(const Ogre::Vector2& u) const;
		bool hasJob(const Ogre::Vector2& u, Job::JobType type) const;
		bool assignJob(Entity* ent, int id);
		bool unAssignJob(Entity* ent, int id);

	private:
		bool exploreTile(const Ogre::Vector2& u, Job*& job, int filter) const;

		const WorldMap* mWorld;
		JobMap* mJobs;
	};
}

#endif
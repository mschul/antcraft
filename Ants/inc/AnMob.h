#ifndef MOB_H
#define MOB_H

#include "AnLIB.h"
#include "AnCharacter.h"
#include "AnBehaviourTrait.h"

namespace Ants
{
	class AN_ITEM Mob : public Character
	{
	protected:
		static int msMobCount;

		// traits
		template<class Mob> friend class BehaviourTrait;
		template<class Mob> friend class IdleTrait;
		template<class Mob> friend class WarriorTrait;
        template<class Mob> friend class EscapeTrait;
        template<class Mob> friend class RansackTrait;
		static std::vector<BehaviourTrait<Mob>*> msTraits;
		int mActiveTrait;

	public:
		static IdleTrait<Mob>* getIdleTrait();
		static WarriorTrait<Mob>* getWarriorTrait();
		static EscapeTrait<Mob>* getEscapeTrait();

	public:
		Mob (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Mob (const EntityDesc& desc);
		virtual ~Mob();
		
		void move(float dt, ActionReactor* reactor);
        void die (ActionReactor* reactor);

        void prepareForNewJob (int jobIndex);

    protected:

        int mActiveJobIndex;

        int mP1Jobs;
        int mP2Jobs;
        int mP3Jobs;

        // job system
        int mJobFill;
        std::vector<int> mJobs;
        std::vector<std::queue<int>> mSubJobs;
	};
}

#endif
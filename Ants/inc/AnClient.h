#ifndef CLIENT_H
#define CLIENT_H

#include "AnLIB.h"

#include "AnGameObject.h"
#include "AnWorldMap.h"
#include "AnEntityMap.h"
#include "AnJobMap.h"
#include "AnServer.h"
#include "AnDigJob.h"
#include "AnEntityQuery.h"
#include "AnJobQuery.h"
#include "AnWorldQuery.h"
#include "AnRoomQuery.h"
#include "AnRoomMap.h"
#include "AnPheromoneMap.h"
#include "AnGrub.h"
#include "AnEgg.h"
#include "AnAttack.h"
#include "AnAOE.h"
#include "AnMessage.h"

namespace Ants
{
	class AN_ITEM Client : public ActionReactor, public GameObject
	{
	public:
		Client ();
		virtual ~Client ();

		virtual void login(Server* server);
		virtual void logoff();
		
		virtual void init(unsigned int width, unsigned int height);
		virtual void init(const Ogre::Rect& area);

		virtual void sendUpdates();
		virtual void receiveUpdates();
        virtual void onUpdatesReceived (std::vector<PositionTilePair> tiles,
                                        std::vector<EntityDesc> addedEnts,
                                        std::vector<EntityDesc> changedEnts,
                                        std::vector<EntityDesc> removedEnts);
        void removeJobs (int tile, Job::JobType type);
		virtual void getDisplayableMessages(std::vector<DisplayMessage*>& messages);
		virtual void processUpdates();

		virtual void update(float dt0) = 0;
		virtual void settle(std::vector<Ogre::Vector2>& locations) = 0;

	protected:
		void addEntity(const EntityDesc& desc);
		void changeEntity(const EntityDesc& desc);
		void removeEntity(const EntityDesc& desc);

		WorldMap* mWorld;
		WorldQuery* mWorldQuery;

		EntityMap* mEntities;
		EntityQuery* mEntityQuery;

		RoomMap* mRooms;
		RoomQuery* mRoomQuery;

		JobMap* mJobs;
		JobQuery* mJobQuery;

		PheromoneMap* mPheromoneMap;

		Server* mServer;

		Statistics* mStatistics;
		
		std::vector<Attack*> mAttacks;
		std::vector<AOE*> mAOEs;
		std::vector<Message*> mMessages;
	};
}

#endif
#ifndef ENTITY_QUERY_H
#define ENTITY_QUERY_H

#include "AnLIB.h"

#include "AnWorldMap.h"
#include "AnEntityMap.h"
#include "AnTile.h"
#include "AnEntity.h"
#include "AnEgg.h"

namespace Ants
{
	class AN_ITEM EntityQuery
	{
	public:
		EntityQuery(WorldMap* world, EntityMap* entities);
		~EntityQuery();
		
		void getEntities(const Ogre::Vector2& position, int radius, std::vector<Entity*>& ents);

		bool enemyBlocking(const Ogre::Vector2& position, int clientID);

		void findNextIdleWorker(int id, Entity*& targetEntity, int clientID = -1);
		void findNextGrub(int id, Entity*& targetEntity, int clientID = -1);

		int getEntityFlag(const std::vector<Entity::EntityType>& types);

#define ALL_ENTITIES_FLAG (1 << Entity::ET_COUNT) - 1
		void findNextEntity(const Ogre::Vector2& start,
			Ogre::Vector2& targetPosition, Entity*& targetEntity,
			int flags = ALL_ENTITIES_FLAG,
			int clientID = -1, int depth = -1) const;

		void findNextIdleEntity(const Ogre::Vector2& start,
			Ogre::Vector2& targetPosition, Entity*& targetEntity,
			int flags = ALL_ENTITIES_FLAG,
			int clientID = -1, int depth = -1) const;

		void findNextFreelancer(const Ogre::Vector2& start,
			Ogre::Vector2& targetPosition, Entity*& targetEntity,
			int flags = ALL_ENTITIES_FLAG,
			int clientID = -1, int depth = -1) const;

		void findNextIdleEntityForJob(const Ogre::Vector2& start,
			Ogre::Vector2& targetPosition, Entity*& targetEntity,
			Job::JobType jobtype, int flags = ALL_ENTITIES_FLAG,
			int clientID = -1, int depth = -1) const;
#undef ALL_ENTITIES_FLAG

	private:
		bool exploreTile(const Ogre::Vector2& u,
			Ogre::Vector2& targetPosition, Entity*& targetEntity,
			int flag, int clientID) const;
		bool exploreTileForIdle(const Ogre::Vector2& u,
			Ogre::Vector2& targetPosition, Entity*& targetEntity,
			int flag, int clientID) const;
		bool exploreTileForFreelancer(const Ogre::Vector2& u,
			Ogre::Vector2& targetPosition, Entity*& targetEntity,
			int flag, int clientID) const;
		bool exploreTileForIdleAndJob(const Ogre::Vector2& u,
			Ogre::Vector2& targetPosition, Entity*& targetEntity,
			Job::JobType jobtype, int flag, int clientID) const;

		WorldMap* mWorld;
		EntityMap* mEntities;
	};
}

#endif
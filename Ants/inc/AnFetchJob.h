#ifndef FETCH_JOB_H
#define FETCH_JOB_H

#include "AnLIB.h"

#include "AnJob.h"
#include "AnTile.h"
#include "AnRoom.h"

namespace Ants
{
	class Client;
	class AN_ITEM FetchJob : public Job
	{
	public:
		FetchJob (int id, const Ogre::Vector2& position);
		~FetchJob ();
		
		bool update(double dt, ActionReactor* reactor);

		Entity* getFetchEntity() const ;
		void setFetchEntity(Entity* fetchEnt);

		Room* getTargetRoom() const;
		void setTargetRoom(Room* mTargetRoom);

		JobType getType() const;
		void getSubJobs(std::vector<Job::SubJobs>& jobs) const;

	private:
		Entity* mFetchEnt;
		Room* mTargetRoom;
	};
}

#endif
#ifndef PROCEDURAL_GEOMETRY_H
#define PROCEDURAL_GEOMETRY_H

#include <OgreSingleton.h>

namespace Ants
{
	class ProceduralGeometry : public Ogre::Singleton<ProceduralGeometry> 
	{
	public:
		ProceduralGeometry(Ogre::String standardMaterial = "simple");
		~ProceduralGeometry();

		Ogre::String getPrimitive(const Ogre::String& name);
		Ogre::String getPlaneStrip(const Ogre::String& name,
			const std::vector<Ogre::Vector2>& vertices);
		Ogre::String getCubeStrip(const Ogre::String& name,
			const std::vector<Ogre::Vector2>& vertices, float thickness);
		std::vector<Ogre::Vector2> ProceduralGeometry::getCubeStripPolygon(
			const std::vector<Ogre::Vector2>& vertices, float thickness);

	private:
		ProceduralGeometry(const ProceduralGeometry&);
		ProceduralGeometry& operator= (const ProceduralGeometry&);

		Ogre::String mStandardMaterial;
	};
}

#endif
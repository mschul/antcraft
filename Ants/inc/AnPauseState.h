#ifndef PAUSE_STATE_H
#define PAUSE_STATE_H

#include "AnLIB.h"

#include "AnAppState.h"

namespace Ants
{

	class PauseState : public AppState
	{
	public:
		PauseState();

		DECLARE_GAME_STATE_CLASS(PauseState)

		void enter();
		void createScene();
		void exit();

		bool keyPressed(const OIS::KeyEvent &keyEventRef);
		bool keyReleased(const OIS::KeyEvent &keyEventRef);

		bool mouseMoved(const OIS::MouseEvent &evt);
		bool mousePressed(const OIS::MouseEvent &evt, OIS::MouseButtonID id);
		bool mouseReleased(const OIS::MouseEvent &evt, OIS::MouseButtonID id);

		bool backToMenu(const CEGUI::EventArgs& /*e*/);
		bool backToGame(const CEGUI::EventArgs& /*e*/);

		void update(float dT);

	private:
		bool                        mQuit;
	};

}

#endif

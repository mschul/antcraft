#ifndef STATISTICS_H
#define STATISTICS_H

#include "AnLIB.h"
#include "AnGameObject.h"

namespace Ants
{
	class AN_ITEM Statistics : public GameObject
	{
	public:
		Statistics();
		Statistics(ObjectID id);
		~Statistics();

		void set(const Statistics& other);
		void add(const Statistics& other);

		void killed(int type);

		void getTimePassed(int& hours, int& mins, int& seconds);
		Ogre::String getTimePassed();
		int getUnitsProduced() const;
		int getUnitsKilled() const;
		int getUnitCount() const;

		int getRoomCount() const;
		int getRoomArea() const;

		int getScore() const;

		struct UnitStat
		{
			int Produced;
			int Alive;
			int Killed;
		};

		UnitStat SmallWorker;
		UnitStat Worker;
		UnitStat Carrier;
		UnitStat Soldier;
		UnitStat Kamikaze;

		UnitStat Ladybug;
		UnitStat Spider;
		UnitStat BombardierBeetle;

		int GameWon;
		int GameCancelled;

		struct RoomStat
		{
			int Count;
			int Area;
			int Slots;
			int SlotsFree;
		};

		RoomStat Storage;
		RoomStat Brood;
		RoomStat Fungi;

		int AnthillArea;
		double TimePassed;

		Ogre::String Name;

		// non serialized
		int FaunalStored;
		int FloralStored;
		int CopalStored;
		int AStorageFull;

		// factory and streaming frontend
	public:
		static const Ogre::String TYPE;
		virtual const Ogre::String& getClassType() const;
		static bool registerFactory();
		static GameObject* Factory(Deserializer& deserializer);
		virtual int getStreamingSize() const;
		static void initializeFactory();
		virtual void serialize(Serializer& serializer) const;
		virtual void resolvePointers(Deserializer& deserializer);
		virtual void deserialize(Deserializer& deserializer);
	private:
		static bool msStreamRegistered;
	};

	static bool gsStreamRegistered_Statistics = Statistics::registerFactory();
}

#endif
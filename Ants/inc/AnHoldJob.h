#ifndef HOLD_JOB_H
#define HOLD_JOB_H

#include "AnLIB.h"

#include "AnJob.h"
#include "AnTile.h"

namespace Ants
{
	class Client;
	class AN_ITEM HoldJob : public Job
	{
	public:
		HoldJob(int id, const Ogre::Vector2& position, double duration);
		~HoldJob();
		
		bool update(double dt, ActionReactor* reactor);
		JobType getType() const;
		void getSubJobs(std::vector<Job::SubJobs>& jobs) const;

	private:
		double mDuration;
		double mT;
	};
}

#endif
#ifndef WORLD_MAP_H
#define WORLD_MAP_H

#include "AnLIB.h"

#include "AnTile.h"
#include "AnWorldPage.h"
#include "AnPerlin.h"

namespace Ants
{

	class AN_ITEM WorldMap
	{
	public:
		WorldMap ();
		virtual ~WorldMap ();

		void init(unsigned int width, unsigned int height);
		void init(const Ogre::Rect& area);
		
		bool tileExists(const Ogre::Vector2& position) const;

		void createEmptyWorld(std::vector<PositionTilePair>& pairs, int rock, int water, int cave);
		void createTrees(std::vector<PositionTilePair>& pairs, int treeWidth = 20, float density = 0.1);
		void createResources(std::vector<std::vector<Ogre::Vector2>>& grassPairs,
			std::vector<std::vector<Ogre::Vector2>>& copalPairs,
			int grassMinHeight = 3, int grassMaxHeight = 7, float density = 0.5);
		
		static Perlin* msSurfacePerlin;
		static Perlin* msUndergroundPerlin;
		static Perlin* msSurfaceEntityPerlin;
		static Perlin* msDirtPerlin;
		static Perlin* msWaterPerlin;
		static Perlin* msRockPerlin;
		static Perlin* msCavePerlin;

		void getUpdates(std::vector<PositionTilePair>& tiles);
		void clearUpdates();

		Ogre::Rect getSize() const;

		virtual bool setTile(const Ogre::Vector2& position, Tile* tile, bool tracking = false);
		virtual bool setTiles(const std::vector<Ogre::Vector2>& positions,
			const std::vector<Tile*>& tiles, bool tracking = false);
		virtual bool setTiles(const std::vector<PositionTilePair>& tiles, bool tracking = false);
		Tile* getTile(const Ogre::Vector2& position) const;

		unsigned int getAnthillTileCount() const;

		bool isDiggable(int tile) const;
		bool isDiggable(const Ogre::Vector2& position) const;
		bool isResource(int tile) const;
		bool isResource(const Ogre::Vector2& position) const;
		bool isBuildable(int tile) const;
		bool isBuildable(const Ogre::Vector2& position) const;
		bool isBlocking(int tile) const;
		bool isBlocking(const Ogre::Vector2& position) const;

	protected:
		unsigned int mWidth;
		unsigned int mHeight;
		unsigned int mAnthillTiles;
		std::vector<Tile*> mMap;
		std::vector<PositionTilePair> mTileUpdates;
	};
}

#endif
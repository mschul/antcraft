#ifndef GAME_SETTINGS_H
#define GAME_SETTINGS_H

#include "AnLIB.h"

#include "AnAppState.h"

namespace Ants
{

	class GameSettings
	{
	public:
		GameSettings();

		void setRockAmount(int rock);
		int getRockAmount() const;

		void setWaterAmount(int water);
		int getWaterAmount() const;

		void setCaveAmount(int caves);
		int getCaveAmount() const;

		void setMobAmount(int mobs);
		int getMobAmount() const;

		void setGrassAmount(int grass);
		int getGrassAmount() const;

		void setFOW(bool fow);
		bool getFOW() const;

		void setRevealMap(bool reveal);
		bool getRevealMap() const;

		void setUpgrades(bool upgrades);
		bool getUpgrades() const;

	private:
		int mRock;
		int mWater;
		int mCaves;
		int mMobs;
		int mGrass;
		bool mFOW;
		bool mRevealMap;
		bool mUpgrades;
	};

}

#endif

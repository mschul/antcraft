#ifndef CARRIER_H
#define CARRIER_H

#include "AnLIB.h"
#include "AnAnt.h"

namespace Ants
{
	class AN_ITEM Carrier : public Ant
	{
	public:
		Carrier (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Carrier (const EntityDesc& desc);
		~Carrier ();
		
		void selectBehaviour(float dt);
		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;
	};
}

#endif
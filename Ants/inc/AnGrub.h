#ifndef GRUB_H
#define GRUB_H

#include "AnLIB.h"
#include "AnMob.h"

namespace Ants
{

	class AN_ITEM Grub : public Mob
	{
	public:
		Grub (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius,
			WorldQuery* worldQuery, JobQuery* jobQuery,
			EntityQuery* entityQuery, RoomQuery* roomQuery, PheromoneMap* pheromones);
		Grub (const EntityDesc& desc);
		~Grub ();

        virtual void selectBehaviour (float dt);
		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;

		bool spawn(float dt);
		
		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;

	protected:
		float mSpawnDelta;
	};
}

#endif
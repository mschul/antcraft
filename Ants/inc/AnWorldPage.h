#ifndef WORLD_PAGE_H
#define WORLD_PAGE_H

#include "AnLIB.h"

#include "AnTile.h"

namespace Ants
{
	class AN_ITEM WorldPage
	{
	public:
		WorldPage (int x0, int y0, int width, Ogre::SceneManager* sceneManager, bool instanced = false);
		~WorldPage ();

		void build(const std::vector<Tile*>& map,int width, int height,
			const std::vector<Ogre::InstanceManager*>& managers);
		void load(const std::vector<Tile*>& map, int width, int height,
			const std::vector<Ogre::InstanceManager*>& managers);
		void drop(const std::vector<Tile*>& map, int width,
			const std::vector<Ogre::InstanceManager*>& managers);
		void change(const Ogre::Vector2& position, Tile* tile,
					const std::vector<Ogre::InstanceManager*>& managers);

		void setVisible(bool visibility);

	private:
		bool mInstanced;

		bool mBuilt;
		bool mCreated;
		bool mLoaded;

		int mX0;
		int mY0;
		int mWidth;

		std::vector<Ogre::InstancedEntity*> mEntities;
        Ogre::StaticGeometry*	mStaticGeometry;
		Ogre::SceneManager* mSceneManager;
		Ogre::SceneNode* mRoot;
	};
}

#endif

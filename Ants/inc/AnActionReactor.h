#ifndef ACTION_REACTOR_H
#define ACTION_REACTOR_H

#include "AnLIB.h"
#include "AnGameObject.h"
#include "AnAttack.h"

namespace Ants
{
	class Egg;
	class Grub;
	class Ant;
	class Debris;
	class Projectile;
	class Job;
	class Tile;
	class Entity;
	class Room;
	class Character;
	class ActionReactor
	{
	public:
		// job events
		virtual void jobMilestoneReached(Job* job) = 0;
		virtual void jobFinished(Job* job) = 0;

		// special handling for world updates
		virtual void setExitTiles(const std::vector<std::pair<Ogre::Vector2, Tile*>>& tiles) = 0;
		virtual void setTile(const Ogre::Vector2& position, int type) = 0;

		// resource creation / destruction
		virtual void createResource(const Ogre::Vector2& position, int roomType,
			Character* resource = 0) = 0;
		virtual void removeResource(Room* r) = 0;

		// ant evolution
		virtual Egg* spawnEgg(const Ogre::Vector2& position) = 0;
		virtual Grub* hatchEgg(Egg* egg) = 0;
		virtual Ant* spawnAnt(Grub* grub, int type) = 0;
		
		// mob/entity spawning/deleting
		virtual void unitDied(Character* unit, Attack* a = 0) = 0;
		virtual Entity* spawn(const Ogre::Vector2& position, int type) = 0;
		virtual void removeEntities(const std::vector<Entity*>& ents) = 0;

		// combat
		virtual void meleeAttack(float attackPoints, Character* source, Entity* target) = 0;
		virtual void rangedAttack(float attackPoints, Character* source, Entity* target) = 0;
		virtual void aoeAttack(int sourceID, float attackPoints, const Ogre::Vector2& position, float radius) = 0;
	};
}

#endif
#ifndef EGG_H
#define EGG_H

#include "AnLIB.h"
#include "AnEntity.h"

namespace Ants
{

	class AN_ITEM Egg : public Ants::Entity
	{
	public:
		Egg (int id, int clientID,
			const Ogre::Vector2& position,
			const Ogre::Vector2& tilePosition, 
			int visibilityRadius);
		Egg (const EntityDesc& desc);
		~Egg ();
		
		Entity::EntityType getType() const;

		Ogre::String getMaterialName() const;
		Ogre::String getStandardMesh() const;
		
		void update(float dt, ActionReactor* reactor);
		bool hatch(float dt);

		static Ogre::String msMaterialName;
		static Ogre::String msStandardMesh;

	private:
		float mHatchDelta;
	};
}

#endif
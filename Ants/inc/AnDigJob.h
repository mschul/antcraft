#ifndef DIG_JOB_H
#define DIG_JOB_H

#include "AnLIB.h"

#include "AnJob.h"
#include "AnTile.h"

namespace Ants
{
	class Client;
	class AN_ITEM DigJob : public Job
	{
	public:
		DigJob (int id, const Ogre::Vector2& position, double duration);
		~DigJob ();
		
		bool update(double dt, ActionReactor* reactor);
		JobType getType() const;
		void getSubJobs(std::vector<Job::SubJobs>& jobs) const;

	private:
		double mT;
	};
}

#endif
#ifndef ATTACK_H
#define ATTACK_H

#include "AnLIB.h"
#include "AnGameObject.h"

namespace Ants
{

	class AN_ITEM Attack
	{
	public:
		Attack(float attackPoints, ObjectID source, ObjectID target,
			int sourceClientID, int targetClientID);
		~Attack ();
		
		float getAttackPoints() const;
		ObjectID getTarget() const;
		ObjectID getSource() const;
		int getSourceClientID() const;
		int getTargetClientID() const;

	private:
		float mAttackPoints;
		ObjectID mSource;
		ObjectID mTarget;
		int mSourceClientID;
		int mTargetClientID;
	};
}

#endif
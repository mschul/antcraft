#ifndef VISIBILITY_MAP_H
#define VISIBILITY_MAP_H

#include "AnLIB.h"

#include "AnEntity.h"

namespace Ants
{
	class AN_ITEM VisibilityMap
	{
	public:
		VisibilityMap ();
		~VisibilityMap ();

		void init(int width, int height, bool setAll = false);
		void init(const Ogre::Rect& area, bool setAll = false);
		void delta(const Ogre::Vector2 pos, unsigned int radius, int delta);
		void increment(const Ogre::Vector2 pos, unsigned int radius);
		void decrement(const Ogre::Vector2 pos, unsigned int radius);
		void increment(Entity* ent);
		void decrement(Entity* ent);
		void set(const Ogre::Vector2 pos, unsigned int radius);
		void set(Entity* ent);

		bool isVisible(Ogre::Vector2 position) const;
		bool isVisible(Entity* ent) const;

	protected:
		unsigned int mWidth;
		unsigned int mHeight;
		int* mMap;
	};
}

#endif
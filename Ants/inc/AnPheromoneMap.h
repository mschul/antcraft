#ifndef PHEROMONE_MAP_H
#define PHEROMONE_MAP_H

#include "AnLIB.h"
#include "AnTile.h"

namespace Ants
{
	class AN_ITEM PheromoneMap
	{
	public:
		PheromoneMap ();
		~PheromoneMap ();
		
		void init(unsigned int width, unsigned int height);
		void init(const Ogre::Rect& area);

		void increase(const Ogre::Vector2& pos);
		void decrease(const Ogre::Vector2& pos);

		// get a distribution based on binomial prior
		// dir is direction of highest probabiltity
		// distro is an array of length 6
		// sigma is either 0, 1 or 2 giving 'width' of prior
		void getDistribution(const Ogre::Vector2& pos, Tile::Direction dir,
			unsigned char* distro, unsigned int& sum, unsigned int sigma = 2);

	protected:
		unsigned char getValue(const Ogre::Vector2& pos, Tile::Direction dir);

		unsigned int mWidth;
		unsigned int mHeight;
		unsigned char* mMap;

	};
}

#endif
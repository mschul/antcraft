#ifndef ROOM_QUERY_H
#define ROOM_QUERY_H

#include "AnLIB.h"

#include "AnWorldMap.h"
#include "AnRoom.h"
#include "AnThroneChamber.h"
#include "AnRoomMap.h"
#include "AnEntity.h"

namespace Ants
{
	class AN_ITEM RoomQuery
	{
	public:
		RoomQuery (const RoomMap* rooms, const WorldMap* world);
		~RoomQuery ();
		
		Room* findRoom(Room::RoomType type);
		Room* findNearestRoom(Room::RoomType type, const Ogre::Vector2& position);
		int distToNearestRoom(Room::RoomType type, const Ogre::Vector2& position);
		Room* findStorageWithResource(Entity::EntityType type, const Ogre::Vector2& position);

		Room* get(int id);
		ThroneChamber* getThroneChamber();

		void findReachableRoom(const Ogre::Vector2& position, Room*& room,
			Room::RoomType type, int depth = 50);
		void findReachableRoom(const Ogre::Vector2& position, Room*& room,
			const std::vector<Room::RoomType>& allowedTypes, int depth = 50);

	private:
		bool exploreTile(const Ogre::Vector2& u, const std::vector<Room::RoomType>& allowedTypes, Room*& room) const;

		const WorldMap* mWorld;
		const RoomMap* mRooms;
	};
}

#endif
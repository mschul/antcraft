#ifndef MOB_CLIENT_H
#define MOB_CLIENT_H

#include "AnLIB.h"

#include "AnClient.h"

namespace Ants
{
	class AN_ITEM MobClient : public Client
	{
	public:
		MobClient ();
		~MobClient ();
		
		void setMobAmount(int mobs);

        void onUpdatesReceived (std::vector<PositionTilePair> tiles,
                                std::vector<EntityDesc> addedEnts,
                                std::vector<EntityDesc> changedEnts,
                                std::vector<EntityDesc> removedEnts);

		// -> Client Impl
		void update(float dt);
		void settle(std::vector<Ogre::Vector2>& locations);
		// <- Client Impl
		
		// -> Client:ActionReactor Impl
		void jobMilestoneReached(Job* job);
		void jobFinished(Job* job);
		void setExitTiles(const std::vector<std::pair<Ogre::Vector2, Tile*>>& tiles);
		void setTile(const Ogre::Vector2& position, int type);
		void createResource(const Ogre::Vector2& position, int roomType, Character* source = 0);
		void removeResource(Room* r);
		Egg* spawnEgg(const Ogre::Vector2& position);
		Grub* hatchEgg(Egg* egg);
		void unitDied(Character* unit, Attack* a = 0);
		Ant* spawnAnt(Grub* grub, int type);
		Entity* spawn(const Ogre::Vector2& position, int type);
		void removeEntities(const std::vector<Entity*>& ents);
		void aoeAttack(int sourceID, float attackPoints, const Ogre::Vector2& position, float radius);
		void meleeAttack(float attackPoints, Character* source, Entity* target);
		void rangedAttack(float attackPoints, Character* source, Entity* target);
		// <- Client:ActionReactor Impl

	private:
		int mMobAmount;
	};
}

#endif
#ifndef FOW_MASK_MAP_H
#define FOW_MASK_MAP_H

#include "AnLIB.h"
#include "AnVisibilityMap.h"
#include "AnEntity.h"

namespace Ants
{
	class AN_ITEM FowMaskMap
	{
	public:
		FowMaskMap();
		~FowMaskMap();

		void init(unsigned int width, unsigned int height, Ogre::SceneNode* rootNode,
						Ogre::SceneManager* sceneMgr);
		void init(const Ogre::Rect& area, Ogre::SceneNode* rootNode,
			Ogre::SceneManager* sceneMgr);
		void update(Entity* ent);

	protected:
		unsigned int mWidth;
		unsigned int mHeight;
		Ogre::SceneNode* mRootNode;
		Ogre::SceneNode** mNodes;
		Ogre::SceneManager* mSceneMgr;

		VisibilityMap* mMap;
		std::map<Entity*, int> mEntityMap;

	};
}

#endif
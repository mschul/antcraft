#ifndef IO_H
#define IO_H

#include "AnLIB.h"

namespace Ants
{
	class AN_ITEM IO
	{
	public:
		IO();
		virtual ~IO();

		void open(int size, char* buffer);
		void close();
		bool done() const;
		const char* current() const;
		bool advance(int bytes);

		bool read(size_t size, void* datum);
		bool read(size_t size, int items, void* datum);
		bool write(size_t size, const void* datum);
		bool write(size_t size, int items, const void* datum);

		static bool load(const Ogre::String& filename, bool binary, int& size, char*& buffer);
		static bool save(const Ogre::String& filename, bool binary, int size, const char* buffer);
		static bool append(const Ogre::String& filename, bool binary, int size, const char* buffer);

	private:
		void reset();

		char* mBuffer;
		int mTotalBytes;
		int mProcessedBytes;
	};
}

#endif
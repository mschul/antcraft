#ifndef DIG_RESOURCE_JOB_H
#define DIG_RESOURCE_JOB_H

#include "AnLIB.h"

#include "AnJob.h"
#include "AnRoom.h"
#include "AnTile.h"

namespace Ants
{
	class Client;
	class AN_ITEM DigResourceJob : public Job
	{
	public:
		DigResourceJob (int id, const Ogre::Vector2& position, double duration);
		~DigResourceJob ();
		
		bool update(double dt, ActionReactor* reactor);
		JobType getType() const;
		void getSubJobs(std::vector<Job::SubJobs>& jobs) const;

		void setResourceID(int id);
		int getResourceID() const;

	private:
		int mResourceID;
		double mT;
	};
}

#endif
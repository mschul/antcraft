#ifndef GAME_OBJECT_H
#define GAME_OBJECT_H

#include "AnLIB.h"
#include "AnSerializer.h"
#include "AnDeserializer.h"
#include "AnInitTerm.h"

namespace Ants
{
	typedef int ObjectID;
	class AN_ITEM GameObject
	{
	public:
		GameObject();
		GameObject(ObjectID id);
		virtual ~GameObject();

		void setID(ObjectID id);
		ObjectID getID() const;
	protected:
		ObjectID mID;

		// factory and streaming fontend
	public:
		static const Ogre::String TYPE;
		static void initializeFactory();
		static bool registerFactory();
		virtual const Ogre::String& getClassType() const;
		virtual int getStreamingSize() const;
		virtual void serialize(Serializer& serializer) const;
		virtual void resolvePointers(Deserializer& deserializer);
		virtual bool registerObjects(Serializer& serializer) const;
		virtual void deserialize(Deserializer& deserializer);
	private:
		static bool msStreamRegistered;

		// factory and streaming backend
	public:
		typedef GameObject* (*FactoryFunction)(Deserializer& stream);
		typedef std::map<std::string, FactoryFunction> FactoryMap;
		static GameObject* Factory(Deserializer& source);
		static void terminateFactory();
		static FactoryFunction getFactory(const Ogre::String& name);
	protected:
		static FactoryMap* msFactories;

	};
	typedef std::pair<Ogre::Vector2, GameObject*> PositionGameObjectPair; 

	static bool gsStreamRegistered_Object = GameObject::registerFactory();
}

#endif
#ifndef ANLIB_H
#define ANLIB_H

//----------------------------------------------------------------------------
// Platform-specific information.  The defines to control which platform is
// included are listed below.  Add others as needed.
//
// _WIN32 or WIN32          :  Microsoft Windows
// __APPLE__                :  Macintosh OS X
// __linux__ or __LINUX__   :  Linux
//----------------------------------------------------------------------------
// Microsoft Windows platform
//----------------------------------------------------------------------------
#if defined(_WIN32) || defined(WIN32)

#ifndef _WIN32
#define _WIN32
#endif

#ifndef WIN32
#define WIN32
#endif

#define AN_LITTLE_ENDIAN

#if defined(_MSC_VER)
// MSVC6 is version 12.00, MSVC7.0 is version 13.00, MSVC7.1 is version 13.10,
// MSVC8.0 is version 14.00, and MSVC9.0 is version 15.00.  Currently, only
// version 9.0 or later are supported.
#if _MSC_VER < 1400
#error MSVC 8.0 or later is required
#elif _MSC_VER < 1500
#define AN_USING_VC80
#elif _MSC_VER < 1600
#define AN_USING_VC90
#elif _MSC_VER < 1700
#define AN_USING_VC100
#else
#define AN_USING_VC110
#endif

// Disable the Microsoft warnings about not using the secure functions.
#pragma warning(disable : 4996)

// The use of EN_<libname>_ITEM to export an entire class generates warnings
// when member data and functions involving templates or inlines occur.  To
// avoid the warning, EN_<libname>_ITEM can be applied only to those items
// that really need to be exported.
#pragma warning(disable : 4251) 

// Support for standard integer types.  This is only a small part of what
// stdint.h provides on Unix platforms.
#include <climits>

#if defined(AN_USING_VC100) || defined(AN_USING_VC110)
#include <stdint.h>
#else
typedef __int8              int8_t;
typedef __int16             int16_t;
typedef __int32             int32_t;
typedef __int64             int64_t;
typedef unsigned __int8     uint8_t;
typedef unsigned __int16    uint16_t;
typedef unsigned __int32    uint32_t;
typedef unsigned __int64    uint64_t;

#define INT8_MIN            _I8_MIN
#define INT8_MAX            _I8_MAX
#define INT16_MIN           _I16_MIN
#define INT16_MAX           _I16_MAX
#define INT32_MIN           _I32_MIN
#define INT32_MAX           _I32_MAX
#define INT64_MIN           _I64_MIN
#define INT64_MAX           _I64_MAX
#define UINT8_MAX           _UI8_MAX
#define UINT16_MAX          _UI16_MAX
#define UINT32_MAX          _UI32_MAX
#define UINT64_MAX          _UI64_MAX
#endif
#endif

#endif

//----------------------------------------------------------------------------
// Macintosh OS X platform
//----------------------------------------------------------------------------
#if defined(__APPLE__)

#if defined(__BIG_ENDIAN__)
#define AN_BIG_ENDIAN
#else
#define AN_LITTLE_ENDIAN
#endif

#endif
//----------------------------------------------------------------------------
// PC Linux platform
//----------------------------------------------------------------------------
#if !defined(__LINUX__) && defined(__linux__)
// Apparently, many PC Linux flavors define __linux__, but we have used
// __LINUX__.  To avoid breaking code by replacing __LINUX__ by __linux__,
// we will just define __LINUX__.
#define __LINUX__
#endif
#if defined(__LINUX__)

// Support for standard integer types.
#include <inttypes.h>

#define AN_LITTLE_ENDIAN

#endif

#if defined(AN_DLL_EXPORT)
    // For the DLL library.
    #define AN_ITEM __declspec(dllexport)
#elif defined(AN_DLL_IMPORT)
    // For a client of the DLL library.
    #define AN_ITEM __declspec(dllimport)
#else
    // For the static library (and for Apple/Linux).
    #define AN_ITEM
#endif

// Common standard library headers.
#include <cassert>
#include <cctype>
#include <cfloat>
#include <climits>
#include <cmath>
#include <cstdarg>
#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <ctime>
#include <iostream>
#include <fstream>

// Common STL headers.
#include <algorithm>
#include <deque>
#include <limits>
#include <list>
#include <map>
#include <queue>
#include <set>
#include <stack>
#include <string>
#include <utility>
#include <vector>
#include <sstream>

// User-defined keywords for syntax highlighting of special class sections.
#define public_internal public
#define protected_internal protected
#define private_internal private


#ifdef _DEBUG
//#define AN_USE_MEMORY
#endif

// Avoid warnings about unused variables.  This is designed for variables
// that are exposed in debug configurations but are hidden in release
// configurations.
#define AN_UNUSED(variable) (void)variable

// Macros to enable or disable various Assert subsystems.  TODO:  Currently,
// the extended assert system is implemented only for Microsoft Visual Studio.
#ifdef _DEBUG
    #if defined(WIN32) && defined(_MSC_VER)
        #define AN_USE_ASSERT
        #ifdef AN_USE_ASSERT
            #define AN_USE_ASSERT_WRITE_TO_OUTPUT_WINDOW
            #define AN_USE_ASSERT_WRITE_TO_MESSAGE_BOX
        #endif
    #endif
    //#define AN_USE_ASSERT_LOG_TO_FILE
	//#define AN_USE_MEMORY
#endif

// Macros for memory management.  To report memory leaks and related
// file-line information, you must enable WM5_USE_MEMORY.  This in
// turn requires Memory::Initialize and Memory::Terminate to be called
// in your application.  This is handled automatically if you use the
// Wild Magic application layer.  If you do not, see the 'main' function
// in the file
//   WildMagic5/LibApplication/Wm5Application.cpp
// for an example.  Also read the general documentation about start-up
// and shut-down
//   WildMagic5/Documentation/Wm5InitTerm.pdf
//
#ifdef AN_USE_MEMORY
    // Enable an assertion when a allocation occurs before
    // Memory::Initialize was called or when a deallocation occurs after
    // Memory::Terminate was called.
    #define AN_USE_MEMORY_ASSERT_ON_PREINIT_POSTTERM_OPERATIONS

    // When WM5_USE_MEMORY is enabled, the Memory functions delete0,
    // delete1, delete2, and delete3 will fail when the incoming pointer
    // was not allocated by the Wild Magic memory manager.  Enabling the
    // following flag tells Wild Magic's memory manager to attempt
    // 'delete' or 'delete[]' under the assumption that the memory was
    // allocated by 'new' or 'new[]'.
    #define AN_USE_MEMORY_ALLOW_DELETE_ON_FAILED_MAP_LOOKUP
#endif

// Flags for FileIO and BufferIO.
#ifdef _DEBUG
    #define AN_FILEIO_VALIDATE_OPERATION
    #define AN_BUFFERIO_VALIDATE_OPERATION
#endif

#include <OgreCamera.h>
#include <OgreEntity.h>
#include <OgreLogManager.h>
#include <OgreOverlay.h>
#include <OgreOverlayElement.h>
#include <OgreOverlayManager.h>
#include <OgreOverlaySystem.h>
#include <OgreRoot.h>
#include <OgreViewport.h>
#include <OgreSceneManager.h>
#include <OgreRenderWindow.h>
#include <OgreConfigFile.h>
#include <OgreFileSystemLayer.h>
#include <OgreFrameListener.h>
#include <OgreStaticGeometry.h>
#include <OgreInstancedEntity.h>
#include <OgreCompositorManager.h>
#include <OgreShadowCameraSetupFocused.h>
#include <OgreHardwarePixelBuffer.h>
#include <OgreSceneNode.h>
#include <OgreMaterialManager.h>
#include <OgreTechnique.h>
#include <OgreTextureManager.h>

#include <OISEvents.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>

#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>
#include <CEGUI/RendererModules/Ogre/Texture.h>
#include <CEGUI/RendererModules/Ogre/TextureTarget.h>

#include "AnCore.h"

#define WORLD_VISIBILITY_FLAG 1
#define OVERLAY_VISIBILITY_FLAG 5
#define FOWMASK_VISIBILITY_FLAG 4
#define VISIBILE_FLAG (WORLD_VISIBILITY_FLAG)

static inline Ogre::Vector2 rotateVector2(const Ogre::Vector2& in, Ogre::Radian angle)
{
    return Ogre::Vector2(in.x* Ogre::Math::Cos(angle) - in.y * Ogre::Math::Sin(angle),
        in.x * Ogre::Math::Sin(angle) + in.y * Ogre::Math::Cos(angle));
}


static void destroyAllAttachedMovableObjects(Ogre::SceneNode* node)
{
	// Destroy all the attached objects
	Ogre::SceneNode::ObjectIterator itObject = node->getAttachedObjectIterator();

	while (itObject.hasMoreElements())
	{
		Ogre::MovableObject* pObject = static_cast<Ogre::MovableObject*>(itObject.getNext());
		node->getCreator()->destroyMovableObject(pObject);
	}

	// Recurse to child SceneNodes
	Ogre::SceneNode::ChildNodeIterator itChild = node->getChildIterator();

	while (itChild.hasMoreElements())
	{
		Ogre::SceneNode* pChildNode = static_cast<Ogre::SceneNode*>(itChild.getNext());
		destroyAllAttachedMovableObjects(pChildNode);
	}
}

template <typename T>
inline Ogre::Real real(T val) { return static_cast<Ogre::Real>(val); }
template <typename T>
inline int integer(T val) { return static_cast<int>(val); }
template <typename T>
inline unsigned int uinteger(T val) { return static_cast<unsigned int>(val); }

static int stringSize(const Ogre::String& datum)
{
	int length = (int)strlen(datum.c_str());
	int padding = 0;
	if (length > 0)
	{
		padding = (length % 4);
		if (padding > 0)
		{
			padding = 4 - padding;
		}
	}
	return sizeof(int)+length*sizeof(char)+padding;
}
#define POINTER_SIZE(value) 4

#endif
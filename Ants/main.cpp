#include "AnPCH.h"
#include "AnGameApplication.h"
#include "AnInitTerm.h"

#if OGRE_PLATFORM == PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_WIN32
#define WIN32_LEAN_AND_MEAN
#include "windows.h"

INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE, LPSTR strCmdLine, INT)
#else
int main(int argc, char **argv)
#endif
{

#ifdef AN_USE_MEMORY
    Ants::Memory::Initialize();
#endif
	Ants::InitTerm::executeInitializers();

	Ogre::String writableFolder;
	try
	{
		Ants::GameApplication app;
		writableFolder = app.getWritableFolder();
		app.start();
	}
	catch(std::exception& e)
    {
#if OGRE_PLATFORM == PLATFORM_WIN32 || OGRE_PLATFORM == OGRE_PLATFORM_WIN32
        MessageBoxA(NULL, e.what(), "An exception has occurred!", MB_OK | MB_ICONERROR | MB_TASKMODAL);
#else
        fprintf(stderr, "An exception has occurred: %s\n", e.what());
#endif
    }

	Ants::InitTerm::executeTerminators();
#ifdef AN_USE_MEMORY
    Ants::Memory::Terminate(writableFolder + "MemoryReport.txt");
#endif

    return 0;
}